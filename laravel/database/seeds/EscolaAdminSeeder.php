<?php

use Illuminate\Database\Seeder;

class EscolaAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('admin')->delete();

      DB::connection('mysql_escola')->table('admin')->insert(
        [
          'login' => 'trupe',
          'email' => 'contato@trupe.net',
          'password' => bcrypt('senhatrupe'),
        ]
      );
    }
}
