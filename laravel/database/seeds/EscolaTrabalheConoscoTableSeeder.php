<?php

use Illuminate\Database\Seeder;

class EscolaTrabalheConoscoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('trabalhe_conosco')->delete();

      DB::connection('mysql_escola')->table('trabalhe_conosco')->insert([
        [
          'texto' => '<p>Texto de Trabalhe Conosco<p/>'
        ]
      ]);
    }
}
