<?php

use Illuminate\Database\Seeder;

class InfraestruturaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('infraestrutura')->delete();

      DB::connection('mysql_faculdade')->table('infraestrutura')->insert([
        [
          'texto' => '<p>Texto de Infraestrutura<p/>'
        ]
      ]);
    }
}
