<?php

use Illuminate\Database\Seeder;

class FaculdadeAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('admin')->delete();

      DB::connection('mysql_faculdade')->table('admin')->insert(
        [
          'login' => 'trupe',
          'email' => 'contato@trupe.net',
          'password' => bcrypt('senhatrupe'),
        ]
      );
    }
}
