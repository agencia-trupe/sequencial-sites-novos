<?php

use Illuminate\Database\Seeder;

class EscolaBolsasDeEstudoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('bolsas_de_estudo')->delete();

      DB::connection('mysql_escola')->table('bolsas_de_estudo')->insert([
        [
          'texto' => '<p>Texto de Bolsas de Estudo<p/>'
        ]
      ]);
    }
}
