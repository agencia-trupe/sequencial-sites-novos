<?php

use Illuminate\Database\Seeder;

class EscolaASequencialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('a_sequencial')->delete();

      DB::connection('mysql_escola')->table('a_sequencial')->insert([
        [
          'texto' => '<p>Texto de Apresentação<p/>'
        ]
      ]);
    }
}
