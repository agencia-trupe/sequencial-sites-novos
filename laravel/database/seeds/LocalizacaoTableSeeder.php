<?php

use Illuminate\Database\Seeder;

class LocalizacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('sequencial_localizacao')->delete();

      DB::connection('mysql_faculdade')->table('sequencial_localizacao')->insert([
        [
          'endereco' => '<p>Texto de Localização<p/>'
        ]
      ]);
    }
}
