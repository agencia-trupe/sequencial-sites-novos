<?php

use Illuminate\Database\Seeder;

class ChamadasCalhausTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('chamadas')->delete();

      DB::connection('mysql_faculdade')->table('chamadas')->insert([
        [
          'tipo' => 'novidades',
          'is_calhau' => 1,
          'titulo' => 'Chamada Calhau Novidades',
          'fundo_tipo' => 'cor',
          'fundo_cor' => '#FFF'
        ],
        [
          'tipo' => 'eventos',
          'is_calhau' => 1,
          'titulo' => 'Chamada Calhau Eventos',
          'fundo_tipo' => 'cor',
          'fundo_cor' => '#FFF'
        ]
      ]);
    }
}
