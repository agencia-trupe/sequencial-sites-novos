<?php

use Illuminate\Database\Seeder;

class EstagiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('estagios')->delete();

      DB::connection('mysql_faculdade')->table('estagios')->insert([
        [
          'titulo' => 'NUBE',
          'slug' => 'nube',
          'ordem' => 0
        ],
        [
          'titulo' => 'CIEE',
          'slug' => 'ciee',
          'ordem' => 1
        ],
        [
          'titulo' => 'Fundap',
          'slug' => 'fundap',
          'ordem' => 2
        ]
      ]);
    }
}
