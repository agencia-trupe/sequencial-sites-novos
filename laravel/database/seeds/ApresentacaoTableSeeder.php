<?php

use Illuminate\Database\Seeder;

class ApresentacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('sequencial_apresentacao')->delete();

      DB::connection('mysql_faculdade')->table('sequencial_apresentacao')->insert([
        [
          'texto' => '<p>Texto de Apresentação<p/>'          
        ]
      ]);
    }
}
