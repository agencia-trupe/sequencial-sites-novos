<?php

use Illuminate\Database\Seeder;

class EscolaAreasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('areas')->delete();

      DB::connection('mysql_escola')->table('areas')->insert([
        [
          'titulo' => 'Gestão',
          'slug' => 'gestao',
          'ordem' => 0
        ],
        [
          'titulo' => 'Saúde',
          'slug' => 'saude',
          'ordem' => 1
        ],
        [
          'titulo' => 'Indústria',
          'slug' => 'industria',
          'ordem' => 2
        ]
      ]);
    }
}
