<?php

use Illuminate\Database\Seeder;

class OuvidoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('ouvidoria')->delete();

      DB::connection('mysql_faculdade')->table('ouvidoria')->insert([
        [
          'titulo' => 'Ouvidoria Corporativa Sequencial',
          'texto' => '<p>Texto de Ouvidoria<p/>'
        ]
      ]);
    }
}
