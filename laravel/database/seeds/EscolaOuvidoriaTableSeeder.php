<?php

use Illuminate\Database\Seeder;

class EscolaOuvidoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('ouvidoria')->delete();

      DB::connection('mysql_escola')->table('ouvidoria')->insert([
        [
          'texto' => '<p>Texto de Ouvidoria<p/>'
        ]
      ]);
    }
}
