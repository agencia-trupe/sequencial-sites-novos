<?php

use Illuminate\Database\Seeder;

class EscolaCursosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('cursos')->delete();

      DB::connection('mysql_escola')->table('cursos')->insert([
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Administração',
          'slug' => 'administracao',
          'cor_curso' => '#26417B',
          'areas_id' => 1,
          'ordem' => 1
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Secretariado',
          'slug' => 'secretariado',
          'cor_curso' => '#00C9DC',
          'areas_id' => 1,
          'ordem' => 2
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Logística',
          'slug' => 'logistica',
          'cor_curso' => '#71007C',
          'areas_id' => 1,
          'ordem' => 3
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Contabilidade',
          'slug' => 'contabilidade',
          'cor_curso' => '#CE1D81',
          'areas_id' => 1,
          'ordem' => 4
        ]
      ]);

      DB::connection('mysql_escola')->table('cursos')->insert([
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Enfermagem',
          'slug' => 'enfermagem',
          'cor_curso' => '#1A821A',
          'areas_id' => 2,
          'ordem' => 1
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Radiologia',
          'slug' => 'radiologia',
          'cor_curso' => '#008EC0',
          'areas_id' => 2,
          'ordem' => 2
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Farmácia',
          'slug' => 'farmacia',
          'cor_curso' => '#F58634',
          'areas_id' => 2,
          'ordem' => 3
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Estética',
          'slug' => 'estetica',
          'cor_curso' => '#BD6CD4',
          'areas_id' => 2,
          'ordem' => 4
        ]
      ]);

      DB::connection('mysql_escola')->table('cursos')->insert([
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Eletrônica',
          'slug' => 'eletronica',
          'cor_curso' => '#CA0000',
          'areas_id' => 3,
          'ordem' => 1
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Eletrotécnica',
          'slug' => 'eletrotecnica',
          'cor_curso' => '#703A11',
          'areas_id' => 3,
          'ordem' => 2
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Segurança do Trabalho',
          'slug' => 'segurança-do-trabalho',
          'cor_curso' => '#F0AB00',
          'areas_id' => 3,
          'ordem' => 3
        ],
        [
          'prefixo' => 'Técnico em',
          'titulo' => 'Meio Ambiente',
          'slug' => 'meio-ambiente',
          'cor_curso' => '#86C44C',
          'areas_id' => 3,
          'ordem' => 4
        ]
      ]);

    }
}
