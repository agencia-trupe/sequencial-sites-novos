<?php

use Illuminate\Database\Seeder;

class AreasPosGraduacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('areas_pos_graduacao')->delete();

      DB::connection('mysql_faculdade')->table('areas_pos_graduacao')->insert([
        [
          'titulo' => 'Educação',
          'slug' => 'educacao',
          'cor' => '#0078B0',
          'ordem' => 0
        ],
        [
          'titulo' => 'Saúde',
          'slug' => 'saude',
          'cor' => '#00B282',
          'ordem' => 1
        ],
        [
          'titulo' => 'Gestão e Negócios',
          'slug' => 'gestao-e-negocios',
          'cor' => '#663399',
          'ordem' => 2
        ]
      ]);
    }
}
