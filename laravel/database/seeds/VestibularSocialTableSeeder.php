<?php

use Illuminate\Database\Seeder;

class VestibularSocialTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_faculdade')->table('vestibular_social')->delete();

      DB::connection('mysql_faculdade')->table('vestibular_social')->insert(
        [
          'titulo' => 'VENHA FAZER SUA GRADUAÇÃO NA FACULDADE SEQUENCIAL!',
          'frase_downloads' => 'Dramatically build multidisciplinary materials and fully tested manufactured products.',
          'chamada_titulo' => 'INSCREVA-SE AGORA!',
          'chamada_texto' => 'Frase de chamada...'
        ]
      );
    }
}
