<?php

use Illuminate\Database\Seeder;

class EscolaUnidadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::connection('mysql_escola')->table('unidades')->delete();

      DB::connection('mysql_escola')->table('unidades')->insert([
        [
          'titulo' => 'Capão Redondo',
          'slug' => 'capao-redondo',
          'cor_unidade' => '#708F00'
        ],
        [
          'titulo' => 'Grajaú',
          'slug' => 'grajau',
          'cor_unidade' => '#EB7A02'
        ],
        [
          'titulo' => 'Itaim Paulista',
          'slug' => 'itaim-paulista',
          'cor_unidade' => '#5D1F3F'
        ],
        [
          'titulo' => 'Brasilândia',
          'slug' => 'brasilandia',
          'cor_unidade' => '#9A0D1B'
        ],
      ]);

    }
}
