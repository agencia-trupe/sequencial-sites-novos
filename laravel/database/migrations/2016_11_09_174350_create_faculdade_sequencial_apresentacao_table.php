<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeSequencialApresentacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('sequencial_apresentacao', function (Blueprint $table) {
            $table->increments('id');

            $table->string('imagem');

            $table->text('texto');
            $table->text('regimento');
            $table->text('chamada');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('sequencial_apresentacao');
    }
}
