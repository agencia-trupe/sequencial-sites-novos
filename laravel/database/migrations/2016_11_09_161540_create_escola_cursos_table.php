<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_escola')->create('cursos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('prefixo');
            $table->string('titulo');
            $table->text('texto');
            $table->string('cor_curso');
            $table->string('grade_curricular');

            $table->integer('areas_id')->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_escola')->drop('cursos');
    }
}
