<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLocalizacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->table('sequencial_localizacao', function (Blueprint $table) {
          $table->string('endereco_obs')->after('endereco');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->table('sequencial_localizacao', function (Blueprint $table) {
          $table->dropColumn('endereco_obs');
        });
    }
}
