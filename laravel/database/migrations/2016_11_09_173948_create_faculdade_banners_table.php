<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('banners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('imagem');
            $table->string('link');

            $table->string('is_programado');
            $table->date('data_inicio_exibicao');
            $table->date('data_termino_exibicao');

            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('banners');
    }
}
