<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeAreasPosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('areas_pos_graduacao', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug');
            $table->string('cor');
            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('areas_pos_graduacao');
    }
}
