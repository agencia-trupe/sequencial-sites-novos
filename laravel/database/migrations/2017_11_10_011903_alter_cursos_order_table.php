<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCursosOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cursos', function (Blueprint $table) {
          $table->integer('ordem_geral')->default(0)->after('ordem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cursos', function (Blueprint $table) {
          $table->dropColumn('ordem_geral');
        });
    }
}
/*
alter table `cursos` add `ordem_geral` int not null default '0' after `ordem`
*/
