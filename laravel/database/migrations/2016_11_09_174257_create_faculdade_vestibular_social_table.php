<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeVestibularSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('vestibular_social', function (Blueprint $table) {
            $table->increments('id');

            $table->string('imagem');
            $table->string('titulo');
            $table->text('texto');
            $table->text('frase_downloads');

            $table->string('chamada_titulo');
            $table->text('chamada_texto');
            $table->string('chamada_link');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('vestibular_social');
    }
}
