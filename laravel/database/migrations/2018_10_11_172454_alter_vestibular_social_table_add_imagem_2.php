<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVestibularSocialTableAddImagem2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->table('vestibular_social', function (Blueprint $table) {
            $table->string('imagem_2')->after('imagem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->table('vestibular_social', function (Blueprint $table) {
            $table->dropColumn('imagem_2');
        });
    }
}
