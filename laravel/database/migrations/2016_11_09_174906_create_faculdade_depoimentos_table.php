<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeDepoimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('depoimentos', function (Blueprint $table) {
            $table->increments('id');

            $table->string('imagem');
            $table->text('texto');
            $table->string('autor_nome');
            $table->string('autor_descricao');
            $table->integer('ordem');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('depoimentos');
    }
}
