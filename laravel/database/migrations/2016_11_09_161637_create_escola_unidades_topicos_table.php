<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaUnidadesTopicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_escola')->create('unidades_topicos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('unidades_id')->unsigned();
            $table->foreign('unidades_id')->references('id')->on('unidades')->onDelete('cascade');

            $table->text('texto');
            $table->string('tipo');
            
            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_escola')->drop('unidades_topicos');
    }
}
