<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('leads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('curso');
            $table->string('nome');
            $table->string('telefone');
            $table->string('origem');
            $table->string('teste_vocacional');
            $table->text('teste_vocacional_resultado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('leads');
    }
}
