<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeCursosPosGraduacaoTopicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('cursos_pos_graduacao_topicos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('cursos_pos_graduacao_id')->unsigned();
            $table->foreign('cursos_pos_graduacao_id')->references('id')->on('cursos_pos_graduacao')->onDelete('cascade');

            $table->string('titulo');
            $table->text('texto');

            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('cursos_pos_graduacao_topicos');
    }
}
