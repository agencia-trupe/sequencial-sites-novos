<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaChamadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_escola')->create('chamadas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('is_calhau')->default(0);
            $table->string('tipo');

            $table->string('titulo');
            $table->string('subtitulo');
            $table->string('link');

            $table->string('fundo_tipo');
            $table->string('fundo_imagem');
            $table->string('fundo_cor');

            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_escola')->drop('chamadas');
    }
}
