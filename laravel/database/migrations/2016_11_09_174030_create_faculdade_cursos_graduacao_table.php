<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeCursosGraduacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('cursos_graduacao', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug');

            $table->string('prefixo');
            $table->text('legislacao');
            $table->string('imagem');
            $table->string('cor_curso');
            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('cursos_graduacao');
    }
}
