<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVestivularSocialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->table('vestibular_social', function (Blueprint $table) {
            $table->string('link_banner')->after('imagem');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->table('vestibular_social', function (Blueprint $table) {
            $table->dropColumn('link_banner');
        });
    }
}
