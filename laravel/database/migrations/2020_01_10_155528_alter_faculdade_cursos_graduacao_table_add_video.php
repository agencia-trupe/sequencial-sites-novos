<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaculdadeCursosGraduacaoTableAddVideo extends Migration
{
    public function up()
    {
        Schema::connection('mysql_faculdade')->table('cursos_graduacao', function (Blueprint $table) {
            $table->string('video');
        });
    }

    public function down()
    {
        Schema::connection('mysql_faculdade')->table('cursos_graduacao', function (Blueprint $table) {
            $table->dropColumn('video');
        });
    }
}
