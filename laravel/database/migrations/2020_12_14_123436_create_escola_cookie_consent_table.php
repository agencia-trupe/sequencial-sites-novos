<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaCookieConsentTable extends Migration
{
    public function up()
    {
        Schema::create('cookie_consent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('cookie_consent');
    }
}
