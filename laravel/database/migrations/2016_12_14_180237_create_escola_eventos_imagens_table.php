<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaEventosImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos_imagens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('eventos_id')->unsigned();
            $table->foreign('eventos_id')->references('id')->on('eventos')->onDelete('cascade');

            $table->string('imagem');
            $table->integer('ordem');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventos_imagens');
    }
}
