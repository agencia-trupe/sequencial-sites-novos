<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaOuvidoriaMensagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ouvidoria_mensagens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unidade');
            $table->string('tipo');
            $table->string('nome');
            $table->string('cpf');
            $table->string('email');
            $table->string('telefone');
            $table->text('mensagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ouvidoria_mensagens');
    }
}
