<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeCookieConsentTable extends Migration
{
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('cookie_consent', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('cookie_consent');
    }
}
