<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEscolaCursosAreasFkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_escola')->table('cursos', function (Blueprint $table) {
            $table->foreign('areas_id')->references('id')->on('areas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_escola')->table('cursos', function (Blueprint $table) {
            $table->dropForeign('cursos_areas_id_foreign');
        });
    }
}
