<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeCursosPosGraduacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('cursos_pos_graduacao', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->string('slug');

            $table->integer('areas_pos_graduacao_id')->unsigned();
            $table->foreign('areas_pos_graduacao_id')->references('id')->on('areas_pos_graduacao')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('cursos_pos_graduacao');
    }
}
