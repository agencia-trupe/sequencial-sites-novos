<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFaculdadeCursosGraduacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->table('cursos_graduacao', function (Blueprint $table) {
            $table->text('sobre_o_curso');
            $table->string('arquivo_sobre_o_curso');
            $table->text('sobre_o_mercado');
            $table->string('arquivo_sobre_o_mercado');
            $table->string('duracao');
            $table->text('periodo');
            $table->string('investimento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->table('cursos_graduacao', function (Blueprint $table) {
            $table->dropColumn('sobre_o_curso');
            $table->dropColumn('arquivo_sobre_o_curso');
            $table->dropColumn('sobre_o_mercado');
            $table->dropColumn('arquivo_sobre_o_mercado');
            $table->dropColumn('duracao');
            $table->dropColumn('periodo');
            $table->dropColumn('investimento');
        });
    }
}
