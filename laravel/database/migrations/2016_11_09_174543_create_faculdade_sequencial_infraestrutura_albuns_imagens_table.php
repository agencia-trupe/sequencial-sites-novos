<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeSequencialInfraestruturaAlbunsImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('infraestrutura_albuns_imagens', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('infraestrutura_albuns_id')->unsigned()->nullable();
            $table->foreign('infraestrutura_albuns_id')->references('id')->on('infraestrutura_albuns')->onDelete('cascade');

            $table->string('imagem');
            $table->integer('ordem');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('infraestrutura_albuns_imagens');
    }
}
