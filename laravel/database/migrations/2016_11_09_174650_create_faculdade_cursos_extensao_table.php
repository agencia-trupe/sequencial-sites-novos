<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaculdadeCursosExtensaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_faculdade')->create('cursos_extensao', function (Blueprint $table) {
            $table->increments('id');

            $table->date('data');
            $table->string('titulo');
            $table->string('slug');
            $table->string('imagem');
            $table->text('texto');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_faculdade')->drop('cursos_extensao');
    }
}
