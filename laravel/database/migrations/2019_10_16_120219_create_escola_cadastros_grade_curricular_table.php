<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscolaCadastrosGradeCurricularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadastros_grade_curricular', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('token');
            $table->integer('curso_id')->unsigned();
            $table->string('curso_titulo');
            $table->boolean('visualizado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cadastros_grade_curricular');
    }
}
