var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

var paths = {
	src : {
		vendor : './resources/assets/vendor/',
		fonts  : './resources/assets/fonts/'
	},
	output : {
		ckeditor : '../public/assets/ckeditor/'
	},
	faculdade : {
		src : {
			less   : './resources/assets/less/faculdade/',
			js     : './resources/assets/js/faculdade/'
		},
		output : {
			less  : '../public/assets/faculdade/css/',
			js    : '../public/assets/faculdade/js/',
			fonts : '../public/assets/faculdade/fonts/'
		}
	},
	escola : {
		src : {
			less   : './resources/assets/less/escola/',
			js     : './resources/assets/js/escola/'
		},
		output : {
			less  : '../public/assets/escola/css/',
			js    : '../public/assets/escola/js/',
			fonts : '../public/assets/escola/fonts/'
		}
	},
	grupo : {
		src : {
			less   : './resources/assets/less/grupo/',
			js     : './resources/assets/js/grupo/'
		},
		output : {
			less  : '../public/assets/grupo/css/',
			js    : '../public/assets/grupo/js/',
			fonts : '../public/assets/grupo/fonts/'
		}
	}
};

elixir(function(mix) {
	mix
	/***************************************/
	//
	// Cópia do CKEditor para instalação compartilhada
	// entre os 3 sites
	//
	/***************************************/
	.copy( paths.src.vendor + 'ckeditor/', paths.output.ckeditor)

	/***************************************/
	//
	// Cópia de arquivos originais da
	// pasta Vendor para o acesso público
	//
	/***************************************/

	/***************************************/
	// FACULDADE
	/***************************************/
	.copy( paths.src.vendor + 'modernizr/modernizr.js', paths.faculdade.output.js + 'modernizr.js', './')
	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.faculdade.output.js + 'jquery.js', './')
	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.faculdade.output.js + 'bootstrap.js', './')
	.copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.faculdade.output.fonts)
	.copy( paths.src.vendor + 'jquery-ui/themes/base/images/*.*', paths.faculdade.output.less + 'images/')

	.copy( paths.src.vendor + 'fancybox/source/*.gif', paths.faculdade.output.less)
	.copy( paths.src.vendor + 'fancybox/source/*.png', paths.faculdade.output.less)
	// Executar somente 1 vez \/
	//.copy( paths.src.vendor + 'ckeditor/config.js', paths.faculdade.src.js + 'painel/ckeditor_config.js')

	/***************************************/
	// ESCOLA
	/***************************************/
	.copy( paths.src.vendor + 'modernizr/modernizr.js', paths.escola.output.js + 'modernizr.js', './')
	.copy( paths.src.vendor + 'jquery/dist/jquery.min.js', paths.escola.output.js + 'jquery.js', './')
	.copy( paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js', paths.escola.output.js + 'bootstrap.js', './')
	.copy( paths.src.vendor + 'bootstrap/dist/fonts/', paths.escola.output.fonts)
	.copy( paths.src.vendor + 'jquery-ui/themes/base/images/*.*', paths.escola.output.less + 'images/')

	.copy( paths.src.vendor + 'fancybox/source/*.gif', paths.escola.output.less)
	.copy( paths.src.vendor + 'fancybox/source/*.png', paths.escola.output.less)
	// Executar somente 1 vez \/
	//.copy( paths.src.vendor + 'ckeditor/config.js', paths.escola.src.js + 'painel/ckeditor_config.js')

	/***************************************/
	// GRUPO
	/***************************************/
	.copy( paths.src.vendor + 'fancybox/source/*.gif', paths.grupo.output.less)
	.copy( paths.src.vendor + 'fancybox/source/*.png', paths.grupo.output.less)

	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/



	/***************************************/
	//
	// CSS E JS (PAINEL)
	//
	/***************************************/

	/***************************************/
	// FACULDADE
	/***************************************/
	.less( paths.faculdade.src.less + 'painel/painel.less', paths.faculdade.src.less + 'painel/')
	.styles([
		paths.src.vendor + 'css-reset/reset.min.css',
		paths.src.vendor + 'jquery-ui/themes/base/jquery-ui.css',
		paths.src.vendor + 'bootstrap/dist/css/bootstrap.min.css',
		paths.src.vendor + 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
		paths.faculdade.src.less   + 'painel/painel.css',
	], paths.faculdade.output.less + 'painel.css', './')

	.scripts( paths.faculdade.src.js + 'painel/ckeditor_config.js', paths.faculdade.output.js + 'ckeditor_config.js', './')
	.scripts([
		paths.src.vendor + 'jquery-ui/jquery-ui.min.js',
		paths.src.vendor + 'bootbox/bootbox.js',
		paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
		paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
		paths.src.vendor + 'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js',
		paths.faculdade.src.js     + 'painel/painel.js',
		paths.faculdade.src.js     + 'painel/painel-upload-imagens.js',
		paths.faculdade.src.js     + 'painel/painel-form-topicos.js',
		paths.faculdade.src.js     + 'painel/painel-upload-arquivos.js'
	], paths.faculdade.output.js  + 'painel.js', './')

	/***************************************/
	// ESCOLA
	/***************************************/
	.less( paths.escola.src.less + 'painel/painel.less', paths.escola.src.less + 'painel/')
	.styles([
		paths.src.vendor + 'css-reset/reset.min.css',
		paths.src.vendor + 'jquery-ui/themes/base/jquery-ui.css',
		paths.src.vendor + 'bootstrap/dist/css/bootstrap.min.css',
		paths.src.vendor + 'x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
		paths.escola.src.less   + 'painel/painel.css',
	], paths.escola.output.less + 'painel.css', './')

	.scripts( paths.escola.src.js + 'painel/ckeditor_config.js', paths.escola.output.js + 'ckeditor_config.js', './')
	.scripts([
		paths.src.vendor + 'jquery-ui/jquery-ui.min.js',
		paths.src.vendor + 'bootbox/bootbox.js',
		paths.src.vendor + 'bootstrap/dist/js/bootstrap.min.js',
		paths.src.vendor + 'blueimp-file-upload/js/jquery.fileupload.js',
		paths.src.vendor + 'x-editable/dist/bootstrap3-editable/js/bootstrap-editable.js',
		paths.escola.src.js     + 'painel/painel.js',
		paths.escola.src.js     + 'painel/painel-upload-imagens.js'
	], paths.escola.output.js  + 'painel.js', './')

	/******************************************************************************/
	/******************************************************************************/
	/******************************************************************************/



	/***************************************/
	//
	// CSS E JS SITES
	//
	/***************************************/

	/***************************************/
	// FACULDADE
	/***************************************/

	.less([
		paths.faculdade.src.less + 'site/base.less',
		paths.faculdade.src.less + 'site/home.less',
		paths.faculdade.src.less + 'site/cursos-graduacao.less',
		paths.faculdade.src.less + 'site/cursos-pos-graduacao.less',
		paths.faculdade.src.less + 'site/vestibular-social.less',
		paths.faculdade.src.less + 'site/a-faculdade.less',
		paths.faculdade.src.less + 'site/outros-cursos.less',
		paths.faculdade.src.less + 'site/novidades.less',
		paths.faculdade.src.less + 'site/eventos.less',
		paths.faculdade.src.less + 'site/estagios.less',
		paths.faculdade.src.less + 'site/depoimentos.less',
		paths.faculdade.src.less + 'site/fale-conosco.less',
		paths.faculdade.src.less + 'site/ouvidoria.less',
		paths.faculdade.src.less + 'site/busca.less',
		paths.faculdade.src.less + 'site/inscreva-se.less'
	], paths.faculdade.output.less + 'site.css')

	.styles([
		paths.src.vendor + 'css-reset/reset.min.css',
		paths.src.vendor + 'fancybox/source/jquery.fancybox.css'
	], paths.faculdade.output.less + 'vendor.css', './')

	.scripts([
		paths.src.vendor + 'fancybox/source/jquery.fancybox.js',
		paths.src.vendor + 'jquery-cycle2/build/jquery.cycle2.js',
		paths.src.vendor + 'jquery-mask-plugin/dist/jquery.mask.js',
		paths.faculdade.src.js + 'site/site.js'
	], paths.faculdade.output.js + 'site.js', './')


	/***************************************/
	// ESCOLA
	/***************************************/

	.less([
		paths.escola.src.less + 'site/base.less',
		paths.escola.src.less + 'site/home.less',
		paths.escola.src.less + 'site/cursos-tecnicos.less',
		paths.escola.src.less + 'site/bolsas-de-estudo.less',
		paths.escola.src.less + 'site/a-sequencial.less',
		paths.escola.src.less + 'site/novidades.less',
		paths.escola.src.less + 'site/eventos.less',
		paths.escola.src.less + 'site/perguntas-frequentes.less',
		paths.escola.src.less + 'site/trabalhe-conosco.less',
		paths.escola.src.less + 'site/ouvidoria.less',
		paths.escola.src.less + 'site/fale-conosco.less',
		paths.escola.src.less + 'site/unidades.less',
		paths.escola.src.less + 'site/depoimentos.less',
		paths.escola.src.less + 'site/busca.less'
	], paths.escola.output.less + 'site.css')

	.styles([
		paths.src.vendor + 'css-reset/reset.min.css',
		paths.src.vendor + 'fancybox/source/jquery.fancybox.css'
	], paths.escola.output.less + 'vendor.css', './')

	.scripts([
		paths.src.vendor + 'fancybox/source/jquery.fancybox.js',
		paths.src.vendor + 'jquery-cycle2/build/jquery.cycle2.js',
		paths.src.vendor + 'jquery-mask-plugin/dist/jquery.mask.js',
		paths.escola.src.js + 'site/site.js'
	], paths.escola.output.js + 'site.js', './')

	/***************************************/
	// GRUPO
	/***************************************/
	.less(paths.grupo.src.less + 'site.less', paths.grupo.src.less + 'site.css')

	.styles([
		paths.src.vendor + 'css-reset/reset.min.css',
		paths.src.vendor + 'fancybox/source/jquery.fancybox.css',
		paths.grupo.src.less + 'site.css'
	], paths.grupo.output.less + 'site.css', './')

	.scripts([
		paths.src.vendor + 'fancybox/source/jquery.fancybox.pack.js',
		paths.src.vendor + 'jquery-cycle2/build/jquery.cycle2.js',
		paths.grupo.src.js + 'site.js'
	], paths.grupo.output.js + 'site.js', './');

});
