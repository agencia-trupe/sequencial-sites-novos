$('document').ready( function(){

  $('#chamada-cursos ul li a, .chamada-cursos li a').on('mouseover', function(){
    $(this).css('background-color', $(this).attr('data-cor-fundo'));
  });

  $('#chamada-cursos ul li a, .chamada-cursos li a').on('mouseout', function(){
    $(this).css('background-color', $(this).attr('data-cor-original'));
  });

  $('.lista-areas-pos li a').on('mouseover', function(){
    $(this).css('color', $(this).attr('data-cor'));
    $(this).css('border-color', $(this).attr('data-cor'));
  });

  $('.lista-areas-pos li a').on('mouseout', function(){
    $(this).css('color', '#FFF');
    $(this).css('border-color', '#E3EDE9');
  });

  $('.conteudo-cursos-graduacao aside ul li a').on('mouseover', function(){
    $(this).css('color', $(this).attr('data-cor'));
  });

  $('.conteudo-cursos-graduacao aside ul li a').on('mouseout', function(){
    $(this).css('color', '#FFF');
  });

  $('.conteudo-cursos-graduacao section .lista-cursos li a').on('mouseover', function(){
    $(this).find('.texto .prefixo').css('color', $(this).attr('data-cor'));
    $(this).find('.texto .titulo').css('color', $(this).attr('data-cor'));
    $(this).find('.texto .legislacao').css('color', $(this).attr('data-cor'));
  });

  $('.conteudo-cursos-graduacao section .lista-cursos li a').on('mouseout', function(){
    $(this).find('.texto .prefixo').css('color', '#fff');
    $(this).find('.texto .titulo').css('color', '#fff');
    $(this).find('.texto .legislacao').css('color', '#fff');
  });

  $('.lista-imagens-sem-album li a, .lista-albuns li a, .lista-imagens li a').fancybox({
    loop : false,
    helpers : {
      title: {
        type : 'inside'
      }
    }
  });

  $('#menu-toggle').click( function(e){
    e.preventDefault();
    e.stopPropagation();
    $('.menu-principal').toggleClass('aberto');
    $('.menus-secundarios').toggleClass('aberto');
  });

  $('.link-busca a').click( function(e){

    e.preventDefault();

    $('body, html').animate({
      'scrollTop' : 0
    }, 400, function(){
      $('#busca-overlay').addClass('aberto');

      $('#busca-overlay input').click( function(e){
        e.stopPropagation();
      });

      $("#busca-overlay input[type='text']").focus();

    });

  });

  $('html').click(function() {
    if($('#busca-overlay').hasClass('aberto')){
      $('#busca-overlay').removeClass('aberto');
    }
  });

  $("input[name='cpf']").mask('000.000.000-00', {reverse: true});

});
