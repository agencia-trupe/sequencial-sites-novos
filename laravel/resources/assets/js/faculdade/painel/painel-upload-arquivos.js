$('document').ready( function(){

  $('#listaArquivos').sortable().disableSelection();
  $('#listaArquivosExistentes').sortable().disableSelection();

  $('#adicionarArquivo').on('click', function(e){
    e.preventDefault();

    var file_select_template = "";

    file_select_template += "<div class='list-group-item'>";
      file_select_template += "<div class='form-group'>";
        file_select_template += "<input type='text' class='form-control' name='arquivos_titulos[]' placeholder='Título do arquivo'>";
      file_select_template += "</div>";
      file_select_template += "<div class='form-group'>";
        file_select_template += "<input type='file' class='form-control' name='arquivos[]' value=''>";
      file_select_template += "</div>";
      file_select_template += "<div class='form-group'>";
        file_select_template += "<a href='#' class='btn btn-sm btn-danger btn-remover-arquivo'>remover este arquivo</a>";
      file_select_template += "</div>";
    file_select_template += "</div>";
    $('#listaArquivos').append(file_select_template);
    $('#listaArquivos').sortable("refresh");

  });

  $(document).on('click', '#listaArquivos a.btn-remover-arquivo, #listaArquivosExistentes a.btn-remover-arquivo', function(e){
    e.preventDefault();
    var parent = $(this).parent().parent();
    parent.css('opacity', .35);
    setTimeout( function(){
        parent.remove();
    }, 350);
  });

});
