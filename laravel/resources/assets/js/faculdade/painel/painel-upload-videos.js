function bindTituloEditavel(){
  $('.titulo-video-nopost').editable();

  $('.titulo-video-nopost').on('save', function(e, params) {
    $(this).parent().parent().find('.video-titulo').val(params.newValue);
  });
}

function testarUrlThumb(url){
  if (url != undefined || url != '') {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[2].length == 11) {
      return true;
    }
    else {
      return false;
    }
  }else{
    return false;
  }
}

$('document').ready( function(){

  $('#adicionarVideo').click( function(){
    var botao = $(this);
    var input = $('#input-video');

    if(input.val() == '' || !testarUrlThumb(input.val())){
      input.parent().addClass('has-error').addClass('has-feedback');
      input.on('focus', function(){
        input.parent().removeClass('has-error').removeClass('has-feedback');
      });
      return false;
    }

    botao.html("<img src='assets/img/layout/ajax-loader.gif'>");

    // requisição ajax pra pegar titulo e gravar thumb do vídeo
    $.post('painel/videos/consultar', {
      url : input.val()
    }, function(resposta){

      var item_video = "";
      item_video += "<div class='list-group-item projetoVideo'>";
      item_video += "<a href='#' class='btn btn-sm btn-danger btn-remover pull-right' title='remover o vídeo'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover vídeo</strong></a>";
        item_video += "<h4 class='list-group-item-heading'>";
         item_video += "<a href='#' class='titulo-video-nopost' data-type='text' data-title='Alterar título' title='Alterar título'>" + resposta.titulo + "</a"
        item_video += "</h4>";
        item_video += "<p class='list-group-item-text'>";
          item_video += "<img class='img-thumbnail' style='max-width: 60px; margin-right: 15px' src='assets/img/videos/thumbs/" + resposta.thumb + "'>";
          item_video += "<a href='" + resposta.url + "' target='_blank'>" + resposta.url + "</a>";
        item_video += "</p>";
        item_video += "<input type='hidden' name='video_id[]' value=\"" + resposta.video_id + "\">";
        item_video += "<input type='hidden' name='video_url[]' value=\"" + resposta.url + "\">";
        item_video += "<input type='hidden' name='video_titulo[]' class='video-titulo' value=\"" + resposta.titulo + "\">";
        item_video += "<input type='hidden' name='video_thumb[]' value=\"" + resposta.thumb + "\">";
      item_video += "</div>";

      $('#listaVideos').append(item_video);
      $('#listaVideos').sortable("refresh");

      input.val('');

      botao.html("<span class='glyphicon glyphicon-circle-arrow-right'></span>");

      bindTituloEditavel();

    });

  });

  $('#listaVideos').sortable().disableSelection();

  $(document).on('click', '.projetoVideo a.btn-remover', function(e){
    e.preventDefault();
    var parent = $(this).parent();
    parent.css('opacity', .35);
    setTimeout( function(){
        parent.remove();
    }, 350);
  });

  $('.titulo-video').editable();

  bindTituloEditavel();

});
