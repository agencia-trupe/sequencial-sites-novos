$('document').ready( function(){

  $('#listaTopicos').sortable({
    items : ':not(.info)'
  }).disableSelection();
  $('#listaTopicosExistentes').sortable({
    items : ':not(.info)'
  }).disableSelection();

  $('#adicionarTopico').on('click', function(e){
    e.preventDefault();

    var id_input = "id" + Math.random().toString(16).slice(2);
    var topico_input_template = "";

    topico_input_template += "<div class='list-group-item'>";
      topico_input_template += "<div class='form-group'>";
        topico_input_template += "<input type='text' class='form-control' name='topicos_titulos[]' placeholder='Título'>";
      topico_input_template += "</div>";
      topico_input_template += "<div class='form-group'>";
        topico_input_template += "<textarea name='topicos_textos[]' class='form-control' placeholder='Texto' id='"+id_input+"'></textarea>";
      topico_input_template += "</div>";
      topico_input_template += "<div class='form-group'>";
        topico_input_template += "<a href='#' class='btn btn-sm btn-danger btn-remover-arquivo'><span class='glyphicon glyphicon-trash'></span> remover este tópico</a>";
      topico_input_template += "</div>";
    topico_input_template += "</div>";

    $('#listaTopicos').append(topico_input_template);
    $('#listaTopicos').sortable("refresh");

    $('#'+id_input).ckeditor({
      customConfig: '/assets/faculdade/js/ckeditor_config.js'
    });

  });

  $(document).on('click', '#listaTopicos a.btn-remover-arquivo, #listaTopicosExistentes a.btn-remover-arquivo', function(e){
    e.preventDefault();
    var parent = $(this).parent().parent();
    parent.css('opacity', .35);
    setTimeout( function(){
        parent.remove();
    }, 350);
  });

});
