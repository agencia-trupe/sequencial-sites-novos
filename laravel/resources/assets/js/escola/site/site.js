$('document').ready( function(){

    $('.lista-imagens-sem-album li a, .lista-albuns li a, .lista-imagens li a').fancybox({
        loop : false,
        helpers : {
            title: {
                type : 'inside'
            }
        }
    });

    $('#menu-toggle').click( function(e){
        e.preventDefault();
        e.stopPropagation();
        $('.menu-principal').toggleClass('aberto');
        $('.menus-secundarios').toggleClass('aberto');
    });

    $('.link-busca a').click( function(e){

        e.preventDefault();

        $('body, html').animate({
            'scrollTop' : 0
        }, 400, function(){
            $('#busca-overlay').addClass('aberto');

            $('#busca-overlay input').click( function(e){
                e.stopPropagation();
            });

            $("#busca-overlay input[type='text']").focus();

        });

    });

    $('html').click(function() {
        if($('#busca-overlay').hasClass('aberto')){
            $('#busca-overlay').removeClass('aberto');
        }
    });

     $("input[name='cpf']").mask('000.000.000-00', {reverse: true});

    $('#lista-unidades a').on('mouseover', function(){
        $(this).find('span').css('color', $(this).attr('data-color'));
    });

    $('#lista-unidades a').on('mouseout', function(){
        $(this).find('span').css('color', '#FFF');
    });

    $('#curso-por-area ul li a').on('mouseover', function(){
        $(this).css('background', $(this).attr('data-color'));
    });

    $('#curso-por-area ul li a').on('mouseout', function(){
        $(this).css('background', 'transparent');
    });

    $('#lista-unidades-box .unidade-box-link').on('mouseover', function(){
        $(this).find('p').css('color', $(this).attr('data-color'));
    });

    $('#lista-unidades-box .unidade-box-link').on('mouseout', function(){
        $(this).find('p').css('color', '#fff');
    });

    $('.fancybox').fancybox();

    $('.unidades-detalhe-topicos .topico h2').on('click', function(e){
        e.preventDefault();
        $('.unidades-detalhe-topicos .topico.ativo').removeClass('ativo');
        $(this).parent().toggleClass('ativo');
    })

    $('.secao-faq .questao a').on('click', function(e){
        e.preventDefault();
        $('.secao-faq .questao.aberto').removeClass('aberto');
        $(this).parent().toggleClass('aberto');
    });

    $('#faq-lista-cursos li a').on('click', function(e){
      e.preventDefault();
      var btnClicked = $(this);
      var btnActive = $('#faq-lista-cursos li a.aberto');
      var targetSection = $(btnClicked.attr('href'));
      var activeSection = $('section .secao-faq.aberto');

      if (!targetSection.hasClass('aberto')) {
        activeSection.fadeOut('fast', function() {

          activeSection.removeClass('aberto');
          btnActive.removeClass('aberto');

          btnClicked.addClass('aberto');
          targetSection.addClass('aberto');
          targetSection.fadeIn('fast');
        });
      }
    });

});
