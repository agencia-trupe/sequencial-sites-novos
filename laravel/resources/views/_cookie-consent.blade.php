<div class="cookie-consent" style="display: none">
  <p>Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa <a href="https://www.gruposequencial.com.br/politica-de-privacidade" target="_blank">Política de Privacidade</a> e saiba mais.</p>
  <button>ACEITAR E FECHAR</button>
</div>

<style>
  .cookie-consent {
    position: fixed;
    bottom: 0;
    z-index: 9;
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    margin: 0;
    background-color: rgba(0,0,0,.8);
    padding: 25px 7%;
    font-family: 'Asap', sans-serif;
    box-sizing: border-box;
  }

  .cookie-consent p {
    font-size: 14px;
    color: #fff;
    margin: 0;
    line-height: 1.5;
    width: 75%;
  }

  .cookie-consent p a {
    text-decoration: underline;
  }

  .cookie-consent p a:hover {
    font-weight: bold;
  }

  .cookie-consent button {
    width: 20%;
    font-size: 14px;
    color: #fff;
    background: #01669c;
    border: none;
    border-radius: 5px;
    height: 40px;
    margin-left: 20px;
    font-weight: 600;
    cursor: pointer;
    outline: none;
    flex-shrink: 0;
    transition: background .2s;
  }

  .cookie-consent button:hover {
    background: #003d5d;
  }

  @media (max-width: 768px) {
    .cookie-consent {
      display: block;
    }

    .cookie-consent p {
      font-size: 11px;
      width: 100%;
      text-align: center;
    }

    .cookie-consent button {
      width: 100%;
      margin-top: 10px;
      font-size: 12px;
    }
  }
</style>

<script>
  if (localStorage.getItem('{{ $domain }}:cookieConsent') !== '1') {
    $('.cookie-consent').show();
  }

  $('.cookie-consent button').click(function() {
    $(this).parent().hide();
    $.ajax({
      type: 'POST',
      url: BASE + 'cookie-consent',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    }).done(function() {
      localStorage.setItem('{{ $domain }}:cookieConsent', '1');
    });
  });
</script>
