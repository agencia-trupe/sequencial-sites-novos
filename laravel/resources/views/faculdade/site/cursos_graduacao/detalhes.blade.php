@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-cursos-graduacao">

    <div class="centralizar">

      <aside>
        <h2>NOSSOS CURSOS<br> DE GRADUAÇÃO:</h2>
        <ul>
          @foreach($cursos as $curso)
            <li>
              <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" style="background-color:{{$curso->cor_curso}};border:1px {{$curso->cor_curso}} solid;" data-cor="{{$curso->cor_curso}}">
                {{$curso->titulo}}
              </a>
            </li>
          @endforeach
        </ul>
      </aside>

      <section>

        <div class="curso-graduacao-detalhes">
          <div class="coluna">
            <div class="card-curso" style="background-color:{{$cursoDetalhes->cor_curso}};">
              <img src="assets/faculdade/img/cursos_graduacao/{{$cursoDetalhes->imagem}}" alt="{{$cursoDetalhes->titulo}}" />
              <div class="texto">
                <div class="prefixo">{{$cursoDetalhes->prefixo}}</div>
                <div class="titulo">{{$cursoDetalhes->titulo}}</div>
                <p class="legislacao">
                  LEGISLAÇÃO:
                  <span>
                  {{$cursoDetalhes->legislacao}}
                  </span>
                </p>
              </div>
            </div>

            <div class="sobre">
              <h3 style="color:{{$cursoDetalhes->cor_curso}}">SOBRE O CURSO DE {{$cursoDetalhes->titulo}}</h3>
              <div>{!!$cursoDetalhes->sobre_o_curso!!}</div>
              @if($cursoDetalhes->arquivo_sobre_o_curso)
              <a href="assets/faculdade/arquivos/{{$cursoDetalhes->arquivo_sobre_o_curso}}" target="_blank" class="btn-pdf">MAIS INFORMAÇÕES SOBRE O CURSO<br>MATRIZ CURRICULAR | PROJETO PEDAGÓGICO | ESTÁGIO E OUTROS</a>
              @endif
            </div>

            <div class="divider" style="background-color:{{$cursoDetalhes->cor_curso}}"></div>

            <div class="sobre">
              <h3 style="color:{{$cursoDetalhes->cor_curso}}">SOBRE O MERCADO DE TRABALHO</h3>
              <div>{!!$cursoDetalhes->sobre_o_mercado!!}</div>
            </div>

            @if($cursoDetalhes->arquivo_sobre_o_mercado)
            <a href="#" class="box-mercado box-mercado-link">
              <p>Quer saber quanto em média ganha um profissional de {{mb_convert_case($cursoDetalhes->titulo, MB_CASE_TITLE, "UTF-8")}}?</p>
              <p>MAIS SOBRE O MERCADO DE TRABALHO &raquo;</p>
            </a>
            <div class="box-mercado box-mercado-form" style="display:none">
              <p>Informe seu e-mail. Enviaremos um documento completo com mais informações sobre o mercado de trabalho:</p>
              <form action="" id="form-box-mercado">
                <input type="hidden" name="curso" value="{{$cursoDetalhes->id}}">
                <input type="email" name="email" placeholder="e-mail" required>
                <input type="submit" value="ENVIAR">
              </form>
            </div>
            <div class="box-mercado box-mercado-obrigado" style="display:none">
              <p>Obrigado<br>Acabamos de enviar no seu e-mail um<br>documento com as informações.</p>
            </div>
            @endif
          </div>

          <div class="coluna">
            @if($cursoDetalhes->video)
            <style>
                .embed-container {
                    position: relative;
                    padding-bottom: 56.25%;
                    margin-bottom: 20px;
                    height: 0;
                    overflow: hidden;
                    max-width: 100%;
                }
                .embed-container iframe,
                .embed-container object,
                .embed-container embed {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                }
            </style>
            <div class="embed-container">
                <iframe src="https://www.youtube.com/embed/{{ $cursoDetalhes->video }}" frameborder="0" allowfullscreen></iframe>
            </div>
            @endif
            <div class="detalhes duracao">
              <h3 style="color:{{$cursoDetalhes->cor_curso}}">DURAÇÃO</h3>
              <p>{{$cursoDetalhes->duracao}}</p>
            </div>
            <div class="detalhes periodo">
              <h3 style="color:{{$cursoDetalhes->cor_curso}}">PERÍODO</h3>
              {!!$cursoDetalhes->periodo!!}
            </div>
            <div class="detalhes investimento">
              <h3 style="color:{{$cursoDetalhes->cor_curso}}">INVESTIMENTO</h3>
              <p>{{$cursoDetalhes->investimento}}</p>
            </div>
            <a class="btn-descontos" href="{{ route('faculdade.inscreva-se.index', ['curso' => $cursoDetalhes->id]) }}">CONSULTE SOBRE DESCONTOS DE ATÉ 80%</a>

            <div class="box-informacoes">
              @if(session('enviado'))
              <div class="form-enviado">
                  <h3>OBRIGADO!</h3>
                  <p>Você será contatado por nosso atendimento para receber mais informações e confirmar seu interesse nos cursos da Faculdade Sequencial.</p>
              </div>
              @else
              <p>MAIS INFORMAÇÕES E INSCRIÇÕES</p>
              <form action="{{ route('faculdade.inscreva-se.post') }}" method="POST">
                  {!! csrf_field() !!}

                  @if($errors->any())
                  <div class="erro">Preencha todos os campos corretamente.</div>
                  @endif

                  <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                  <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                  <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
                  <input type="hidden" name="curso" value="{{$cursoDetalhes->titulo}}">
                  <input type="hidden" name="origem" value="Página do Curso">
                  <button>
                      QUERO SABER MAIS SOBRE ESTE CURSO
                      <span></span>
                  </button>
                  <a href="{{ route('faculdade.inscreva-se.index', ['curso' => $cursoDetalhes->id]) }}" class="btn-inscreva-se">
                    <span class="titulo">QUERO ME INSCREVER<br>NO VESTIBULAR SOCIAL<br>PARA ESTE CURSO</span>
                    <span class="divider"></span>
                    <span class="subtitulo">Com até 80% de desconto!</span>
                  </a>
              </form>
              @endif
            </div>
          </div>
        </div>

      </section>

    </div>
  </div>

  <div class="chamada-cursos">
    <div class="centralizar">
      <h1>CONHEÇA OUTROS CURSOS DE GRADUAÇÃO DA FACULDADE SEQUENCIAL:</h1>
      <ul class="chamada-cursos">
        @foreach($cursos as $curso)
          <li>
            <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" data-cor-fundo="{{$curso->cor_curso}}" data-cor-original="transparent">                <img src="assets/faculdade/img/cursos_graduacao/{{$curso->imagem}}" alt="{{$curso->titulo}}" />
              <span style="color:{{$curso->cor_curso}};">{{$curso->titulo}}</span>
            </a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>

  <style>
    .curso-graduacao-detalhes .sobre .btn-pdf:hover {
      background-color: {{ $cursoDetalhes->cor_curso }} !important;
    }
    .box-mercado {
      background-color: {{ $cursoDetalhes->cor_curso }} !important;
    }
  </style>
  <script>
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $('.box-mercado-link').click(function(e) {
      e.preventDefault();
      $(this).hide();
      $('.box-mercado-form').fadeIn();
    });
    $('#form-box-mercado').submit(function(e) {
      e.preventDefault();

      var email = $(this).find('input[name=email]').val();
      var curso = $(this).find('input[name=curso]').val();

      var $form = $(this);
      if ($form.hasClass('sending')) return false;

      $form.addClass('sending');

      $.ajax({
        type: 'POST',
        url: BASE + '/lead',
        data: {
          email: email,
          curso: curso
        },
        success: function() {
          $form.parent().hide();
          $('.box-mercado-obrigado').fadeIn();
        },
        error: function(data) {
          if(data.status == 422) {
            alert('Insira um endereço de e-mail válido.');
          } else {
            alert('Ocorreu um erro. Tente novamente.');
          }
        },
        dataType: 'json'
      })
      .always(function() {
        $form.removeClass('sending');
      });
    });
  </script>

@stop
