@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-cursos-graduacao">

    <div class="chamada-cursos">
      <div class="centralizar">
        <h1>CONHEÇA OS CURSOS DE GRADUAÇÃO DA FACULDADE SEQUENCIAL:</h1>
        <ul>
          @foreach($cursos as $curso)
            <li>
              <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" data-cor-fundo="{{$curso->cor_curso}}" data-cor-original="transparent">
                <img src="assets/faculdade/img/cursos_graduacao/{{$curso->imagem}}" alt="{{$curso->titulo}}" />
                <span style="color:{{$curso->cor_curso}};">{{$curso->titulo}}</span>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
    </div>

    <div class="centralizar">

      <aside>
        <h2>NOSSOS CURSOS<br> DE GRADUAÇÃO:</h2>
        <ul>
          @foreach($cursos as $curso)
            <li>
              <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" style="background-color:{{$curso->cor_curso}};border:1px {{$curso->cor_curso}} solid;" data-cor="{{$curso->cor_curso}}">
                {{$curso->titulo}}
              </a>
            </li>
          @endforeach
        </ul>
      </aside>

      <section>
        <ul class="lista-cursos">
          @foreach($cursos as $curso)
            <li>
              <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" style="background-color:{{$curso->cor_curso}};border:1px {{$curso->cor_curso}} solid;" data-cor="{{$curso->cor_curso}}">
                <img src="assets/faculdade/img/cursos_graduacao/{{$curso->imagem}}" alt="{{$curso->titulo}}" />
                <div class="texto">
                  <div class="prefixo">{{$curso->prefixo}}</div>
                  <div class="titulo">{{$curso->titulo}}</div>
                  <p class="legislacao">
                    LEGISLAÇÃO:
                    <span>
                    {{$curso->legislacao}}
                    </span>
                  </p>
                </div>
              </a>
            </li>
          @endforeach
        </ul>
      </section>

    </div>
  </div>

@stop
