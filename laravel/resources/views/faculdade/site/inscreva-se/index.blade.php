@extends('faculdade.site.template')

@section('conteudo')

<div class="inscreva-se centralizar">
    <div class="lateral">
        <img src="{{ asset('assets/faculdade/img/vestibular_social/'.$texto->imagem_2) }}" alt="">

        <div class="arquivos">
            @foreach($arquivos as $arquivo)
            <a href="{{ asset('assets/faculdade/arquivos/'.$arquivo->arquivo) }}" class="arquivo" target="_blank">{{ $arquivo->titulo }} &raquo;</a>
            @endforeach
        </div>
    </div>

    <div class="formulario">
        <h2>Inscreva-se nos cursos da Faculdade Sequencial.</h2>
        <h3>Você receberá nosso contato com mais informações.</h3>

        @if(session('enviado'))
        <div class="form-enviado">
            <h3>OBRIGADO!</h3>
            <p>Você será contatado por nosso atendimento para receber mais informações e confirmar seu interesse nos cursos da Faculdade Sequencial.</p>
        </div>
        @else
        <form action="{{ route('faculdade.inscreva-se.post') }}" method="POST">
            {!! csrf_field() !!}

            @if($errors->any())
            <div class="erro">Preencha todos os campos corretamente.</div>
            @endif

            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
            <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
            <select name="curso" required>
                <option value="">curso de interesse (selecione...)</option>
                @foreach($cursos as $curso)
                @if(old('curso'))
                <option value="{{ $curso->titulo }}" @if(old('curso')==$curso->titulo) selected @endif>{{ $curso->titulo }}</option>
                @else
                <option value="{{ $curso->titulo }}" @if(request('curso')==$curso->id) selected @endif>{{ $curso->titulo }}</option>
                @endif
                @endforeach
            </select>
            <div class="inscricao-opt-in" style="display: flex; align-items: center; padding: 10px 5px;">
                <input type="checkbox" id="optInInscrevaSe" name="optIn" value="1" required>
                <p class="texto-checkbox" style="font-size: 10px; margin-left: 10px; font-family: Lato,sans-serif;">Declaro que li e aceito os termos do Edital e do Regulamento do Vestibular Social da Faculdade Sequencial.</p>
            </div>
            <input type="hidden" name="origem" value="Inscreva-se">
            <button>
                QUERO OBTER ATÉ 100% DE DESCONTO! &raquo;
            </button>
            <button class="secundario">
                QUERO MAIS INFORMAÇÕES SOBRE O VESTIBULAR SOCIAL &raquo;
            </button>
        </form>
        @endif
    </div>
</div>

@stop