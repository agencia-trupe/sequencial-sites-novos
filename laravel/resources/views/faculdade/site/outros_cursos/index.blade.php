@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-outros-cursos">

    <div class="centralizar">

      <p class="entrada">
        Além dos cursos de <strong>Graduação</strong> e <strong>Pós-Graduação</strong> a Faculdade Sequencial oferece também:
      </p>

      <div class="coluna extensao">
        <h1>EXTENSÃO UNIVERSITÁRIA</h1>
        <p>
          Para complementar a formação universitária a Faculdade Sequencial
          oferece cursos de Extensão, nas áreas de Educação e Saúde. Acompanhe
          aqui a divulgação de novos cursos e inscreva-se!
        </p>
        <ul class="lista-cursos">
          @forelse($cursos_extensao as $curso)
            <li>
              <a href="cursos/curso-de-extensao/{{$curso->slug}}" title="{{$curso->data->formatLocalized("%e %b %Y").' - '.$curso->titulo}}"><span>{{strtoupper($curso->data->formatLocalized("%e %b %Y"))}}</span>{{$curso->titulo}}</a>
            </li>
          @empty
            <li class="nenhum-curso">
              NO MOMENTO NÃO HÁ CURSOS AGENDADOS.<br>
              CONSULTE EM BREVE A PROGRAMAÇÃO ATUALIZADA.
            </li>
          @endforelse
        </ul>
      </div>

      <div class="coluna livres">
        <h1>CURSOS LIVRES</h1>
        <p>
          A Faculdade Sequencial promove com regularidade cursos livres com
          temas diversos. Alguns cursos são restritos a alunos e alguns são
          oferecidos aos alunos e à comunidade. Veja abaixo os cursos que serão
          realizados em breve e prepara-se:
        </p>
        <ul class="lista-cursos">
          @forelse($cursos_livres as $curso)
            <li>
              <a href="cursos/curso-livre/{{$curso->slug}}" title="{{$curso->data->formatLocalized("%e %b %Y").' - '.$curso->titulo}}"><span>{{strtoupper($curso->data->formatLocalized("%e %b %Y"))}}</span>{{$curso->titulo}}</a>
            </li>
          @empty
            <li class="nenhum-curso">
              NO MOMENTO NÃO HÁ CURSOS AGENDADOS.<br>
              CONSULTE EM BREVE A PROGRAMAÇÃO ATUALIZADA.
            </li>
          @endforelse
        </ul>
      </div>

    </div>

    <div class="faixa-azul">
      <div class="centralizar">

        <h1>
          SE VOCÊ AINDA NÃO CONHECE, CONFIRA OS CURSOS DE GRADUAÇÃO E
          PÓS-GRADUAÇÃO DA FACULDADE SEQUENCIAL:
        </h1>

        <div class="links">
          <a href="{{URL::route('faculdade.cursos-de-graduacao.index')}}" title="Cursos de Graduação">Cursos de Graduação</a>
          <a href="{{URL::route('faculdade.cursos-de-pos-graduacao.index')}}" title="Cursos de Pós-Graduação">Cursos de Pós-Graduação</a>
        </div>

      </div>
    </div>

  </div>

@stop
