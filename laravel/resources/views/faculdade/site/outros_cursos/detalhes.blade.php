@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-outros-cursos">

    <div class="centralizar">

      <aside class="{{$slug_area}}">

        <h1>{{$titulo_pagina}}</h1>

        <ul>
          @foreach($cursos as $c)
            <li>
              <a href="cursos/{{$rota}}/{{$c->slug}}" title="{{$c->data->formatLocalized("%e %b %Y").' - '.$c->titulo}}" @if($c->id == $curso->id) class="ativo" @endif>
                <span>{{strtoupper($c->data->formatLocalized("%e %b %Y"))}}</span>
                {{$c->titulo}}
              </a>
            </li>
          @endforeach
        </ul>

      </aside>

      <section class="{{$slug_area}}">

        <h2>{{strtoupper($c->data->formatLocalized("%e %b %Y"))}}</h2>

        <h1>{{$curso->titulo}}</h1>

        @if($curso->imagem)
          <img class="imagem-curso" src="assets/faculdade/img/{{$slug_area}}/{{$curso->imagem}}" alt="{{$curso->titulo}}" />
        @endif

        <div class="texto cke">
          {!! $curso->texto !!}
        </div>

        <a href="download/{{$regimento->regimento}}" class="link-regimento" target="_blank" title="Regimento da Faculdade Sequencial (ver documento)">&raquo; Regimento da Faculdade Sequencial (ver documento)</a>

      </section>

    </div>

    <div class="faixa-azul">
      <div class="centralizar">

        <h1>
          CONFIRA MAIS CURSOS DA FACULDADE SEQUENCIAL:
        </h1>

        <div class="links reduzido">
          <a href="{{URL::route('faculdade.cursos.index')}}" title="EXTENSÃO UNIVERSITÁRIA">EXTENSÃO UNIVERSITÁRIA</a>
          <a href="{{URL::route('faculdade.cursos.index')}}" title="Cursos Livres">Cursos Livres</a>
          <a href="{{URL::route('faculdade.cursos-de-graduacao.index')}}" title="Cursos de Graduação">Cursos de Graduação</a>
          <a href="{{URL::route('faculdade.cursos-de-pos-graduacao.index')}}" title="Cursos de Pós-Graduação">Cursos de Pós-Graduação</a>
        </div>

      </div>
    </div>

  </div>

@stop
