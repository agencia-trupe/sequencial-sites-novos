@extends('faculdade.site.teste-vocacional.template')

@section('conteudo')

    <form action="{{ route('faculdade.teste-vocacional.etapasPost') }}" method="POST" class="teste-vocacional-etapa">
        {!! csrf_field() !!}

        <h2>{{ $etapa['ordem'] }}. {{ $etapa['questao'] }}</h2>

        @if($errors->any())
        <ul class="erros">
            <li>Não use notas repetidas na mesma etapa.</li>
        </ul>
        @endif

        <input type="hidden" name="etapa" value="{{ $etapa['ordem'] }}">

        @foreach($etapa['opcoes'] as $key => $opcao)
        <div class="questao">
            <p @if($errors->has($key)) class="erro" @endif>{{ $key }} - {{ $opcao }}</p>
            @foreach(range(1, 7) as $i)
                <label>
                    <input type="radio" name="{{ $key }}" value="{{ $i }}" @if(old($key) && old($key) == $i) checked @endif required>
                    <span>{{ $i }}</span>
                </label>
            @endforeach
        </div>
        @endforeach

        <input type="submit" class="teste-vocacional-botao" value="PROSSEGUIR &raquo;">
    </form>

@endsection
