@extends('faculdade.site.teste-vocacional.template')

@section('conteudo')

    <div class="teste-vocacional-instrucoes">
        <h3>INSTRUÇÕES PARA A REALIZAÇÃO DO TESTE</h3>
        <p>
            Preparado com base na teoria das inteligências múltiplas, criada pelo psicólogo e educador Howard Gardner.
        </p>
        <p>
            São <strong>10 etapas</strong> onde você deve numerar os itens de acordo com o nível de identificação com a resposta, sendo 7 a maior concordância e 1 a menor.<br><strong>Não use notas repetidas entre as respostas da mesma etapa.</strong>
        </p>
        <p class="small">
            Teste extraído do livro: BELLAN, Zezina. Heutagogia – Aprender a Aprender Mais e Melhor. Santa Bárbara do Oeste: SOCEP, 2009.
        </p>

        <a href="{{ route('faculdade.teste-vocacional.etapas') }}" class="teste-vocacional-botao">PROSSEGUIR &raquo;</a>
    </div>

@endsection
