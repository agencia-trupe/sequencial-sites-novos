@extends('faculdade.site.teste-vocacional.template')

@section('conteudo')

    <form action="{{ route('faculdade.teste-vocacional.cadastro') }}" method="POST" class="teste-vocacional-index">
        {!! csrf_field() !!}

        <p>Responda as questões e descubra qual ou quais são os tipos de inteligencias que são predominantes em você e, por consequência, quais as profissões que poderão ser mais alinhadas com a sua facilidade de aprendizado.</p>
        <h3>Cadastre-se para realizar o teste:</h3>

        @if($errors->any())
        <ul class="erros">
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
        @endif

        <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}">
        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}">
        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
        <input type="submit" class="teste-vocacional-botao" value="FAZER O TESTE VOCACIONAL &raquo;">
    </form>

@endsection
