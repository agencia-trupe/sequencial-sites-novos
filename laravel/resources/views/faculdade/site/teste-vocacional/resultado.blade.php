@extends('faculdade.site.teste-vocacional.template')

@section('conteudo')

    <div class="teste-vocacional-resultado">
        @if(session('enviado'))
        <div class="enviado">E-mail enviado com sucesso.</div>
        @endif

        <h2>
            <small>Obrigado por responder ao teste.</small><br>
            Veja agora o seu resultado.
        </h2>

        <table>
        @foreach($resultado['pontuacao'] as $inteligencia)
            <tr>
                <td>{{ mb_strtoupper($inteligencia['titulo']) }}</td>
                <td>{{ $inteligencia['pontos'] }}</td>
            </tr>
        @endforeach
        </table>

        <div class="inteligencia">
            <p>
                Segundo o teste
                @if(count($resultado['inteligencias']) > 1)
                SUAS MAIORES INTELIGÊNCIAS são
                @else
                SUA MAIOR INTELIGÊNCIA é a
                @endif
            </p>

            @foreach($resultado['inteligencias'] as $inteligencia)
            <h3>{{ mb_strtoupper($inteligencia['titulo']) }}</h3>
            <p>{{ $inteligencia['descricao'] }}</p>
            @endforeach
        </div>

        <div class="ocupacoes">
            <p>Algumas ocupações recomendadas:</p>
            <ul>
                @foreach($resultado['ocupacoes'] as $ocupacao)
                <li>{{ $ocupacao }}</li>
                @endforeach
            </ul>
        </div>

        <p class="referencias">
            Teste extraído do livro: BELLAN,Zezina. Heutagogia – Aprender a Aprender Mais e Melhor. Santa Bárbara do Oeste: SOCEP, 2009.<br>
            Referências: www.businessballs.com - Wikipédia – Inteligências múltiplas (sobre Teoria das Inteligências Múltiplas, de Gardner)
        </p>

        @if(! session('testeVocacional')['enviado'])
        <a href="{{ route('faculdade.teste-vocacional.enviar') }}" class="teste-vocacional-botao">ENCAMINHAR RESULTADO PARA O MEU E-MAIL &raquo;</a>
        @endif
        <a href="{{ route('faculdade.cursos-de-graduacao.index') }}" class="teste-vocacional-botao teste-vocacional-botao-email">FINALIZAR E CONHECER OS CURSOS DA FACULDADE SEQUENCIAL &raquo;</a>
    </div>

@endsection
