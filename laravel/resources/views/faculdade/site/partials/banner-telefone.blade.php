<div id="banner-telefone">

  <span class="horario">
    Segunda a Sexta<br>
    das 7:30h às 21h
  </span>

  CENTRAL DE ATENDIMENTO SEQUENCIAL: 11 <strong>2362-6886</strong>

</div>

<div id="banner-whatsapp">
  <a href="https://api.whatsapp.com/send?phone=5511980640488" target="_blank">
    TIRE SUAS DÚVIDAS VIA WHATSAPP:
    11 <span>98064-0488</span>
  </a>
</div>
