<footer>

  <div class="centralizar-reduzido">

    <div class="coluna coluna-larga footer-info">
      <img src="assets/faculdade/img/layout/marca-faculdade-sequencial-rodape.png" alt="Faculdade Sequencial" />
      <p class="telefone">
        CENTRAL DE ATENDIMENTO SEQUENCIAL:
        <span>11 <strong>2362-6886</strong></span>
      </p>
      <p class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=5511980640488" target="_blank">
          TIRE SUAS DÚVIDAS VIA WHATSAPP:
          <span>11 <strong>98064-0488</strong></span>
        </a>
      </p>
      <a href="https://www.facebook.com/faculdade.sequencial" target="_blank" class="link-facebook">
        <img src="assets/faculdade/img/layout/ico-facebook-rodape.png" alt="Sequencial - Facebook" /> ACOMPANHE A FACULDADE SEQUENCIAL TAMBÉM NO FACEBOOK:
      </a>
    </div>

    <div class="coluna coluna-larga">
      <ul class="destaque">
        <li><a href="{{URL::route('faculdade.cursos-de-graduacao.index')}}" title="CURSOS DE GRADUAÇÃO">&raquo; CURSOS DE GRADUAÇÃO</a></li>
        <li><a href="{{URL::route('faculdade.cursos-de-pos-graduacao.index')}}" title="CURSOS DE PÓS-GRADUAÇÃO">&raquo; CURSOS DE PÓS-GRADUAÇÃO</a></li>
        <li><a href="{{URL::route('faculdade.vestibular-social.index')}}" title="VESTIBULAR SOCIAL">&raquo; VESTIBULAR SOCIAL</a></li>
        <li><a href="{{ route('faculdade.inscreva-se.index') }}" title="INSCREVA-SE">&raquo; INSCREVA-SE</a></li>
      </ul>
    </div>

    <div class="coluna">
      <ul>
        <li><a href="{{URL::route('faculdade.index')}}" title="Home">&raquo; Home</a></li>
        <li><a href="{{URL::route('faculdade.apresentacao')}}" title="A Faculdade">&raquo; A Faculdade</a></li>
        <li><a href="{{URL::route('faculdade.cursos.index')}}" title="Cursos">&raquo; Cursos</a></li>
        <li><a href="{{URL::route('faculdade.novidades.index')}}" title="Novidades">&raquo; Novidades</a></li>
        <li><a href="{{URL::route('faculdade.eventos.index')}}" title="Eventos">&raquo; Eventos</a></li>
        <li><a href="{{URL::route('faculdade.estagios.index')}}" title="Estágios">&raquo; Estágios</a></li>
        <li><a href="{{URL::route('faculdade.depoimentos.index')}}" title="Depoimentos">&raquo; Depoimentos</a></li>
        <li><a href="{{URL::route('faculdade.fale-conosco.index')}}" title="Fale Conosco">&raquo; Fale Conosco</a></li>
        <li><a href="{{URL::route('faculdade.ouvidoria.index')}}" title="Ouvidoria">&raquo; Ouvidoria</a></li>
      </ul>
    </div>

    <div class="coluna">
      <ul>
        <li><a href="https://faculdadesequencial.perseus.com.br/servicos/Autenticacao/Login?ReturnUrl=%2Fservicos%2Fportaleducacional%2F" target="_blank" title="PORTAL DO ALUNO">&raquo; PORTAL DO ALUNO</a></li>
        <li><a href="http://sequencial.bnweb.org/scripts/bnportal/bnportal.exe/index" target="_blank" title="BIBLIOTECA ONLINE">&raquo; BIBLIOTECA ONLINE</a></li>
        <li class="link-busca"><a href="#" title="BUSCA"><img src="assets/faculdade/img/layout/ico-busca.png" alt="Busca" /> &raquo; BUSCA</a></li>
        <li style="margin-top:25px"><a href="{{URL::route('faculdade.termos-de-uso.index')}}">&raquo; TERMOS DE USO</a></li>
      </ul>
    </div>

  </div>

  <div class="assinatura">
    &copy; {{Date('Y')}} Faculdade Sequencial - Todos os direitos reservados | <a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Criação de sites: Trupe Agência Criativa</a>
  </div>

</footer>
