<header>

  <div class="links-topo">
    <div class="centralizar">
      <div class="link-escola">
        <a href="http://www.escolasequencial.com.br" title="Conheça também a ESCOLA TÉCNICA SEQUENCIAL">Conheça também a <strong>ESCOLA TÉCNICA SEQUENCIAL</strong></a>
      </div>
      <div class="link-inscreva-se-mobile">
				<a href="{{ route('faculdade.inscreva-se.index') }}" target="_blank" title="Inscreva-se">
					INSCREVA-SE
				</a>
      </div>
      <style>
        .link-inscreva-se-mobile {
          display: none;
        }

        .link-inscreva-se-mobile a {
          display: block;
          border: 1px solid #03a6cd;
          border-top: none;
          background-color: #0078B0;
          padding: 17px 20px;
          line-height: 1;
          text-align: center;
          font-family: 'Asap', sans-serif;
          font-size: 16px;
          font-weight: bold;
          color: #CBFF66;
        }

        @media (max-width: 750px) {
          .link-escola {
            display: none !important;
          }

          .link-inscreva-se-mobile {
            display: block;
          }
        }
      </style>
      <div class="info-telefone item-topo">
        CENTRAL DE ATENDIMENTO SEQUENCIAL: 11 <strong>2362-6886</strong>
      </div>
      <div class="link-portal-aluno item-topo">
        <a href="https://faculdadesequencial.perseus.com.br/servicos/Autenticacao/Login?ReturnUrl=%2Fservicos%2Fportaleducacional%2F" title="PORTAL DO ALUNO"><img src="assets/faculdade/img/layout/ico-portalaluno.png" alt="PORTAL DO ALUNO" /> PORTAL DO ALUNO</a>
      </div>
      <div class="link-biblioteca-online item-topo">
        <a href="http://sequencial.bnweb.org/scripts/bnportal/bnportal.exe/index" title="BIBLIOTECA ONLINE"><img src="assets/faculdade/img/layout/ico-biblioteca.png" alt="BIBLIOTECA ONLINE" /> BIBLIOTECA ONLINE</a>
      </div>
      <div class="link-facebook item-topo">
        <a href="https://www.facebook.com/faculdade.sequencial" target="_blank" title="Facebook"><img src="assets/faculdade/img/layout/ico-facebook.png" alt="Facebook" /></a>
      </div>
      <div class="link-busca item-topo">
        <a href="#" title="Busca"><img src="assets/faculdade/img/layout/ico-busca.png" alt="Busca" /></a>
      </div>
    </div>
  </div>

  <nav>
    <div class="centralizar">

      <a href="#" id="menu-toggle" title="Abrir Menu">MENU</a>

      <a href="{{URL::route('faculdade.index')}}" title="Página Inicial" id="link-home"><img src="assets/faculdade/img/layout/marca-faculdade-sequencial.png" alt="Faculdade Sequencial" /></a>

      <ul class="menu-principal">
        <li><a href="{{URL::route('faculdade.cursos-de-graduacao.index')}}" title="CURSOS DE GRADUAÇÃO" @if(str_is('faculdade.cursos-de-graduacao*', Route::currentRouteName())) class="ativo" @endif >CURSOS DE GRADUAÇÃO</a></li>
        <li><a href="{{URL::route('faculdade.cursos-de-pos-graduacao.index')}}" title="CURSOS DE PÓS-GRADUAÇÃO" @if(str_is('faculdade.cursos-de-pos-graduacao*', Route::currentRouteName())) class="ativo" @endif >CURSOS DE <br>PÓS-GRADUAÇÃO</a></li>
        <li><a href="{{URL::route('faculdade.vestibular-social.index')}}" title="VESTIBULAR SOCIAL" @if(str_is('faculdade.vestibular-social*', Route::currentRouteName())) class="ativo" @endif >VESTIBULAR SOCIAL</a></li>
        <li class="link-inscreva-se"><a href="{{ route('faculdade.inscreva-se.index') }}" title="INSCREVA-SE">INSCREVA-SE</a></li>
      </ul>

      <div class="menus-secundarios">
        <ul>
          <li><a href="{{URL::route('faculdade.index')}}" title="Home" @if(str_is('faculdade.index*', Route::currentRouteName())) class="ativo" @endif >Home</a></li>
          <li><a href="{{URL::route('faculdade.apresentacao')}}" title="A Faculdade" @if(preg_match('~faculdade.(apresentacao|localizacao|infraestrutura)~', Route::currentRouteName())) class="ativo" @endif >A Faculdade</a></li>
          <li><a href="{{URL::route('faculdade.cursos.index')}}" title="Cursos" @if(str_is('faculdade.cursos.*', Route::currentRouteName())) class="ativo" @endif >Cursos</a></li>
          <li><a href="{{URL::route('faculdade.novidades.index')}}" title="Novidades" @if(str_is('faculdade.novidades.*', Route::currentRouteName())) class="ativo" @endif >Novidades</a></li>
        </ul>
        <ul>
          <li><a href="{{URL::route('faculdade.eventos.index')}}" title="Eventos" @if(str_is('faculdade.eventos*', Route::currentRouteName())) class="ativo" @endif >Eventos</a></li>
          <li><a href="{{URL::route('faculdade.estagios.index')}}" title="Estágios" @if(str_is('faculdade.estagios*', Route::currentRouteName())) class="ativo" @endif >Estágios</a></li>
          <li><a href="{{URL::route('faculdade.depoimentos.index')}}" title="Depoimentos" @if(str_is('faculdade.depoimentos*', Route::currentRouteName())) class="ativo" @endif >Depoimentos</a></li>
          <li><a href="{{URL::route('faculdade.fale-conosco.index')}}" title="Fale Conosco" @if(str_is('faculdade.fale-conosco*', Route::currentRouteName())) class="ativo" @endif >Fale Conosco</a></li>
        </ul>
      </div>

    </div>
  </nav>

</header>
