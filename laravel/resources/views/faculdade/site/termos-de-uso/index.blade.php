@extends('faculdade.site.template')

@section('conteudo')

  <style>
    .conteudo-termos-de-uso {
      padding-bottom: 50px;
      padding-top: 20px;
    }
    .conteudo-termos-de-uso h1 {
      font-family: Lato,sans-serif;
      font-size: 36px;
      font-weight: 300;
      text-transform: uppercase;
      color: #06549c;
      margin-bottom: 35px;
    }
    .termos-de-uso-texto {
      max-width: 700px;
      margin: 0 auto;
      font-family: Lato,sans-serif;
      line-height: 1.5;
      font-size: 16px;
    }
    .termos-de-uso-texto p {
      margin: 1em 0;
      font-weight: normal !important;
    }
    .termos-de-uso-texto h3 {
      font-weight: bold !important;
      margin: 2em 0 1em;
    }
  </style>

  <div class="conteudo conteudo-termos-de-uso">
    <div class="centralizar">
      <h1 class="main-title">TERMOS DE USO</h1>

      <div class="termos-de-uso-texto">
        <h3>
          Política de Privacidade
        </h3>

        <p>
          Ao cadastrar-se nos sites do Grupo Educacional Sequencial, são solicitados dados pessoais como: Nome, RG, CPF, endereço, telefone e e-mail. As inscrições de cursos e solicitações de serviços podem ser feitas pelos web sites:   www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br, por opção do usuário, que demonstram a concordância com nossa política de privacidade.
        </p>

        <p>
          O Instituto Educacional Sequencial manterá sigilo sobre as informações coletadas e não irá repassá-las a terceiros sem a autorização expressa do usuário, exceto por determinação judicial ou de órgãos públicos competentes.
        </p>

        <h3>
          Termos e Condições de Uso
        </h3>

        <p>
          Os web sites www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br, mantidos pelo Instituto Educacional Sequencial são destinados à informação dos cursos, serviços e a inscrição dos usuários interessados, com base nos presentes Termos e Condições de Uso.
        </p>

        <p>
          1. OBJETO E VINCULAÇÃO
        </p>

        <p>
          O objeto do presente Termo é regular a utilização dos web sites  www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br, estabelecendo os direitos e as obrigações dos usuários e gerando a vinculação necessária com o Grupo Educacional Sequencial.
        </p>

        <p>
          Este documento contém informações importantes e indispensáveis à utilização dos web sites sendo, portanto, obrigatória a sua leitura e compreensão por parte do Usuário.
        </p>

        <p>
          A aceitação formal deste Termo se dará no ato da utilização dos serviços dos web sites. Deste modo, ao utilizar as funcionalidades disponibilizadas nos web sites www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br ou efetuar seu cadastramento para utilizar serviços de acesso restrito, o Usuário declara sua plena, integral e irrestrita concordância com as condições previstas neste Termo. Se o Usuário não concordar com os termos e condições descritos nesse documento, não deve utilizar os web sites www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br
        </p>

        <p>
          As disposições deste Termo podem ser atualizadas a qualquer tempo e a exclusivo critério do Grupo Educacional Sequencial, sendo de responsabilidade do Usuário verificá-lo a cada acesso.
        </p>

        <p>
          A aceitação a este Termo implicará, ainda, na aceitação da Política de Eventuais dúvidas de usuários podem ser resolvidas pela Central de Atendimento.
        </p>

        <p>
          2. REGRAS DE USO
        </p>

        <p>
          2.1 Cadastro no web site   www.sequencialmatriculas.com.br
        </p>

        <p>
          O cadastro no web site  www.sequencialmatriculas.com.br  é realizado por meio da Internet, no momento da inscrição, cadastro este feito por livre e espontânea vontade do Usuário, condicionado ao fornecimento de dados cadastrais, informados durante o preenchimento de campos específicos como: nome, endereço, CPF, e-mail e telefones para contato, em que o Usuário manifesta o consentimento livre, expresso e informado para a coleta, uso, armazenamento, tratamento e compartilhamento.
        </p>

        <p>
          O Usuário deverá ser civilmente capaz, com pelo menos 18 anos de idade, ou representado por seus pais, tutores ou representantes legais, que assumem a responsabilidade pelas inscrições e atos praticados por meio dos www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br
        </p>

        <p>
          O Usuário assume como verdadeiras as informações passadas e garante a autenticidade dos dados fornecidos ao cadastro, se responsabilizando civil e criminalmente por qualquer inexatidão que venha a causar prejuízos, se obrigando, ainda, a manter seus dados e informações constantemente atualizados, sempre que houver alterações.
        </p>

        <p>
          O Instituto Educacional Sequencial se reserva o direito de utilizar todos os meios válidos e possíveis para identificar o Usuário, bem como de solicitar dados adicionais e documentos para conferir os dados pessoais informados.
        </p>

        <p>
          O acesso a áreas restritas dos web sites www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br só é permitido a Usuários devidamente cadastrados, utilizando login e senha pessoal, que são pessoais e intransferíveis.
        </p>

        <p>
          2.2 Segurança de logins e senhas
        </p>

        <p>
          O Usuário assume neste ato integral responsabilidade pelo sigilo e confidencialidade de seu login e senha de acesso, e eventual uso indevido de seu cadastro por terceiros. Ao confirmar o cadastro nos web sites    www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br o Usuário se declara ciente de que será o único responsável pelas transações efetuadas com uso de seu login, cujo acesso somente será permitido após o fornecimento da senha de uso exclusivo, pessoal e intransferível.
        </p>

        <p>
          O usuário se declara ciente e de acordo que é responsável pelo computador ou dispositivo digital (tablet, smartphone ou outro) para acesso dos web sites    www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br e que deverá prover recursos de segurança suficientes para evitar acesso por terceiros, inclusive com renovação periódica de sua senha.
        </p>

        <p>
          O Instituto Educacional Sequencial se reserva o direito de bloquear o cadastro, suspender temporariamente ou cancelar definitivamente o cadastro do Usuário, sem prévia comunicação, sempre que entender que houve utilização indevida, ato ilícito ou descumprimento do presente Termo, sem prejuízo de outras medidas legais cabíveis.
        </p>

        <p>
          2.3 Comunicação
        </p>

        <p>
          O Usuário autoriza o Instituto Educacional Sequencial a utilizar como meio de comunicação, além das ferramentas dos próprios sites, outros canais, como, telefone, e-mail, SMS e correspondência postada, com base nos dados cadastrais informados pelo Usuário.
        </p>

        <p>
          O e-mail do Usuário é utilizado para confirmar inscrições nos cursos, repassar informações relevantes sobre início, frequência, cobrança e atividades educacionais e quando solicitado, para divulgação de outros serviços.
        </p>

        <p>
          2.4 Uso correto dos web sites
        </p>

        <p>
          Ao acessar e utilizar web sites www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br o Usuário expressamente assume que não poderá:
        </p>

        <p>
          a) Utilizar as informações, imagens, programas de cursos e demais dados para qualquer outro fim que não os usos legais previstos neste Termo e para fins de educação
        </p>

        <p>
          b) Inserir conteúdo adulto, pejorativo, ilícito ou de qualquer modo contrário à moral e aos bons costumes ou que viole direitos da empresa, outros alunos / Usuários ou de terceiros;
        </p>

        <p>
          c) Infringir qualquer norma ou lei aplicável;
        </p>

        <p>
          d) Inserir conteúdo que contenha vírus, cópias ocultas, aplicativos ou mensagens que possam causar dano, subtrair dados da empresa ou de terceiros, ou impedir o normal funcionamento do web site ou de equipamentos dos Usuários;
        </p>

        <p>
          Assim, o Usuário compromete-se a utilizar os web sites    www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br e todo o conteúdo disponibilizado, inclusive textos e imagens  dentro de princípios de civilidade e respeito e no cumprimento das normas legais brasileiras.
        </p>

        <p>
          No caso de descumprimento de algum dos itens previstos neste Termo, o Instituto Educacional Sequencial se reserva no direito de suspender, bloquear e/ou excluir o cadastro e/ou o acesso do Usuário sem necessidade de notificação prévia e sem prejuízo de comunicar as autoridades competentes, a seu exclusivo critério.
        </p>

        <p>
          2.5 Propriedade Intelectual e Industrial
        </p>

        <p>
          Todo o conteúdo, texto, imagens e informações dos web sites    www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br são de propriedade exclusiva do Instituto Educacional Sequencial, sendo vedada sua cópia, reprodução ou qualquer outro tipo de utilização, ficando os infratores sujeitos às sanções civis e criminais correspondentes.
        </p>

        <p>
          3. INSCRIÇÕES PARA OS CURSOS
        </p>

        <p>
          Após a realização do Cadastro, o Usuário poderá inscrever-se para os cursos e solicitar outros serviços por meio dos web sites www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br.
        </p>

        <p>
          As condições de pagamento serão apresentadas em cada curso e o Usuário poderá efetuar os pagamentos por meios de boletos bancários emitidos com os dados cadastrados.
        </p>

        <p>
          4. INÍCIO DOS CURSOS
        </p>

        <p>
          O Instituto Educacional Sequencial confirmará a data de início dos cursos pelo e-mail cadastrado pelo Usuário, que fica ciente da necessidade de quórum (número mínimo de inscrições), antes do qual, as turmas não serão iniciadas.
        </p>

        <p>
          O Usuário poderá solicitar o cancelamento de sua inscrição seguindo os procedimentos descritos no próprio web site.
        </p>

        <p>
          5. DIREITO DE ARREPENDIMENTO
        </p>

        <p>
          Nas situações aplicáveis, ao Usuário será facultado exercer o arrependimento/desistência da inscrição no curso antes de seu início, em até sete dias corridos a contar da data da inscrição pelo web site.
        </p>

        <p>
          6. DEVOLUÇÃO DE VALORES PAGOS
        </p>

        <p>
          Nas situações aplicáveis, o valor pago será devolvido ao Usuário, de acordo com as condições descritas na respectiva inscrição do curso, observadas as regras e prazos e eventuais custos administrativos ou tributos intercorrentes.
        </p>

        <p>
          7. EXCLUSÃO DE RESPONSABILIDADE
        </p>

        <p>
          O Instituto Educacional Sequencial não se responsabiliza por quaisquer danos, de qualquer natureza, que possam advir a terceiros, em decorrência de conduta do Usuário imprópria, ofensiva ou de qualquer forma, contrária à legislação vigente.
        </p>

        <p>
          O Usuário declara estar ciente que o acesso aos web sites depende de diversos alheios à empresa fornecedora, como a interação de servidores e serviços de telecomunicações de terceiros, a adequação dos equipamentos do Usuário, e outros, a estes não se limitando. Assim o Instituto Educacional Sequencial se isenta de qualquer responsabilidade por falhas de acesso aos web sites por motivos que fogem ao seu controle e diligência.
        </p>

        <p>
          Não há garantia ou compromisso de que o acesso e a navegação nos web sites estejam livres de interrupções, erros, vírus, quedas de conexão ou outras situações que impeçam ou dificultem o acesso ou inscrição nos cursos, não havendo responsabilidade do Instituto Educacional Sequencial pela qualidade de conexão ou pela segurança da rede usada pelo Usuário para acesso digital, bem como pela eventual utilização pelo Usuário, de aplicativos, plataformas ou tecnologias desatualizadas e/ou desautorizadas, não consideradas seguras.
        </p>

        <p>
          O Instituto Educacional Sequencial não se responsabiliza por atos de terceiros que busquem de alguma forma acessar dados cadastrais e informações disponibilizadas pelo Usuário.
        </p>

        <p>
          8.  VIGÊNCIA
        </p>

        <p>
          O presente Termo tem vigência por prazo indeterminado, enquanto o Usuário utilizar os web sites www.escolasequencial.com.br, www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br.
        </p>

        <p>
          O Instituto Educacional Sequencial poderá suspender ou interromper a qualquer momento e sem prévio aviso, o acesso aos web sites sem qualquer tipo de indenização aos usuários.
        </p>

        <p>
          A violação das condições descritas neste Termo poderá acarretar ao Usuário o cancelamento de seu cadastro e/ou de seu acesso aos web sites    www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br Independentemente de prévio aviso ou notificação, sem prejuízo do Usuário responder pelos eventuais danos causados.
        </p>

        <p>
          9. OUTRAS DISPOSIÇÕES
        </p>

        <p>
          Eventual tolerância relativamente a descumprimento de qualquer das obrigações ora assumidas, não será considerada novação ou renúncia a qualquer direito, constituindo mera liberalidade.
        </p>

        <p>
          Antes de se inscrever para algum curso, cumpre ao Usuário analisar o respectivo programa e as condições de apresentação.
        </p>

        <p>
          Em caso de qualquer cláusula do presente Termo ser declarada nula, as demais cláusulas permanecerão vigentes.
        </p>

        <p>
          Os cursos ministrados pelo Instituto Educacional Sequencial não são consideradas de risco, sendo inaplicável a responsabilidade objetiva disposta no artigo 927, parágrafo único, do Código Civil, o que o Usuário declara concordar, neste ato.
        </p>

        <p>
          Os web sites www.escolasequencial.com.br , www.sequencialmatriculas.com.br e www.faculdadesequencial.com.br são mantidos pela INSTITUTO EDUCACIONAL SEQUENCIAL, pessoa jurídica de direito privado, inscrita no CNPJ/MF sob o nº 07.043.459/0002-33, com sede na Avenida Marechal Tito, 6084, Itaim Paulista, CEP: 08115-000, São Paulo – SP, para onde deverão ser direcionadas quaisquer notificações, intimações ou citações, que somente serão válidas com a expedição de aviso de recebimento.
        </p>
      </div>
    </div>
  </div>

@stop
