@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-fale-conosco">
    <div class="centralizar">

      <h1>FALE CONOSCO</h1>

      @include('faculdade.site.partials.banner-telefone')

      <div class="coluna">

        <div class="texto-endereco">
          {!! nl2br($texto->endereco) !!}
          <span>{!! $texto->endereco_obs !!}</span>
        </div>

        <div class="embed">
          {!! $texto->google_maps !!}
        </div>

      </div>

      <div class="coluna">

        <h2>Envie uma mensagem:</h2>

        <form action="{{URL::route('faculdade.fale-conosco.enviar')}}" method="post">

          @if($errors->any())
            <div class="resposta-form erro">{{ $errors->first() }}</div>
          @endif

          @if(session('contato_enviado'))
            <div class="resposta-form sucesso">Sua mensagem foi enviada com Sucesso!</div>
          @endif

          {!! csrf_field() !!}

          <input type="text" name="nome" placeholder="nome" value="{{old('nome')}}" required>
          <input type="email" name="email" placeholder="e-mail" value="{{old('email')}}" required>
          <input type="text" name="telefone" placeholder="telefone" value="{{old('telefone')}}">
          <input type="text" name="cpf" placeholder="[se você é aluno - informe seu CPF para localizarmos seu cadastro]" value="{{old('cpf')}}">
          <textarea name="mensagem" placeholder="mensagem" required>{{old('mensagem')}}</textarea>
          <input type="submit" value="ENVIAR" class="btn-voltar">

        </form>
        <a href="https://bancodetalentos.gruposequencial.com.br/" target="_blank" class="link-ouvidoria" title="Trabalhe conosco" style="display: block;">&raquo; Trabalhe conosco</a>
        <a href="{{URL::route('faculdade.ouvidoria.index')}}" class="link-ouvidoria" title="Fale com a Ouvidoria" style="display: block; margin-top: 10px;">&raquo; Fale com a Ouvidoria</a>

      </div>

    </div>
  </div>

@stop
