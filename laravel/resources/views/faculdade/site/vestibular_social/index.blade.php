@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-vestibular-social">

    <div class="centralizar">

      <section @if(count($arquivos) == 0) class="full-width" @endif>

        <div class="banner">
          @if($texto->link_banner) <a href="{{$texto->link_banner}}" title="{{$texto->titulo}}"> @endif
            <img src="assets/faculdade/img/vestibular_social/{{$texto->imagem}}" alt="Vestibular Social Sequencial" />
          @if($texto->link_banner) </a> @endif
        </div>

        <h1>{{$texto->titulo}}</h1>

        <div class="texto cke">
          {!! $texto->texto !!}
        </div>

      </section>

      <aside @if(count($arquivos) == 0) class="hidden" @endif>

        <div class="frase-downloads">
          {!! $texto->frase_downloads !!}
        </div>

        <ul class="lista-downloads">
          @foreach($arquivos as $arquivo)
            <li>
              <a href="assets/faculdade/arquivos/{{$arquivo->arquivo}}" title="Download - {{$arquivo->titulo}}" target="_blank">
                <div class="icone">
                  <img src="assets/faculdade/img/layout/pdf-download.png" alt="Download" />
                </div>
                <div class="texto">
                  DOWNLOAD
                  <br>
                  {{$arquivo->titulo}}
                </div>
              </a>
            </li>
          @endforeach
        </ul>

        @if($texto->chamada_titulo)
          <a class="chamada-inscricao" href="{{$texto->chamada_link}}" title="{{$texto->chamada_titulo}}">
            <strong>{{$texto->chamada_titulo}}</strong>
            <p>
              {{$texto->chamada_texto}}
            </p>
            <span><img src="assets/faculdade/img/layout/ico-inscrever-se.png" alt="INSCREVER-ME" />INSCREVER-ME &raquo;</span>
          </a>
        @endif

      </aside>

    </div>

  </div>

@stop
