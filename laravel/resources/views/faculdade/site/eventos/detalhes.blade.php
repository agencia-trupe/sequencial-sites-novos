@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-eventos">

    <div class="centralizar">

      <h1>EVENTOS</h1>

      <aside>

        <h2>VEJA OUTROS EVENTOS:</h2>

        <ul>
          @foreach($eventos as $eve)
            <li>
              <a href="eventos/{{$eve->slug}}" title="{{$eve->titulo}}" @if($eve->id == $detalhe->id) class="ativo" @endif >
                <div class="data">{{$eve->data->formatLocalized("%e %b %Y")}}</div>
                <div class="titulo">{{$eve->titulo}}</div>
              </a>
            </li>
          @endforeach
        </ul>

        <a href="{{URL::route('faculdade.eventos.index')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>

      </aside>

      <section>

        @if($detalhe->imagem)
          <div class="imagem">
            <img src="assets/faculdade/img/eventos/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}" />
          </div>
        @endif

        <div class="titulo">
          <h2>{{$detalhe->data->formatLocalized("%e %b %Y")}}</h2>
          <h1>{{$detalhe->titulo}}</h1>
        </div>

        <h3>{{$detalhe->subtitulo}}</h3>

        <div class="texto cke">
          {!! $detalhe->texto !!}
        </div>

        @if(count($detalhe->imagens) > 0)
          <h4>VEJA MAIS IMAGENS DO EVENTO</h4>
          <ul class="lista-imagens">
          @foreach($detalhe->imagens as $img)
              <li>
                <a href="assets/faculdade/img/eventos/redimensionadas/{{$img->imagem}}" rel="galeria-evento" title="{{$detalhe->titulo}}"><img src="assets/faculdade/img/eventos/thumbs-quadradas/{{$img->imagem}}" /></a>
              </li>
          @endforeach
        </ul>
        @endif

        <a href="{{URL::route('faculdade.eventos.index')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>

      </section>


    </div>

  </div>

@stop
