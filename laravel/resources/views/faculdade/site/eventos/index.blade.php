@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-eventos">

    <div class="centralizar">

      <h1>EVENTOS</h1>

      <h2>PRÓXIMOS EVENTOS:</h2>

      <ul class="lista-proximos-eventos">
        @forelse($proximos as $evento)
          <li>
            <a href="eventos/{{$evento->slug}}" title="{{$evento->titulo}}">
              @if($evento->imagem)
                <div class="imagem">
                  <img src="assets/faculdade/img/eventos/{{$evento->imagem}}" alt="{{$evento->titulo}}" />
                </div>
              @endif
              <div class="texto">
                <div class="data">{{$evento->data->formatLocalized("DIA %e DE %B %Y")}}</div>
                <h2>{{$evento->titulo}}</h2>
              </div>
            </a>
          </li>
        @empty
          <div class="nenhum-evento">
            PRÓXIMOS EVENTOS: Aguarde atualização da programação para Eventos futuros aqui.
          </div>
        @endforelse
      </ul>

      <h2>HISTÓRICO DE EVENTOS:</h2>

      <ul class="lista-eventos-passados">
        @foreach($historico as $evento)
          <li>
            <a href="eventos/{{$evento->slug}}" title="{{$evento->titulo}}">
              <div class="texto">
                <div class="data">{{$evento->data->formatLocalized("%e %B %Y")}}</div>
                <h2>{{$evento->titulo}}</h2>
              </div>
            </a>
          </li>
        @endforeach
      </ul>

      {!! $historico->links() !!}

    </div>

  </div>

@stop
