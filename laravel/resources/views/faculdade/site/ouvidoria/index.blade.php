@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-ouvidoria">
    <div class="centralizar">

      <h1>OUVIDORIA</h1>

      @include('faculdade.site.partials.banner-telefone')

      <div class="coluna">

        <h2>{{$texto->titulo}}</h2>

        <div class="texto cke">{!! $texto->texto !!}</div>

      </div>

      <div class="coluna">

        <h2>Envie uma mensagem:</h2>

        <form action="{{URL::route('faculdade.ouvidoria.enviar')}}" method="post">

          @if($errors->any())
            <div class="resposta-form erro">{{ $errors->first() }}</div>
          @endif

          @if(session('contato_enviado'))
            <div class="resposta-form sucesso">Sua mensagem foi enviada com Sucesso!</div>
          @endif

          {!! csrf_field() !!}

          <input type="text" name="nome" placeholder="nome" value="{{old('nome')}}" required>
          <input type="email" name="email" placeholder="e-mail" value="{{old('email')}}" required>
          <input type="text" name="telefone" placeholder="telefone" value="{{old('telefone')}}">
          <input type="text" name="cpf" placeholder="CPF" value="{{old('cpf')}}" required>
          <textarea name="mensagem" placeholder="mensagem" required>{{old('mensagem')}}</textarea>
          <input type="submit" value="ENVIAR" class="btn-voltar">

        </form>

      </div>

    </div>
  </div>

@stop
