@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-estagios">

    <div class="centralizar">

      <h1>ESTÁGIOS</h1>

      <div class="coluna">
        <div class="texto-estagio texto-nube">
          @if($texto_nube->imagem)
            <div class="imagem"><img src="assets/faculdade/img/estagios/{{$texto_nube->imagem}}" alt="{{$texto_nube->titulo}}" /></div>
          @endif
          <div class="texto cke">{!! $texto_nube->texto !!}</div>

          <iframe src="http://www.nube.com.br/top_estagios/superior_sp" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="150" height="223" style="display:block; margin:30px auto;"></iframe>

        </div>
      </div>

      <div class="coluna">

        <div class="texto-estagio texto-ciee">
          @if($texto_ciee->imagem)
            <div class="imagem"><img src="assets/faculdade/img/estagios/{{$texto_ciee->imagem}}" alt="{{$texto_ciee->titulo}}" /></div>
          @endif
          <div class="texto cke">{!! $texto_ciee->texto !!}</div>
        </div>

        <div class="texto-estagio texto-fundap">
          @if($texto_fundap->imagem)
            <div class="imagem"><img src="assets/faculdade/img/estagios/{{$texto_fundap->imagem}}" alt="{{$texto_fundap->titulo}}" /></div>
          @endif
          <div class="texto cke">{!! $texto_fundap->texto !!}</div>
        </div>

      </div>

    </div>

  </div>

@stop
