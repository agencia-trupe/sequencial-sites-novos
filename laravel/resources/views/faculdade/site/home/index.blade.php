@extends('faculdade.site.template')

@section('conteudo')

<div class="conteudo">

  <div class="centralizar">

    <div id="banners">
      @if(count($banners))

        <div id="banners-pager"></div>

        <div class="cycle-slideshow"
             data-slides="> .banner"
             data-pause-on-hover="true"
             data-pager="#banners-pager"
             data-cycle-timeout=5000>
          @foreach($banners as $k => $banner)
            <div class="banner" @if($k > 0) style="display:none;" @endif>
              @if($banner->link)
                <a href="{{$banner->link}}" title="{{$banner->titulo}}">
                  <img src="assets/faculdade/img/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}" />
                </a>
              @else
                <img src="assets/faculdade/img/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}" />
              @endif
            </div>
          @endforeach
        </div>

      @endif
    </div>

    <div id="chamadas">

      @forelse($chamadas as $i => $chamada)

        <div class="chamada @if($i == 1) eventos @else novidades @endif">
          <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
            <span class="titulo">{{$chamada->tipo_extenso}}</span>
            @if($chamada->fundo_tipo == 'imagem')
              <img src="assets/faculdade/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
            @endif
            <div class="over">
              <h1>{{$chamada->titulo}}</h1>
              <h2>{{$chamada->subtitulo}}</h2>
            </div>
          </a>
        </div>

      @empty

        @foreach($calhaus as $i => $chamada)
          <div class="chamada @if($i == 1) eventos @else novidades @endif">
            <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
              <span class="titulo">{{$chamada->tipo_extenso}}</span>
              @if($chamada->fundo_tipo == 'imagem')
                <img src="assets/faculdade/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
              @endif
              <div class="over">
                <h1>{{$chamada->titulo}}</h1>
                <h2>{{$chamada->subtitulo}}</h2>
              </div>
            </a>
          </div>
        @endforeach

      @endforelse

      @if(count($chamadas) == 1)
        @foreach($calhaus as $i => $chamada)
          @if($i == 0)
            <div class="chamada @if($i == 1) eventos @else novidades @endif">
              <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
                <span class="titulo">{{$chamada->tipo_extenso}}</span>
                @if($chamada->fundo_tipo == 'imagem')
                  <img src="assets/faculdade/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
                @endif
                <div class="over">
                  <h1>{{$chamada->titulo}}</h1>
                  <h2>{{$chamada->subtitulo}}</h2>
                </div>
              </a>
            </div>
          @endif
        @endforeach
      @endif

    </div>

    @include('faculdade.site.partials.banner-telefone')

    <div id="chamada-cursos">
      <h1>NOSSOS CURSOS DE GRADUAÇÃO:</h1>
      <ul>
        @foreach($cursos as $curso)
          <li>
            <a href="cursos-de-graduacao/{{$curso->slug}}" title="{{$curso->titulo}}" data-cor-fundo="{{$curso->cor_curso}}" data-cor-original="#fff">
              <img src="assets/faculdade/img/cursos_graduacao/{{$curso->imagem}}" alt="{{$curso->titulo}}" />
              <span style="color:{{$curso->cor_curso}};">{{$curso->titulo}}</span>
            </a>
          </li>
        @endforeach
      </ul>
    </div>

  </div>

  <div id="faixa-azul">

    <div class="centralizar">

      <div id="chamada-depoimentos">
        @if($depoimento)

          <a href="depoimentos" title="Ver depoimento completo">

            @if($depoimento->imagem)
              <img src="assets/faculdade/img/depoimentos/{{$depoimento->imagem}}" alt="{{$depoimento->autor_nome}}" />
            @endif

            <h2>DEPOIMENTOS</h2>

            <p>
              {!! str_words(strip_tags($depoimento->texto), 35) !!}
            </p>

            <div class="autor">
              {{$depoimento->autor_nome}}
              <br>
              {{$depoimento->autor_descricao}}
            </div>

            <div class="ver-completo">VER DEPOIMENTO COMPLETO &raquo;</div>

          </a>

        @endif
      </div>

      <div id="embeds">

        <div class="fb-like-box" data-href="https://www.facebook.com/pages/Faculdade-Sequencial/127798883992592" data-width="340" data-height="280" data-show-faces="true" data-stream="false" data-header="false"></div>

        <iframe src="http://www.nube.com.br/top_estagios/superior_sp" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="150" height="223" style="margin:0 0 0 27px;"></iframe>

      </div>

    </div>

  </div>

</div>

<style>
  @media (min-width: 1200px) {
    .conteudo #chamada-cursos ul {
      text-align: center;
    }
    .conteudo #chamada-cursos ul li {
      width: 20%;
    }
    .conteudo #chamada-cursos ul li a span { font-size: 20px }
  }
</style>

@stop
