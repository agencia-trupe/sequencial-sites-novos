@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-novidades">

    <div class="centralizar">

      <h1>NOVIDADES</h1>

      <ul class="lista-novidades">
        @foreach($novidades as $novidade)
          <li>
            <a href="novidades/{{$novidade->slug}}" title="{{$novidade->titulo}}">
              @if($novidade->imagem)
                <div class="imagem">
                  <img src="assets/faculdade/img/novidades/thumbs/{{$novidade->imagem}}" alt="{{$novidade->titulo}}" />
                </div>
              @endif
              <div class="texto">
                <div class="data">{{$novidade->data->formatLocalized("%e %b %Y")}}</div>
                <h2>{{$novidade->titulo}}</h2>
              </div>
            </a>
          </li>
        @endforeach
      </ul>

      {!! $novidades->links() !!}

    </div>

  </div>

@stop
