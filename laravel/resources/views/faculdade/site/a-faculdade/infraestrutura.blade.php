@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-a-faculdade infraestrutura">

    <div class="centralizar">

      @include('faculdade.site.a-faculdade.menu')

      <section>

        <div class="coluna larga">

          <div class="card">
            <h1>{{ $texto->titulo }}</h1>
            <img src="assets/faculdade/img/infraestrutura/{{$texto->imagem}}" alt="Faculdade Sequencial - infraestrutura" />
            <div class="texto">
              {!! $texto->texto !!}
            </div>
          </div>

        </div>

        <div class="coluna">
          @if(count($albuns_infra))
            <h2>INFRAESTRUTURA:</h2>
            <ul class="lista-albuns">
              @foreach($albuns_infra as $album)
                <li>
                  @foreach($album->imagens as $k => $imagem)
                    <a href="assets/faculdade/img/infraestrutura/albuns/redimensionadas/{{$imagem->imagem}}" rel="galeria-{{$album->id}}" @if($k > 0) style="display:none;" @endif title="{{$album->titulo}}">&raquo; {{$album->titulo}}</a>
                  @endforeach
                </li>
              @endforeach
            </ul>
          @endif

          @if(count($albuns_labs))
            <h2>LABORATÓRIOS:</h2>
            <ul class="lista-albuns">
              @foreach($albuns_labs as $album)
                <li>
                  @foreach($album->imagens as $k => $imagem)
                    <a href="assets/faculdade/img/infraestrutura/albuns/redimensionadas/{{$imagem->imagem}}" rel="galeria-{{$album->id}}" @if($k > 0) style="display:none;" @endif title="{{$album->titulo}}">&raquo; {{$album->titulo}}</a>
                  @endforeach
                </li>
              @endforeach
            </ul>
          @endif

        </div>

        @if(count($imagens_sem_album))
          <ul class="lista-imagens-sem-album">
            @foreach($imagens_sem_album as $img)
              <li>
                <a href="assets/faculdade/img/infraestrutura/albuns/redimensionadas/{{$img->imagem}}" rel="galeria-sem-album" title="Faculdade Sequencial">
                  <img src="assets/faculdade/img/infraestrutura/albuns/thumbs-retangulares/{{$img->imagem}}" alt="Faculdade Sequencial - Infraestrutura" />
                </a>
              </li>
            @endforeach
          </ul>
        @endif


      </section>

    </div>

  </div>

@stop
