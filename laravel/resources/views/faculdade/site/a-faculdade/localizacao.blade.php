@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-a-faculdade localizacao">

    <div class="centralizar">

      @include('faculdade.site.a-faculdade.menu')

      <section>

        <div class="coluna larga">
          <div class="texto-endereco">
            <h2>ENDEREÇO:</h2>
            {!! nl2br($texto->endereco) !!}
            <span>{!! $texto->endereco_obs !!}</span>
          </div>
        </div>

        <div class="coluna">
          <div class="texto-horario">
            <h2>HORÁRIO DE ATENDIMENTO:</h2>
            {!! nl2br($texto->horario_atendimento) !!}
          </div>
        </div>

        @include('faculdade.site.partials.banner-telefone')

        <div class="embed">
          {!! $texto->google_maps !!}
        </div>


      </section>

    </div>

  </div>

@stop
