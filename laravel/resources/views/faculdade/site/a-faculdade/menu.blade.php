<aside>
  <h1>A FACULDADE</h1>
  <ul>
    <li>
      <a href="a-faculdade/apresentacao" title="Apresentação" @if($marcar == 'apresentacao') class="ativo" @endif >APRESENTAÇÃO</a>
    </li>
    <li>
      <a href="a-faculdade/localizacao" title="Localização" @if($marcar == 'localizacao') class="ativo" @endif >LOCALIZAÇÃO</a>
    </li>
    <li>
      <a href="a-faculdade/infraestrutura" title="Infraestrutura" @if($marcar == 'infraestrutura') class="ativo" @endif >INFRAESTRUTURA</a>
    </li>
  </ul>
</aside>
