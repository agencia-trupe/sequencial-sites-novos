@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-a-faculdade apresentacao">

    <div class="centralizar">

      @include('faculdade.site.a-faculdade.menu')

      <section>

        <div class="coluna larga">

          <div class="texto-apresentacao">
            <div class="cke">
              {!! $texto->texto !!}
            </div>

            <a href="assets/faculdade/arquivos/{{$texto->regimento}}" class="link-regimento" target="_blank" title="Regimento da Faculdade Sequencial (ver documento)">&raquo; Regimento da Faculdade Sequencial (ver documento)</a>

            <div class="chamada">
              <div class="video-container">
                <iframe src="https://www.youtube.com/embed/VPvPBRMvl1g" frameborder="0" allowfullscreen></iframe>
              </div>
              {!! $texto->chamada !!}
            </div>

          </div>

        </div>


        <div class="coluna lateral">

          <img src="assets/faculdade/img/apresentacao/{{$texto->imagem}}" alt="Faculdade Sequencial" />

          @if(count($arquivos))
            <ul class="lista-downloads">
              @foreach($arquivos as $arquivo)
                <li>
                  <a href="assets/faculdade/arquivos/{{$arquivo->arquivo}}" title="Download - {{$arquivo->titulo}}" target="_blank">
                    <img src="assets/faculdade/img/layout/pdf-download.png" alt="Download" />
                    <div class="texto">
                      <span>&raquo; DOWNLOAD DO DOCUMENTO</span>
                      {{$arquivo->titulo}}
                    </div>
                  </a>
                </li>
              @endforeach
            </ul>
          @endif

        </div>

        @include('faculdade.site.partials.banner-telefone')

      </section>

    </div>

  </div>

@stop
