@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-cursos-pos-graduacao detalhes">

    <div class="centralizar">

      <h1>CURSOS DE PÓS-GRADUAÇÃO DA FACULDADE SEQUENCIAL</h1>

      <h2 style="background-color:{{$area->cor}};">{{$cursoDetalhes->titulo}}</h2>

      <div class="detalhes-curso-pos">
        <div class="coluna">
          @foreach($cursoDetalhes->topicos as $indice => $topico)
            <h3 style="color:{{$area->cor}};">{{$topico->titulo}}</h3>
            <div class="texto cke">{!! $topico->texto !!}</div>

            @if(count($cursoDetalhes->topicos) > 1 && ($indice + 1) == ceil(count($cursoDetalhes->topicos)/2))
              </div>
              <div class="coluna">
            @endif

          @endforeach
        </div>
      </div>

      <a href="{{URL::route('faculdade.cursos-de-pos-graduacao.index')}}" class="btn-voltar">&laquo; voltar</a>

    </div>

  </div>

@stop
