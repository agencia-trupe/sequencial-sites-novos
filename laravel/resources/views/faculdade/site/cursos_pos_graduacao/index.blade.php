@extends('faculdade.site.template')

@section('conteudo')

  <div class="conteudo conteudo-cursos-pos-graduacao">

    <div class="centralizar">
      <p class="entrada">
        Para tornar sua formação ainda mais completa a Sequencial oferece cursos
        de Pós-Graduação e MBA nas áreas de Educação, Saúde, Gestão e Negócios.
        Clique no curso desejado para saber mais:
      </p>
    </div>

    <div class="faixa-azul">
      <div class="centralizar">

        <h1>CONHEÇA OS CURSOS DE PÓS GRADUAÇÃO DA FACULDADE SEQUENCIAL:</h1>

        @foreach($areas_pos as $area)
          <ul class="lista-areas-pos">
            @foreach($area->cursos as $curso)
              <li>
                <a href="cursos-de-pos-graduacao/{{$area->slug}}/{{$curso->slug}}"
                   title="{{$curso->titulo}}"
                   style="background-color:{{$area->cor}};"
                   data-cor="{{$area->cor}}">
                   {{$curso->titulo}}
                 </a>
               </li>
            @endforeach
          </ul>
        @endforeach

      </div>
    </div>

  </div>

@stop
