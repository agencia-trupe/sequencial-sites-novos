@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Pop-up</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.popups.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            @if($registro->imagem)
              Imagem atual<br>
              <img src="assets/faculdade/img/popups/{{$registro->imagem}}"><br>
            @endif
            <label for="inputImagem">Imagem</label>
            <input type="file" class="form-control" id="inputImagem" name="imagem">
          </div>

          <div class="form-group">
            <label>
              <input type="checkbox" name="mostrar_todas_paginas" value="1" @if($registro->mostrar_todas_paginas == 1) checked @endif>
              <span> Mostrar em todas as páginas internas também<span>
            </label>
          </div>

          <div class="form-group">
            <label for="inputDataInicio">Data de início da exibição</label>
            <input type="text" name="data_inicio" class="form-control datepicker" id="inputDataInicio" value="{{$registro->data_inicio->format('d/m/Y')}}" required>
          </div>

          <div class="form-group">
            <label for="inputDataFim">Data de término da exibição</label>
            <input type="text" name="data_fim" class="form-control datepicker" id="inputDataTermino" value="{{$registro->data_fim->format('d/m/Y')}}" required>
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.popups.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
