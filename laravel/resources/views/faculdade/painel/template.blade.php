<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  <meta name="robots" content="index, nofollow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>Faculdade Sequencial - Painel Administrativo</title>

	<base href="{{ base_url() }}/">
	<script>var BASE = "{{ base_url() }}"</script>

	<link rel="stylesheet" href="assets/faculdade/css/painel.css">

	<script src="assets/faculdade/js/jquery.js"></script>
	<script src='assets/faculdade/js/modernizr.js'></script>

</head>
	<body class="body-painel">

		@include('faculdade.painel.partials.menu')

		@yield('conteudo')

		<footer>
			<div class="container-fluid">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		<script src='assets/ckeditor/ckeditor.js'></script>
		<script src='assets/ckeditor/adapters/jquery.js'></script>
		<script src='assets/faculdade/js/painel.js?nocache={{ time() }}'></script>

	</body>
</html>
