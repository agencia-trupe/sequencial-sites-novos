@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Depoimentos</h2>

        <hr>

      	@include('faculdade.painel.partials.mensagens')

        <a href="{{ URL::route('painel.faculdade.depoimentos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Depoimento</a>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Texto</th>
              <th>Autor</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
                <td>
                  {{ $registro->autor_nome }} <br>
                  {{ $registro->autor_descricao }}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.faculdade.depoimentos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.faculdade.depoimentos.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
