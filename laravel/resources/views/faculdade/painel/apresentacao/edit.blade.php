@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Apresentação</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.apresentacao.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
					</div>

          <div class="form-group">
            <label for="inputChamada">Chamada</label>
            <textarea name="chamada" class="form-control textarea-simples" id="inputChamada">{{$registro->chamada}}</textarea>
          </div>

          <div class="form-group">
            @if($registro->imagem)
              Imagem atual<br>
              <img src="assets/faculdade/img/apresentacao/{{$registro->imagem}}"><br>
            @endif
            <label for="inputImagem">Imagem</label>
            <input type="file" class="form-control" id="inputImagem" name="imagem">
          </div>

          <div class="well">
            <div class="form-group">

              <label for="Regimento">Regimento</label>

              <hr>

              @if($registro->regimento)
                Regimento atual: <a href="assets/faculdade/arquivos/{{$registro->regimento}}" target="_blank" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-open-file"></span> {{$registro->regimento}}</a><br>
              @endif

              <hr>

              <label for="Regimento">Trocar Arquivo</label><br>

              <input type="file" class="form-control" id="Regimento" name="regimento">

            </div>
          </div>

          @include('faculdade.painel.partials.form-arquivos')

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.apresentacao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
