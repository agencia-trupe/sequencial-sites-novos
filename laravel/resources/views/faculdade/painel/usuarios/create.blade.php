@extends('faculdade.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">
    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">

		      	<h2>
		        	Adicionar Usuário do Painel Administrativo
		        </h2>

		        <hr>

		        @include('faculdade.painel.partials.mensagens')

				<form action="{{ URL::route('painel.faculdade.usuarios.store') }}" method="post">

					{!! csrf_field() !!}

			    	<div class="form-group">
						<label for="inputUsuario">Usuário</label>
						<input type="text" class="form-control" id="inputUsuario" name="login"  value="{{ old('login') }}" required>
					</div>

					<div class="form-group">
						<label for="inputEmail">E-mail</label>
						<input type="email" class="form-control" id="inputEmail" name="email" value="{{ old('email') }}">
					</div>

					<div class="form-group">
						<label for="inputSenha">Senha</label>
						<input type="password" class="form-control" id="inputSenha" name="password" required>
					</div>

					<div class="form-group">
						<label for="inputConfSenha">Digite novamente a Senha</label>
						<input type="password" class="form-control" id="inputConfSenha" name="password_confirm" required>
					</div>

					<hr>

					<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

					<a href="{{ URL::route('painel.faculdade.usuarios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

				</form>
			</div>
		</div>
    </div>

@endsection
