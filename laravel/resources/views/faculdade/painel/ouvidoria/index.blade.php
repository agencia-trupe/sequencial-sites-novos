@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Ouvidoria</h2>

        <hr>

				@include('faculdade.painel.partials.mensagens')

				<div class="btn-group">
          <a href="{{ URL::route('painel.faculdade.ouvidoria.mensagens.index') }}" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-envelope"></span> Ver Mensagens</a>
          <a href="{{ route('painel.faculdade.ouvidoria.mensagens.exportar') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar XLS</a>
        </div>

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Título</th>
              <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  {{$registro->titulo}}
                </td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.faculdade.ouvidoria.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
