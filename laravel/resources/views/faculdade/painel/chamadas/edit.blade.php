@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Chamada</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.chamadas.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTipo">Tipo</label>
            <select class="form-control" name="tipo" required>
              <option value="">Selecione um Tipo</option>
              <option value="eventos" @if($registro->tipo == 'eventos') selected @endif >EVENTOS</option>
              <option value="novidades" @if($registro->tipo == 'novidades') selected @endif >NOVIDADES</option>
              <option value="acompanhe" @if($registro->tipo == 'acompanhe') selected @endif>ACOMPANHE</option>
              <option value="saiba-mais" @if($registro->tipo == 'saiba-mais') selected @endif>SAIBA MAIS</option>
              <option value="especial" @if($registro->tipo == 'especial') selected @endif>ESPECIAL</option>
              <option value="fique-por-dentro" @if($registro->tipo == 'fique-por-dentro') selected @endif>FIQUE POR DENTRO</option>
              <option value="biblioteca" @if($registro->tipo == 'biblioteca') selected @endif>BIBLIOTECA</option>
              <option value="cursos" @if($registro->tipo == 'cursos') selected @endif>CURSOS</option>
              <option value="na-sequencial" @if($registro->tipo == 'na-sequencial') selected @endif>NA SEQUENCIAL</option>
            </select>
          </div>

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

			    <div class="form-group">
			      <label for="inputSubtitulo">Subtítulo</label>
			      <input type="text" name="subtitulo" class="form-control" id="inputSubtitulo" value="{{$registro->subtitulo}}">
			    </div>

          <div class="form-group">
            <label for="inputLink">Link</label>
            <input type="text" name="link" class="form-control" id="inputLink" value="{{$registro->link}}">
          </div>

          <div class="well">
            Tipo de fundo
            <hr>

            <label style="width:25%;">
              <input type="radio" name="fundo_tipo" value="imagem" @if($registro->fundo_tipo == 'imagem') checked @endif > Imagem
            </label>

            <label style="width:25%;">
              <input type="radio" name="fundo_tipo" value="cor" @if($registro->fundo_tipo == 'cor') checked @endif > Cor (Hexa)
            </label>

            <hr>

            <div class="form-group collapse @if($registro->fundo_tipo == 'imagem') in @endif" id="input-tipo_fundo_imagem">
              @if($registro->fundo_imagem)
                Imagem atual<br>
                <img src="assets/faculdade/img/chamadas/{{$registro->fundo_imagem}}"><br>
              @endif
              <label for="inputTipoFundoImagem">Imagem</label>
              <input type="file" class="form-control" id="inputTipoFundoImagem" name="fundo_imagem">
            </div>

            <div class="form-group collapse @if($registro->fundo_tipo == 'cor') in @endif" id="input-tipo_fundo_cor">
              <label for="inputCor">Cor</label>
              <input type="text" name="fundo_cor" class="form-control" id="inputCor" value="{{$registro->fundo_cor}}" placeholder="Exemplo: #FF0000">
            </div>

          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.chamadas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
