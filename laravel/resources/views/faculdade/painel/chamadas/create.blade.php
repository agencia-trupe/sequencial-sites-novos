@extends('faculdade.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Chamada</h2>

	        <hr>

	        @include('faculdade.painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.faculdade.chamadas.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
              <label for="inputTipo">Tipo</label>
              <select class="form-control" name="tipo" required>
                <option value="">Selecione um Tipo</option>
                <option value="eventos" @if(old('tipo') == 'eventos') selected @endif >EVENTOS</option>
                <option value="novidades" @if(old('tipo') == 'novidades') selected @endif >NOVIDADES</option>
                <option value="acompanhe" @if(old('tipo') == 'acompanhe') selected @endif>ACOMPANHE</option>
                <option value="saiba-mais" @if(old('tipo') == 'saiba-mais') selected @endif>SAIBA MAIS</option>
                <option value="especial" @if(old('tipo') == 'especial') selected @endif>ESPECIAL</option>
                <option value="fique-por-dentro" @if(old('tipo') == 'fique-por-dentro') selected @endif>FIQUE POR DENTRO</option>
                <option value="biblioteca" @if(old('tipo') == 'biblioteca') selected @endif>BIBLIOTECA</option>
                <option value="cursos" @if(old('tipo') == 'cursos') selected @endif>CURSOS</option>
                <option value="na-sequencial" @if(old('tipo') == 'na-sequencial') selected @endif>NA SEQUENCIAL</option>
              </select>
            </div>

            <div class="form-group">
              <label for="inputTitulo">Título</label>
              <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
            </div>

            <div class="form-group">
              <label for="inputSubtítulo">Subtítulo</label>
              <input type="text" name="subtitulo" class="form-control" id="inputSubtítulo" value="{{old('subtitulo')}}">
            </div>

            <div class="form-group">
  						<label for="inputLink">Link</label>
  						<input type="text" name="link" class="form-control" id="inputLink" value="{{old('link')}}">
  					</div>

            <div class="well">
              Tipo de fundo
              <hr>

              <label style="width:25%;">
                <input type="radio" name="fundo_tipo" value="imagem" @if(old('fundo_tipo') == 'imagem') checked @endif > Imagem
              </label>

              <label style="width:25%;">
                <input type="radio" name="fundo_tipo" value="cor" @if(old('fundo_tipo') == 'cor') checked @endif > Cor (Hexa)
              </label>

              <hr>

              <div class="form-group collapse" id="input-tipo_fundo_imagem">
                @if(old('fundo_imagem'))
                  Imagem atual<br>
                  <img src="assets/faculdade/img/chamadas/{{old('fundo_imagem')}}"><br>
                @endif
    						<label for="inputTipoFundoImagem">Imagem</label>
                <input type="file" class="form-control" id="inputTipoFundoImagem" name="fundo_imagem">
    					</div>

              <div class="form-group collapse" id="input-tipo_fundo_cor">
    						<label for="inputCor">Cor</label>
    						<input type="text" name="fundo_cor" class="form-control" id="inputCor" value="{{old('fundo_cor')}}" placeholder="Exemplo: #FF0000">
    					</div>

            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.faculdade.chamadas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
