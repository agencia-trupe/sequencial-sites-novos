@extends('faculdade.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Curso</h2>

	        <hr>

	        @include('faculdade.painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.faculdade.cursos_pos_graduacao.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
              <label for="inputArea">Área do Curso</label>
              <select class="form-control" name="areas_pos_graduacao_id" required>
                <option value=""></option>
                @forelse($areas as $area)
                  <option value="{{$area->id}}" @if(old('areas_pos_graduacao_id') == $area->id) selected @endif >{{$area->titulo}}</option>
                @empty
                  <option value="">Sem cadastros</option>
                @endforelse
              </select>
            </div>

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

            <hr>


  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.faculdade.cursos_pos_graduacao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
