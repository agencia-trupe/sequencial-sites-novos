@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Curso</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.cursos_pos_graduacao.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputArea">Área do Curso</label>
            <select class="form-control" name="areas_pos_graduacao_id" required>
              <option value=""></option>
              @forelse($areas as $areas)
                <option value="{{$areas->id}}" @if($registro->areas_pos_graduacao_id == $areas->id) selected @endif >{{$areas->titulo}}</option>
              @empty
                <option value="">Sem cadastros</option>
              @endforelse
            </select>
          </div>

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.cursos_pos_graduacao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
