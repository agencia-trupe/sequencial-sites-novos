@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Curso de Pós-Graduação: {{$curso->titulo}}</h2>
        <h3>Tópicos</h3>

        <hr>

      	@include('faculdade.painel.partials.mensagens')

        <a href="{{URL::route('painel.faculdade.cursos_pos_graduacao.index')}}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> voltar</a>
        <a href="{{ URL::route('painel.faculdade.cursos_pos_graduacao.topicos.create', ['curso_id' => $curso->id]) }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Tópico</a>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="cursos_pos_graduacao_topicos">

          <thead>
          	<tr>
              <th>Ordenar</th>
          		<th>Título</th>
              <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">
                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                <td>
                  {{$registro->titulo}}
                </td>
            		<td>
                  {{ str_words(strip_tags($registro->texto), 15)  }}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.faculdade.cursos_pos_graduacao.topicos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.faculdade.cursos_pos_graduacao.topicos.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
