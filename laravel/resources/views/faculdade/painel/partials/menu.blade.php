<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
			    <span class="sr-only">Navegação</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" title="Página Inicial" href="{{ URL::route('faculdade.painel.dashboard') }}">Faculdade Sequencial</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('faculdade.painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('faculdade.painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->

				@if(Auth::guard('faculdade')->user()->acessoAdmin())
				<li class="dropdown @if(preg_match('~painel.faculdade.(chamadas|banners|popups*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.faculdade.banners*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/banners' title='Banners'>Banners</a></li>
						<li @if(str_is('painel.faculdade.chamadas*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/chamadas' title='Chamadas'>Chamadas</a></li>
						<li @if(str_is('painel.faculdade.popups*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/popups' title='Pop-ups'>Pop-ups</a></li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.faculdade.(apresentacao|localizacao|vestibular_social|estagios|depoimentos|infraestrutura|eventos|novidades|ouvidoria).*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Páginas <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.faculdade.apresentacao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/apresentacao' title='Apresentação'>Apresentação</a></li>
						<li @if(str_is('painel.faculdade.localizacao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/localizacao' title='Localização'>Localização</a></li>
						<li @if(str_is('painel.faculdade.infraestrutura*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/infraestrutura' title='Infraestrutura'>Infraestrutura</a></li>
						<li role="separator" class="divider"></li>
						<li @if(str_is('painel.faculdade.vestibular_social*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/vestibular_social' title='Vestibular Social'>Vestibular Social</a></li>
						<li @if(str_is('painel.faculdade.estagios*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/estagios' title='Estágios'>Estágios</a></li>
						<li @if(str_is('painel.faculdade.depoimentos*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/depoimentos' title='Depoimentos'>Depoimentos</a></li>
						<li @if(str_is('painel.faculdade.eventos*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/eventos' title='Eventos'>Eventos</a></li>
						<li @if(str_is('painel.faculdade.novidades*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/novidades' title='Novidades'>Novidades</a></li>
						<li @if(str_is('painel.faculdade.ouvidoria*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/ouvidoria' title='Ouvidoria'>Ouvidoria</a></li>
					</ul>
				</li>

				<li @if(str_is('painel.faculdade.cursos_graduacao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/cursos_graduacao' title='Cursos de Graduação'>Graduação</a></li>

				<li class="dropdown @if(preg_match('~painel.faculdade.(areas_pos_graduacao|cursos_pos_graduacao*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pós Graduação <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.faculdade.areas_pos_graduacao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/areas_pos_graduacao' title='Áreas da Pós Graduação'>Áreas</a></li>
						<li @if(str_is('painel.faculdade.cursos_pos_graduacao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/cursos_pos_graduacao' title='Cursos de Pós Graduação'>Cursos</a></li>
					</ul>
				</li>

				<li @if(str_is('painel.faculdade.cursos_livres*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/cursos_livres' title='Cursos Livres'>Cursos Livres</a></li>

				<li @if(str_is('painel.faculdade.cursos_extensao*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/cursos_extensao' title='Cursos de Extensão'>Cursos de Extensão</a></li>

				<li @if(str_is('painel.faculdade.inscricoes*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/inscricoes' title='Inscrições'>Inscrições</a></li>
				<li @if(str_is('painel.faculdade.leads*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/leads' title='Leads'>Leads</a></li>

				<li class="dropdown @if(preg_match('~painel.faculdade.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.faculdade.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.faculdade.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('faculdade.painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>
				@elseif(Auth::guard('faculdade')->user()->acessoInscricoes())
					<li @if(str_is('painel.faculdade.inscricoes*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/inscricoes' title='Inscrições'>Inscrições</a></li>
					<li @if(str_is('painel.faculdade.leads*', Route::currentRouteName())) class='active' @endif><a href='painel/faculdade/leads' title='Leads'>Leads</a></li>
					<li>
						<a href="{{URL::route('faculdade.painel.logout')}}" title="Logout">Logout</a>
					</li>
				@endif
			</ul>
		</div>

	</div>
</nav>
