@extends('faculdade.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Graduação - Cadastrar Curso</h2>

	        <hr>

	        @include('faculdade.painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.faculdade.cursos_graduacao.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

            <div class="form-group">
  			      <label for="inputPrefixo">Prefixo</label>
  			      <input type="text" name="prefixo" class="form-control" id="inputPrefixo" value="{{old('prefixo')}}">
  			    </div>

            <div class="form-group">
              <label for="inputLegislação">Legislação</label>
              <textarea name="legislacao" class="form-control textarea-simples" id="inputLegislação">{{old('legislacao')}}</textarea>
            </div>

            <div class="form-group">
              @if(old('imagem'))
                Imagem atual<br>
                <img src="assets/faculdade/img/cursos_graduacao/{{old('imagem')}}"><br>
              @endif
              <label for="inputImagem">Imagem</label>
              <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

            <div class="form-group">
              <label for="inputCor">Cor do Curso</label>
              <input type="text" name="cor_curso" class="form-control" id="inputCor" value="{{old('cor_curso')}}">
            </div>

            <hr>

            <div class="form-group">
              <label for="inputSobre">Sobre o Curso</label>
              <textarea name="sobre_o_curso" class="form-control" id="inputSobre">{{old('sobre_o_curso')}}</textarea>
            </div>

            <div class="well">
              <div class="form-group">
                <label for="ArquivoSobreOCurso">Arquivo Sobre o Curso</label>
                @if(isset($registro) && $registro->arquivo_sobre_o_curso)
                <hr>
                  Arquivo atual: <a href="assets/faculdade/arquivos/{{$registro->arquivo_sobre_o_curso}}" target="_blank" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-open-file"></span> {{$registro->arquivo_sobre_o_curso}}</a><br>
                <hr>
                <label for="ArquivoSobreOCurso">Trocar Arquivo</label><br>
                @endif
                <input type="file" class="form-control" id="ArquivoSobreOCurso" name="arquivo_sobre_o_curso">
              </div>
            </div>

            <div class="form-group">
              <label for="inputSobreMercado">Sobre o Mercado de Trabalho</label>
              <textarea name="sobre_o_mercado" class="form-control" id="inputSobreMercado">{{old('sobre_o_mercado')}}</textarea>
            </div>

            <div class="well">
              <div class="form-group">
                <label for="ArquivoSobreOMercado">Arquivo Sobre o Mercado de Trabalho</label>
                @if(isset($registro) && $registro->arquivo_sobre_o_mercado)
                <hr>
                  Arquivo atual: <a href="assets/faculdade/arquivos/{{$registro->arquivo_sobre_o_mercado}}" target="_blank" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-open-file"></span> {{$registro->arquivo_sobre_o_mercado}}</a><br>
                <hr>
                <label for="ArquivoSobreOMercado">Trocar Arquivo</label><br>
                @endif
                <input type="file" class="form-control" id="ArquivoSobreOMercado" name="arquivo_sobre_o_mercado">
              </div>
            </div>

            <div class="form-group">
  			      <label for="inputDuracao">Duração</label>
  			      <input type="text" name="duracao" class="form-control" id="inputDuracao" value="{{old('duracao')}}">
  			    </div>

            <div class="form-group">
              <label for="inputPeriodo">Período</label>
              <textarea name="periodo" class="form-control" id="inputPeriodo">{{old('periodo')}}</textarea>
            </div>

            <div class="form-group">
  			      <label for="inputInvestimento">Investimento</label>
  			      <input type="text" name="investimento" class="form-control" id="inputInvestimento" value="{{old('investimento')}}">
            </div>

            <div class="form-group">
              <label for="inputVideo">Vídeo (código do YouTube)</label>
              <input type="text" name="video" class="form-control" id="inputVideo" value="{{old('video')}}">
            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.faculdade.cursos_graduacao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
