@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Leads</h2>

        <hr>

        <a href="{{ route('painel.faculdade.leads.exportar') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar XLS</a>

        <hr>

        <form action="" method="GET">
          <input type="text" name="filtro" placeholder="Filtrar..." value="{{ Request::get('filtro') }}" class="form-control inline" style="width:210px">
          <input type="submit" value="ENVIAR" class="btn btn-info inline" style="width: auto">
        </form>

        <table class="table table-striped table-bordered table-hover">

          <thead>
            <tr>
              <th>Data</th>
              <th>E-mail</th>
              <th>Nome</th>
              <th>Telefone</th>
              <th>Curso</th>
              <th>Teste Vocacional</th>
              <th>Origem</th>
            </tr>
            </thead>

            <tbody>
            @foreach ($registros as $registro)

                <tr class="tr-row">
                <td>{{$registro->data}}</td>
                <td>{{$registro->email}}</td>
                <td>{{$registro->nome ?: '-'}}</td>
                <td>{{$registro->telefone ?: '-'}}</td>
                <td>{{$registro->curso ?: '-'}}</td>
                <td>
                  {!!$registro->data_teste!!}
                  @if($registro->teste_vocacional_resultado)
                  <small style="display:block;border-top: 1px solid #ddd;padding-top:10px;margin-top:10px;font-size:75%;">
                    {!!$registro->resultado_teste!!}
                  </small>
                  @endif
                </td>
                <td>{{$registro->origem ?: '-'}}</td>
            @endforeach
            </tbody>
        </table>

        {{ $registros->appends($_GET)->links() }}
      </div>
    </div>
  </div>

@endsection
