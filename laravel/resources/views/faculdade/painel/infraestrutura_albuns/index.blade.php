@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Infraestrutura - Álbuns</h2>

        <hr>

      	@include('faculdade.painel.partials.mensagens')

        <a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Álbum</a>

        <hr>

        <div class="btn-group">
          <a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.index', ['tipo' => 'infraestrutura']) }}" class="btn btn-sm btn-default @if($tipo == 'infraestrutura') btn-primary @endif">Álbuns de Infraestrutura</a>
          <a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.index', ['tipo' => 'laboratorios']) }}" class="btn btn-sm btn-default @if($tipo == 'laboratorios') btn-primary @endif">Álbuns de Laboratórios</a>
          <a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.index', ['tipo' => 'sem-album']) }}" class="btn btn-sm btn-default @if($tipo == 'sem-album') btn-primary @endif">Imagens sem Álbum</a>
        </div>

        <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="infraestrutura_albuns">

          <thead>
          	<tr>
              <th>Ordenar</th>
              <th>Título</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>

            @if($tipo == 'sem-album')
              <tr id="sem-album-row">
                <td></td>
                <td>Imagens sem álbum</td>
                <td><a href="{{ URL::route('painel.faculdade.infraestrutura_imgs_sem_album.edit') }}" class="btn btn-primary btn-sm">editar</a></td>
              </tr>
            @endif

          	@foreach ($registros as $registro)

            	<tr class="tr-row" id="row_{{ $registro->id }}">

                <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                <td>
                  {{$registro->titulo}}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                  <form action="{{ URL::route('painel.faculdade.infraestrutura_albuns.destroy', $registro->id) }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                  </form>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
