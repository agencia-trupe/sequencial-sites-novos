@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Álbum</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.infraestrutura_albuns.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

          <div class="form-group">
            <label for="inputTipo">Tipo</label>
            <select class="form-control" name="tipo" required required>
              <option value="infraestrutura" @if($registro->tipo == 'infraestrutura') selected @endif >Infraestrutura</option>
              <option value="laboratorios" @if($registro->tipo == 'laboratorios') selected @endif >Laboratórios</option>
            </select>
          </div>

			    @include('faculdade.painel.partials.upload-imagens', ['path' => 'infraestrutura/albuns'])

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
