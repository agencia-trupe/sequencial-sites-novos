@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Imagens sem Álbum</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.infraestrutura_imgs_sem_album.update') }}" method="post" enctype="multipart/form-data">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          @include('faculdade.painel.partials.upload-imagens', ['path' => 'infraestrutura/albuns'])

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.infraestrutura_albuns.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
