@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Localização</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.localizacao.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputEndereco">Endereço</label>
            <textarea name="endereco" class="form-control textarea-simples" id="inputEndereco">{{$registro->endereco}}</textarea>
          </div>

          <div class="form-group">
            <label for="inputObs">Endereço - observação</label>
            <input type="text" name="endereco_obs" class="form-control" id="inputObs" value="{{$registro->endereco_obs}}">
          </div>

          <div class="form-group">
            <label for="inputHorario">Horário de Atendimento</label>
            <textarea name="horario_atendimento" class="form-control textarea-simples" id="inputHorario">{{$registro->horario_atendimento}}</textarea>
          </div>

          <div class="form-group">
            <label for="inputGoogle">Código de incorporação do Google Maps</label>
            <input type="text" name="google_maps" class="form-control" id="inputGoogle" value="{{$registro->google_maps}}">
          </div>

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.localizacao.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
