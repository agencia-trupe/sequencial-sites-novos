@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Localização</h2>

        <hr>

      	@include('faculdade.painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

          <thead>
          	<tr>
          		<th>Endereço</th>
              <th>Horário de Atendimento</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
          	</tr>
        	</thead>

        	<tbody>
          	@foreach ($registros as $registro)

            	<tr class="tr-row">
                <td>
                  {!! nl2br($registro->endereco) !!}
                </td>
            		<td>
                  {!! nl2br($registro->horario_atendimento) !!}
                </td>
            		<td class="crud-actions">
              		<a href="{{ URL::route('painel.faculdade.localizacao.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
            		</td>
            	</tr>

          	@endforeach
        	</tbody>

      	</table>

      </div>
    </div>
  </div>

@endsection
