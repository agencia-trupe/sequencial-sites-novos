@extends('faculdade.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

	      	<h2>Cadastrar Novidade</h2>

	        <hr>

	        @include('faculdade.painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.faculdade.novidades.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

            <div class="form-group">
              <label for="inputSubtitulo">Subtítulo</label>
              <input type="text" name="subtitulo" class="form-control" id="inputSubtitulo" value="{{old('subtitulo')}}">
            </div>

            <div class="form-group">
              <label for="inputData">Data</label>
              <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{old('data')}}">
            </div>

  			    <div class="form-group">
  						<label for="inputTexto">Texto</label>
  						<textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
  					</div>

            <div class="form-group">
              @if(old('imagem'))
                Imagem atual<br>
                <img src="assets/faculdade/img/novidades/thumbs/{{old('imagem')}}"><br>
              @endif
              <label for="inputImagem">Imagem</label>
              <input type="file" class="form-control" id="inputImagem" name="imagem">
            </div>

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.faculdade.novidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
