@extends('faculdade.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Vestibular Social</h2>

		    <hr>

		    @include('faculdade.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.faculdade.vestibular_social.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            @if($registro->imagem)
              Imagem atual<br>
              <img src="assets/faculdade/img/vestibular_social/{{$registro->imagem}}"><br>
            @endif
            <label for="inputImagem">Imagem</label>
            <input type="file" class="form-control" id="inputImagem" name="imagem">
          </div>

          <div class="form-group">
            @if($registro->imagem_2)
              Imagem 2 (página Inscreva-se)<br>
              <img src="assets/faculdade/img/vestibular_social/{{$registro->imagem_2}}"><br>
            @endif
            <label for="inputImagem">Imagem 2 atual (página Inscreva-se)</label>
            <input type="file" class="form-control" id="inputImagem" name="imagem_2">
          </div>

          <div class="form-group">
            <label for="inputLinkBanner">Link do Banner (opcional)</label>
            <input type="text" name="link_banner" class="form-control" id="inputLinkBanner" value="{{$registro->link_banner}}">
          </div>

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

			    <div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
					</div>

          <div class="form-group">
            <label for="inputTextoDownloads">Texto para seção de Downloads</label>
            <textarea name="frase_downloads" class="form-control textarea-simples" id="inputTextoDownloads">{{$registro->frase_downloads}}</textarea>
          </div>

          <hr>

          <div class="well">

            <label>Chamada Lateral</label>

            <hr>

            <div class="form-group">
              <label for="inputTítulo">Título</label>
              <input type="text" name="chamada_titulo" class="form-control" id="inputTítulo" value="{{$registro->chamada_titulo}}">
            </div>

            <div class="form-group">
              <label for="inputTexto">Texto</label>
              <input type="text" name="chamada_texto" class="form-control" id="inputTexto" value="{{$registro->chamada_texto}}">
            </div>

            <div class="form-group">
              <label for="inputLink">Link</label>
              <input type="text" name="chamada_link" class="form-control" id="inputLink" value="{{$registro->chamada_link}}">
            </div>

          </div>

          @include('faculdade.painel.partials.form-arquivos')

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.faculdade.vestibular_social.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
