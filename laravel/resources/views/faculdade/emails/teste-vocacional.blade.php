<!DOCTYPE html>
<html>
<head>
    <title>[TESTE VOCACIONAL] Faculdade Sequencial</title>
    <meta charset="utf-8">
</head>
<body>
    <div style='color:#000;font-size:14px;font-family:Helvetica, Verdana, Arial, sans-serif;'>
        <img src="http://www.faculdadesequencial.com.br/assets/faculdade/img/layout/marca-faculdade-sequencial-email.png">

        <h2>
            <span style="font-size:.7em">Obrigado por responder ao teste.</span><br>
            Veja agora o seu resultado.
        </h2>

        <table style="width:100%;" border="1" cellspacing="0">
        @foreach($pontuacao as $inteligencia)
            <tr>
                <td>{{ $inteligencia['titulo'] }}</td>
                <td>{{ $inteligencia['pontos'] }}</td>
            </tr>
        @endforeach
        </table>

        <p>
            Segundo o teste
            @if(count($inteligencias) > 1)
            SUAS MAIORES INTELIGÊNCIAS são
            @else
            SUA MAIOR INTELIGÊNCIA é a
            @endif
        </p>
        @foreach($inteligencias as $inteligencia)
            <h2 style="margin-bottom: 8px;">{{ $inteligencia['titulo'] }}</h2>
            <p style="margin-top: 0;">{{ $inteligencia['descricao'] }}</p>
        @endforeach

        <hr>

        <p>Algumas ocupações recomendadas:</p>
        <ul>
            @foreach($ocupacoes as $ocupacao)
            <li>{{ $ocupacao }}</li>
            @endforeach
        </ul>

        <p>
            <span style="font-size:.6em">
                Teste extraído do livro: BELLAN,Zezina. Heutagogia – Aprender a Aprender Mais e Melhor. Santa Bárbara do Oeste: SOCEP, 2009.<br>
                Referências: www.businessballs.com - Wikipédia – Inteligências múltiplas (sobre Teoria das Inteligências Múltiplas, de Gardner)
            </span>
        </p>

        <hr>

        <h4>
            INSCREVA-SE AGORA NO VESTIBULAR SOCIAL DA FACULDADE SEQUENCIAL E DÊ UM IMPULSO NA SUA VOCAÇÃO PROFISSIONAL!<br>
            Conheça nossos cursos: <a href="www.faculdadesequencial.com.br"> www.faculdadesequencial.com.br</a>
        </h4>
    </div>
</body>
</html>
