<!DOCTYPE html>
<html>
<head>
    <title>O Mercado de Trabalho para o Profissional de {{ $curso }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='color:#000;font-size:14px;font-family:Verdana;'>
        Olá! Tudo bem?<br><br>

        Que bom que você tem interesse no curso de {{$curso}} da Faculdade Sequencial.<br><br>

        Preparamos um resumo sobre o mercado de trabalho para você se informar e tirar algumas dúvidas. Além disso, para qualquer outra dúvida que você tiver sobre o curso ou sobre a Faculdade você pode falar com a nossa Central de Atendimento: 11 2362-6886 – de segunda a sexta-feira das 7h às 21h.<br><br>

        Clique aqui para ler o documento que preparamos pra você: <a href="{{ $arquivo }}" style="font-weight:bold;text-decoration:underline">O MERCADO DE TRABALHO PARA O PROFISSIONAL DE {{mb_strtoupper($curso)}}</a><br><br>

        Esperamos que você goste.<br>
        <span style="font-style:italic">Faculdade Sequencial</span>
    </span>
</body>
</html>
