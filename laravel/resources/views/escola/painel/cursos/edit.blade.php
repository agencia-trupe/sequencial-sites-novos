@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

                <h2>Editar Curso</h2>

                <hr>

                @include('escola.painel.partials.mensagens')

            </div>
        </div>

        <form action="{{ URL::route('painel.escola.cursos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

            <input type="hidden" name="_method" value="PUT">

            {!! csrf_field() !!}

            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

                    <div class="form-group">
                        <label for="inputÁreadoCurso">Área do Curso</label>
                        <select class="form-control" name="areas_id" required>
                            <option value=""></option>
                            @forelse($areas as $area)
                                <option value="{{$area->id}}" @if($registro->areas_id == $area->id) selected @endif >{{$area->titulo}}</option>
                            @empty
                                <option value="">Sem cadastros</option>
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="inputPrefixo">Prefixo</label>
                        <input type="text" name="prefixo" class="form-control" id="inputPrefixo" value="{{$registro->prefixo}}">
                    </div>

                    <div class="form-group">
                        <label for="inputTitulo">Título</label>
                        <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
                    </div>

                    <div class="form-group">
                        <label for="inputTexto">Texto</label>
                        <textarea name="texto" class="form-control" id="inputTexto">{!! $registro->texto !!}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="inputCordoCurso">Cor do Curso</label>
                        <input type="text" name="cor_curso" class="form-control" id="inputCordoCurso" value="{{$registro->cor_curso}}">
                    </div>

                    <div class="form-group">
                        <label for="inputGradeCurricular">Grade Curricular</label>
                        @if($registro->grade_curricular_arquivo)
                        <p>
                            <a href="{{ asset('assets/escola/grade/'.$registro->grade_curricular_arquivo) }}" target="_blank">{{ $registro->grade_curricular_arquivo }}</a>
                        </p>
                        @endif
                        <input type="file" class="form-control" id="inputGradeCurricular" name="grade_curricular_arquivo">
                    </div>

                    <div class="form-group">
                        @if($registro->imagem)
                            Imagem atual<br>
                            <img src="assets/escola/img/cursos/{{$registro->imagem}}" class="img-thumbnail"><br>
                        @endif
                        <label for="inputImagem">Imagem</label>
                        <input type="file" class="form-control" id="inputImagem" name="imagem">
                    </div>

                    <div class="form-group">
                        <label for="inputVideo">Vídeo (código do YouTube)</label>
                        <input type="text" name="video" class="form-control" id="inputVideo" value="{{$registro->video}}">
                    </div>

                    <hr>

                </div>
            </div>

            <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

            <a href="{{ URL::route('painel.escola.cursos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </form>

    </div>

@endsection
