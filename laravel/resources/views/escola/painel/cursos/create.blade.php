@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

                <h2>Cadastrar Curso</h2>

                <hr>

                @include('escola.painel.partials.mensagens')

            </div>
        </div>

        <form action="{{ URL::route('painel.escola.cursos.store') }}" method="post" enctype="multipart/form-data">

            {!! csrf_field() !!}

            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

                    <div class="form-group">
                        <label for="inputÁreadoCurso">Área do Curso</label>
                        <select class="form-control" name="areas_id" required>
                            <option value=""></option>
                            @forelse($areas as $area)
                                <option value="{{$area->id}}" @if(old('areas_id') == $area->id) selected @endif >{{$area->titulo}}</option>
                            @empty
                                <option value="">Sem cadastros</option>
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="inputPrefixo">Prefixo</label>
                        <input type="text" name="prefixo" class="form-control" id="inputPrefixo" value="{{old('prefixo')}}">
                    </div>

                    <div class="form-group">
                        <label for="inputTitulo">Título</label>
                        <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
                    </div>

                    <div class="form-group">
                        <label for="inputTexto">Texto</label>
                        <textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="inputCordoCurso">Cor do Curso</label>
                        <input type="text" name="cor_curso" class="form-control" id="inputCordoCurso" value="{{old('cor_curso')}}">
                    </div>

                    <div class="form-group">
                        <label for="inputGradeCurricular">Grade Curricular</label>
                        <input type="file" class="form-control" id="inputGradeCurricular" name="grade_curricular_arquivo">
                    </div>

                    <div class="form-group">
                        @if(old('imagem'))
                            Imagem atual<br>
                            <img src="assets/escola/img/cursos/{{old('imagem')}}" class="img-thumbnail"><br>
                        @endif
                        <label for="inputImagem">Imagem</label>
                        <input type="file" class="form-control" id="inputImagem" name="imagem">
                    </div>

                    <div class="form-group">
                        <label for="inputVideo">Vídeo (código do YouTube)</label>
                        <input type="text" name="video" class="form-control" id="inputVideo" value="{{old('video')}}">
                    </div>

                    <hr>

                </div>
            </div>

            <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

            <a href="{{ URL::route('painel.escola.cursos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

        </form>

    </div>

@endsection
