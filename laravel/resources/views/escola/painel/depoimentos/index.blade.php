@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<h2>Depoimentos</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

				<a href="{{ URL::route('painel.escola.depoimentos.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Depoimento</a>

				<table class="table table-striped table-bordered table-hover table-sortable" data-tabela='depoimentos'>

					<thead>
						<tr>
							<th>Ordenar</th>
							<th>Autor</th>
							<th>Texto</th>
							<th><span class="glyphicon glyphicon-cog"></span></th>
						</tr>
					</thead>

					<tbody>
						@foreach ($registros as $registro)

							<tr class="tr-row" id="row_{{ $registro->id }}">
								<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
								<td>
									{{$registro->autor_nome}}
								</td>
								<td>
									{{ str_words(strip_tags($registro->texto), 15)  }}
								</td>
								<td class="crud-actions">
									<a href="{{ URL::route('painel.escola.depoimentos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

									<form action="{{ URL::route('painel.escola.depoimentos.destroy', $registro->id) }}" method="post">
										{!! csrf_field() !!}
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
									</form>
								</td>
							</tr>

						@endforeach
					</tbody>

				</table>

			</div>
		</div>
	</div>

@endsection
