@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

				<h2>Editar Depoimento</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

			</div>
		</div>

		<form action="{{ URL::route('painel.escola.depoimentos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

					<div class="form-group">
						@if($registro->imagem)
							Imagem atual<br>
							<img src="assets/escola/img/depoimentos/{{$registro->imagem}}" class="img-thumbnail"><br>
						@endif
						<label for="inputImagem">Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem">
					</div>

					<div class="form-group">
						<label for="inputAutor">Autor do Depoimento</label>
						<input type="text" name="autor_nome" class="form-control" id="inputAutor" value="{{$registro->autor_nome}}">
					</div>

					<div class="form-group">
						<label for="inputAutor">Sobre o Autor</label>
						<input type="text" name="autor_descricao" class="form-control" id="inputAutor" value="{{$registro->autor_descricao}}">
					</div>

					<div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control textarea-simples" id="inputTexto">{{$registro->texto}}</textarea>
					</div>

					<hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.escola.depoimentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

	</div>

@endsection
