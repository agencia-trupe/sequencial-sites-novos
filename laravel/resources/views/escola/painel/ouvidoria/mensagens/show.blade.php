@extends('escola.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Mensagens Ouvidoria</h2>

        <hr>

        <div class="form-group">
            <label>Data</label>
            <div class="well">{{ $registro->created_at->formatLocalized("%d/%m/%Y") }}</div>
        </div>

        <div class="form-group">
            <label>Unidade</label>
            <div class="well">{{ $registro->unidade }}</div>
        </div>

        <div class="form-group">
            <label>Tipo</label>
            <div class="well">{{ $registro->tipo }}</div>
        </div>

        <div class="form-group">
            <label>Nome</label>
            <div class="well">{{ $registro->nome }}</div>
        </div>

        <div class="form-group">
            <label>CPF</label>
            <div class="well">{{ $registro->cpf }}</div>
        </div>

        <div class="form-group">
            <label>E-mail</label>
            <div class="well">{{ $registro->email }}</div>
        </div>

        <div class="form-group">
            <label>Telefone</label>
            <div class="well">{{ $registro->telefone }}</div>
        </div>

        <div class="form-group">
            <label>Mensagem</label>
            <div class="well">{{ $registro->mensagem }}</div>
        </div>

        <a href="{{ route('painel.escola.ouvidoria.mensagens.index') }}" class="btn btn-default btn-voltar">Voltar</a>

      </div>
    </div>
  </div>

@endsection
