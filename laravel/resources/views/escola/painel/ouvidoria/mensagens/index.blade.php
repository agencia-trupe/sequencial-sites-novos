@extends('escola.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Mensagens Ouvidoria</h2>

        <hr>

        @include('escola.painel.partials.mensagens')

        <table class="table table-striped table-bordered table-hover">

          <thead>
            <tr>
              <th>Data</th>
              <th>Unidade</th>
              <th>Tipo</th>
              <th>Nome</th>
              <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
            </thead>

            <tbody>
            @foreach ($registros as $registro)

                <tr class="tr-row">
                <td>
                  {{$registro->created_at->formatLocalized("%d/%m/%Y")}}
                </td>
                <td>
                  {{$registro->unidade}}
                </td>
                <td>
                  {{$registro->tipo}}
                </td>
                <td>
                  {{$registro->nome}}
                </td>
                </td>
                    <td class="crud-actions">
                    <a href="{{ URL::route('painel.escola.ouvidoria.mensagens.show', $registro->id ) }}" class="btn btn-primary btn-sm">ver mensagem</a>
                    </td>
                </tr>

            @endforeach
            </tbody>

        </table>

      </div>
    </div>
  </div>

@endsection
