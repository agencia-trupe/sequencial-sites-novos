@extends('escola.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <h2>Pré-Inscrições</h2>

        <hr>

        @include('escola.painel.partials.mensagens')

        <a href="{{ route('painel.escola.pre-inscricoes.exportar') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar XLS</a>

        <hr>

        <form action="" method="GET">
          <input type="text" name="filtro" placeholder="Filtrar..." value="{{ Request::get('filtro') }}" class="form-control inline" style="width:210px">
          <input type="submit" value="ENVIAR" class="btn btn-info inline" style="width: auto">
        </form>

        <table class="table table-striped table-bordered table-hover">

          <thead>
            <tr>
              <th>Data</th>
              <th>Nome</th>
              <th>E-mail</th>
              <th>Telefone</th>
              <th>Origem</th>
            </tr>
            </thead>

            <tbody>
            @foreach ($registros as $registro)

                <tr class="tr-row">
                <td>{{$registro->data}}</td>
                <td>{{$registro->nome}}</td>
                <td>{{$registro->email}}</td>
                <td>{{$registro->telefone}}</td>
                <td>{{$registro->origem}}</td>
            @endforeach
            </tbody>
        </table>

        {{ $registros->appends($_GET)->links() }}
      </div>
    </div>
  </div>

@endsection
