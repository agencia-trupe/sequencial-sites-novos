@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<h2>Chamadas</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

				@if($pode_adicionar)
					<a href="{{ URL::route('painel.escola.chamadas.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Chamada</a>
				@endif

				<hr>

				<div class="btn-group">
					<a href="{{ URL::route('painel.escola.chamadas.index') }}" class="btn btn-sm btn-default @if($filtro != 'calhaus') btn-primary @endif">Cadastros Normais</a>
						<a href="{{ URL::route('painel.escola.chamadas.index', ['filtro' => 'calhaus']) }}" class="btn btn-sm btn-default @if($filtro == 'calhaus') btn-primary @endif">Calhaus</a>
						</div>

						<table class="table table-striped table-bordered table-hover table-sortable" data-tabela='chamadas'>

							<thead>
								<tr>
									@if($filtro != 'calhaus')
										<th>Ordenar</th>
									@endif
									<th>Título</th>
									<th>Subtítulo</th>
									<th><span class="glyphicon glyphicon-cog"></span></th>
								</tr>
							</thead>

							<tbody>
								@foreach ($registros as $registro)

									<tr class="tr-row" id="row_{{ $registro->id }}">
										@if($registro->is_calhau == 0)
											<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
										@endif
										<td>{{$registro->titulo}}</td>
										<td>{{$registro->subtitulo}}</td>
										<td class="crud-actions">
											<a href="{{ URL::route('painel.escola.chamadas.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

											@if($registro->is_calhau == 0)
												<form action="{{ URL::route('painel.escola.chamadas.destroy', $registro->id) }}" method="post">
													{!! csrf_field() !!}
													<input type="hidden" name="_method" value="DELETE">
													<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
												</form>
											@endif

										</td>
									</tr>

								@endforeach
							</tbody>

						</table>

					</div>
				</div>
			</div>

		@endsection
