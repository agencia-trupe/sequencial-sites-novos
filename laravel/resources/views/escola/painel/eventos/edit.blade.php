@extends('escola.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Evento</h2>

		    <hr>

		    @include('escola.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.escola.eventos.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

          <div class="form-group">
            <label for="inputSubtitulo">Subtítulo</label>
            <input type="text" name="subtitulo" class="form-control" id="inputSubtitulo" value="{{$registro->subtitulo}}">
          </div>

          <div class="form-group">
            <label for="inputData">Data</label>
            <input type="text" name="data" class="form-control datepicker" id="inputData" value="{{$registro->data->format('d/m/Y')}}">
          </div>

			    <div class="form-group">
			      @if($registro->imagem)
			        Imagem atual<br>
			        <img src="assets/escola/img/eventos/thumbs/{{$registro->imagem}}"><br>
			      @endif
			      <label for="inputImagem">Imagem</label>
			      <input type="file" class="form-control" id="inputImagem" name="imagem">
			    </div>

          <div class="form-group">
            <label for="inputTexto">Texto</label>
            <textarea name="texto" class="form-control" id="inputTexto">{{$registro->texto}}</textarea>
          </div>

          <div class="well">
            <label><input type="checkbox" name="is_juramento" value="1" @if($registro->is_juramento == 1) checked @endif > Este evento é um Juramento</label>
          </div>

          @include('escola.painel.partials.upload-imagens', ['path' => 'eventos'])

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.escola.eventos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
