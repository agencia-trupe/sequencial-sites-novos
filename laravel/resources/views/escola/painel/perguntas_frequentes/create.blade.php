@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

				<h2>Cadastrar Pergunta</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

			</div>
		</div>

		<form action="{{ URL::route('painel.escola.perguntas_frequentes.store') }}" method="post" enctype="multipart/form-data">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

					<div class="form-group">
						<label for="inputCurso">Curso</label>
						<select class="form-control" name="curso_id" required>
							<option value=""></option>
							@forelse($cursos as $curso)
								<option value="{{$curso->id}}" @if(old('curso_id') == $curso->id) selected @endif >{{$curso->titulo}}</option>
							@empty
								<option value="">Sem cadastros</option>
							@endforelse
						</select>
					</div>

					<div class="form-group">
						<label for="inputTitulo">Título</label>
						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
					</div>

					<div class="form-group">
						<label for="inputTexto">Texto</label>
						<textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
					</div>

					<hr>

				</div>
			</div>

			<button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			<a href="{{ URL::route('painel.escola.perguntas_frequentes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

	</div>

@endsection
