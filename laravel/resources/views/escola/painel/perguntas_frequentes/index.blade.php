@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

				<h2>Perguntas Frequentes</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

				<a href="{{ URL::route('painel.escola.perguntas_frequentes.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Pergunta</a>

				<hr>

				<div class="btn-group">
					@foreach ($cursos as $key => $value)
						<a href="{{URL::route('painel.escola.perguntas_frequentes.index', ['filtro' => $value->id])}}" title="{{$value->titulo}}" class="btn btn-sm btn-default @if($filtro == $value->id) btn-primary @endif ">{{$value->titulo}}</a>
					@endforeach
				</div>

				<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="perguntas_frequentes">

					<thead>
						<tr>
							<th>Ordenar</th>
							<th>Curso</th>
							<th>Título</th>
							<th>Texto</th>
							<th><span class="glyphicon glyphicon-cog"></span></th>
						</tr>
					</thead>

					<tbody>
						@forelse ($registros as $registro)

							<tr class="tr-row" id="row_{{ $registro->id }}">
								<td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
								<td>
									{{$registro->curso->titulo}}
								</td>
								<td>
									{{$registro->titulo}}
								</td>
								<td>
									{{ str_words(strip_tags($registro->texto), 15)  }}
								</td>
								<td class="crud-actions">
									<a href="{{ URL::route('painel.escola.perguntas_frequentes.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

									<form action="{{ URL::route('painel.escola.perguntas_frequentes.destroy', $registro->id) }}" method="post">
										{!! csrf_field() !!}
										<input type="hidden" name="_method" value="DELETE">
										<button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
									</form>
								</td>
							</tr>

						@empty

							<tr>
								<td colspan="4" style="text-align:center">
									@if ($filtro)
										Nenhum Registro encontrado
									@else
										Selecione um Curso
									@endif
								</td>
							</tr>

						@endforelse
					</tbody>

				</table>

			</div>
		</div>
	</div>

@endsection
