@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2>TV</h2>
                <hr>
                @include('escola.painel.partials.mensagens')
                <div class="btn-group">
                    @foreach($unidades as $slug => $titulo)
                    <a href="{{ route('painel.escola.tv.index', ['unidade' => $slug]) }}" class="btn {{ $unidade == $slug ? 'btn-primary' : 'btn-default' }}">{{ $titulo }}</a>
                    @endforeach
                </div>
                <hr>
                <form action="{{ route('painel.escola.tv.store') }}" method="POST" enctype="multipart/form-data">
                    {!! csrf_field() !!}

                    <input type="hidden" name="unidade" value="{{ $unidade }}">

                    <div class="well">
                        <div class="form-group">
                            <label>Adicionar Imagem</label>
                            <input type="file" class="form-control" id="inputImagem" name="imagem">
                        </div>
                        <div class="form-group">
                            <label>Adicionar Vídeo</label>
                            <input type="file" class="form-control" id="inputVideo" name="video">
                        </div>
                        <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>
                    </div>
                </form>
                <hr>
                @if(!count($imagens))
                <div class="alert alert-block alert-warning">
                    Nenhuma imagem cadastrada.
                </div>
                @else
                <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="tv_imagens">
                    <thead>
                        <tr>
                            <th>Ordenar</th>
                            <th>Imagem/Vídeo</th>
                            <th><span class="glyphicon glyphicon-cog"></span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($imagens as $registro)
                        <tr class="tr-row" id="row_{{ $registro->id }}">
                            <td class='move-actions'><a href='#' class='btn btn-info btn-move btn-sm'>mover</a></td>
                            <td>
                                @if($registro->imagem)
                                    <img src="{{ asset('assets/escola/img/tv/'.$registro->imagem) }}" style="display:block;max-width:100%;width:500px">
                                @elseif($registro->video)
                                    <video muted controls style="max-width: 100%; width: 500px">
                                        <source src="{{ asset('assets/escola/img/tv/'.$registro->video) }}" type="video/mp4">
                                    </video>
                                @endif
                            </td>
                            <td class="crud-actions">
                                <form action="{{ URL::route('painel.escola.tv.destroy', $registro->id) }}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                </form>
                            </td>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>

@endsection
