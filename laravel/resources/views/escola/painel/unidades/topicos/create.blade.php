@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">

    	<div class="row">
    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <h1>Unidade : {{$unidade->titulo}}</h1>
	      	<h2>Cadastrar Tópico</h2>

	        <hr>

	        @include('escola.painel.partials.mensagens')

		    </div>
		  </div>

      <form action="{{ URL::route('painel.escola.unidades.topicos.store') }}" method="post" enctype="multipart/form-data">

  			{!! csrf_field() !!}

  			<div class="row">
  				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

            <div class="form-group">
              <label for="inputTip">Tipo</label>
              <select class="form-control" name="tipo" required>
                <option value="laboratorios" @if(old('tipo') == 'laboratorios') selected @endif >Laboratórios</option>
                <option value="infraestrutura" @if(old('tipo') == 'infraestrutura') selected @endif >Infraestrutura</option>
              </select>
            </div>

            <div class="form-group">
  						<label for="inputTitulo">Título</label>
  						<input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{old('titulo')}}">
  					</div>

  			    <div class="form-group">
  						<label for="inputTexto">Texto</label>
  						<textarea name="texto" class="form-control" id="inputTexto">{{old('texto')}}</textarea>
  					</div>

            <input type="hidden" name="unidades_id" value="{{$unidade->id}}">

            <hr>

  				</div>
  			</div>

			  <button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

			  <a href="{{ URL::route('painel.escola.unidades.topicos.index', ['unidades_id' => $unidade->id])}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

	  	</form>

    </div>

@endsection
