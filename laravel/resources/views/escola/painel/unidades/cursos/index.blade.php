@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">

                <h2>Unidades</h2>

                <h3>Cursos disponíveis na Unidade: {{$unidade->titulo}}</h3>

                @include('escola.painel.partials.mensagens')

                <hr>

                <form action="{{URL::route('painel.escola.unidades.cursos.store')}}" method="post">

                    {!! csrf_field() !!}

                    <input type="hidden" name="unidades_id" value="{{$unidade->id}}">

                    <div class="well">
                        @foreach($cursos as $curso)
                            <label style="width: 32%; display:inline-block;">
                                <input type="checkbox" name="cursos[]" value="{{$curso->id}}" @if(in_array($curso->id, $cursosIdArray)) checked @endif > {{$curso->titulo}}
                            </label>
                        @endforeach
                    </div>

                    <hr>

                    <a href="{{URL::route('painel.escola.unidades.index')}}" title="Voltar" class="btn btn-default">Voltar</a>
                    <input type="submit" value="Salvar" class="btn btn-success">
                </form>

            </div>
        </div>
    </div>

@endsection
