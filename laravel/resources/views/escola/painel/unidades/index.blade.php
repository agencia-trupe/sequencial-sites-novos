@extends('escola.painel.template')

@section('conteudo')

    <div class="container-fluid padded-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h2>Unidades</h2>

                <hr>

                @include('escola.painel.partials.mensagens')

                <a href="{{ URL::route('painel.escola.unidades.create') }}" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar Unidade</a>

                <table class="table table-striped table-bordered table-hover">

                    <thead>
                    <tr>
                        <th>Título</th>
                        <th>Endereço</th>
                        <th>Horários de Atendimento</th>
                        <th></th>
                        <th><span class="glyphicon glyphicon-cog"></span></th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($registros as $registro)

                        <tr class="tr-row" id="row_{{ $registro->id }}">
                            <td>
                                {{$registro->titulo}}
                            </td>
                            <td>
                                {!! nl2br($registro->endereco) !!}
                            </td>
                            <td>
                                {!! nl2br($registro->horarios) !!}
                            </td>
                            <td>
                                <a href="{{URL::route('painel.escola.unidades.topicos.index', ['unidades_id' => $registro->id])}}" class="btn btn-sm btn-default">Tópicos</a>
                                <a href="{{URL::route('painel.escola.unidades.cursos.index', ['unidades_id' => $registro->id])}}" class="btn btn-sm btn-default">Cursos dessa Unidade</a>
                            </td>
                            <td class="crud-actions">
                                <a href="{{ URL::route('painel.escola.unidades.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

                                <form action="{{ URL::route('painel.escola.unidades.destroy', $registro->id) }}" method="post">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
                                </form>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>

                </table>

            </div>
        </div>
    </div>

@endsection
