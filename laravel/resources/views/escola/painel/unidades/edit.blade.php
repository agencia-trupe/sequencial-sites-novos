@extends('escola.painel.template')

@section('conteudo')

  <div class="container-fluid padded-bottom">

    <div class="row">
    	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

		  	<h2>Editar Unidade</h2>

		    <hr>

		    @include('escola.painel.partials.mensagens')

		  </div>
		</div>

    <form action="{{ URL::route('painel.escola.unidades.update', $registro->id) }}" method="post" enctype="multipart/form-data">

			<input type="hidden" name="_method" value="PUT">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

          <div class="form-group">
            <label for="inputTitulo">Título</label>
            <input type="text" name="titulo" class="form-control" id="inputTitulo" value="{{$registro->titulo}}">
          </div>

          <div class="form-group">
            @if($registro->imagem)
              Imagem atual<br>
              <img src="assets/escola/img/unidades/thumbs/{{$registro->imagem}}"><br>
            @endif
            <label for="inputImagem">Imagem</label>
            <input type="file" class="form-control" id="inputImagem" name="imagem">
          </div>

			    <div class="form-group">
			      <label for="inputEndereço">Endereço</label>
			      <textarea name="endereco" class="form-control textarea-simples" id="inputEndereço">{{$registro->endereco}}</textarea>
			    </div>

          <div class="form-group">
            <label for="inputHorários">Horários</label>
            <textarea name="horarios" class="form-control textarea-simples" id="inputHorários">{{$registro->horarios}}</textarea>
          </div>

          <div class="form-group">
            <label for="inputGoogleMaps">Código de Incorporação do Google Maps</label>
            <input type="text" name="google_maps" class="form-control" id="inputGoogleMaps" value="{{$registro->google_maps}}">
          </div>

          <div class="form-group">
            <label for="inputCordaUnidade">Cor da Unidade</label>
            <input type="text" name="cor_unidade" class="form-control" id="inputCordaUnidade" value="{{$registro->cor_unidade}}">
          </div>

          @include('escola.painel.partials.upload-imagens', ['path' => 'unidades'])

          <hr>

				</div>
			</div>

			<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

			<a href="{{ URL::route('painel.escola.unidades.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</form>

    </div>

@endsection
