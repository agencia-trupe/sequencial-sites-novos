<hr>

<div class="well">

	<label style='display:block;'>Tópicos </label>

	<div class='list-group' id="listaTopicosExistentes" style='clear:right; margin: 15px 0;'>
    @if(isset($registro) && sizeof($registro->topicos))
			<div class='list-group-item info'>
	      Tópicos cadastrados
	    </div>
			@foreach($registro->topicos as $topico)
        <div class='list-group-item'>
          <div class='form-group'>
            <input type='text' class='form-control' name='topicos_titulos[]' placeholder='Título' value="{{$topico->titulo}}">
          </div>
          <div class='form-group'>
            <textarea name='topicos_textos[]' class='form-control' placeholder='Texto'>{{$topico->texto}}</textarea>
          </div>
          <div class='form-group'>
            <a href='#' class='btn btn-sm btn-danger btn-remover-arquivo'><span class="glyphicon glyphicon-trash"></span> remover este tópico</a>
          </div>
        </div>
			@endforeach
		@endif
  </div>

  <hr>

  <div class='list-group' id="listaTopicos" style='clear:right; margin: 15px 0;'>
    <div class='list-group-item info'>
      <a href="#" class="btn btn-xs btn-success pull-right" id="adicionarTopico"><span class="glyphicon glyphicon-plus-sign"></span> adicionar tópico</a> Novos Tópicos
    </div>
  </div>

  <hr>

  <div class="panel panel-default">
		<div class="panel-body">
	    	Você pode clicar e arrastar os tópicos para ordená-los.
	  	</div>
	</div>

</div>
