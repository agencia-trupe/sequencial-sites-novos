<hr>

<div class="well">

	<label>Vídeos</label>

	<div class="multiUpload-Videos">

    <div class="input-group">
      <div class="form-group">
        <input type="text" id="input-video" class="form-control">
        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
      </div>
      <span class="input-group-btn">
        <button class="btn btn-success" id="adicionarVideo" type="button"><span class="glyphicon glyphicon-circle-arrow-right"></span></button>
      </span>
    </div>
		<p>
			Informe o endereço do Vídeo do Youtube no campo acima.
		</p>
	</div>

	<div id="listaVideos" class="list-group">
		@if(isset($registro) && sizeof($registro->videos) > 0)
			@foreach($registro->videos as $k => $v)
				<div class='list-group-item projetoVideo'>
					<a href='#' class='pull-right btn btn-sm btn-danger btn-remover' title='remover o vídeo'><span class='glyphicon glyphicon-remove-sign'></span> <strong>remover vídeo</strong></a>

          <h4 class="list-group-item-heading">
						<a href="#" class="titulo-video-nopost" data-type="text" data-title="Alterar título" title="Alterar título">{{$v->titulo}}</a
					</h4>
          <p class="list-group-item-text">
            <img class='img-thumbnail' style='max-width: 60px; margin-right: 15px' src="assets/img/videos/thumbs/{{$v->thumb}}">
            <a href="{{$v->url}}" target="_blank">{{$v->url}}</a>
          </p>

					<input type='hidden' name='video_url[]' value="{{$v->url}}">
					<input type='hidden' name='video_id[]' value="{{$v->video_id}}">
          <input type='hidden' name='video_titulo[]' class='video-titulo' value="{{$v->titulo}}">
          <input type='hidden' name='video_thumb[]' value="{{$v->thumb}}">

      	</div>
			@endforeach
		@endif
	</div>







	<div class="panel panel-default">
		<div class="panel-body">
	    	Você pode clicar e arrastar os vídeos para ordená-los.
	  	</div>
	</div>
</div>
