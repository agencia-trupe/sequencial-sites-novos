<nav class="navbar navbar-default">
	<div class="container-fluid">

		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-painel" aria-expanded="false">
				<span class="sr-only">Navegação</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" title="Página Inicial" href="{{ URL::route('escola.painel.dashboard') }}">Escola Técnica Sequencial</a>
		</div>

		<div class="collapse navbar-collapse" id="menu-painel">
			<ul class="nav navbar-nav">

				<li @if(str_is('escola.painel.dashboard*', Route::currentRouteName())) class="active" @endif>
					<a href="{{URL::route('escola.painel.dashboard')}}" title="Início">Início</a>
				</li>

				<!--RESOURCEMENU-->
				@if(Auth::user()->acessoAdmin())
				<li class="dropdown @if(preg_match('~painel.escola.(chamadas|banners|popups*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.escola.banners.*', Route::currentRouteName())) class='active' @endif >
							<a href='painel/escola/banners' title='Banners'>Banners</a>
						</li>
						<li @if(str_is('painel.escola.chamadas.*', Route::currentRouteName())) class='active' @endif >
							<a href='painel/escola/chamadas' title='Chamadas'>Chamadas</a>
						</li>
						<li @if(str_is('painel.escola.popups*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/popups' title='Pop Ups'>Pop Ups</a>
						</li>
					</ul>
				</li>

				<li class="dropdown @if(preg_match('~painel.escola.(a-sequencial|bolsas-de-estudo|ouvidoria|trabalhe_conosco|perguntas_frequentes|depoimentos|novidades|estagios|eventos*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Páginas <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.escola.a-sequencial*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/a-sequencial' title='A Sequencial'>A Sequencial</a>
						</li>
						<li @if(str_is('painel.escola.bolsas-de-estudo*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/bolsas-de-estudo' title='Bolsas de Estudo'>Bolsas de Estudo</a>
						</li>
						<li @if(str_is('painel.escola.novidades*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/novidades' title='Novidades'>Novidades</a>
						</li>
						<li @if(str_is('painel.escola.eventos*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/eventos' title='Eventos'>Eventos</a>
						</li>
						<li @if(str_is('painel.escola.perguntas_frequentes*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/perguntas_frequentes' title='Perguntas Frequentes'>Perguntas Frequentes</a>
						</li>
						<li @if(str_is('painel.escola.trabalhe_conosco*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/trabalhe_conosco' title='Trabalhe Conosco'>Trabalhe Conosco</a>
						</li>
						<li @if(str_is('painel.escola.ouvidoria*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/ouvidoria' title='Ouvidoria'>Ouvidoria</a>
						</li>
						<li @if(str_is('painel.escola.depoimentos*', Route::currentRouteName())) class='active' @endif>
							<a href='painel/escola/depoimentos' title='Depoimentos'>Depoimentos</a>
						</li>
					</ul>
				</li>

				<li @if(str_is('painel.escola.unidades*', Route::currentRouteName())) class='active' @endif><a href='painel/escola/unidades' title='Unidades'>Unidades</a></li>

				<li class="dropdown @if(preg_match('~painel.escola.(cursos|areas).*~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cursos <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(str_is('painel.escola.areas*', Route::currentRouteName())) class='active' @endif><a href='painel/escola/areas' title='Áreas dos Cursos'>Áreas</a></li>
						<li @if(str_is('painel.escola.cursos*', Route::currentRouteName())) class='active' @endif><a href='painel/escola/cursos' title='Cursos'>Cursos</a></li>
					</ul>
				</li>

				<li @if(str_is('painel.escola.pre-inscricoes*', Route::currentRouteName())) class='active' @endif>
					<a href='painel/escola/pre-inscricoes' title='Pré-Inscrições'>Pré-Inscrições</a>
				</li>
				<li @if(str_is('painel.escola.cadastros-gradecurricular*', Route::currentRouteName())) class='active' @endif>
					<a href='painel/escola/cadastros-gradecurricular' title='Cadastros Grade Curricular'>Cadastros Grade Curricular</a>
				</li>

				<li @if(str_is('painel.escola.tv*', Route::currentRouteName())) class='active' @endif>
					<a href='painel/escola/tv' title='TV'>TV</a>
				</li>

				<li class="dropdown @if(preg_match('~painel.escola.(usuarios|idiomas*)~', Route::currentRouteName())) active @endif ">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sistema <b class="caret"></b></a>

					<ul class="dropdown-menu">
						<li @if(preg_match('~painel.(usuarios*)~', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.escola.usuarios.index')}}" title="Usuários do Painel">Usuários</a>
						</li>
						<li role="separator" class="divider"></li>
						<li>
							<a href="{{URL::route('escola.painel.logout')}}" title="Logout">Logout</a>
						</li>
					</ul>
				</li>
				@elseif(Auth::user()->acessoPreInscricoes())
				<li @if(str_is('painel.escola.pre-inscricoes*', Route::currentRouteName())) class='active' @endif>
					<a href='painel/escola/pre-inscricoes' title='Pré-Inscrições'>Pré-Inscrições</a>
				</li>
				<li @if(str_is('painel.escola.cadastros-gradecurricular*', Route::currentRouteName())) class='active' @endif>
					<a href='painel/escola/cadastros-gradecurricular' title='Cadastros Grade Curricular'>Cadastros Grade Curricular</a>
				</li>
				@endif

				<li>
					<a href="{{URL::route('escola.painel.logout')}}" title="Logout">Logout</a>
				</li>
			</ul>
		</div>

	</div>
</nav>
