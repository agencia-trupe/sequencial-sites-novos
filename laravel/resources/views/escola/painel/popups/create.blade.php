@extends('escola.painel.template')

@section('conteudo')

	<div class="container-fluid padded-bottom">

		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

				<h2>Cadastrar Pop-up</h2>

				<hr>

				@include('escola.painel.partials.mensagens')

			</div>
		</div>

		<form action="{{ URL::route('painel.escola.popups.store') }}" method="post" enctype="multipart/form-data">

			{!! csrf_field() !!}

			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">

					<div class="form-group">
						@if(old('imagem'))
							Imagem atual<br>
							<img src="assets/img/popups/{{old('imagem')}}"><br>
						@endif
						<label for="inputImagem">Imagem</label>
						<input type="file" class="form-control" id="inputImagem" name="imagem" required>
					</div>

					<div class="form-group">
						<label>
							<input type="checkbox" name="mostrar_todas_paginas" value="1">
							<span> Mostrar em todas as páginas internas também<span>
							</label>
						</div>

						<div class="form-group">
							<label for="inputDataInicio">Data de início da exibição</label>
							<input type="text" name="data_inicio" class="form-control datepicker" id="inputDataInicio" value="{{old('data_inicio')}}" required>
						</div>

						<div class="form-group">
							<label for="inputDataFim">Data de término da exibição</label>
							<input type="text" name="data_fim" class="form-control datepicker" id="inputDataFim" value="{{old('data_fim')}}" required>
						</div>

						<hr>

					</div>
				</div>

				<button type="submit" title="Cadastrar" class="btn btn-success">Cadastrar</button>

				<a href="{{ URL::route('painel.escola.popups.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</form>

		</div>

	@endsection
