@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-novidades">

    <div class="centralizar">

      <h1>
        NOVIDADES
      </h1>

      <aside>

        <h2>VEJA OUTRAS NOVIDADES RECENTES:</h2>

        <ul>
          @foreach($novidades as $n)
            <li>
              <a href="novidades/{{$n->slug}}" title="{{$n->titulo}}" @if($n->id == $detalhe->id) class="ativo" @endif >
                <div class="data">{{$n->data->formatLocalized("%e %b %Y")}}</div>
                <div class="titulo">{{$n->titulo}}</div>
              </a>
            </li>
          @endforeach
        </ul>

        <a href="{{URL::route('escola.novidades')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>

      </aside>

      <section>

        @if($detalhe->imagem)
          <div class="imagem">
            <img src="assets/escola/img/novidades/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}" />
          </div>
        @endif

        <div class="titulo">
          <h2>{{$detalhe->data->formatLocalized("%e %b %Y")}}</h2>
          <h1>{{$detalhe->titulo}}</h1>
        </div>

        <h3>{{$detalhe->subtitulo}}</h3>

        <div class="texto cke">
          {!! $detalhe->texto !!}
        </div>

        @if($detalhe->categoria == 'Notícias')
        <a href="{{URL::route('escola.novidades.noticias')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>
        @elseif($detalhe->categoria == 'Juramentos')
        <a href="{{URL::route('escola.novidades.juramentos')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>
        @else
        <a href="{{URL::route('escola.novidades')}}" class="btn-voltar" title="Voltar">&laquo; voltar</a>
        @endif

      </section>


    </div>

  </div>

@stop
