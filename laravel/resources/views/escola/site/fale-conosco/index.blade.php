@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-fale-conosco">
    <div class="centralizar">

      <h1 class="main-title">
        FALE CONOSCO
      </h1>

      <div id="banner-telefone">
        <span class="horario">
          Segunda a Sexta<br>
          das 7h às 21h
        </span>
        CENTRAL DE ATENDIMENTO SEQUENCIAL: 11 <strong>3371-2828</strong>
      </div>

      <div id="banner-whatsapp">
        <a href="https://api.whatsapp.com/send?phone=5511961664021" target="_blank">
          TIRE SUAS DÚVIDAS VIA WHATSAPP:
          11 <span>96166-4021</span>
        </a>
      </div>

      <div class="colunas">
        <div class="coluna">
          <p>
            PARA ENDEREÇO DAS UNIDADES CONSULTE SEUS DESCRITIVOS:
          </p>
          @include('escola.site.partials.menu-unidades')
        </div>
        <div class="coluna">
          <form action="{{URL::route('escola.fale-conosco.enviar')}}" method="post">

            {!! csrf_field() !!}

            <h2>Envie sua mensagem:</h2>

            @if($errors->any())
              <p class="resposta-form erro">{{ $errors->first() }}</p>
            @endif

            @if(session('contato_enviado'))
              <p class="resposta-form sucesso">Sua mensagem foi enviada com Sucesso!</p>
            @endif

            <fieldset>
              <input type="text" name="nome" placeholder="nome" required value="{{old('nome')}}">
              <input type="email" name="email" placeholder="e-mail" required value="{{old('email')}}">
              <input type="text" name="telefone" placeholder="telefone" required value="{{old('telefone')}}">
              <input type="text" name="cpf" placeholder="[se você é aluno - informe seu CPF para localizarmos seu cadastro]" value="{{old('cpf')}}">
              <textarea name="mensagem" placeholder="mensagem" required>{{old('mensagem')}}</textarea>
            </fieldset>

            <input type="submit" value="ENVIAR">

          </form>
        </div>
      </div>
    </div>
  </div>

@stop
