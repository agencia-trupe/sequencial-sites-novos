<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Escola Técnica Sequencial</title>
    <style>
        body { background-color: #000; }
        .slide, .tv {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }
        .slide {
            display: none;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }
        .slide video {
            position: absolute;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
        }
        .empty {
            display: inline-block;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translateX(-50%) translateY(-50%);
            color: #fff;
            font-family: 'Arial', sans-serif;
            letter-spacing: 2px;
            font-size: 13px;
        }
    </style>
</head>
<body>
    @if(count($imagens))
    <div class="tv">
        @foreach($imagens as $imagem)
        @if($imagem->video)
            <div class="slide slide-video">
                <video muted @if(count($imagens) == 1) loop @endif>
                    <source src="{{ asset('assets/escola/img/tv/'.$imagem->video) }}" type="video/mp4">
                </video>
            </div>
        @else
            <div class="slide" style="background-image: url({{ asset('assets/escola/img/tv/'.$imagem->imagem) }})" alt=""></div>
        @endif
        @endforeach
    </div>
    @else
    <div class="empty">NENHUMA IMAGEM CADASTRADA</div>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            var TRANSITION = 1000;
            var TIMEOUT = 15000;

            var count = $('.slide').length;
            var index = 0;

            function nextSlide() {
                if (count === 1) return;
                if (++index >= count) index = 0;
                changeSlide();
            }

            function changeSlide() {
                var $slide = $('.slide').eq(index);

                $('.slide.active').fadeOut(TRANSITION).removeClass('active');
                $slide.addClass('active').fadeIn(TRANSITION);

                if ($slide.hasClass('slide-video')) {
                    $('video', $slide)[0].play();
                } else {
                    setTimeout(function() {
                        nextSlide();
                    }, TIMEOUT);
                }
            }

            $('video').on('ended',function() {
                nextSlide();
                var _video = this;
                setTimeout(function() {
                    _video.currentTime = 0;
                }, TIMEOUT);
            });

            changeSlide();
        });
    </script>
</body>
</html>
