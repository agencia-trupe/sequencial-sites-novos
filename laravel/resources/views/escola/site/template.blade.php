<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
	<meta name="robots" content="index, follow" />
	<meta name="author" content="Trupe Design" />
	<meta name="copyright" content="2017 Trupe Design" />
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<meta name="keywords" content="escola técnica, curso técnico, cursos técnicos, curso profissionalizante, auxiliar de enfermagem, técnico em enfermagem, técnico em farmácia, técnico em radiologia, técnico em estética, técnico em segurança do trabalho, técnico em eletrônica, técnico em eletrotécnica," />

	<title>Escola Técnica Sequencial - Cursos técnicos profissionalizantes</title>
	<meta name="description" content="Escola Técnica Sequencial - Cursos técnicos profissionalizantes" />
	<meta property="og:title" content="Escola Técnica Sequencial - Cursos técnicos profissionalizantes"/>
	<meta property="og:description" content="Escola Técnica Sequencial - Cursos técnicos profissionalizantes"/>

	<meta property="og:site_name" content="Escola Técnica Sequencial"/>
	<meta property="og:type" content="website"/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ base_url() }}">
	<script>var BASE = "{{ base_url() }}"</script>
	<meta name="csrf-token" content="{{ csrf_token() }}" />

	<link href="https://fonts.googleapis.com/css?family=Asap:400,700|Lato:300" rel="stylesheet">

	<link rel="stylesheet" href="assets/escola/css/vendor.css">

	<link rel="stylesheet" href="assets/escola/css/site.css?cachebuster={{ time() }}">

	<script src="assets/escola/js/jquery.js"></script>
	<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script> -->

	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-20140557-1', 'auto');
	ga('send', 'pageview');
	</script>

	<meta name="facebook-domain-verification" content="0x12my4f6vjxzp4b46kcmv7olwzvyv" />

</head>
<body>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=393627327333856";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	@include('escola.site.partials.menu')

	@yield('conteudo')

	@include('escola.site.partials.footer')

	<div id="busca-overlay">
		<div class="centralizar">
			<form action="{{URL::route('escola.busca')}}" method="post">
				{!! csrf_field() !!}
				<input type="text" name="termo" placeholder="buscar">
				<input type="submit" value="buscar">
			</form>
		</div>
	</div>

	<script src="assets/escola/js/site.js?cachebuster={{ time() }}"></script>

	@if(count($popup))
		@if($popup->mostrar_todas_paginas || str_is('escola.index', Route::currentRouteName()))
			<script type="text/javascript">
			$.fancybox.helpers.overlay.open({parent: $('body')});
			$.fancybox.open([
				{
					type : 'image',
					href : 'assets/escola/img/popups/{{$popup->imagem}}'
				}
			]);
			</script>
		@endif
	@endif

	@include('_cookie-consent', ['domain' => 'EscolaSequencial'])
</body>
</html>
