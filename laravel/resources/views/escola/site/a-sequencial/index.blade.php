@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-a-sequencial">
        <div class="centralizar">

            <aside>
                @include('escola.site.partials.menu-cursos')
            </aside>

            <section>
                <h1 class="main-title">{{$conteudo->titulo}}</h1>
                <div class="colunas">
                    <div class="coluna">
                        <img src="assets/escola/img/a-sequencial/{{$conteudo->imagem}}"
                             alt="A Sequencial">
                    </div>
                    <div class="coluna">
                        <div class="cke">{!! $conteudo->texto !!}</div>
                    </div>
                </div>

                @include('escola.site.partials.menu-unidades')

            </section>

        </div>
    </div>

@stop
