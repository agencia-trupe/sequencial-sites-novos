@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-trabalhe-conosco">
        <div class="centralizar">

            <aside>
                @include('escola.site.partials.menu-cursos')
            </aside>

            <section>
                <h1 class="main-title">{{$conteudo->titulo}}</h1>
                <div class="cke">{!! $conteudo->texto !!}</div>
            </section>

        </div>
    </div>

@stop
