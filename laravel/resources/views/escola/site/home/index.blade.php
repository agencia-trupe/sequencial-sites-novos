@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-home">
        <div class="centralizar">
            <section>

                <form action="{{ route('escola.pre-inscricao') }}" class="pre-inscricao-home" method="POST">
                    {!! csrf_field() !!}

                    @if (count($errors) > 0)
                        <script>
                            window.onload = function(event) {
                                alert('Preencha todos os campos corretamente');
                            };
                        </script>
                    @endif
                    @if (session('preInscricaoEnviada'))
                        <script>
                            window.onload = function() {
                                alert('Pré-inscrição efetuada com sucesso!');
                            };
                        </script>
                    @endif

                    <a href="http://www.sequencialmatriculas.com.br/escolatecnica/" class="imagem-responsive">
                        <img src="assets/escola/img/layout/bannerHome-Bolsas18-2sem-PREINSCRICAO-responsivo.png" alt="">
                    </a>
                    <div class="texto-responsive">
                        <span>Rápido e fácil!</span>
                        FAÇA SUA PRÉ-INSCRIÇÃO AGORA:
                    </div>

                    <div class="wrapper">
                        <div class="campos">
                            <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                            <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                            <input type="telefone" name="telefone" placeholder="telefone" value="{{ old('telefone') }}" required>
                            <input type="hidden" name="origem" value="Banner Home">
                        </div>
                        <input type="submit" value="ENVIAR">
                    </div>

                    <a href="http://www.sequencialmatriculas.com.br/escolatecnica/" class="link-rodape">VOCÊ PODE FAZER SUA INSCRIÇÃO DEFINITIVA CLICANDO AQUI &raquo;</a>
                </form>
                <style>
                    .pre-inscricao-home {
                        font-size: 14px;
                        width: 100%;
                        height: 400px;
                        position: relative;
                        background: url('assets/escola/img/layout/bannerHome-Bolsas18-2sem-PREINSCRICAO.png');
                        font-family: 'Asap', sans-serif;
                        font-style: italic;
                    }
                    .pre-inscricao-home .campos {
                        position: absolute;
                        left: 180px;
                        top: 227px;
                    }
                    .pre-inscricao-home .campos input {
                        display: block;
                        width: 350px;
                        height: 36px;
                        border-radius: 4px;
                        padding: 0 10px;
                        background: #fff;
                        color: #189958;
                        margin-bottom: 4px;
                        border: 0;
                        outline: 0;
                    }
                    .pre-inscricao-home input[type=submit] {
                        display: block;
                        position: absolute;
                        width: 145px;
                        height: 116px;
                        top: 227px;
                        left: 554px;
                        background: #189958;
                        color: #FFFF3F;
                        font-size: 18px;
                        border-radius: 4px;
                        cursor: pointer;
                        transition: background .3s;
                        outline: 0;
                        border: 0;
                    }
                    .pre-inscricao-home input[type=submit]:hover {
                        background: #0B7CB6;
                    }
                    .pre-inscricao-home .link-rodape {
                        position: absolute;
                        display: inline-block;
                        color: #FFFF3F;
                        font-size: 22px;
                        font-weight: 700;
                        bottom: 7px;
                        left: 40px;
                        transition: color .3s;
                    }
                    .pre-inscricao-home .link-rodape:hover {
                        color: #fff;
                    }
                    .pre-inscricao-home .imagem-responsive,
                    .pre-inscricao-home .texto-responsive {
                        display: none;
                    }

                    @media (max-width: 1199px) {
                        .pre-inscricao-home {
                            height: auto;
                            background: none;
                        }
                        .pre-inscricao-home .link-rodape {
                            display: none;
                        }
                        .pre-inscricao-home .imagem-responsive {
                            display: block;
                        }
                        .pre-inscricao-home .imagem-responsive img {
                            width: 100%;
                            display: block;
                        }
                        .pre-inscricao-home .texto-responsive {
                            display: block;
                            margin-top: 10px;
                            font-size: 16px;
                            color: #0B7CB6;
                            font-weight: bold;
                        }
                        .pre-inscricao-home .texto-responsive span {
                            display: block;
                            font-size: 13px;
                            margin-bottom: 5px;
                            color: #189958;
                            font-weight: normal;
                        }
                        .pre-inscricao-home .wrapper {
                            background: #0B7CB6;
                            width: calc(100% - 40px);
                            padding: 20px;
                            margin-top: 10px;
                        }
                        .pre-inscricao-home .campos {
                            position: relative;
                            left: auto;
                            top: auto;
                        }
                        .pre-inscricao-home .campos input {
                            position: static;
                            display: block;
                            width: calc(100% - 20px);
                        }
                        .pre-inscricao-home input[type=submit] {
                            position: static;
                            display: block;
                            width: 100%;
                            height: auto;
                            padding: 15px 0;
                        }
                    }
                </style>

                {{--<div id="banners">
                    @if(count($banners))

                        <div id="banners-pager"></div>

                        <div class="cycle-slideshow"
                             data-slides="> .banner"
                             data-pause-on-hover="true"
                             data-pager="#banners-pager">
                            @foreach($banners as $k => $banner)
                                <div class="banner" @if($k > 0) style="display:none;" @endif>
                                    @if($banner->link)
                                        <a href="{{$banner->link}}" title="{{$banner->titulo}}">
                                            <img src="assets/escola/img/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}" />
                                        </a>
                                    @else
                                        <img src="assets/escola/img/banners/{{$banner->imagem}}" alt="{{$banner->titulo}}" />
                                    @endif
                                </div>
                            @endforeach
                        </div>

                    @endif
                </div>--}}

                <div id="chamadas">
                    @forelse($chamadas as $i => $chamada)

                        <div class="chamada @if($i == 1) eventos @else novidades @endif">
                            <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
                                <span class="titulo">{{$chamada->tipo_extenso}}</span>
                                @if($chamada->fundo_tipo == 'imagem')
                                    <img src="assets/escola/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
                                @endif
                                <div class="over">
                                    <h1>{{$chamada->titulo}}</h1>
                                    <h2>{{$chamada->subtitulo}}</h2>
                                </div>
                            </a>
                        </div>

                    @empty

                        @foreach($calhaus as $i => $chamada)
                            <div class="chamada @if($i == 1) eventos @else novidades @endif">
                                <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
                                    <span class="titulo">{{$chamada->tipo_extenso}}</span>
                                    @if($chamada->fundo_tipo == 'imagem')
                                        <img src="assets/escola/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
                                    @endif
                                    <div class="over">
                                        <h1>{{$chamada->titulo}}</h1>
                                        <h2>{{$chamada->subtitulo}}</h2>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                    @endforelse

                    @if(count($chamadas) == 1)
                        @foreach($calhaus as $i => $chamada)
                            @if($i == 0)
                                <div class="chamada @if($i == 1) eventos @else novidades @endif">
                                    <a href="{{$chamada->link}}" title="{{$chamada->titulo}}" @if($chamada->fundo_tipo == 'cor') style="background-color:{{$chamada->fundo_cor}};" @endif>
                                        <span class="titulo">{{$chamada->tipo_extenso}}</span>
                                        @if($chamada->fundo_tipo == 'imagem')
                                            <img src="assets/escola/img/chamadas/{{$chamada->fundo_imagem}}" alt="{{$chamada->titulo}}" />
                                        @endif
                                        <div class="over">
                                            <h1>{{$chamada->titulo}}</h1>
                                            <h2>{{$chamada->subtitulo}}</h2>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    <div class="chamada" id="box-central-atendimento">
                        <h2>Central de Atendimento</h2>
                        <span class="telefone"><small>11</small> 3371&middot;2828</span>
                        <p>Segunda a Sexta &middot; das 7h às 21h</p>
                    </div>
                </div>

                <h1>ESCOLHA UMA UNIDADE PERTO DE VOCÊ!</h1>

                @include('escola.site.partials.menu-unidades')

                <a href="ouvidoria" class="link-ouvidoria">
                    <img src="assets/escola/img/layout/img-ouvidoria.png" alt="Ouvidoria Sequencial">
                </a>

                <div id="convenios">
                    <div class="box">
                        <h2>Convênios</h2>
                        <div class="grid">
                          <div class="cell image-cell">
                            <img src="assets/escola/img/layout/nube.jpg" alt="NUBE Estágios">
                          </div>
                          <div class="cell">
                            <iframe src="http://www.nube.com.br/top_estagios/medio_tecnico_sp" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" width="100%" height="223"></iframe>
                          </div>
                          <div class="cell image-cell">
                            <img src="assets/escola/img/layout/ciee.png" alt="CIEE">
                          </div>
                        </div>
                    </div>
                </div>

            </section>
            <aside>

                @include('escola.site.partials.menu-cursos')

                @if($depoimento)
                    <div id="depoimento">
                        <div class="padding">
                            <h2>DEPOIMENTOS</h2>
                            @if($depoimento->imagem)
                              <div class="imagem">
                                  <img src="assets/escola/img/depoimentos/thumbs/{{$depoimento->imagem}}" alt="Escola Técnica Sequencial">
                              </div>
                            @endif
                            <p>{!! str_words(strip_tags($depoimento->texto), 55) !!}</p>
                            <p class="assinatura">{{$depoimento->autor_nome}}</p>
                            <p class="sobre">{{$depoimento->autor_descricao}}</p>
                        </div>
                        <a href="depoimentos" title="VER DEPOIMENTO COMPLETO">VER DEPOIMENTO COMPLETO &raquo;</a>
                    </div>
                @endif

            </aside>
        </div>
    </div>

@stop
