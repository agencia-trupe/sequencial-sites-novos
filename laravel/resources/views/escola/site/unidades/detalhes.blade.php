@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-unidades">
        <div class="centralizar">

            <aside>
                <h1>
                    ESCOLHA SUA UNIDADE!
                </h1>
                <h2>
                    MATRICULE-SE JÁ!
                </h2>
                @include('escola.site.partials.menu-unidades')
            </aside>

            <section>
                <div class="unidades-colunas">
                    <div class="unidades-coluna">
                        <div class="unidades-detalhe-box" style="background:{{$unidade->cor_unidade}}">
                            <h2>UNIDADE</h2>
                            <h1>{{$unidade->titulo}}</h1>
                            <img src="assets/escola/img/unidades/{{$unidade->imagem}}" alt="{{$unidade->imagem}}">
                            <div class="coluna-endereco">
                                {!! nl2br($unidade->endereco) !!}
                                <a href="#map-iframe-{{$unidade->id}}" class="fancybox" title="Unidade {{$unidade->titulo}}">
                                    <img src="assets/escola/img/layout/ico-mapa.png" alt="VER MAPA">
                                    <span>VER MAPA &raquo;</span>
                                    <div id="map-iframe-{{$unidade->id}}" style="display:none; overflow:hidden;">
                                        {!! $unidade->google_maps !!}
                                    </div>
                                </a>
                            </div>
                            <div class="coluna-horario">
                                <strong>HORÁRIOS DE ATENDIMENTO:</strong>
                                {!! nl2br($unidade->horarios) !!}
                            </div>
                            <h3>CURSOS OFERECIDOS</h3>

                            @foreach($unidade->cursos as $curso)
                                <a href="cursos-tecnicos/{{$curso->slug}}" title="{{$curso->tituloCompleto}}">{{$curso->prefixo}} <strong>{{$curso->titulo}}</strong></a>
                            @endforeach
                        </div>
                    </div>
                    <div class="unidades-coluna">
                        <div class="unidades-detalhe-topicos">

                            @if(count($topicosEstrutura))
                                <h1>INFRAESTRUTURA:</h1>
                                @foreach($topicosEstrutura as $topico)
                                    <div class="topico">
                                        <h2 style="background:{{$unidade->cor_unidade}}; border: 1px {{$unidade->cor_unidade}} solid;">&raquo; {{$topico->titulo}}</h2>
                                        <div class="texto cke">{!! $topico->texto !!}</div>
                                    </div>
                                @endforeach
                            @endif

                            @if(count($topicosLaboratorios))
                                <h1>LABORATÓRIOS:</h1>
                                @foreach($topicosLaboratorios as $topico)
                                    <div class="topico">
                                        <h2 style="background:{{$unidade->cor_unidade}}; border: 1px {{$unidade->cor_unidade}} solid;">&raquo; {{$topico->titulo}}</h2>
                                        <div class="texto cke">{!! $topico->texto !!}</div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                    <div class="unidades-galeria">
                        @foreach($unidade->imagens as $img)
                            <a href="assets/escola/img/unidades/redimensionadas/{{$img->imagem}}" class="fancybox" rel="galeriaunidade">
                                <img src="assets/escola/img/unidades/thumbs-retangulares/{{$img->imagem}}" alt="Ampliar imagem">
                            </a>
                        @endforeach
                    </div>
                </div>
            </section>

        </div>
    </div>

@stop
