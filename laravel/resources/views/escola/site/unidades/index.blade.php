@extends('escola.site.template')

@section('conteudo')

<div class="conteudo conteudo-unidades">
    <div class="centralizar">

        <aside>
            <h1>
                ESCOLHA SUA UNIDADE!
            </h1>
            <h2>
                MATRICULE-SE JÁ!
            </h2>
            @include('escola.site.partials.menu-unidades')
        </aside>

        <section>

            <p class="olho">
                Cada unidade da Sequencial tem a infraestrutura adequada para a realização
                do seu curso. Salas de aula equipadas garantem o melhor aproveitamento dos
                ensinamentos ministrados, e laboratórios completos permitem a realização
                de aulas práticas constantes e proveitosas. Tudo que um bom curso técnico
                pode oferecer. Conheça nossas unidades:
            </p>
            
            <div id="lista-unidades-box">
                @foreach($listaUnidades as $unidade)
                    <div class="unidade-box">
                        <div class="unidade-box-item unidade-box-endereco" style="background:{{$unidade->cor_unidade}}">
                            <h1>UNIDADE</h1>
                            <h2>{{$unidade->titulo}}</h2>
                            <p>{!! nl2br($unidade->endereco) !!}</p>
                            <a href="#map-iframe-{{$unidade->id}}" class="fancybox" title="Unidade {{$unidade->titulo}}">
                                <img src="assets/escola/img/layout/ico-mapa.png" alt="VER MAPA">
                                <span>VER MAPA &raquo;</span>
                                <div id="map-iframe-{{$unidade->id}}" style="display:none; overflow:hidden;">
                                    {!! $unidade->google_maps !!}
                                </div>
                            </a>
                        </div>
                        <div class="unidade-box-item unidade-box-horario" style="background:{{$unidade->cor_unidade}}">
                            <h2>HORÁRIO DE ATENDIMENTO</h2>
                            <p>
                                {!! nl2br($unidade->horarios) !!}
                            </p>
                        </div>
                        <a href="unidades/{{$unidade->slug}}"
                           class="unidade-box-item unidade-box-link"
                           title="SAIBA MAIS SOBRE A INFRAESTRUTURA DA UNIDADE"
                           style="background:{{$unidade->cor_unidade}};border:1px {{$unidade->cor_unidade}} solid"
                           data-color="{{$unidade->cor_unidade}}">
                            <p>
                                SAIBA MAIS SOBRE A INFRAESTRUTURA DA UNIDADE &raquo;
                            </p>
                            @if($unidade->imagens->first())
                                <img src="assets/escola/img/unidades/thumbs/{{$unidade->imagens->first()->imagem}}" alt="SAIBA MAIS SOBRE A INFRAESTRUTURA DA UNIDADE">
                            @endif
                        </a>
                    </div>
                @endforeach
            </div>

        </section>

    </div>
</div>

@stop
