@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-eventos">

    <div class="centralizar">

      <div class="conteudo-novidades" style="padding:0">
        <h1>
          EVENTOS
          <nav>
            <a href="{{ route('escola.eventos') }}" @if(Route::currentRouteName() == 'escola.eventos') class="active" @endif>eventos</a>
            <a href="{{ route('escola.eventos.juramentos') }}" @if(Route::currentRouteName() == 'escola.eventos.juramentos') class="active" @endif>juramentos</a>
          </nav>
        </h1>
      </div>

      @if(sizeof($proximosJuramentos) > 0)
        <div class="lista-proximos-juramentos">
          <h1>JURAMENTOS</h1>
          <ul>
            @foreach($proximosJuramentos as $juramento)
              <li>
                <a href="eventos/{{$juramento->slug}}" title="{{$juramento->titulo}}">
                  <p>
                    PRÓXIMA CERIMÔNIA DE JURAMENTO ACONTECERÁ DIA
                    <strong>
                      {{$juramento->data->formatLocalized("%e de %B %Y")}} -  {{$juramento->titulo}}
                    </strong>
                  </p>
                </a>
              </li>
            @endforeach
          </ul>
        </div>
      @endif

      @unless(Route::currentRouteName() == 'escola.eventos.juramentos')
      <h2>PRÓXIMOS EVENTOS:</h2>

      <ul class="lista-proximos-eventos">
        @forelse($proximos as $evento)
          <li>
            <a href="eventos/{{$evento->slug}}" title="{{$evento->titulo}}">
              @if($evento->imagem)
                <div class="imagem">
                  <img src="assets/escola/img/eventos/{{$evento->imagem}}" alt="{{$evento->titulo}}" />
                </div>
              @endif
              <div class="texto">
                <div class="data">{{$evento->data->formatLocalized("DIA %e DE %B %Y")}}</div>
                <h2>{{$evento->titulo}}</h2>
              </div>
            </a>
          </li>
        @empty
          <div class="nenhum-evento">
            PRÓXIMOS EVENTOS: Aguarde atualização da programação para Eventos futuros aqui.
          </div>
        @endforelse
      </ul>
      @endunless

      @if(Route::currentRouteName() == 'escola.eventos')
      <h2>HISTÓRICO DE EVENTOS:</h2>
      @else
      <h2>HISTÓRICO DE JURAMENTOS:</h2>
      @endif

      <ul class="lista-eventos-passados">
        @foreach($historico as $evento)
          <li>
            <a href="eventos/{{$evento->slug}}" title="{{$evento->titulo}}">
              <div class="texto">
                <div class="data">{{$evento->data->formatLocalized("%e %B %Y")}}</div>
                <h2>
                  @if($evento->is_juramento)
                      JURAMENTO -
                  @endif
                  {{$evento->titulo}}
                </h2>
              </div>
            </a>
          </li>
        @endforeach
      </ul>

      {!! $historico->links() !!}

    </div>

  </div>

@stop
