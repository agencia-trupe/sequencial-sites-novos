@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-busca">

    <div class="centralizar">

      <h1>RESULTADO DA BUSCA: <small>{{$termo}}</small></h1>

      @if(count($resultados))
        <ul class="lista-resultados">
        @foreach($resultados as $area)
          @foreach($area as $result)
            <li>
              <a href="{{$result['link']}}" title="{{ $result['secao'] . ' - ' . $result['titulo'] }}">
                <h2>{{$result['secao']}}</h2>
                <div class="texto">
                  <h3>{{$result['titulo']}}</h3>
                  <p>
                    {!! str_words(strip_tags($result['texto']), 20) !!}
                  </p>
                </div>
              </a>
            </li>
          @endforeach
        @endforeach
        </ul>
      @else
        <div class="nenhum-resultado">
          Não foi encontrado nenhum resultado para <strong>{{$termo}}</strong>.
        </div>
      @endif

    </div>

  </div>

@stop
