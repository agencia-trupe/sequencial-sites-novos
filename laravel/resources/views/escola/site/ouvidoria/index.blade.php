@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-ouvidoria">
    <div class="centralizar">

      <aside>
        @include('escola.site.partials.menu-cursos')
      </aside>

      <section>
        <h1 class="main-title">{{$conteudo->titulo}}</h1>
        <div class="colunas">
          <div class="coluna">
            <div class="cke">{!! $conteudo->texto !!}</div>
          </div>
          <div class="coluna">
            <form action="{{URL::route('escola.ouvidoria.enviar')}}" method="post">

              {!! csrf_field() !!}

              <h2>Envie sua mensagem:</h2>

              @if($errors->any())
                <p class="resposta-form erro">{{ $errors->first() }}</p>
              @endif

              @if(session('contato_enviado'))
                <p class="resposta-form sucesso">Sua mensagem foi enviada com Sucesso!</p>
              @endif

              <fieldset>
                <select name="unidade" id="select-unidade" required>
                  <option value="">unidade (Selecione)</option>
                  @foreach ($listaUnidades as $unidade)
                    <option value="{{$unidade->titulo}}" @if(old('unidade') == $unidade->id) selected @endif >Unidade {{$unidade->titulo}}</option>
                  @endforeach
                </select>
                <select name="tipo" id="select-tipo-comunicado" required>
                  <option value="">tipo de comunicado (Selecione)</option>
                  <option value="Críticas e Elogios" @if(old('tipo') == 'Críticas e Elogios') selected @endif >Críticas e Elogios</option>
                  <option value="Denúncias" @if(old('tipo') == 'Denúncias') selected @endif >Denúncias</option>
                  <option value="Reclamações" @if(old('tipo') == 'Reclamações') selected @endif >Reclamações</option>
                  <option value="Apreciações e Comentários" @if(old('tipo') == 'Apreciações e Comentários') selected @endif >Apreciações e Comentários</option>
                  <option value="Pedido de Informação / Esclarecimentos" @if(old('tipo') == 'Pedido de Informação / Esclarecimentos') selected @endif >Pedido de Informação / Esclarecimentos</option>
                  <option value="Sugestões" @if(old('tipo') == 'Sugestões') selected @endif >Sugestões</option>
                </select>
                <input type="text" name="nome" placeholder="nome" required value="{{old('nome')}}">
                <input type="text" name="cpf" placeholder="CPF" required value="{{old('cpf')}}">
                <input type="email" name="email" placeholder="e-mail" required value="{{old('email')}}">
                <input type="text" name="telefone" placeholder="telefone" required value="{{old('telefone')}}">
                <textarea name="mensagem" placeholder="mensagem" required>{{old('mensagem')}}</textarea>
              </fieldset>

              <input type="submit" value="ENVIAR">

            </form>
          </div>
        </div>
      </section>

      </div>
    </div>

@stop
