<div id="curso-por-area">

    <h1>{{{ $titulo or 'ESCOLHA SEU CURSO POR ÁREA:' }}}</h1>

    @foreach($listaAreas as $k => $area)
        <div @if($k == 1) class="margem" @endif >
            <h2>{{$area->titulo}}</h2>
            <ul>
                @foreach($area->cursos as $curso)
                    <li>
                        <a href="cursos-tecnicos/{{$curso->slug}}"
                           title="{{$curso->titulo}}"
                           style="color:{{$curso->cor_curso}}"
                           data-color="{{$curso->cor_curso}}">
                            {{$curso->titulo}}</a>
                    </li>
                @endforeach
            </ul>
        </div>
    @endforeach
</div>