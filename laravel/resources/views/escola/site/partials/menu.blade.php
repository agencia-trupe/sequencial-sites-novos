<header>

	<div class="links-topo">
		<div class="centralizar">
			<div class="link-faculdade">
				<a href="http://www.faculdadesequencial.com.br" title="Conheça também a FACULDADE SEQUENCIAL">Conheça também a <strong>FACULDADE SEQUENCIAL</strong></a>
			</div>
			<div class="link-inscreva-se-mobile">
				<a href="http://www.sequencialmatriculas.com.br/escolatecnica/" target="_blank" title="Inscreva-se">
					INSCREVA-SE
				</a>
			</div>
			<div class="info-telefone item-topo">
				<p class="telefone">
					CENTRAL DE ATENDIMENTO:<br>
					11 <strong>3371-2828</strong>
				</p>
				<p class="whatsapp">
					<a href="https://api.whatsapp.com/send?phone=5511961664021" target="_blank" style="height:auto;line-height:1;padding:0;display:inline">
						TIRE SUAS DÚVIDAS VIA WHATSAPP:<br>
						11 <strong>96166-4021</strong>
					</a>
				</p>
				<style>header .links-topo .item-topo .whatsapp a:hover { background:transparent; }</style>
			</div>
			<div class="link-portal-aluno item-topo">
				<a href="{{ Cache::get('dominios.portal-aluno', 'http://sequencialmatriculas.com.br/portaltec/aluno/login/') }}" target="_blank"	title="PORTAL DO ALUNO"><img src="assets/escola/img/layout/ico-portalaluno.png" alt="PORTAL DO ALUNO" /> PORTAL DO ALUNO</a>
			</div>
			<div class="link-biblioteca-online item-topo">
				<a href="http://sequencial.bnweb.org/scripts/bnportal/bnportal.exe/index" target="_blank" title="BIBLIOTECA ONLINE"><img src="assets/escola/img/layout/ico-biblioteca.png" alt="BIBLIOTECA ONLINE" /> BIBLIOTECA ONLINE</a>
			</div>
			<div class="item-topo link-social">
				<a href="https://www.facebook.com/sequencial.escola.tecnica" target="_blank" class="social facebook">facebook</a>
				<a href="https://www.instagram.com/escola.sequencial/" target="_blank" class="social instagram">instagram</a>
			</div>
			<div class="link-busca item-topo">
				<a href="#" title="Busca"><img src="assets/escola/img/layout/ico-busca.png" alt="Busca" /></a>
			</div>
		</div>
	</div>

	<nav>
		<div class="centralizar">

			<a href="#" id="menu-toggle" title="Abrir Menu">MENU</a>

			<a href="{{URL::to('/')}}" title="Página Inicial" id="link-home">
				<img src="assets/escola/img/layout/marca-escolasequencial.png" alt="Escola Técnica Sequencial">
			</a>

			<ul class="menu-principal">
				<li>
					<a href="cursos-tecnicos" title="Cursos Técnicos" @if(str_is('escola.cursos-tecnicos*', Route::currentRouteName())) class="ativo" @endif >
						CURSOS TÉCNICOS
					</a>
				</li>
				<li>
					<a href="unidades" title="Unidades" @if(str_is('escola.unidades*', Route::currentRouteName())) class="ativo" @endif >
						UNIDADES
					</a>
				</li>
				<li>
					<a href="bolsas-de-estudo" title="Bolsas de Estudo" @if(str_is('escola.bolsas-de-estudo*', Route::currentRouteName())) class="ativo" @endif >
						BOLSAS DE ESTUDO
					</a>
				</li>
				<li class="link-inscreva-se">
					<a href="http://www.sequencialmatriculas.com.br/escolatecnica/" target="_blank" title="Inscreva-se">
						INSCREVA-SE
					</a>
				</li>
			</ul>

			<div class="menus-secundarios">
				<ul>
					<li>
						<a href="/" title="Página Inicial" @if(str_is('escola.index*', Route::currentRouteName())) class="ativo" @endif >Home</a>
					</li>
					<li>
						<a href="a-sequencial" title="A Sequencial" @if(str_is('escola.a-sequencial*', Route::currentRouteName())) class="ativo" @endif >A Sequencial</a>
					</li>
					<li>
						<a href="novidades" title="Novidades" @if(str_is('escola.novidades*', Route::currentRouteName()) && !str_is('escola.novidades.estagios', Route::currentRouteName())) class="ativo" @endif >Novidades</a>
					</li>
					<li>
						<a href="eventos" title="Eventos" @if(str_is('escola.eventos*', Route::currentRouteName()) && !str_is('escola.eventos.juramentos', Route::currentRouteName())) class="ativo" @endif >Eventos</a>
					</li>
					<li>
						<a href="eventos/juramentos" title="Juramentos" @if(str_is('escola.eventos.juramentos', Route::currentRouteName())) class="ativo" @endif >Juramentos</a>
					</li>
				</ul>
				<ul>
					<li>
						<a href="perguntas-frequentes" title="Perguntas Frequentes" @if(str_is('escola.perguntas-frequentes*', Route::currentRouteName())) class="ativo" @endif >
							Perguntas Frequentes
						</a>
					</li>
					<li>
						<a href="trabalhe-conosco" title="Trabalhe Conosco" @if(str_is('escola.trabalhe-conosco*', Route::currentRouteName())) class="ativo" @endif >
							Trabalhe Conosco
						</a>
					</li>
					<li>
						<a href="ouvidoria" title="Ouvidoria" @if(str_is('escola.ouvidoria*', Route::currentRouteName())) class="ativo" @endif >
							Ouvidoria
						</a>
					</li>
					<li>
						<a href="fale-conosco" title="Fale Conosco" @if(str_is('escola.fale-conosco*', Route::currentRouteName())) class="ativo" @endif >
							Fale Conosco
						</a>
					</li>
				</ul>
			</div>

		</div>
	</nav>
</header>
