@if(count($listaUnidades) > 0)
    <div id="lista-unidades">
        @foreach($listaUnidades as $unidade)
            <a href="unidades/{{$unidade->slug}}"
               title="{{$unidade->titulo}}"
               style="background-color:{{$unidade->cor_unidade}};border:1px {{$unidade->cor_unidade}} solid;"
               data-color="{{$unidade->cor_unidade}}">
                <span>UNIDADE</span>
                <span class="titulo">{{$unidade->titulo}}</span>
                @if($unidade->imagem)
                    <img src="assets/escola/img/unidades/thumbs/{{$unidade->imagem}}" alt="{{$unidade->titulo}}">
                @endif
            </a>
        @endforeach
    </div>
@endif