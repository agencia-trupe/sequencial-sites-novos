<footer>

  <div class="centralizar">

    <div class="coluna footer-info">
      <img src="assets/escola/img/layout/marca-escolasequencial-rodape.png" alt="Escola Técnica Sequencial" />
      <p class="telefone">
        CENTRAL DE ATENDIMENTO SEQUENCIAL:
        <span>11 <strong>3371-2828</strong></span>
      </p>
      <p class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=5511961664021" target="_blank">
          TIRE SUAS DÚVIDAS VIA WHATSAPP:
          <span>11 <strong>96166-4021</strong></span>
        </a>
      </p>
      <div class="redes-sociais">
        <span>ACOMPANHE A ESCOLA SEQUENCIAL TAMBÉM NAS REDES SOCIAIS:</span>
        <a href="https://www.facebook.com/sequencial.escola.tecnica" target="_blank" class="facebook">facebook</a>
        <a href="https://www.instagram.com/escola.sequencial/" target="_blank" class="instagram">instagram</a>
      </div>
    </div>

    <div class="coluna ">
      <ul class="destaque">
        <li><a href="{{URL::route('escola.cursos-tecnicos')}}" title="CURSOS TÉCNICOS">&raquo; CURSOS TÉCNICOS</a></li>
        <li><a href="{{URL::route('escola.unidades')}}" title="UNIDADES">&raquo; UNIDADES</a></li>
        <li><a href="{{URL::route('escola.bolsas-de-estudo')}}" title="BOLSAS DE ESTUDO">&raquo; BOLSAS DE ESTUDO</a></li>
        <li><a href="http://www.sequencialmatriculas.com.br/escolatecnica/" target="_blank" title="INSCREVA-SE">&raquo; INSCREVA-SE</a></li>
      </ul>
    </div>

    <div class="coluna">
      <ul>
        <li><a href="{{URL::route('escola.index')}}" title="Home">&raquo; Home</a></li>
        <li><a href="{{URL::route('escola.a-sequencial')}}" title="A Sequencial">&raquo; A Sequencial</a></li>
        <li><a href="{{URL::route('escola.novidades')}}" title="Novidades">&raquo; Novidades</a></li>
		<li><a href="{{URL::route('escola.eventos')}}" title="Eventos">&raquo; Eventos</a></li>
		<li><a href="{{URL::route('escola.depoimentos')}}" title="Depoimentos">&raquo; Depoimentos</a></li>
		<li><a href="{{URL::route('escola.perguntas-frequentes')}}" title="Perguntas Frequentes">&raquo; Perguntas Frequentes</a></li>
		<li><a href="{{URL::route('escola.trabalhe-conosco')}}" title="Trabalhe Conosco">&raquo; Trabalhe Conosco</a></li>
		<li><a href="{{URL::route('escola.ouvidoria')}}" title="Ouvidoria">&raquo; Ouvidoria</a></li>
		<li><a href="{{URL::route('escola.fale-conosco')}}" title="Fale Conosco">&raquo; Fale Conosco</a></li>
      </ul>
    </div>

    <div class="coluna coluna-cursos">
      <ul>
        @foreach($listaCursos->sortBy('titulo') as $curso)
        <li><a href="cursos-tecnicos/{{$curso->slug}}">&raquo; {{ mb_strtoupper($curso->titulo) }}</a></li>
        @endforeach
      </ul>
    </div>

    <div class="coluna">
      <ul>
        <li><a href="{{ Cache::get('dominios.portal-aluno', 'http://sequencialmatriculas.com.br/portaltec/aluno/login/') }}" target="_blank" title="PORTAL DO ALUNO">&raquo; PORTAL DO ALUNO</a></li>
        <li><a href="{{ Cache::get('dominios.portal-professor', 'http://sequencialonline.com.br/portaltec/professor/login/') }}" target="_blank" title="PORTAL DO PROFESSOR">&raquo; PORTAL DO PROFESSOR</a></li>
        <li><a href="http://sequencial.bnweb.org/scripts/bnportal/bnportal.exe/index" target="_blank" title="BIBLIOTECA ONLINE">&raquo; BIBLIOTECA ONLINE</a></li>
        <li class="link-busca"><a href="#" title="BUSCA"><img src="assets/escola/img/layout/ico-busca.png" alt="Busca" /> &raquo; BUSCA</a></li>
        <li style="margin-top:25px"><a href="{{URL::route('escola.termos-de-uso.index')}}">&raquo; TERMOS DE USO</a></li>
      </ul>
    </div>

  </div>

  <div class="assinatura">
    &copy; {{Date('Y')}} Escola Técnica Sequencial - Todos os direitos reservados | <a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank">Criação de sites: Trupe Agência Criativa</a>
  </div>

</footer>
