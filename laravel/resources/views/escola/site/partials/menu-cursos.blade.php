<div id="chamada-cursos">
    <h1>NOSSOS<br>CURSOS TÉCNICOS</h1>
    <p>A SEQUENCIAL tem sempre um curso perfeito pra você escolher o seu futuro!</p>
    <h2>MATRICULE-SE JÁ!</h2>
</div>

@if(count($listaCursos) > 0)
    <ul id="menu-cursos">
        @foreach($listaCursos as $curso)
            <li>
                <a href="cursos-tecnicos/{{$curso->slug}}"
                   title="{{$curso->titulo}}"
                   data-color="{{$curso->cor_curso}}"
                   style="background-color:{{$curso->cor_curso}}; border: 1px {{$curso->cor_curso}} solid;">
                    {{$curso->titulo}}
                </a>
            </li>
        @endforeach
    </ul>
@endif