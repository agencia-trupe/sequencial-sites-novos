@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-depoimentos">

    <div class="centralizar">

      <h1>DEPOIMENTOS</h1>

      <div class="coluna">

        @foreach($depoimentos as $indice => $dep)
          <div class="depoimento cke">
            @if($dep->imagem)
              <img src="assets/escola/img/depoimentos/{{$dep->imagem}}" alt="{{$dep->autor_nome}}" />
            @endif

            {!! $dep->texto !!}

            <div class="autor">
              {{$dep->autor_nome}}<br>
              {{$dep->autor_descricao}}<br>
            </div>
          </div>

          @if(count($depoimentos) > 1 && ($indice + 1) == ceil(count($depoimentos)/2))
            </div>
            <div class="coluna">
          @endif

        @endforeach

      </div>
    </div>
  </div>

@stop
