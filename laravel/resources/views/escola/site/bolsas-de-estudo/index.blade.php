@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-bolsas-de-estudo">
        <div class="centralizar">

            <aside>
                @include('escola.site.partials.menu-cursos')
            </aside>

            <section>
                <h1 class="main-title">{{$conteudo->titulo}}</h1>
                <div class="colunas">
                    <div class="coluna">
                        <img src="assets/escola/img/bolsas-de-estudo/{{$conteudo->imagem}}"
                             alt="Veja como obter descontos nos cursos técnicos da escola sequencial">
                    </div>
                    <div class="coluna">
                        <h2>{{$conteudo->olho}}</h2>
                        <div class="cke">{!! $conteudo->texto !!}</div>
                    </div>
                </div>

                @include('escola.site.partials.menu-cursos-por-area', ['titulo' => 'ESCOLHA SEU CURSO TÉCNICO:'])

            </section>

        </div>
    </div>

@stop
