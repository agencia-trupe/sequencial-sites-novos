@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-cursos-tecnicos">
        <div class="centralizar">

            <aside>
                @include('escola.site.partials.menu-cursos')
            </aside>

            <section>
               <div class="cursos-colunas">

                   <div class="cursos-coluna">
                        <div class="cursos-box" style="background-color: {{$curso->cor_curso}}">
                            <h1><small>CURSO {{$curso->prefixo}}</small> {{$curso->titulo}}</h1>
                            <img src="assets/escola/img/cursos/{{$curso->imagem}}" alt="{{$curso->tituloCompleto}}">
                        </div>

                        <div class="cursos-unidades">
                            <p>Este curso é aplicado nas unidades:</p>
                            @foreach($curso->unidade as $unidade)
                                <a href="unidades/{{$unidade->slug}}" title="Unidade {{$unidade->titulo}}" style="color:{{$unidade->cor_unidade}}">&raquo; Unidade <strong>{{$unidade->titulo}}</strong></a>
                            @endforeach
                        </div>
                   </div>

                   <div class="cursos-coluna">
                        <div class="cursos-detalhes">
                            @if($curso->video)
                            <style>
                                .embed-container {
                                    position: relative;
                                    padding-bottom: 56.25%;
                                    margin-bottom: 20px;
                                    height: 0;
                                    overflow: hidden;
                                    max-width: 100%;
                                }
                                .embed-container iframe,
                                .embed-container object,
                                .embed-container embed {
                                    position: absolute;
                                    top: 0;
                                    left: 0;
                                    width: 100%;
                                    height: 100%;
                                }
                            </style>
                            <div class="embed-container">
                                <iframe src="https://www.youtube.com/embed/{{ $curso->video }}" frameborder="0" allowfullscreen></iframe>
                            </div>
                            @endif

                            <div class="cke">
                                {!! $curso->texto !!}
                            </div>

                            @if($curso->grade_curricular_arquivo)
                                <div class="topico">
                                    <h2>CONHEÇA A GRADE CURRICULAR DO CURSO</h2>
                                    @include('escola.site.cursos-tecnicos._download-grade', compact('curso'))
                                </div>
                            @endif

                            @foreach($curso->topicos as $topico)
                                <div class="topico">
                                    <h3>{{$topico->titulo}}</h3>
                                    <div class="cke">{!! $topico->texto !!}</div>
                                </div>
                            @endforeach
                        </div>
                   </div>

               </div>
            </section>

        </div>
    </div>

@stop
