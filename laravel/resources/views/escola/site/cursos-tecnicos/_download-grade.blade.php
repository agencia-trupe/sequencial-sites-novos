<div class="download-grade">
  <div class="download-grade__titulo">
    <img src="{{ asset('assets/escola/img/layout/icone-download-grade.png') }}" alt="">
    <span>OBTER A GRADE CURRICULAR</span>
  </div>

  <form action="{{ route('escola.cursos-tecnicos.post-grade', $curso->slug) }}" class="download-grade__form" data-token="{{ csrf_token() }}">
    <input type="email" name="email" placeholder="e-mail" required>
    <input type="submit" value="ENVIAR">
  </form>

  <div class="download-grade__success">
    A Sequencial agradece o interesse. A Grade Curricular foi enviada no seu e-mail.
    Para mais dúvidas ligue: 11 3371 2828
    <a href="#" class="download-grade__voltar">[VOLTAR]</a>
  </div>
</div>

<style>
  .download-grade {
    width: 100%;
    max-width: 435px;
    box-sizing: border-box;
    margin: 15px 0 25px;
    padding: 15px;
    border: 1px solid #06549C;
    cursor: pointer;
    font-size: 1rem;
    font-family: Asap, sans-serif;
  }
  .download-grade.success, .download-grade.form { cursor: default; }
  .download-grade__form, .download-grade__success { display: none; }
  .download-grade.form .download-grade__form { display: flex; }
  .download-grade.success .download-grade__success { display: block; }
  .download-grade__titulo {
    display: flex;
    align-items: center;
    color: #06549C;
  }
  .download-grade__titulo img { margin-right: 15px; }
  .download-grade__form {
    margin-top: 15px;
    width: 100%;
  }
  .download-grade__form input[type=email] {
    flex: 1;
    min-width: 0;
    background: #E3FAFF;
    padding: 0 10px;
    color: #06549C;
    outline: 0;
  }
  .download-grade__form input[type=submit] {
    padding: 8px;
    background: #00CCFF;
    color: #06549C;
    font-weight: bold;
    font-size: 13px;
    cursor: pointer;
    outline: 0;
  }
  .download-grade__form.sending input[type=submit] {
    cursor: wait;
    opacity: .65;
  }
  .download-grade__success {
    font-size: 14px;
    line-height: 1.3;
    margin-top: 15px;
    color: #00CCFF;
  }
  .download-grade__success a {
    font-weight: bold;
    color: #06549C;
  }
</style>

<script>
(function() {
  var submitForm = function(event) {
    event.preventDefault();

    var $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
      type: 'POST',
      url: $form.attr('action'),
      headers: {
        'X-CSRF-TOKEN': $form.data('token')
      },
      data: {
        email: $form.find('input[name=email]').val(),
      },
    }).done(function() {
      $form[0].reset();
      $form.parent().removeClass('form').addClass('success');
    }).fail(function(data) {
      if (data.status !== 422) {
        alert('Ocorreu um erro. Tente novamente.');
      } else {
        alert('Insira um endereço de e-mail válido.');
      }
    }).always(function() {
      $form.removeClass('sending');
    });
  };

  $(document).ready(function() {
    $('.download-grade').click(function() {
      if ($(this).hasClass('open')) return;
      $(this).addClass('open form');
    });

    $('.download-grade__voltar').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      $('.download-grade').removeClass('success open');
    })

    $('.download-grade__form').submit(submitForm);
  });
})();
</script>
