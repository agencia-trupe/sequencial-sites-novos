@extends('escola.site.template')

@section('conteudo')

    <div class="conteudo conteudo-cursos-tecnicos">
        <div class="centralizar">

            <aside>
                @include('escola.site.partials.menu-cursos')
            </aside>

            <section>
                <p class="olho">
                    Os cursos da Sequencial são especialmente criados para garantir seu melhor desempenho no mercado de trabalho.
                    Sempre atuais e completo os cursos formam profissionais altamente capacitados para atuarem em suas áreas. A Sequencial
                    também mantém convênio com empresas e entidades para facilitar a realização de estágios profissionais.
                </p>

                @include('escola.site.partials.menu-cursos-por-area')

                <div id="curso-por-unidade">
                    <h1>ESCOLHA SEU CURSO POR UNIDADE:</h1>
                    @foreach($listaUnidades as $unidade)
                        <div style="background-color:{{$unidade->cor_unidade}}">
                            <p>UNIDADE</p>
                            <h2>{{$unidade->titulo}}</h2>
                            <ul>
                                @foreach($unidade->cursos as $curso)
                                    <li>
                                        <a href="cursos-tecnicos/{{$curso->slug}}"
                                           title="{{$curso->tituloCompleto}}">
                                            {{$curso->tituloCompleto}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </section>

        </div>
    </div>

@stop
