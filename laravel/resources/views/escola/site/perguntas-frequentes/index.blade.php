@extends('escola.site.template')

@section('conteudo')

  <div class="conteudo conteudo-perguntas-frequentes">
    <div class="centralizar">

      <h1 class="main-title">PERGUNTAS FREQUENTES</h1>

      <aside>
        <ul id="faq-lista-cursos">
          @foreach($cursosComFaq as $k =>  $curso)
            <li>
              <a href="#faq-{{$curso->slug}}" @if($k == 0) class="aberto" @endif title="{{$curso->tituloCompleto}}">{{$curso->tituloCompleto}}</a>
            </li>
          @endforeach
        </ul>
      </aside>

      <section>
        @foreach($cursosComFaq as $k =>  $curso)
          <div id="faq-{{$curso->slug}}" class="secao-faq @if($k == 0) aberto @endif">
            @foreach($curso->faq as $questao)
              <div class="questao">
                <a href="#faq-{{$curso->slug}}-questao-{{$questao->id}}" title="{{$questao->titulo}}" class="faq-title">
                  {{$questao->titulo}}
                </a>
                <div class="resposta">
                  <div class="cke">
                    {!! $questao->texto !!}
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        @endforeach
      </section>

    </div>
  </div>

@stop
