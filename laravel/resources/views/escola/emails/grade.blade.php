<!DOCTYPE html>
<html>
<head>
    <title>Escola Técnica Sequencial - Grade Curricular ({{ $curso->titulo }})</title>
    <meta charset="utf-8">
</head>
<body>
  <span style='color:#000;font-size:14px;font-family:Verdana;'>
    Olá! Tudo bem?<br><br>

    Que bom que você tem interesse no curso de {{ $curso->titulo }} da Escola Técnica Sequencial.<br><br>

    <a href="{{ route('escola.cursos-tecnicos.grade', compact('slug', 'email', 'token')) }}" style="font-weight:bold;text-decoration:underline">CLIQUE AQUI PARA FAZER DOWNLOAD DA GRADE CURRICULAR DO CURSO</a>

    <br><br>

    <span style="font-style:italic">Faculdade Sequencial</span>
  </span>
</body>
</html>
