<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Domínios</title>
</head>
<body>
  <div class="wrapper">
    <form action="{{ url()->current() }}" method="POST">
      {!! csrf_field() !!}

      <input type="hidden" name="token" value="{{ request()->token }}">
      <label>
        <span>Portal do Aluno:</span>
        <input type="text" name="portal-aluno" value="{{ Cache::get('dominios.portal-aluno') }}">
      </label>
      <label>
        <span>Portal do Professor:</span>
        <input type="text" name="portal-professor" value="{{ Cache::get('dominios.portal-professor') }}">
      </label>
      <input type="submit" value="SALVAR">
    </form>
  </div>

  <style>
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
    html, body {
      height: 100%;
    }
    body {
      font-family: 'Arial', sans-serif;
      color: #333;
      background: #eee;
    }
    .wrapper {
      display:flex;
      align-items:center;
      justify-content:center;
      padding: 0;
      height: 100%;
    }
    form {
      width: 100%;
      max-width: 600px;
      box-shadow: 0 5px 10px rgba(0,0,0,.1);
      padding: 35px;
      display: flex;
      flex-direction: column;
      background: #fff;
      border-radius: 4px;
    }
    label {
      margin-bottom: 20px;
    }
    label span {
      display: block;
      font-size: 13px;
      margin-bottom: 4px;
    }
    input[type=text] {
      width: 100%;
      height: 40px;
      padding: 0 10px;
      outline: 0;
    }
    input[type=submit] {
      align-self: flex-end;
      background: teal;
      color: #fff;
      border: 0;
      padding: 1em 2.5em;
      cursor: pointer;
    }
  </style>
</body>
</html>
