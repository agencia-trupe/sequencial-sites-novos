<div class="centralizar">
  <div id="chamadas">

    <a href="http://www.faculdadesequencial.com.br" title="Faculdade Sequencial" target="_blank" class="link-faculdade">
      <h1>FACULDADE</h1>
      <h2>GRADUAÇÃO E PÓS-GRADUAÇÃO</h2>
      <div class="imagem">
        <img src="assets/grupo/img/marca-faculade-branco.png" alt="Faculdade Sequencial" />
      </div>
      <span>visitar o site <img src="assets/grupo/img/setinha-branco.png" alt="->" /></span>
    </a>

    <a href="http://www.sequencialctp.com.br" title="Sequencial Escola Técnica" target="_blank" class="link-escola">
      <h1>ESCOLA TÉCNICA</h1>
      <h2>CURSOS TÉCNICOS PROFISSIONALIZANTES</h2>
      <div class="imagem">
        <img src="assets/grupo/img/marca-escola-branco.png" alt="Sequencial Escola Técnica" />
      </div>
      <span>visitar o site <img src="assets/grupo/img/setinha-branco.png" alt="->" /></span>
    </a>

    <a href="http://www.sequencialead.com.br" title="EAD Sequencial" target="_blank" class="link-ead">
      <h1>EAD SEQUENCIAL</h1>
      <h2>ENSINO À DISTÂNCIA</h2>
      <div class="imagem">
        <img src="assets/grupo/img/marca-ead-branco.png" alt="EAD Sequencial" />
      </div>
      <span>visitar o site <img src="assets/grupo/img/setinha-branco.png" alt="->" /></span>
    </a>

  </div>
</div>
