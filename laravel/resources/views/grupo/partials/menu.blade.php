<header>
  <div clas="centralizar">

    <nav>
      <ul>
        <li>
          <a href="{{ URL::route('grupo.nosso-historico') }}" title="NOSSO HISTÓRICO" @if(str_is('grupo.nosso-historico', Route::currentRouteName())) class='ativo' @endif >NOSSO HISTÓRICO</a>
        </li>
        <li>
          <a href="{{ URL::route('grupo.faculdade') }}" title="FACULDADE" @if(str_is('grupo.faculdade', Route::currentRouteName())) class='ativo' @endif >FACULDADE</a>
        </li>
        <li>
          <a href="{{ URL::route('grupo.escola-tecnica') }}" title="ESCOLA TÉCNICA" @if(str_is('grupo.escola-tecnica', Route::currentRouteName())) class='ativo' @endif >ESCOLA TÉCNICA</a>
        </li>
        <li>
          <a href="{{ URL::route('grupo.ensino-a-distancia') }}" title="ENSINO A DISTÂNCIA" @if(str_is('grupo.ensino-a-distancia', Route::currentRouteName())) class='ativo' @endif >ENSINO A DISTÂNCIA</a>
        </li>
        <li>
          <a href="{{ URL::route('grupo.fale-conosco') }}" title="FALE CONOSCO" @if(str_is('grupo.fale-conosco', Route::currentRouteName())) class='ativo' @endif >FALE CONOSCO</a>
        </li>
      </ul>
    </nav>

    <div id="link-home">
      <a href="{{ URL::route('grupo.index') }}" title="Página Inicial">
        <img src="assets/grupo/img/marca-grupo-sequencial.png" alt="Grupo Sequencial" />
      </a>
    </div>

  </div>
</header>
