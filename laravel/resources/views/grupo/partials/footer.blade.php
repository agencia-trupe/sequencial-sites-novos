<footer>

  <div clas="centralizar">
    <div class="central-atendimento">
      NOSSA CENTRAL DE ATENDIMENTO SEQUENCIAL <span class="telefone">11 <strong>3371·2828</strong></span>
      <small>Segunda à Sexta · das 7h às 21h</small>
    </div>
  </div>

  <div class="assinatura">
    <div clas="centralizar">

      <ul>
        <li>
          <a href="http://www.faculdadesequencial.com.br" title="FACULDADE SEQUENCIAL" target="_blank">FACULDADE SEQUENCIAL</a>
        </li>
        <li>
          <a href="http://www.sequencialctp.com.br" title="ESCOLA TÉCNICA SEQUENCIAL" target="_blank">ESCOLA TÉCNICA SEQUENCIAL</a>
        </li>
        <li>
          <a href="http://www.sequencialead.com.br" title="EAD SEQUENCIAL" target="_blank">EAD SEQUENCIAL</a>
        </li>
      </ul>

      <div class="copyright">
        &copy; <?php echo Date("Y"); ?> GRUPO SEQUENCIAL - &middot; Todos os direitos reservados | <a href="http://www.trupe.net" title="Criação de site: Trupe Agência Criativa" target="_blank">Criação de site: Trupe Agência Criativa</a>
      </div>
    </div>
  </div>

</footer>

<script defer src="assets/grupo/js/site.js"></script>
