@extends('grupo.template')

@section('conteudo')

  <div class="area-banner">
  	<div class="centralizar">
  		<div class="banner banner-historico">

  			<p>
          Uma história de ousadia, superação e liderança <br>
          alicerçada em quatro valores fundamentais: <br>
          o saber, a ética, o trabalho e o progresso.
  			</p>

  		</div>
  	</div>
  </div>

  <div class="conteudo centralizar">
    <div class="coluna esquerda">
      <img src="assets/grupo/img/img-unidades.png" alt="Grupo Sequencial" />
    </div>
    <div class="coluna direita">
      <p>Assim pode ser definida a trajetória do Grupo Sequencial, que, em 2017, completa 15 anos com uma presença relevante e empreendedora nas áreas em que atua.</p>

      <p>Em 2002, seus fundadores decidiram acreditar num sonho e mudar os rumos de sua própria história. </p>

      <p>Movidos pela determinação e forte espírito empreendedor, eles influenciaram o destino de diversas gerações de estudantes ao desenvolver um novo padrão e metodologia de ensino focado na conquista de uma profissão.</p>

      <p>É essa história, pautada por um forte empreendedorismo, que você acompanha na Linha do Tempo do Grupo Sequencial.</p>

      <p>Guiado pelos quatro pilares da educação : Aprender a conhecer, aprender a fazer, aprender a ser e aprender a viver juntos  o Grupo Sequencial trabalha alinhado à sua política de responsabilidade social que tem como foco a educação e a inclusão social, presente em suas unidades de ensino na zona Sul (Capão Redondo, Parque Maria Helena e Grajaú), Zona Norte (Brasilândia) e Zona Leste (Itaim Paulista) do município de São Paulo.</p>

      <p>Buscamos sempre incentivar e fazer valer a pena a conquista pelo esforço e dedicação.</p>
    </div>
  </div>

  @include('grupo.partials.chamadas')

@stop
