<!DOCTYPE html>
<html lang="pt-BR" class="no-js">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2016 Trupe Design" />
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <title>Grupo Sequencial</title>

  <meta name="keywords" content="" />
  <meta name="description" content="">

  <meta property="og:title" content="Grupo Sequencial"/>
  <meta property="og:description" content=""/>

  <meta property="og:site_name" content="Grupo Sequencial"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content="{{ base_url() }}"/>

  <base href="{{ base_url() }}">
  <script>var BASE = "{{ base_url() }}"</script>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Work+Sans:200,400,700" rel="stylesheet">

  <link rel="stylesheet" href="assets/grupo/css/site.css">

  <script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0=" crossorigin="anonymous"></script>

</head>

	<body>

    @include('grupo.partials.menu')

    @yield('conteudo')

    @include('grupo.partials.footer')

	</body>
</html>
