@extends('grupo.template')

@section('conteudo')

  @include('grupo.partials.chamadas')

  <div class="area-banner">
    <div class="centralizar cycle-slideshow"
          data-slides="> .banner"
          data-pause-on-hover="true">

      <div class="banner banner-home">

        <p>
          Nosso foco é formar profissionais completos,<br>
          com alta qualidade de ensino, prontos e<br>
          atualizados para o mercado de trabalho.
        </p>

        <p class="destaque">
          O <strong>GRUPO EDUCACIONAL SEQUENCIAL</strong> é formado atualmente pela<br>
          <strong>Faculdade Sequencial</strong>, pela <strong>Escola Técnica Sequencial</strong> e pelo<br>
          <strong>EAD Sequencial</strong>, oferece cursos de graduação, cursos técnicos,<br>
          pós-graduação, cursos livres e ensino à distância. Suas unidades<br>
          de ensino estão nas zonas Norte, Sul e Leste da cidade de São Paulo.
        </p>

      </div>

      <div class="banner banner-padrao">
        <a href="http://www.sequencialmatriculas.com.br/faculdade/" target="_blank" title="Faculdade Sequencial">
          <img src="assets/grupo/img/banner-faculdade-novo.png" alt="Faculdade Sequencial" />
        </a>
      </div>

      <div class="banner banner-padrao">
        <a href="http://www.sequencialmatriculas.com.br/escolatecnica/" target="_blank" title="Escola Técnica Sequencial">
          <img src="assets/grupo/img/bannerGrupo-escolaTecnica.png" alt="Escola Técnica Sequencial" />
        </a>
      </div>

    </div>
  </div>

@stop
