@extends('grupo.template')

@section('conteudo')

  <div class="area-banner">
		<div class="centralizar">
			<div class="banner banner-contato">

          <div class="tel">
            CENTRAL DE ATENDIMENTO SEQUENCIAL
            <strong><span>11</span> 3371&bull;2828</strong>
          </div>

        <form action="{{ URL::route('grupo.enviar-contato') }}" method="post">

          {{ csrf_field() }}

          @if(session('contato_enviado'))
            <div class="resposta-form">
              Mensagem enviada com sucesso!<br>
              Entraremos em contato assim que possível!
            </div>
          @endif

          @if($errors->any())
            <div class="resposta-form erro">
              {{ $errors->first() }}
            </div>
          @endif

          <div class="colunas">

            <div class="coluna">
              <div class="coluna reduzida">
                <p>
                  Caso prefira<br>
                  envie-nos uma<br>
                  mensagem:
                </p>
              </div>
              <div class="coluna ampliada espaco-direita">
                <input type="text" name="nome" placeholder="nome" required>
                <input type="email" name="email" placeholder="e-mail" required>
                <input type="text" name="telefone" placeholder="telefone">
              </div>
            </div>

            <div class="coluna">
              <div class="coluna ampliada espaco-esquerda">
                <textarea name="mensagem" required placeholder="mensagem"></textarea>
              </div>

              <div class="coluna reduzida">
                <input type="submit" value="ENVIAR">
              </div>
            </div>

          </div>

        </form>

			</div>
		</div>
	</div>

  @include('grupo.partials.chamadas')

@stop
