@extends('grupo.template')

@section('conteudo')

  <div class="area-banner">
		<div class="centralizar">
			<div class="banner banner-internas">
        <img src="assets/grupo/img/marca-tecnico-gde.png" alt="Escola Técnica Sequencial" />
			</div>
		</div>
	</div>

  <div class="conteudo conteudo-internas">
    <div class="centralizar">

      <h1>
        NOSSOS CURSOS TÉCNICOS
      </h1>

      <ul>
        <li>TÉCNICO EM <strong>ADMINISTRAÇÃO</strong></li>
        <li>TÉCNICO EM <strong>CONTABILIDADE</strong></li>
        <li>TÉCNICO EM <strong>ELETRÔNICA</strong></li>
        <li>TÉCNICO EM <strong>ELETROTÉCNICA</strong></li>
        <li>TÉCNICO EM <strong>ENFERMAGEM</strong></li>
        <li>TÉCNICO EM <strong>ESTÉTICA</strong></li>
        <li>TÉCNICO EM <strong>FARMÁCIA</strong></li>
        <li>TÉCNICO EM <strong>LOGÍSTICA</strong></li>
        <li>TÉCNICO EM <strong>MEIO AMBIENTE</strong></li>
        <li>TÉCNICO EM <strong>RADIOLOGIA</strong></li>
        <li>TÉCNICO EM <strong>SECRETARIADO</strong></li>
        <li>TÉCNICO EM <strong>SEGURANÇA DO TRABALHO</strong></li>
      </ul>

      <a href="http://www.sequencialctp.com.br" target="_blank" title="SAIBA MAIS NO SITE" class="saiba-mais">
        SAIBA MAIS NO SITE: <img src="assets/grupo/img/marca-tecnico-peq.png" alt="Escola Técnica Sequencial" />
      </a>

    </div>
  </div>

  @include('grupo.partials.chamadas')

@stop
