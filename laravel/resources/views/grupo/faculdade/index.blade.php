@extends('grupo.template')

@section('conteudo')

  <div class="area-banner">
		<div class="centralizar">
			<div class="banner banner-internas">
        <img src="assets/grupo/img/marca-faculdade-gde.png" alt="Faculdade Sequencial" />
			</div>
		</div>
	</div>

  <div class="conteudo conteudo-internas">
    <div class="centralizar">

      <h1>
        NOSSOS CURSOS DE GRADUAÇÃO E PÓS-GRADUAÇÃO
      </h1>

      <ul>
        <li>
          BACHARELADO EM <strong>ADMINISTRAÇÃO</strong>
        </li>
        <li>
          LICENCIATURA EM <strong>CIÊNCIAS BIOLÓGICAS</strong>
        </li>
        <li>
          BACHARELADO EM <strong>ENFERMAGEM</strong>
        </li>
        <li>
          TECNÓLOGO EM <strong>GESTÃO DE RECURSOS HUMANOS</strong>
        </li>
        <li>
          TECNÓLOGO EM <strong>LOGÍSTICA</strong>
        </li>
        <li>
          LICENCIATURA EM <strong>PEDAGOGIA</strong>
        </li>
      </ul>

      <a href="http://www.faculdadesequencial.com.br" target="_blank" title="SAIBA MAIS NO SITE" class="saiba-mais">
        SAIBA MAIS NO SITE: <img src="assets/grupo/img/marca-faculdade-peq.png" alt="Faculdade Sequencial" />
      </a>

    </div>
  </div>

  @include('grupo.partials.chamadas')

@stop
