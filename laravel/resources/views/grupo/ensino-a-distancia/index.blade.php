@extends('grupo.template')

@section('conteudo')

  <div class="area-banner">
		<div class="centralizar">
			<div class="banner banner-internas">
        <img src="assets/grupo/img/marca-ead-gde.png" alt="Ensino à distância Sequencial" />
			</div>
		</div>
	</div>

  <div class="conteudo conteudo-internas">
    <div class="centralizar">

      <a href="http://www.sequencialead.com.br" target="_blank" title="SAIBA MAIS NO SITE" class="saiba-mais">
        SAIBA MAIS NO SITE: <img src="assets/grupo/img/marca-ead-peq.png" alt="Ensino à distância Sequencial" />
      </a>

    </div>
  </div>

  @include('grupo.partials.chamadas')

@stop
