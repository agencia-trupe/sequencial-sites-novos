<?php

namespace Sequencial\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

      setlocale(LC_ALL, 'pt_BR.utf8');

      $this->app->bind('path.public', function() {
        return app_path().'/../../'.ENV('PUBLIC_FOLDER', 'public/');
      });
    }
}
