<?php

namespace Sequencial\Models\Faculdade;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{

  protected $table = 'eventos';
  protected $connection = 'mysql_faculdade';
  protected $hidden = ['id'];
  protected $dates = [
    'data',
    'created_at',
    'updated_at'
  ];

  public function setDataAttribute($value){
    $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeProximos($query)
  {
    return $query->where('data', '>=', date('Y-m-d'));
  }

  public function scopeHistorico($query)
  {
    return $query->where('data', '<', date('Y-m-d'));
  }

  public function imagens()
  {
    return $this->hasMany('Sequencial\Models\Faculdade\EventoImagem', 'eventos_id')->orderBy('ordem', 'asc');
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo', 'like', '%' . $termo . '%')
                 ->orWhere('subtitulo', 'like', '%' . $termo . '%')
                 ->orWhere('texto', 'like', '%' . $termo . '%');
  }
}
