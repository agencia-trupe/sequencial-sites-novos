<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class CookieConsent extends Model
{
  protected $table      = 'cookie_consent';
  protected $connection = 'mysql_faculdade';
  protected $guarded    = ['id'];
}
