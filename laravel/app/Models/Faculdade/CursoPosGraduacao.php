<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class CursoPosGraduacao extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos_pos_graduacao';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function area()
  {
    return $this->belongsTo('Sequencial\Models\Faculdade\AreaPosGraduacao', 'areas_pos_graduacao_id');
  }

  public function topicos()
  {
    return $this->hasMany('Sequencial\Models\Faculdade\CursoPosGraduacaoTopicos', 'cursos_pos_graduacao_id')->orderBy('ordem', 'asc');
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo', 'like', '%' . $termo . '%')
                ->orWhereHas('topicos', function($query) use ($termo){
                  $query->where('titulo', 'like', '%' . $termo . '%')
                        ->orWhere('texto', 'like', '%' . $termo . '%');
                });
  }
}
