<?php

namespace Sequencial\Models\Faculdade;

class TesteVocacional
{
    const etapas = [
        [
            'questao' => 'Coisas que mais gosto de fazer:',
            'opcoes'  => [
                'A' => 'Praticar esportes.',
                'B' => 'Dirigir.',
                'C' => 'Compartilhar atividades.',
                'D' => 'Refletir sobre meus sentimentos.',
                'E' => 'Debater ideias.',
                'F' => 'Ordenar coisas.',
                'G' => 'Cantar.'
            ]
        ],
        [
            'questao' => 'Tenho facilidade em:',
            'opcoes'  => [
                'A' => 'Aprender novos esportes.',
                'B' => 'Executar tarefas delicadas.',
                'C' => 'Trabalhar em equipe.',
                'D' => 'Analisar meus sentimentos.',
                'E' => 'Contar histórias e fatos.',
                'F' => 'Construir e organizar planilhas.',
                'G' => 'Tocar instrumentos.'
            ]
        ],
        [
            'questao' => 'Quando estou no trabalho ou na escola prefiro:',
            'opcoes'  => [
                'A' => 'Levantar e andar periodicamente.',
                'B' => 'Fazer algo funcionar.',
                'C' => 'Trabalhar com pessoas.',
                'D' => 'Trabalhar sozinho.',
                'E' => 'Conversar sobre ideias.',
                'F' => 'Analisar dados.',
                'G' => 'Identificar padrões sonoros em equipamentos.'
            ]
        ],
        [
            'questao' => 'O tipo de pergunta que mais faço é:',
            'opcoes'  => [
                'A' => 'Onde?',
                'B' => 'Como?',
                'C' => 'Quem?',
                'D' => 'Para quê?',
                'E' => 'Por quê?',
                'F' => 'O quê?',
                'G' => 'Quando?'
            ]
        ],
        [
            'questao' => 'No tempo livre gosto mais de:',
            'opcoes'  => [
                'A' => 'Dançar.',
                'B' => 'Fazer um trabalho manual.',
                'C' => 'Sair com os amigos.',
                'D' => 'Meditar e refletir.',
                'E' => 'Ler um livro.',
                'F' => 'Passar o tempo com jogos de estratégia.',
                'G' => 'Ouvir música.'
            ]
        ],
        [
            'questao' => 'Tenho facilidade em:',
            'opcoes'  => [
                'A' => 'Adquirir habilidades pela prática.',
                'B' => 'Analisar e descobrir formas e detalhes.',
                'C' => 'Ouvir e compartilhar ideias.',
                'D' => 'Elaborar teorias.',
                'E' => 'Discutir informações.',
                'F' => 'Obter e classificar informações.',
                'G' => 'Ler ouvindo música.'
            ]
        ],
        [
            'questao' => 'Em minha casa:',
            'opcoes'  => [
                'A' => 'Não fico parado(a).',
                'B' => 'Conserto as coisas.',
                'C' => 'Ajudo outros nas tarefas.',
                'D' => 'Fico em meu canto.',
                'E' => 'Falo sobre meu dia.',
                'F' => 'Organizo cada detalhe.',
                'G' => 'Sempre escuto música.'
            ]
        ],
        [
            'questao' => 'As pessoas podem me definir por esta palavra:',
            'opcoes'  => [
                'A' => 'Esportista.',
                'B' => 'Competente.',
                'C' => 'Perceptivo.',
                'D' => 'Analítico.',
                'E' => 'Teórico.',
                'F' => 'Lógico.',
                'G' => 'Artista.'
            ]
        ],
        [
            'questao' => 'Gosto mais de aprender através de:',
            'opcoes'  => [
                'A' => 'Demonstrações e experiências.',
                'B' => 'Atividades estruturadas passo a passo.',
                'C' => 'Discussão de caso voltados para pessoas.',
                'D' => 'Leitura de livros-textos.',
                'E' => 'Palestras formais.',
                'F' => 'Exercícios de fatos, dados e números.',
                'G' => 'Histórias e música.'
            ]
        ],
        [
            'questao' => 'Eu me considero:',
            'opcoes'  => [
                'A' => 'Ágil.',
                'B' => 'Detalhista.',
                'C' => 'Amigo.',
                'D' => 'Sensível.',
                'E' => 'Comunicativo.',
                'F' => 'Racional.',
                'G' => 'Musical.'
            ]
        ]
    ];

    const inteligencias = [
        'A' => 'Cinestésica',
        'B' => 'Espacial',
        'C' => 'Interpessoal',
        'D' => 'Intrapessoal',
        'E' => 'Linguística',
        'F' => 'Lógico-matemática',
        'G' => 'Musical',
    ];

    const resultados = [
        'A' => [
            'texto' => 'controles de movimentos e do próprio corpo',
            'ocupacoes' => [
                'Atletas',
                'Mergulhadores',
                'Esportistas',
                'Profissionais da área de educação física',
                'Atores',
                'Bombeiros',
                'Chefes de Cozinha'
            ]
        ],
        'B' => [
            'texto' => 'compreensão e boa relação com imagens e espaço',
            'ocupacoes' => [
                'Desenhistas',
                'Arquitetos',
                'Engenheiros',
                'Planejadores de Espaços',
                'Consultores de beleza',
                'Profissionais que trabalham com cosméticos'
            ]
        ],
        'C' => [
            'texto' => 'percepção das sensações das outras pessoas, capacidade de relacionar-se com os outros, interpretação de comportamentos e comunicações, entende as relações entre as pessoas',
            'ocupacoes' => [
                'Profissionais de RH',
                'Professores',
                'Enfermeiros',
                'Terapeutas',
                'Psicólogos',
                'Líderes (gestores/administradores)'
            ]
        ],
        'D' => [
            'texto' => 'auto consciência, objetividade pessoal, capacidade de entender a relação de alguém com o outro e consigo mesmo',
            'ocupacoes' => [
                'Terapeutas',
                'Psicólogos',
                'Escritores',
                'Conselheiros',
                'Professores'
            ]
        ],
        'E' => [
            'texto' => 'palavras e línguas escritas e faladas, interpretação e explicação de ideias e informações',
            'ocupacoes' => [
                'Professores',
                'Advogados',
                'Escritores',
                'Jornalistas',
                'Profissionais do meio de comunicação (Rádio / TV)',
                'Poetas'
            ]
        ],
        'F' => [
            'texto' => 'pensamento lógico, análise de problemas, entende a relação entre causa e efeito, cálculos matemáticos',
            'ocupacoes' => [
                'Engenheiros',
                'Contadores',
                'Estatísticos',
                'Pesquisadores',
                'Profissionais da área de informática',
                'Administradores',
                'Empreendedores',
                'Matemáticos',
                'Biomédicos',
                'Profissionais de Farmácia'
            ]
        ],
        'G' => [
            'texto' => 'capacidade musical, avaliação e uso do som, entende a relação entre som e sensações',
            'ocupacoes' => [
                'Músicos',
                'Cantores',
                'Compositores',
                'Locutores',
                'Engenheiros Acústicos',
                'Produtores Musicais'
            ]
        ],
    ];
}
