<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class Inscricao extends Model
{
  protected $table      = 'inscricoes';
  protected $connection = 'mysql_faculdade';
  protected $guarded    = ['id'];

  public function getDataAttribute($date)
  {
      return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
  }
}
