<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
  protected $table      = 'leads';
  protected $connection = 'mysql_faculdade';
  protected $guarded    = ['id'];

  public function getDataAttribute()
  {
    return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y');
  }

  public function getDataTesteAttribute()
  {
    if ($data = $this->teste_vocacional) {
      return '<span class="label label-success">realizado em '.\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->teste_vocacional)->format('d/m/Y').'</span>';
    } else {
      return '<span class="label label-warning">não realizado</span>';
    }
  }

  public function getResultadoTesteAttribute()
  {
    $inteligencias = json_decode($this->teste_vocacional_resultado, true);

    if (! $inteligencias) {
      return null;
    }

    arsort($inteligencias);

    $maiorValor = array_values($inteligencias)[0];

    $string = '';
    foreach($inteligencias as $inteligencia => $resultado) {
      if ($inteligencia != array_keys($inteligencias)[0]) {
        $string .= '<br>';
      }
      if ($resultado == $maiorValor) {
        $string .= '<strong>';
      }
      $string .= TesteVocacional::inteligencias[$inteligencia];
      $string .= ': '.$resultado;
      if ($resultado == $maiorValor) {
        $string .= '</strong>';
      }
    }

    return $string;
  }
}
