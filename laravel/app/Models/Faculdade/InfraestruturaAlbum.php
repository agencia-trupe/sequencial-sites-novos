<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class InfraestruturaAlbum extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'infraestrutura_albuns';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('tipo', 'infraestrutura')->orderBy('ordem', 'asc');
  }

  public function imagens()
  {
    return $this->hasMany('Sequencial\Models\Faculdade\InfraestruturaAlbumImagem', 'infraestrutura_albuns_id')->orderBy('ordem', 'asc');
  }
}
