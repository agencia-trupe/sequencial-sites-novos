<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class Chamada extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'chamadas';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function getTipoExtensoAttribute()
  {
    return strtoupper(str_replace('-', ' ', $this->tipo));
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeSemCalhaus($query)
  {
    return $query->where('is_calhau', 0);
  }

  public function scopeCalhaus($query)
  {
    return $query->where('is_calhau', 1);
  }

}
