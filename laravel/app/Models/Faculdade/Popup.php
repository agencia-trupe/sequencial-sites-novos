<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

class Popup extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'popups';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'mostrar_todas_paginas',
    'imagem',
    'data_inicio',
    'data_fim'
  ];

  protected $dates = [
    'data_inicio',
    'data_fim',
    'created_at',
    'updated_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataInicioAttribute($value){
    $this->attributes['data_inicio'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
  }

  public function setDataFimAttribute($value){
    $this->attributes['data_fim'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
  }

  public function scopeAtivo($query)
  {
    $hoje = Carbon::now();

    return $query->where('data_inicio', '<=', $hoje)->where('data_fim', '>=', $hoje);
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data_fim', 'desc');
  }
}
