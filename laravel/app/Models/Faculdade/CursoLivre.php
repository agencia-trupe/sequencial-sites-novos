<?php

namespace Sequencial\Models\Faculdade;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class CursoLivre extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos_livres';
  protected $connection = 'mysql_faculdade';

  protected $dates = [
    'data',
    'created_at',
    'updated_at'
  ];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function setDataAttribute($value){
    $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeProximos($query)
  {
    return $query->where('data', '>=', date('Y-m-d'));
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo', 'like', '%' . $termo . '%')
                 ->orWhere('texto', 'like', '%' . $termo . '%');
  }
}
