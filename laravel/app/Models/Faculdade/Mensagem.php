<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class Mensagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'mensagens';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nome',
    'cpf',
    'email',
    'telefone',
    'mensagem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }
}
