<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class CursoGraduacaoTopicos extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos_graduacao_topicos';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'cursos_graduacao_id',
    'titulo',
    'texto',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function curso()
  {
    return $this->belongsTo('Sequencial\Models\Faculdade\CursoGraduacao', 'cursos_graduacao_id');
  }
}
