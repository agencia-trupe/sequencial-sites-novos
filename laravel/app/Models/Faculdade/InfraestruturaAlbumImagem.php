<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class InfraestruturaAlbumImagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'infraestrutura_albuns_imagens';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'imagem',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function album()
  {
    return $this->belongsTo('Sequencial\Models\Faculdade\InfraestruturaAlbum', 'infraestrutura_albuns_id');
  }
}
