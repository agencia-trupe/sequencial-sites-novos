<?php

namespace Sequencial\Models\Faculdade;

use Illuminate\Database\Eloquent\Model;

class AreaPosGraduacao extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'areas_pos_graduacao';
  protected $connection = 'mysql_faculdade';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function cursos()
  {
    return $this->hasMany('Sequencial\Models\Faculdade\CursoPosGraduacao', 'areas_pos_graduacao_id')->orderBy('titulo', 'asc');
  }
}
