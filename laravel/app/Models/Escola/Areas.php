<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'areas';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function cursos()
  {
      return $this->hasMany('Sequencial\Models\Escola\Curso', 'areas_id')->orderBy('ordem', 'asc');
  }
}
