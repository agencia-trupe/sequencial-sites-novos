<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class PreInscricao extends Model
{
  protected $table      = 'pre_inscricoes';
  protected $connection = 'mysql_escola';
  protected $guarded    = ['id'];

  public function getDataAttribute($date)
  {
      return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
  }
}
