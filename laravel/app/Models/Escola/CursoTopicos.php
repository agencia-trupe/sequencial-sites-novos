<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class CursoTopicos extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos_topicos';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'cursos_id',
    'titulo',
    'texto',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function curso()
  {
    return $this->belongsTo('Sequencial\Models\Escola\Curso', 'cursos_id');
  }
}
