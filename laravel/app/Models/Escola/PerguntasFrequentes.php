<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class PerguntasFrequentes extends Model
{

	/**
	* The database table used by the model.
	*
	* @var string
	*/
	protected $table = 'perguntas_frequentes';
	protected $connection = 'mysql_escola';

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'curso_id',
		'titulo',
		'texto',
		'ordem'
	];

	/**
	* The attributes excluded from the model's JSON form.
	*
	* @var array
	*/
	protected $hidden = ['id'];

	public function curso()
	{
		return $this->belongsTo('Sequencial\Models\Escola\Curso', 'curso_id');
	}

	public function scopeOrdenado($query)
	{
		return $query->orderBy('ordem', 'asc');
	}

	public function scopeFiltroCurso($query, $cursoId)
	{
		return $query->where('curso_id', $cursoId);
	}
}
