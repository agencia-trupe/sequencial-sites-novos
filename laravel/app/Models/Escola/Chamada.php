<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class Chamada extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'chamadas';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function getTipoExtensoAttribute()
  {
    return strtoupper(str_replace('-', ' ', $this->tipo));
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function scopeSemCalhaus($query)
  {
    return $query->where('is_calhau', 0);
  }

  public function scopeCalhaus($query)
  {
    return $query->where('is_calhau', 1);
  }

}
