<?php

namespace Sequencial\Models\Escola;

use Carbon\Carbon as Carbon;
use Illuminate\Database\Eloquent\Model;

class Novidade extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'novidades';
  protected $connection = 'mysql_escola';
  protected $dates = [
    'data',
    'created_at',
    'updated_at'
  ];

  public function setDataAttribute($value){
    $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
  }

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'categoria',
    'data',
    'titulo',
    'subtitulo',
    'slug',
    'imagem',
    'texto'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('data', 'desc');
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo', 'like', '%' . $termo . '%')
                 ->orWhere('subtitulo', 'like', '%' . $termo . '%')
                 ->orWhere('texto', 'like', '%' . $termo . '%');
  }
}
