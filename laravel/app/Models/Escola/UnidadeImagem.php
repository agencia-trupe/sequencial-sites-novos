<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class UnidadeImagem extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'unidades_imagens';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'unidades_id',
    'imagem',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }
}
