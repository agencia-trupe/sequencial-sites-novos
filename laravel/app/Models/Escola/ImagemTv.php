<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class ImagemTv extends Model
{
    protected $table      = 'tv_imagens';
    protected $connection = 'mysql_escola';
    protected $guarded    = ['id'];

    const unidades = [
        'brasilandia'   => 'Brasilândia',
        'grajau'        => 'Grajaú',
        'capaoredondo'  => 'Capão Redondo',
        'itaimpaulista' => 'Itaim Paulista',
        'pirajussara'   => 'Pirajussara',
        'faculdade'     => 'Faculdade'
    ];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
