<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class MensagemFaleConosco extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'fale_conosco_mensagens';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'nome',
    'cpf',
    'email',
    'telefone',
    'mensagem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('created_at', 'desc');
  }
}
