<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'cursos';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function getTituloCompletoAttribute()
  {
      return $this->prefixo . ' ' . $this->titulo;
  }

  public function topicos()
  {
    return $this->hasMany('Sequencial\Models\Escola\CursoTopicos', 'cursos_id')->orderBy('ordem', 'asc');
  }

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem_geral', 'asc');
  }

  public function unidade()
  {
    return $this->belongsToMany('Sequencial\Models\Escola\Unidade', 'unidades_cursos', 'cursos_id', 'unidades_id');
  }

  public function faq()
  {
    return $this->hasMany('Sequencial\Models\Escola\PerguntasFrequentes', 'curso_id')->ordenado();
  }

  public function scopeBusca($query, $termo)
  {
    return $query->where('titulo', 'like', '%' . $termo . '%')
                 ->orWhere('texto', 'like', '%' . $termo . '%');
  }
}
