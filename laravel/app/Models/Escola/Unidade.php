<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class Unidade extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'unidades';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    '',
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function topicos()
  {
    return $this->hasMany('Sequencial\Models\Escola\UnidadeTopicos', 'unidades_id')->orderBy('ordem', 'asc');
  }

  public function imagens()
  {
    return $this->hasMany('Sequencial\Models\Escola\UnidadeImagem', 'unidades_id')->orderBy('ordem', 'asc');
  }

  public function cursos()
  {
      return $this->belongsToMany('Sequencial\Models\Escola\Curso', 'unidades_cursos', 'unidades_id', 'cursos_id');
  }
}
