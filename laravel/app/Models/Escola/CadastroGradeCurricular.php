<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class CadastroGradeCurricular extends Model
{
  protected $table = 'cadastros_grade_curricular';
  protected $connection = 'mysql_escola';

  protected $guarded = ['id'];
  protected $hidden = ['id'];

  public function curso()
  {
    return $this->belongsTo('Sequencial\Models\Escola\Curso', 'curso_id');
  }

  public function getDataAttribute($date)
  {
    return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i');
  }
}
