<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class UnidadeTopicos extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'unidades_topicos';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'unidades_id',
    'titulo',
    'texto',
    'ordem'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }

  public function unidade()
  {
    return $this->belongsTo('Sequencial\Models\Escola\Unidade', 'unidades_id');
  }

  public function scopeFiltro($query, $value)
  {
      return $query->where('tipo', $value);
  }
}
