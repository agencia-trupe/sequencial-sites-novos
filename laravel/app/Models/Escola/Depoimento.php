<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class Depoimento extends Model
{

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'depoimentos';
  protected $connection = 'mysql_escola';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'imagem',
    'texto',
    'autor_nome',
    'autor_descricao',
    'ordem',
    'created_at'
  ];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['id'];

  public function scopeOrdenado($query)
  {
    return $query->orderBy('ordem', 'asc');
  }
}
