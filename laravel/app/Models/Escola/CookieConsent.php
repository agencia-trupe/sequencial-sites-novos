<?php

namespace Sequencial\Models\Escola;

use Illuminate\Database\Eloquent\Model;

class CookieConsent extends Model
{
  protected $table      = 'cookie_consent';
  protected $connection = 'mysql_escola';
  protected $guarded    = ['id'];
}
