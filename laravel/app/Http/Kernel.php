<?php

namespace Sequencial\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \Sequencial\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \Sequencial\Http\Middleware\VerifyCsrfToken::class,
        ],

        'api' => [
            'throttle:60,1',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Sequencial\Http\Middleware\Authenticate::class,

        'authAdminEscola' => \Sequencial\Http\Middleware\AuthenticateAdminEscola::class,
        'authEstagiosEscola' => \Sequencial\Http\Middleware\AuthenticateEstagiosEscola::class,
        'authPreInscricoesEscola' => \Sequencial\Http\Middleware\AuthenticatePreInscricoesEscola::class,

        'authAdminFaculdade' => \Sequencial\Http\Middleware\AuthenticateAdminFaculdade::class,
        'authInscricoesFaculdade' => \Sequencial\Http\Middleware\AuthenticateInscricoesFaculdade::class,

        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'can' => \Illuminate\Foundation\Http\Middleware\Authorize::class,
        'guest' => \Sequencial\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    ];
}
