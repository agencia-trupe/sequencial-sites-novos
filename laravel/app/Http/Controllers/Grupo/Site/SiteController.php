<?php

namespace Sequencial\Http\Controllers\Grupo\Site;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;
use Mail;

class SiteController extends Controller
{

  public function getIndex(Request $request)
  {
    return view('grupo.home.index');
  }

  public function getNossoHistorico(Request $request)
  {
    return view('grupo.nosso-historico.index');
  }

  public function getFaculdade(Request $request)
  {
    return view('grupo.faculdade.index');
  }

  public function getEscolaTecnica(Request $request)
  {
    return view('grupo.escola-tecnica.index');
  }

  public function getEnsinoADistancia(Request $request)
  {
    return view('grupo.ensino-a-distancia.index');
  }

  public function getFaleConosco(Request $request)
  {
    return view('grupo.fale-conosco.index');
  }

  public function postEnviarContato(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'mensagem' => 'required'
    ]);

    $data['nome'] = $request->nome;
    $data['email'] = $request->email;
    $data['telefone'] = $request->telefone;
    $data['mensagem'] = $request->mensagem;

    Mail::send('grupo.emails.contato', $data, function($message) use ($data)
    {
      $message->to('contato@anossacozinha.com.br')
              ->subject('Contato via site - '.$data['nome'])
              ->bcc('bruno@trupe.net')
              ->replyTo($data['email'], $data['nome']);
    });

    $request->session()->flash('contato_enviado', true);

    return redirect()->route('grupo.fale-conosco');
  }

}
