<?php

namespace Sequencial\Http\Controllers\Escola\Site\Unidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Unidade;

class UnidadesController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.unidades.index');
	}

	public function getDetalhes(Request $request, $slug)
	{
		$unidade = Unidade::where('slug', $slug)->first();

        if (!$unidade)
            return redirect()->to('/unidades');

        $topicosEstrutura = $unidade->topicos()->filtro('infraestrutura')->get();
        $topicosLaboratorios = $unidade->topicos()->filtro('laboratorios')->get();

        return view('escola.site.unidades.detalhes', [
            'unidade' => $unidade,
            'topicosEstrutura' => $topicosEstrutura,
            'topicosLaboratorios' => $topicosLaboratorios
        ]);
	}
}
