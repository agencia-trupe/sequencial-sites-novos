<?php

namespace Sequencial\Http\Controllers\Escola\Site\Ouvidoria;

use Mail;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Ouvidoria;
use Sequencial\Models\Escola\MensagemOuvidoria;

class OuvidoriaController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.ouvidoria.index', [
			'conteudo' => Ouvidoria::first()
		]);
	}

	public function postEnviar(Request $request)
	{
    $this->validate($request, [
			'unidade' => 'required',
			'tipo' => 'required',
			'nome' => 'required',
			'cpf' => 'required',
			'telefone' => 'required',
			'email' => 'required|email',
			'mensagem' => 'required'
    ]);

		$data['unidade'] = $request->unidade;
		$data['tipo'] = $request->tipo;
    $data['nome'] = $request->nome;
    $data['email'] = $request->email;
    $data['telefone'] = $request->telefone;
    $data['cpf'] = $request->cpf;
    $data['mensagem'] = $request->mensagem;

    Mail::send('escola.emails.ouvidoria', $data, function($message) use ($data)
    {
			$message->to('contato@sequencialctp.com.br')
                    ->cc('rosilene.vasconcelos@sequencialctp.com.br')
                    ->cc('louise.missu@sequencialctp.com.br')
                    ->subject('Mensagem para Ouvidoria Sequencial Escola Técnica - '.$data['nome'])
        			->replyTo($data['email'], $data['nome']);
    });

    $registro = MensagemOuvidoria::create($data);
    $registro->save();

    $request->session()->flash('contato_enviado', true);

    return redirect()->route('escola.ouvidoria');
	}
}
