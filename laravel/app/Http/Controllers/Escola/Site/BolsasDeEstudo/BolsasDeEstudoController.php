<?php

namespace Sequencial\Http\Controllers\Escola\Site\BolsasDeEstudo;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\BolsasDeEstudo;

class BolsasDeEstudoController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.bolsas-de-estudo.index', [
			'conteudo' => BolsasDeEstudo::first()
		]);
	}
}
