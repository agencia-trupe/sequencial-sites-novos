<?php

namespace Sequencial\Http\Controllers\Escola\Site\Depoimentos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;

use Sequencial\Models\Escola\Depoimento;

class DepoimentosController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		$depoimentos = Depoimento::ordenado()->get();

		return view('escola.site.depoimentos.index', [
			'depoimentos' => $depoimentos
		]);
	}
}
