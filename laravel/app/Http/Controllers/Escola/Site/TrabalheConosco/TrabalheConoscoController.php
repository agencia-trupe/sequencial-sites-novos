<?php

namespace Sequencial\Http\Controllers\Escola\Site\TrabalheConosco;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\TrabalheConosco;

class TrabalheConoscoController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.trabalhe-conosco.index', [
			'conteudo' => TrabalheConosco::first()
		]);
	}
}
