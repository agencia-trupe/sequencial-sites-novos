<?php

namespace Sequencial\Http\Controllers\Escola\Site\Import;

use DB;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Evento;
use Sequencial\Models\Escola\EventoImagem;
use Sequencial\Libs\Thumbs;

class ImportJuramentosController extends SiteBaseController {

  protected $tiposNoticias = [
    '1' => 'Noticias',
    '2' => 'Juramentos',
    '3' => 'Eventos',
  ];

  protected $images;


  public function execute() {
    ini_set('default_socket_timeout', 900);
    echo '<h1>Import: Juramentos</h1>';

    /*
      Importação de Juramentos
      origem:
        connection: mysql_escola_antigo
        tabela: noticias
        campos: titulo, olho, textoMateria, dataCadastro, id_categorias, dataDivulgacao, data
      destino:
        model: Sequencial\Models\Escola\Evento
        tabela: eventos
        campos:
    */

    $noticiasAntigos = DB::connection('mysql_escola_antigo')->table('noticias')->where('id_categorias', '2')->get();

    DB::table('eventos')->where('is_juramento', '1')->delete();

    echo count($noticiasAntigos) . ' resultados<br><br>';

    foreach($noticiasAntigos as $k => $noticia) {
      echo $k . ' ) ' . $noticia->titulo . '<br>';

      $dataArray = [
        'data' => $this->setData($noticia),
        'titulo' => $noticia->titulo,
        'subtitulo' => $noticia->olho,
        'slug' => str_slug($noticia->titulo),
        'imagem' => '',
        'texto' => $this->tratarConteudo($noticia->textoMateria),
        'created_at' => date('Y-m-d H:i:s'),
        'is_juramento' => 1
      ];

      $novoId = DB::table('eventos')->insertGetId($dataArray);
      //buscar imagens em noticias_galeria.id_noticias
      $imagens = DB::connection('mysql_escola_antigo')->table('noticias_galeria')->where('id_noticias', $noticia->id)->get();

      foreach ($imagens as $j => $img) {
        $imgPath = 'img/site/eventos/' . $img->arquivo;

        $destino = 'assets/escola/img/eventos/originais/';
        $thumbs = [
          'assets/escola/img/eventos/redimensionadas/',
          'assets/escola/img/eventos/thumbs/',
          'assets/escola/img/eventos/thumbs-quadradas/',
          'assets/escola/img/eventos/thumbs-retangulares/'
        ];

        $filenameImg = 'eventos_imgs_' . date('YmdHis') . $img->arquivo;
        $imgfile = file_get_contents('http://sequencialctp.com.br/'.$imgPath);
        file_put_contents($destino.$filenameImg, $imgfile);

        foreach ($thumbs as $value) {
          $size = [
            'assets/escola/img/eventos/redimensionadas/' => [
              'width' => 1200,
              'height' => null
            ],
            'assets/escola/img/eventos/thumbs/' => [
              'width' => 400,
              'height' => null
            ],
            'assets/escola/img/eventos/thumbs-quadradas/' => [
              'width' => 300,
              'height' => 300
            ],
            'assets/escola/img/eventos/thumbs-retangulares/' => [
              'width' => 220,
              'height' => 165
            ],
          ];
          Thumbs::makeFromFile($destino, $filenameImg, $size[$value]['width'], $size[$value]['height'], public_path($value), 'rgba(255,255,255,1)', false);
        }

        $dataImageArray = [
          'eventos_id' => $novoId,
          'imagem' => $filenameImg,
          'ordem' => $j,
          'created_at' => date('Y-m-d H:i:s')
        ];

        DB::table('eventos_imagens')->insert($dataImageArray);
      }
    }

    echo '<hr>';
  }

  private function tratarConteudo($str)
  {
    $str = $this->buscarImagens($str);
    return $str;
  }

  private function buscarImagens($str)
  {
    $regex = '/<img.*src=\"(.*)\"/U';

    preg_match_all($regex, $str, $results);

    foreach ($results[1] as $imgPath) {
      if($imgPath != ''){
        /*
          - Tem imagem no texto
          Baixar do servidor
          gravar renomeada
          substituir no texto pelo path novo
        */
        $imagemNova = $this->salvarImagem($imgPath);
        $str = str_replace($imgPath, $imagemNova, $str);
      }
    }

    return $str;
  }

  public function listComLink()
  {
    $ignoreList = [
      
    ];

    $noticiasAntigos = DB::table('eventos')->where('texto', 'LIKE', '%<a%')->where('is_juramento', '1')->get();

    foreach($noticiasAntigos as $noticia) {
      preg_match_all('/<a href=\"(.*)\"/U', $noticia->texto, $matches);
      if(count($matches) > 0){
        foreach($matches[1] as $match){
          if(!in_array($match, $ignoreList)){
            echo $match . " (<a href='/painel/escola/eventos/{$noticia->id}/edit' target='_blank'>editar</a>)" . '<br>';
          }
        }
      }

    }

    echo '<hr>';
  }

  private function salvarImagem($value)
  {
    $explode = explode('/', $value);
    $filename = end($explode);
    $filename = 'eventos_'.date('YmdHis').$filename;
    $destino = '/assets/escola/img/uploads/';

    if(strpos($value, 'http://sequencialctp.com.br/') === false && strpos($value, 'http://www.sequencialctp.com.br/') === false) {
      $value = 'http://sequencialctp.com.br/' . $value;
    }

    $img = file_get_contents($value);
    file_put_contents($destino.$filename, $img);

    return $destino.$filename;
  }

  private function setData($noticia)
  {
    if (is_null($noticia->data)) {
      $date = explode(' ', $noticia->dataCadastro);
      return $date[0];
    }

    return $noticia->data;
  }
}
