<?php

namespace Sequencial\Http\Controllers\Escola\Site\Import;

use DB;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Evento;
use Sequencial\Models\Escola\EventoImagem;
use Sequencial\Libs\Thumbs;

class ImportDepoimentosController extends SiteBaseController {


  public function execute() {
    ini_set('default_socket_timeout', 900);
    echo '<h1>Import: Depoimentos</h1>';

    DB::table('depoimentos')->delete();
    $depoimentos = DB::connection('mysql_escola_antigo')->table('depoimentos')->get();
    // campos: id, nome, curso, olho, texto, imagem, dataCadastro

    foreach ($depoimentos as $depoimento) {

      $data = [
        'imagem' => $depoimento->imagem ? $this->downloadImage($depoimento->imagem) : '',
        'texto' => $depoimento->texto,
        'autor_nome' => $depoimento->nome,
        'autor_descricao' => $depoimento->curso,
        'created_at' => date('Y-m-d H:i:s')
      ];

      DB::table('depoimentos')->insert($data);
      echo 'Depoimentos de ' . $depoimento->nome . ' inserido<br>';
    }
    echo 'ok';
  }

  private function downloadImage($imagem)
  {
    $filename = 'depoimentos_'.date('YmdHis').$imagem;
    $destino = 'assets/escola/img/depoimentos/';

    $value = 'http://sequencialctp.com.br/img/uploads_depoimentos/' . $imagem;
    $img = file_get_contents($value);
    file_put_contents($destino.$filename, $img);

    Thumbs::makeFromFile($destino, $filename, 200, 200, '/assets/escola/img/depoimentos/thumbs/', 'rgba(255,255,255,1)', false);

    return $filename;
  }
}
