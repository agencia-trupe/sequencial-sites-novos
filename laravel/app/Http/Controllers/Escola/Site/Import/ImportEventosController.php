<?php

namespace Sequencial\Http\Controllers\Escola\Site\Import;

use DB;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Evento;
use Sequencial\Models\Escola\EventoImagem;
use Sequencial\Libs\Thumbs;

class ImportEventosController extends SiteBaseController {

  protected $tiposNoticias = [
    '1' => 'Noticias',
    '2' => 'Juramentos',
    '3' => 'Eventos',
  ];

  protected $images;

  public function corrigeImagens()
  {
    $eventos = EventoImagem::all();
    foreach ($eventos as $value) {
      $source = public_path('/assets/escola/img/eventos/originais/');
      //Thumbs::makeFromFile($source, $value->imagem, 300, 300, public_path('assets/escola/img/eventos/thumbs-quadradas/'), 'rgba(255,255,255,1)', true);
      Thumbs::makeFromFile($source, $value->imagem, 220, 165, public_path('assets/escola/img/eventos/thumbs-retangulares/'), 'rgba(255,255,255,1)', true);
    }
    echo 'ok';
  }

  public function execute() {
    ini_set('default_socket_timeout', 900);
    echo '<h1>Import: Eventos</h1>';

    /*
      Importação de Eventos
      origem:
        connection: mysql_escola_antigo
        tabela: noticias
        campos: titulo, olho, textoMateria, dataCadastro, id_categorias, dataDivulgacao, data
      destino:
        model: Sequencial\Models\Escola\Evento
        tabela: eventos
        campos:
    */

    $noticiasAntigos = DB::connection('mysql_escola_antigo')->table('noticias')->where('id_categorias', '3')->get();

    DB::table('eventos')->where('is_juramento', '0')->delete();

    echo count($noticiasAntigos) . ' resultados<br><br>';

    foreach($noticiasAntigos as $k => $noticia) {
      echo $k . ' ) ' . $noticia->titulo . '<br>';

      $dataArray = [
        'data' => $this->setData($noticia),
        'titulo' => $noticia->titulo,
        'subtitulo' => $noticia->olho,
        'slug' => str_slug($noticia->titulo),
        'imagem' => '',
        'texto' => $this->tratarConteudo($noticia->textoMateria),
        'created_at' => date('Y-m-d H:i:s'),
        'is_juramento' => 0
      ];

      $novoId = DB::table('eventos')->insertGetId($dataArray);
      //buscar imagens em noticias_galeria.id_noticias
      $imagens = DB::connection('mysql_escola_antigo')->table('noticias_galeria')->where('id_noticias', $noticia->id)->get();

      foreach ($imagens as $j => $img) {
        $imgPath = 'img/site/eventos/' . $img->arquivo;

        $destino = 'assets/escola/img/eventos/originais/';
        $thumbs = [
          'assets/escola/img/eventos/redimensionadas/',
          'assets/escola/img/eventos/thumbs/',
          'assets/escola/img/eventos/thumbs-quadradas/',
          'assets/escola/img/eventos/thumbs-retangulares/'
        ];

        $filenameImg = 'eventos_imgs_' . date('YmdHis') . $img->arquivo;
        $imgfile = file_get_contents('http://sequencialctp.com.br/'.$imgPath);
        file_put_contents($destino.$filenameImg, $imgfile);

        foreach ($thumbs as $value) {
          $size = [
            'assets/escola/img/eventos/redimensionadas/' => [
              'width' => 1200,
              'height' => null,
              'crop' => false
            ],
            'assets/escola/img/eventos/thumbs/' => [
              'width' => 400,
              'height' => null,
              'crop' => false
            ],
            'assets/escola/img/eventos/thumbs-quadradas/' => [
              'width' => 300,
              'height' => 300,
              'crop' => true
            ],
            'assets/escola/img/eventos/thumbs-retangulares/' => [
              'width' => 220,
              'height' => 165,
              'crop' => true
            ],
          ];
          Thumbs::makeFromFile($destino, $filenameImg, $size[$value]['width'], $size[$value]['height'], public_path($value), 'rgba(255,255,255,1)', $size[$value]['crop']);
        }

        $dataImageArray = [
          'eventos_id' => $novoId,
          'imagem' => $filenameImg,
          'ordem' => $j,
          'created_at' => date('Y-m-d H:i:s')
        ];

        DB::table('eventos_imagens')->insert($dataImageArray);
      }
    }

    echo '<hr>';
  }

  private function tratarConteudo($str)
  {
    $str = $this->buscarImagens($str);
    return $str;
  }

  private function buscarImagens($str)
  {
    $regex = '/<img.*src=\"(.*)\"/U';

    preg_match_all($regex, $str, $results);

    foreach ($results[1] as $imgPath) {
      if($imgPath != ''){
        /*
          - Tem imagem no texto
          Baixar do servidor
          gravar renomeada
          substituir no texto pelo path novo
        */
        $imagemNova = $this->salvarImagem($imgPath);
        $str = str_replace($imgPath, $imagemNova, $str);
      }
    }

    return $str;
  }

  public function listComLink()
  {
    $ignoreList = [
      'http://www.diariodeitaqua.com.br/',
      'http://semanact.mct.gov.br/index.php/content/view/5932/A_Semana.html',
      'http://www.vence.sp.gov.br/',
      'http://g1.globo.com/concursos-e-emprego/noticia/2013/10/autarquia-hospitalar-municipal-de-sp-fara-concurso-para-44-mil-vagas.html',
      'https://www.facebook.com/sequencial.escola.tecnica',
      'https://www.facebook.com/sequencial.escola.tecnica',
      'mailto:felipe.moraes@sequencialctp.com.br',
      'https://www.ipen.br/',
      'assets/escola/arquivos/1SPS-auditorioPrincipal-manha.pdf',
      'assets/escola/arquivos/1SPS-salaMultiuso-manha.pdf',
      'assets/escola/arquivos/1SPS-auditorioPrincipal-noite.pdf',
      'assets/escola/arquivos/1SPS-salaMultiuso-noite.pdf',
      'assets/escola/arquivos/1SPS-Minicursos-noite.pdf',
      'assets/escola/arquivos/cronograma-setefarm.pdf',
      'assets/escola/arquivos/Sequencial_CronogramaSimposioEletronica.pdf',
      'assets/escola/arquivos/2SIPAT.pdf'
    ];

    $noticiasAntigos = DB::table('eventos')->where('texto', 'LIKE', '%<a%')->where('is_juramento', '0')->get();

    foreach($noticiasAntigos as $noticia) {
      preg_match_all('/<a href=\"(.*)\"/U', $noticia->texto, $matches);
      if(count($matches) > 0){
        foreach($matches[1] as $match){
          if(!in_array($match, $ignoreList)){
            $link = " (<a href='{$match}' target='_blank'>check</a>)";
            echo $match . " (<a href='/painel/escola/eventos/{$noticia->id}/edit' target='_blank'>editar</a>)" . $link . '<br>';
          }
        }
      }

    }

    echo '<hr>';
  }

  private function salvarImagem($value)
  {
    $explode = explode('/', $value);
    $filename = end($explode);
    $filename = 'eventos_'.date('YmdHis').$filename;
    $destino = 'assets/escola/img/uploads/';

    if(strpos($value, 'http://sequencialctp.com.br/') === false && strpos($value, 'http://www.sequencialctp.com.br/') === false) {
      $value = 'http://sequencialctp.com.br/' . $value;
    }

    $img = file_get_contents($value);
    file_put_contents($destino.$filename, $img);

    return $destino.$filename;
  }

  private function setData($noticia)
  {
    if (is_null($noticia->data)) {
      $date = explode(' ', $noticia->dataCadastro);
      return $date[0];
    }

    return $noticia->data;
  }
}
