<?php

namespace Sequencial\Http\Controllers\Escola\Site\Import;

use DB;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Novidade;
use Sequencial\Models\Escola\Evento;

class ImportNovidadesController extends SiteBaseController {

  protected $tiposNoticias = [
    '1' => 'Noticias',
    '2' => 'Juramentos',
    '3' => 'Eventos',
  ];

  protected $images;

  public function corrigeImagens()
  {
    $novidades = Novidade::all();
    foreach ($novidades as $key => $value) {
      $value->texto = str_replace("src=\"assets", "src=\"/assets", $value->texto);
      $value->save();
    }
    echo 'ok';
  }

  public function execute() {
    echo '<h1>Import: Novidades</h1>';

    /*
      Importação de Notícias
      origem:
        connection: mysql_escola_antigo
        tabela: noticias
        campos: titulo, olho, textoMateria, dataCadastro, id_categorias, dataDivulgacao, data
      destino:
        model: Sequencial\Models\Escola\Novidade
        tabela: novidades
        campos: data, titulo, subtitulo, slug, imagem, texto
    */

    $noticiasAntigos = DB::connection('mysql_escola_antigo')->table('noticias')->where('id_categorias', '1')->get();

    DB::table('novidades')->delete();

    echo count($noticiasAntigos) . ' resultados<br><br>';

    foreach($noticiasAntigos as $k => $noticia) {
      echo $k . ' ) ' . $noticia->titulo . '<br>';

      $dataArray = [
        'data' => $this->setData($noticia),
        'titulo' => $noticia->titulo,
        'subtitulo' => $noticia->olho,
        'slug' => str_slug($noticia->titulo),
        'imagem' => '',
        'texto' => $this->tratarConteudo($noticia->textoMateria),
        'created_at' => date('Y-m-d H:i:s')
      ];

       DB::table('novidades')->insert($dataArray);
    }

    echo '<hr>';
  }

  private function tratarConteudo($str)
  {
    $str = $this->buscarImagens($str);
    return $str;
  }

  private function buscarImagens($str)
  {
    $regex = '/<img src=\"(.*)\"/U';

    preg_match_all($regex, $str, $results);

    foreach ($results[1] as $imgPath) {
      if($imgPath != ''){
        /*
          - Tem imagem no texto
          Baixar do servidor
          gravar renomeada
          substituir no texto pelo path novo
        */
        $imagemNova = $this->salvarImagem($imgPath);
        $str = str_replace($imgPath, $imagemNova, $str);
      }
    }

    return $str;
  }

  public function listComLink()
  {
    $ignoreList = [
        'http://www.estadao.com.br/noticias/vidae,promotores-querem-prisao-para-acusados-de-bullying,708347,0.htm',
        'http://retec.edunet.sp.gov.br/remt/inscricao/',
        'http://www.sequencialmatriculas.com.br/escolatecnica',
        'http://www.escolasequencial.com.br/matriculas',
        'http://www.institutocimas.com.br/noticias.asp?noticia=228',
        'http://globotv.globo.com/rede-globo/bom-dia-brasil/v/brasil-vai-abrir-sete-milhoes-de-vagas-para-profissionais-com-nivel-tecnico-ate-2015/2169335/',
        'http://www.vence.sp.gov.br',
        'http://bilheteunico.sptrans.com.br/',
        'http://sintesp.org.br/noticia-04.php',
        'http://www.acheconcursos.com.br/noticia/autarquia-hospitalar-municipal-ahm-sp-realiza-concursos-para-4411-vagas-1266',
        'http://sequencialmatriculas.com.br/escolatecnica/',
        'http://www.vence.sp.gov.br/remt/av/outros/site/pdf/manualDoAluno.pdf',
        'http://www.sequencialmatriculas.com.br/escolatecnica/',
        'http://www.sequencialmatriculas.com.br/escolatecnica/pre-cadastro',
        'http://www.educacao.sp.gov.br',
        'http://sisutec.mec.gov.br/selecionados',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjYzNTQiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/pesquisar-vagas',
        'http://sistec.mec.gov.br/login/servlet',
        'http://sistec.mec.gov.br/login/cadastrar',
        'http://sistec.mec.gov.br',
        'http://www.vence.sp.gov.br/remt/av/Padrao/modulo-inscricao/aplicacao-site/',
        'http://tvuol.uol.com.br/video/curso-tecnico-ajuda-na-escolha-da-carreira-ou-devo-focar-so-na-faculdade-04020D183060DC895326/',
        'http://www.nube.com.br/',
        'http://www.sisutec.mec.gov.br/',
        'http://www.vence.sp.gov.br/remt/av/outros/site/pdf/ManualAlunoVENCE_5Edv3.pdf',
        'http://www.vence.sp.gov.br/remt/av/Padrao/modulo-inscricao/aplicacao-site/',
        'http://www.vence.sp.gov.br/remt/av/Padrao/modulo-resultado/aplicacao-site/',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjQ0MjUiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjYzNTUiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjQ0MjQiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjQ1MDkiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjY0MzUiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjcyNDgiO30=',
        'http://sisutecinscricaoonline.mec.gov.br/inscricao-online/confirmar-cadastro/oferta/YToxOntzOjE2OiJjb19zaXN1dGVjX3R1cm1hIjtzOjQ6IjQ1MTQiO30=',
        'http://www.techtudo.com.br/dicas-e-tutoriais/noticia/2013/04/como-se-prevenir-do-virus-que-altera-boletos-bancarios.html',
        'http://carreiras.empregos.com.br/carreira/administracao/noticias/especial-profissoes-profissao-secretariado.shtm',
        'http://www.logisticadescomplicada.com/profissao-e-carreira-em-logistica-o-que-faz-o-profissional-de-logistica',
        'http://guiadoestudante.abril.com.br/profissoes/administracao-negocios/logistica-686493.shtml',
        'http://g1.globo.com/jornal-da-globo/noticia/2015/07/numero-de-vagas-para-tecnicos-cresce-15-em-sp.html',
        'http://globotv.globo.com/rede-globo/sptv-1a-edicao/t/edicoes/v/mutirao-da-hepatite-c-faz-teste-para-identificar-a-doenca-em-terminais-rodoviarios-de-sp/4349508/',
        'http://globotv.globo.com/rede-globo/jornal-hoje/t/edicoes/v/campanha-em-sao-paulo-faz-teste-gratuito-para-detectar-hepatite-c/4349808/',
        'http://sisutec.mec.gov.br/',
        'http://download.inep.gov.br/educacao_basica/enem/edital/2014/edital_enem_2014.pdf',
        'http://enem.inep.gov.br/',
        'http://sequencialctp11.emktsender.net/registra_clique.php?id=TH|teste|231154|34834&url=http%3A%2F%2Fwww.escolasequencial.com.br',
        'http://www.escolasequencial.com.br/',
        'http://sequencialmatriculas.com.br/escolatecnica/pre-cadastro/',
        'assets/escola/arquivos/lista_aprovados_bolsas2011_unidade1.pdf',
        'assets/escola/arquivos/lista_aprovados_bolsas2011_unidade2.pdf',
        'assets/escola/arquivos/lista_aprovados_bolsas2011_unidade3.pdf',
        'http://www.escolasequencial.com.br/bolsas/Edital-ProvaDeBolsas-EscolaTecnicaSequencial-2Sem-2012.pdf',
        'assets/escola/arquivos/boletim-scamilo-ago2011.pdf',
        'assets/escola/arquivos/folhetoMaioZonaSul-2.jpg',
        'http://sequencialctp11.emktsender.net/registra_clique.php?id=TH|teste|231154|34834&url=http%3A%2F%2Fwww.escolasequencial.com.br ',
        'http://sequencialctp11.emktsender.net/registra_clique.php?id=TH|teste|231154|34834&url=http%3A%2F%2Fwww.escolasequencial.com.br ',
        '/cursos-tecnicos',
        '/unidades',
    ];

    $noticiasAntigos = DB::table('novidades')->where('texto', 'LIKE', '%<a%')->get();

    foreach($noticiasAntigos as $noticia) {
      preg_match_all('/<a href=\"(.*)\"/U', $noticia->texto, $matches);
      if(count($matches) > 0){
        foreach($matches[1] as $match){
          if(!in_array($match, $ignoreList)){
            $link = " (<a href='http://www.sequencialctp.com.br{$match}' target='_blank'>check</a>)";
            echo $match . " (<a href='/painel/escola/novidades/{$noticia->id}/edit' target='_blank'>editar</a>)" . $link . '<br>';
          }
        }
      }

    }

    echo '<hr>';
  }

  private function salvarImagem($value)
  {
    $explode = explode('/', $value);
    $filename = end($explode);
    $filename = date('YmdHis').$filename;
    $destino = 'assets/escola/img/uploads/';

    if(strpos($value, 'http://sequencialctp.com.br/') === false && strpos($value, 'http://www.sequencialctp.com.br/') === false) {
      $value = 'http://sequencialctp.com.br/' . $value;
    }

    $img = file_get_contents($value);
    file_put_contents($destino.$filename, $img);

    return $destino.$filename;
  }

  private function setData($noticia)
  {
    if (is_null($noticia->data)) {
      $date = explode(' ', $noticia->dataCadastro);
      return $date[0];
    }

    return $noticia->data;
  }
}
