<?php

namespace Sequencial\Http\Controllers\Escola\Site\CursosTecnicos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Areas;
use Sequencial\Models\Escola\Curso;
use Sequencial\Models\Escola\CadastroGradeCurricular;

use Validator, Mail;

class CursosTecnicosController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.cursos-tecnicos.index');
	}

	public function getDetalhes(Request $request, $slug)
	{
		$curso = Curso::where('slug', $slug)->first();

		if (!$curso)
		    return redirect()->to('/cursos-tecnicos');

		return view('escola.site.cursos-tecnicos.detalhes', [
			'curso' => $curso
		]);
	}

	public function postGrade(Request $request, $slug)
	{
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
		]);

		if ($validator->fails()) {
			return response('Preencha seu e-mail', 422);
		}

		$curso = Curso::where('slug', $slug)->first();

		if (!$curso || !$curso->grade_curricular_arquivo) {
			abort('404');
		}

		$cadastro = CadastroGradeCurricular::where([
			'email' => $request->email,
			'curso_id' => $curso->id,
		])->first();

		if (!$cadastro) {
			$cadastro = CadastroGradeCurricular::create([
				'email' => $request->email,
				'curso_id' => $curso->id,
				'curso_titulo' => $curso->titulo,
				'token' => str_random(32)
			]);
		}

		$data = [
			'email' => $request->email,
			'curso' => $curso,
			'slug'  => $curso->slug,
			'token' => $cadastro->token,
		];

		Mail::send('escola.emails.grade', $data, function($message) use ($data)
		{
			$message
				->to($data['email'])
				->subject('Escola Técnica Sequencial - Grade Curricular ('.$data['curso']->titulo.')');
		});

		return response('', 200);
	}

	public function getGrade(Request $request)
	{
		$cadastro = CadastroGradeCurricular::where([
			'email' => $request->get('email'),
			'token' => $request->get('token')
		])->first();

		if (
			!$cadastro ||
			!$cadastro->curso ||
			!$cadastro->curso->grade_curricular_arquivo
		) {
			abort('404');
		}

		$cadastro->update(['visualizado' => true]);

		$arquivo  = $cadastro->curso->grade_curricular_arquivo;
		$extensao = pathinfo($arquivo, PATHINFO_EXTENSION);

		return response()->download(
			'assets/escola/grade/'.$arquivo,
			'Sequencial-GradeCurricular_'.$cadastro->curso->slug.'.'.$extensao
		);
	}
}
