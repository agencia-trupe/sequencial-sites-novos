<?php

namespace Sequencial\Http\Controllers\Escola\Site\Home;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Banner;
use Sequencial\Models\Escola\Chamada;
use Sequencial\Models\Escola\Depoimento;
use Sequencial\Models\Escola\PreInscricao;

class HomeController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		$banners  = Banner::ordenado()->get();
        $chamadas = Chamada::semCalhaus()->ordenado()->limit(2)->get();
        $calhaus  = Chamada::calhaus()->ordenado()->get();
        $depoimento = Depoimento::inRandomOrder()->first();

		return view('escola.site.home.index', [
			'banners' => $banners,
            'chamadas' => $chamadas,
            'calhaus' => $calhaus,
            'depoimento' => $depoimento
		]);
	}

    public function postPreInscricao(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required',
            'origem'   => 'required'
        ]);

        try {
            PreInscricao::create($request->all());
            return redirect()->back()->with('preInscricaoEnviada', true);
        } catch(\Exception $e) {
            return redirect()->back();
        }
    }
}
