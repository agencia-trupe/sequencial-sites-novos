<?php

namespace Sequencial\Http\Controllers\Escola\Site\FaleConosco;

use Mail;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\MensagemFaleConosco;

class FaleConoscoController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
		return view('escola.site.fale-conosco.index');
	}

	public function postEnviar(Request $request)
	{
		$this->validate($request, [
			'nome' => 'required',
			'email' => 'required|email',
			'telefone' => 'required',
			'mensagem' => 'required'
		]);

		$data['nome'] = $request->nome;
		$data['email'] = $request->email;
		$data['telefone'] = $request->telefone;
		$data['cpf'] = $request->cpf;
		$data['mensagem'] = $request->mensagem;

		Mail::send('escola.emails.contato', $data, function($message) use ($data)
		{
			$message->to('contato@sequencialctp.com.br')
					->subject('Contato via site - Sequencial Escola Técnica - '.$data['nome'])
					->replyTo($data['email'], $data['nome']);
		});

		$registro = MensagemFaleConosco::create($data);
		$registro->save();

		$request->session()->flash('contato_enviado', true);

		return redirect()->route('escola.fale-conosco');
	}
}
