<?php

namespace Sequencial\Http\Controllers\Escola\Site\Busca;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;

use Sequencial\Models\Escola\Curso;
use Sequencial\Models\Escola\Evento;
use Sequencial\Models\Escola\Novidade;

class BuscaController extends SiteBaseController
{

  public function postBusca(Request $request)
  {
    $termo = $request->termo;
    $resultados = [];

    $qry_cursos = Curso::busca($termo)->get();
    foreach($qry_cursos as $qry){
      $resultados['cursos'][$qry->id]['link']  = 'cursos-tecnicos/'.$qry->slug;
      $resultados['cursos'][$qry->id]['secao'] = 'CURSOS DE GRADUAÇÃO';
      $resultados['cursos'][$qry->id]['titulo']= $qry->titulo;
      $resultados['cursos'][$qry->id]['texto'] = count($qry->topicos) > 0 ? $qry->topicos[0]->texto : '';
    }

    $qry_eventos = Evento::busca($termo)->get();
    foreach($qry_eventos as $qry){
      $resultados['eventos'][$qry->id]['link']  = 'eventos/'.$qry->slug;
      $resultados['eventos'][$qry->id]['secao'] = 'EVENTOS';
      $resultados['eventos'][$qry->id]['titulo']= $qry->titulo;
      $resultados['eventos'][$qry->id]['texto'] = $qry->texto;
    }

    $qry_novidades = Novidade::busca($termo)->get();
    foreach($qry_novidades as $qry){
      $resultados['novidades'][$qry->id]['link']  = 'novidades/'.$qry->slug;
      $resultados['novidades'][$qry->id]['secao'] = 'NOVIDADES';
      $resultados['novidades'][$qry->id]['titulo']= $qry->titulo;
      $resultados['novidades'][$qry->id]['texto'] = $qry->texto;
    }

    if (stripos('LISTA DE CAMPOS DE ESTÁGIO', $termo) !== false || stripos('LISTA DE CAMPOS DE ESTAGIO', $termo) !== false) {
      if (!array_key_exists('novidades', $resultados)) {
        $resultados['novidades'] = [];
      }

      array_push($resultados['novidades'], [
        'link'   => 'novidades/estagios',
        'secao'  => 'NOVIDADES',
        'titulo' => 'LISTA DE CAMPOS DE ESTÁGIO',
        'texto'  => 'Acompanhe aqui a divulgação das Listas de Campo de Estágio das Unidades Sequencial'
      ]);
    }

    return view('escola.site.busca.index', [
      'resultados' => $resultados,
      'termo' => $termo
    ]);
  }

}
