<?php

namespace Sequencial\Http\Controllers\Escola\Site\ImagensTv;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\ImagemTv;

class ImagensTvController extends Controller
{
    public function index($unidade)
    {
        if (! array_key_exists($unidade, ImagemTv::unidades)) {
            abort('404');
        }

        $imagens = ImagemTv::where('unidade', $unidade)->ordenados()->get();

        return view('escola.site.tv.index', compact('imagens'));
    }
}
