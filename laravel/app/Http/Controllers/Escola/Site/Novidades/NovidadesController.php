<?php

namespace Sequencial\Http\Controllers\Escola\Site\Novidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;

use Sequencial\Models\Escola\Novidade;

class NovidadesController extends SiteBaseController
{

	public function getNoticias(Request $request)
	{
		$novidades = Novidade::where('categoria', 'Notícias')->ordenado()->paginate(20);

        return view('escola.site.novidades.index', [
          'novidades' => $novidades,
          'categoria' => 'Notícias',
        ]);
	}

    public function getPromocoes(Request $request)
    {
        $novidades = Novidade::where('categoria', 'Promoções')->ordenado()->paginate(20);

        return view('escola.site.novidades.index', [
          'novidades' => $novidades,
          'categoria' => 'Promoções',
        ]);
    }

	public function getDetalhes(Request $request, $slug)
	{
		$novidades = Novidade::ordenado()->paginate(20);
        $novidade = Novidade::where('slug', $slug)->first();

        if (!$novidade) abort('404');

        view()->share('categoriaNovidade', $novidade->categoria);

        return view('escola.site.novidades.detalhes', [
          'novidades' => $novidades,
          'detalhe' => $novidade
        ]);
	}
}
