<?php

namespace Sequencial\Http\Controllers\Escola\Site\Eventos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;

use Sequencial\Models\Escola\Evento;

class EventosController extends SiteBaseController
{

	public function getIndex()
  {
    $proximos = Evento::proximos()->semJuramentos()->ordenado()->get();
    $proximosJuramentos = Evento::proximos()->juramentos()->ordenado()->get();
    $historico = Evento::historico()->semJuramentos()->ordenado()->paginate(20);

    return view('escola.site.eventos.index', [
      'proximos' => $proximos,
      'proximosJuramentos' => $proximosJuramentos,
      'historico' => $historico
    ]);
  }

  public function getJuramentos()
  {
    $proximosJuramentos = Evento::proximos()->juramentos()->ordenado()->get();
    $historico = Evento::historico()->juramentos()->ordenado()->paginate(20);

    return view('escola.site.eventos.index', [
      'proximos' => [],
      'proximosJuramentos' => $proximosJuramentos,
      'historico' => $historico
    ]);
  }

  public function getDetalhes($slug)
  {
    $eventos = Evento::proximos()->ordenado()->get();
    $detalhe = Evento::where('slug', $slug)->first();

    return view('escola.site.eventos.detalhes', [
      'eventos' => $eventos,
      'detalhe' => $detalhe
    ]);
  }
}
