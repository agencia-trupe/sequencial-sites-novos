<?php

namespace Sequencial\Http\Controllers\Escola\Site;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

use Sequencial\Models\Escola\Areas;
use Sequencial\Models\Escola\Popup;
use Sequencial\Models\Escola\Curso;
use Sequencial\Models\Escola\Unidade;

class SiteBaseController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){
      view()->share([
		  'popup' => Popup::ativo()->first(),
          'listaAreas' => Areas::ordenado()->get(),
		  'listaCursos' => Curso::ordenado()->get(),
          'listaUnidades' => Unidade::ordenado()->get()
	  ]);

    }
}
