<?php

namespace Sequencial\Http\Controllers\Escola\Site\ASequencial;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\ASequencial;

class ASequencialController extends SiteBaseController
{

	public function getIndex(Request $request)
	{
	    return view('escola.site.a-sequencial.index', [
	        'conteudo' => ASequencial::first()
        ]);
	}
}
