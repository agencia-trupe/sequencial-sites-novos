<?php

namespace Sequencial\Http\Controllers\Escola\Site\PerguntasFrequentes;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Escola\Site\SiteBaseController;
use Sequencial\Models\Escola\Curso;

class PerguntasFrequentesController extends SiteBaseController
{

	public function getIndex()
	{
		$cursosComFaq = Curso::ordenado()->has('faq')->get();

		return view('escola.site.perguntas-frequentes.index', ['cursosComFaq' => $cursosComFaq]);
	}
}
