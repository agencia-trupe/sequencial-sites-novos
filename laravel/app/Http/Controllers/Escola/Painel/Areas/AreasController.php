<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Areas;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Areas;
use Sequencial\Libs\Thumbs;

class AreasController extends Controller
{
    protected $pathImagens = 'areas/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Areas::ordenado()->get();

      return view('escola.painel.areas.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.areas.edit')->with('registro', Areas::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_escola.areas,titulo,'.$id
    	]);

      $object = Areas::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.areas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
