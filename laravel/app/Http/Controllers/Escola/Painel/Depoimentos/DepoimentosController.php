<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Depoimentos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Depoimento;
use Sequencial\Libs\Thumbs;

class DepoimentosController extends Controller
{

	protected $pathImagens = 'depoimentos/';

	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	public function index(Request $request)
	{
		$registros = Depoimento::ordenado()->get();

		return view('escola.painel.depoimentos.index')->with(compact('registros'));
	}

	public function create(Request $request)
	{
		return view('escola.painel.depoimentos.create');
	}

	public function store(Request $request)
	{
		$object = new Depoimento;

		$object->texto = $request->texto;
		$object->autor_nome = $request->autor_nome;
		$object->autor_descricao = $request->autor_descricao;

		$imagem = Thumbs::make($request, 'escola', 'imagem', 400, null, $this->pathImagens);
		if($imagem){
			Thumbs::make($request, 'escola', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
			$object->imagem = $imagem;
		}

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Registro criado com sucesso.');

			return redirect()->route('painel.escola.depoimentos.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

		}
	}


	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
		return view('escola.painel.depoimentos.edit')->with('registro', Depoimento::find($id));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  Request  $request
	* @param  int  $id
	* @return Response
	*/
	public function update(Request $request, $id)
	{
		$object = Depoimento::find($id);

		$object->texto = $request->texto;
		$object->autor_nome = $request->autor_nome;
		$object->autor_descricao = $request->autor_descricao;

		$imagem = Thumbs::make($request, 'escola', 'imagem', 400, null, $this->pathImagens);
		if($imagem){
			Thumbs::make($request, 'escola', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
			$object->imagem = $imagem;
		}

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

			return redirect()->route('painel.escola.depoimentos.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id){
		$object = Depoimento::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Registro removido com sucesso.');

		return redirect()->route('painel.escola.depoimentos.index');
	}

}
