<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Imagens;

use Image;
use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Libs\Thumbs;

class ImagensController extends Controller
{

  /*
    Armazena as imagens enviadas via Ajax
  */
  public function postUpload(Request $request)
  {
    $arquivo = $request->file('files');
    $path    = $request->input('path');

    $path_original = "assets/escola/img/{$path}/originais/";
    $path_asset    = "assets/escola/img/{$path}/redimensionadas/";
    $path_thumb2   = "assets/escola/img/{$path}/thumbs-quadradas/";
    $path_thumb3   = "assets/escola/img/{$path}/thumbs-retangulares/";
    $path_thumb    = "assets/escola/img/{$path}/thumbs/";
    $path_upload   = public_path($path_original);

    //$this->criarDiretoriosPadrao($path);

    $nome_arquivo = $arquivo->getClientOriginalName();
    $extensao_arquivo = $arquivo->getClientOriginalExtension();
    $nome_arquivo_sem_extensao = str_replace($extensao_arquivo, '', $nome_arquivo);

    $filename = date('YmdHis').str_slug($nome_arquivo_sem_extensao).'.'.$extensao_arquivo;

    // Armazenar Original
    $arquivo->move($path_upload, $filename);

    $name = $path_original.$filename;
    $thumb = $path_thumb.$filename;
    $thumb_quadrada = $path_thumb2.$filename;

    // Armazenar Redimensionada

    // redimensionadas
    Thumbs::makeFromFile($path_upload, $filename, 1200, null, public_path($path_asset), 'rgba(255,255,255,1)', false);

    // Armazenar Thumb
    Thumbs::makeFromFile($path_upload, $filename, 400, null, public_path($path_thumb), 'rgba(255,255,255,1)', false);
    Thumbs::makeFromFile($path_upload, $filename, 300, 300, public_path($path_thumb2), 'rgba(255,255,255,1)', true);
    Thumbs::makeFromFile($path_upload, $filename, 220, 165, public_path($path_thumb3), 'rgba(255,255,255,1)', true);

    return [
      'envio' => 1,
      'thumb' => $thumb_quadrada,
      'filename' => $filename
    ];
  }

  public function postCkeUpload()
  {
    if ( isset($_FILES['userfile']) ) {
        $filename = date('dmYHis').basename($_FILES['userfile']['name']);
        $error = true;

        $path = public_path('/assets/escola/img/uploads/'.$filename);
        $error = !move_uploaded_file($_FILES['userfile']['tmp_name'], $path);

        $image = Image::make($path)->resize(800, null, function($constraint){
            $constraint->aspectRatio();
            $constraint->upsize();
        })->save($path, 100);

        $rsp = array(
            'error' => $error, // Used in JS
            'filename' => $filename,
            'filepath' => '/assets/escola/img/uploads/' . $filename, // Web accessible
        );
        echo json_encode($rsp);
        exit;
    }else{
        echo json_encode(array('error' => 'No file'));
    }
  }


  private function criarDiretoriosPadrao($path)
  {
    if(!file_exists(public_path("assets/img/{$path}")))
      mkdir(public_path("assets/img/{$path}"), 0777);

    if(!file_exists(public_path("assets/img/{$path}/originais/")))
      mkdir(public_path("assets/img/{$path}/originais/"), 0777);

    if(!file_exists(public_path("assets/img/{$path}/redimensionadas/")))
      mkdir(public_path("assets/img/{$path}/redimensionadas/"), 0777);

    if(!file_exists(public_path("assets/img/{$path}/thumbs/")))
      mkdir(public_path("assets/img/{$path}/thumbs/"), 0777);
  }

}
