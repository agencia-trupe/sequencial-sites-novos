<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Ouvidoria;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\MensagemOuvidoria;

class OuvidoriaMensagensController extends Controller
{
    public function index()
    {
      $registros = MensagemOuvidoria::ordenado()->get();

      return view('escola.painel.ouvidoria.mensagens.index')->with(compact('registros'));
    }

    public function show($id)
    {
      return view('escola.painel.ouvidoria.mensagens.show')->with('registro', MensagemOuvidoria::find($id));
    }
}
