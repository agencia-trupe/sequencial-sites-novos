<?php

namespace Sequencial\Http\Controllers\Escola\Painel\ASequencial;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\ASequencial;
use Sequencial\Libs\Thumbs;

class ASequencialController extends Controller
{
    protected $pathImagens = 'a-sequencial/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = ASequencial::all();

      return view('escola.painel.a-sequencial.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.a-sequencial.edit')->with('registro', ASequencial::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = ASequencial::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 400, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.a-sequencial.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
