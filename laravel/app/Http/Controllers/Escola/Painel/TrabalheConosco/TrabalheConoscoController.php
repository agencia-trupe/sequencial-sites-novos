<?php

namespace Sequencial\Http\Controllers\Escola\Painel\TrabalheConosco;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\TrabalheConosco;
use Sequencial\Libs\Thumbs;

class TrabalheConoscoController extends Controller
{
	protected $pathImagens = 'trabalhe_conosco/';

	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	public function index(Request $request)
	{
		$registros = TrabalheConosco::all();

		return view('escola.painel.trabalhe_conosco.index')->with(compact('registros'));
	}

	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
		return view('escola.painel.trabalhe_conosco.edit')->with('registro', TrabalheConosco::find($id));
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  Request  $request
	* @param  int  $id
	* @return Response
	*/
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'titulo' => 'required'
		]);

		$object = TrabalheConosco::find($id);

		$object->titulo = $request->titulo;
		$object->texto = $request->texto;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

			return redirect()->route('painel.escola.trabalhe_conosco.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

		}
	}

}
