<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Novidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Novidade;
use Sequencial\Libs\Thumbs;

class NovidadesController extends Controller
{
    protected $pathImagens = 'novidades/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Novidade::ordenado()->get();

      return view('escola.painel.novidades.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('escola.painel.novidades.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'categoria' => 'required',
        'titulo' => 'required|unique:mysql_escola.novidades,titulo',
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = new Novidade;

      $object->categoria = $request->categoria;
      $object->titulo = $request->titulo;
      $object->subtitulo = $request->subtitulo;
      $object->slug = str_slug($request->titulo);
      $object->data = $request->data;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 730, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.escola.novidades.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.novidades.edit')->with('registro', Novidade::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'categoria' => 'required',
      	'titulo' => 'required|unique:mysql_escola.novidades,titulo,'.$id,
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = Novidade::find($id);

      $object->categoria = $request->categoria;
      $object->titulo = $request->titulo;
      $object->subtitulo = $request->subtitulo;
      $object->slug = str_slug($request->titulo);
      $object->data = $request->data;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 730, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.novidades.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Novidade::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.escola.novidades.index');
    }

}
