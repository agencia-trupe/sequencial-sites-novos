<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Unidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Unidade;
use Sequencial\Models\Escola\UnidadeImagem;
use Sequencial\Libs\Thumbs;

class UnidadesController extends Controller
{
    protected $pathImagens = 'unidades/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Unidade::all();

      return view('escola.painel.unidades.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('escola.painel.unidades.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_escola.unidades,titulo'
    	]);

      $object = new Unidade;

      $object->titulo = $request->titulo;
      $object->endereco = $request->endereco;
      $object->horarios = $request->horarios;
      $object->google_maps = $request->google_maps;
      $object->cor_unidade = $request->cor_unidade;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 600, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 215, 105, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $imagens = $request->imagem_album;
        if(sizeof($imagens)){
          foreach ($imagens as $ordem => $img) {
            $object->imagens()->save( new UnidadeImagem([
              'imagem' => $img,
              'ordem' => $ordem
            ]));
          }
        }

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.escola.unidades.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.unidades.edit')->with('registro', Unidade::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_escola.unidades,titulo,'.$id
    	]);

      $object = Unidade::find($id);

      $object->titulo = $request->titulo;
      $object->endereco = $request->endereco;
      $object->horarios = $request->horarios;
      $object->google_maps = $request->google_maps;
      $object->cor_unidade = $request->cor_unidade;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 600, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 215, 105, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        foreach ($object->imagens as $img_atual)
          $img_atual->delete();

        $imagens = $request->imagem_album;
        if(sizeof($imagens)){
          foreach ($imagens as $ordem => $img) {
            $object->imagens()->save( new UnidadeImagem([
              'imagem' => $img,
              'ordem' => $ordem
            ]));
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.unidades.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Unidade::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.escola.unidades.index');
    }

}
