<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Unidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Unidade;
use Sequencial\Models\Escola\UnidadeTopicos as Topico;
use Sequencial\Libs\Thumbs;

class TopicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $unidade = Unidade::find($request->unidades_id);
      $registros = $unidade->topicos;

      return view('escola.painel.unidades.topicos.index')->with(compact('registros', 'unidade'));
    }

    public function create(Request $request)
    {
      $unidade = Unidade::find($request->unidades_id);
      return view('escola.painel.unidades.topicos.create')->with(compact('unidade'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required',
        'unidades_id' => 'required|exists:mysql_escola.unidades,id'
    	]);

      $object = new Topico;

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->tipo = $request->tipo;
      $object->unidades_id = $request->unidades_id;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.escola.unidades.topicos.index', ['unidades_id' => $object->unidades_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $registro = Topico::find($id);
      $unidade = $registro->unidade;
      return view('escola.painel.unidades.topicos.edit')->with('registro', $registro)->with('unidade', $unidade);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Topico::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->tipo = $request->tipo;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.unidades.topicos.index', ['unidades_id' => $object->unidades_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Topico::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.escola.unidades.topicos.index', ['unidades_id' => $object->unidades_id]);
    }

}
