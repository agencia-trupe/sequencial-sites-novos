<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Unidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;
use Sequencial\Models\Escola\Unidade;
use Sequencial\Models\Escola\Curso;

class UnidadesCursosController extends Controller
{

    public function getIndex(Request $request)
    {
        $unidade = Unidade::findOrFail($request->unidades_id);
        $cursos = Curso::ordenado()->get();
        $cursosIdArray = $unidade->cursos()->pluck('cursos.id')->toArray();

        return view('escola.painel.unidades.cursos.index', [
            'unidade' => $unidade,
            'cursos' => $cursos,
            'cursosIdArray' => $cursosIdArray
        ]);
    }

    public function postStore(Request $request)
    {
        $this->validate($request, [
            'unidades_id' => 'required|exists:unidades,id'
        ]);

        $unidade = Unidade::findOrFail($request->unidades_id);

        $cursos = $request->cursos;

        $unidade->cursos()->detach();
        $unidade->cursos()->attach($cursos);

        try {

            $unidade->save();

            $request->session()->flash('sucesso', 'Cursos relacionados com sucesso.');

            return redirect()->route('painel.escola.unidades.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao relacionar cursos! ('.$e->getMessage().')'));
        }
    }

}