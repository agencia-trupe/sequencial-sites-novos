<?php

namespace Sequencial\Http\Controllers\Escola\Painel\PreInscricoes;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\PreInscricao;

class PreInscricoesController extends Controller
{
    public function index(Request $request)
    {
        $fields = [
            'created_at as data',
            'nome',
            'email',
            'telefone',
            'origem'
        ];

        $filtro = $request->get('filtro');
        if ($filtro) {
            $registros = PreInscricao::where('nome', 'like', '%'.$filtro.'%')
                ->orWhere('email', 'like', '%'.$filtro.'%')
                ->orWhere('telefone', 'like', '%'.$filtro.'%')
                ->orderBy('id', 'DESC')
                ->paginate(20, $fields);
        } else {
            $registros = PreInscricao::orderBy('id', 'DESC')
                ->paginate(20, $fields);
        }

        return view('escola.painel.pre-inscricoes.index', compact('registros'));
    }
}
