<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Chamadas;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Chamada;
use Sequencial\Libs\Thumbs;

class ChamadasController extends Controller
{

    protected $pathImagens = 'chamadas/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $filtro = $request->filtro;

      if($filtro == 'calhaus')
        $registros = Chamada::calhaus()->ordenado()->get();
      else
        $registros = Chamada::semCalhaus()->ordenado()->get();

      $pode_adicionar = $this->verificaChamadas();

      return view('escola.painel.chamadas.index')->with(compact('registros', 'filtro', 'pode_adicionar'));
    }

    private function verificaChamadas()
    {
      $nro_chamadas = count(Chamada::semCalhaus()->get());

      if($nro_chamadas >= 2) return false;

      return true;
    }

    public function create(Request $request)
    {
      return view('escola.painel.chamadas.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'fundo_tipo' => 'required|in:imagem,cor'
    	]);

      $object = new Chamada;

      $object->titulo = $request->titulo;
      $object->subtitulo = $request->subtitulo;
      $object->tipo = $request->tipo;
      $object->link = $request->link;
      $object->fundo_tipo = $request->fundo_tipo;

      if(count(Chamada::semCalhaus()->where('tipo', $request->tipo)->get())){
        $request->flash();
        return back()->withErrors(array('Já existe uma chamada deste tipo cadastrada!'));
      }

      if($object->fundo_tipo == 'imagem'){

        $imagem = Thumbs::make($request, 'escola', 'fundo_imagem', 320, 205, $this->pathImagens);
        if($imagem){
        	Thumbs::make($request, 'escola', 'fundo_imagem', 160, 97, $this->pathImagens.'thumbs/');
        	$object->fundo_imagem = $imagem;
        }
        $object->fundo_cor = '';

      }elseif($object->fundo_tipo == 'cor'){
        $object->fundo_imagem = '';
        $object->fundo_cor = $request->fundo_cor;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.escola.chamadas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.chamadas.edit')->with('registro', Chamada::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'fundo_tipo' => 'required|in:imagem,cor'
    	]);

      $object = Chamada::find($id);

      $object->titulo = $request->titulo;
      $object->subtitulo = $request->subtitulo;
      $object->tipo = $request->tipo;
      $object->link = $request->link;
      $object->fundo_tipo = $request->fundo_tipo;

      if(count(Chamada::semCalhaus()->where('tipo', $request->tipo)->where('id', '!=', $id)->get())){
        $request->flash();
        return back()->withErrors(array('Já existe uma chamada deste tipo cadastrada!'));
      }

      if($object->fundo_tipo == 'imagem'){

        $imagem = Thumbs::make($request, 'escola', 'fundo_imagem', 320, 205, $this->pathImagens);
        if($imagem){
        	Thumbs::make($request, 'escola', 'fundo_imagem', 160, 97, $this->pathImagens.'thumbs/');
        	$object->fundo_imagem = $imagem;
        }
        $object->fundo_cor = '';

      }elseif($object->fundo_tipo == 'cor'){
        $object->fundo_imagem = '';
        $object->fundo_cor = $request->fundo_cor;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.chamadas.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Chamada::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.escola.chamadas.index');
    }

}
