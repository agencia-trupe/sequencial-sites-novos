<?php

namespace Sequencial\Http\Controllers\Escola\Painel\PerguntasFrequentes;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\PerguntasFrequentes;
use Sequencial\Models\Escola\Curso;
use Sequencial\Libs\Thumbs;

class PerguntasFrequentesController extends Controller
{
	protected $pathImagens = 'perguntas_frequentes/';

	/**
	* Display a listing of the resource.
	*
	* @return Response
	*/
	public function index(Request $request)
	{
		$cursos = Curso::ordenado()->get();
		$registros = [];
		$filtro = $request->filtro;

		if ($filtro)
			$registros = PerguntasFrequentes::filtroCurso($filtro)->ordenado()->get();

		return view('escola.painel.perguntas_frequentes.index')->with(compact('registros', 'cursos', 'filtro'));
	}

	public function create()
	{
		$cursos = Curso::ordenado()->get();
		return view('escola.painel.perguntas_frequentes.create')->with('cursos', $cursos);
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'curso_id' => 'required|exists:cursos,id',
			'titulo' => 'required'
		]);

		$object = new PerguntasFrequentes;

		$object->curso_id = $request->curso_id;
		$object->titulo = $request->titulo;
		$object->texto = $request->texto;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Registro criado com sucesso.');

			return redirect()->route('painel.escola.perguntas_frequentes.index', ['filtro' => $object->curso_id]);

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

		}
	}


	/**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function edit($id)
	{
		return view('escola.painel.perguntas_frequentes.edit')->with('registro', PerguntasFrequentes::find($id))
															  ->with('cursos', Curso::ordenado()->get());
	}

	/**
	* Update the specified resource in storage.
	*
	* @param  Request  $request
	* @param  int  $id
	* @return Response
	*/
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'curso_id' => 'required|exists:cursos,id',
			'titulo' => 'required'
		]);

		$object = PerguntasFrequentes::find($id);

		$object->curso_id = $request->curso_id;
		$object->titulo = $request->titulo;
		$object->texto = $request->texto;

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Registro alterado com sucesso.');

			return redirect()->route('painel.escola.perguntas_frequentes.index', ['filtro' => $object->curso_id]);

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id){
		$object = PerguntasFrequentes::find($id);
		$filtro = $object->curso_id;
		$object->delete();

		$request->session()->flash('sucesso', 'Registro removido com sucesso.');

		return redirect()->route('painel.escola.perguntas_frequentes.index', ['filtro' => $object->curso_id]);
	}

}
