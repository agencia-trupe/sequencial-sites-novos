<?php

namespace Sequencial\Http\Controllers\Escola\Painel\CadastrosGradeCurricular;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\CadastroGradeCurricular;

class CadastrosGradeCurricularController extends Controller
{
    public function index(Request $request)
    {
        $fields = [
            'created_at as data',
            'email',
            'curso_titulo as curso',
            'visualizado'
        ];

        $filtro = $request->get('filtro');
        if ($filtro) {
            $registros = CadastroGradeCurricular::where('email', 'like', '%'.$filtro.'%')
                ->orWhere('curso_titulo', 'like', '%'.$filtro.'%')
                ->orderBy('id', 'DESC')
                ->paginate(20, $fields);
        } else {
            $registros = CadastroGradeCurricular::orderBy('id', 'DESC')
                ->paginate(20, $fields);
        }

        return view('escola.painel.cadastros-gradecurricular.index', compact('registros'));
    }

    public function exportar()
    {
        $file = 'escolasequencial_cadastros-gradecurricular_'.date('d-m-Y_His');

        $campos = ['Data', 'E-mail', 'Curso', 'Visualizado'];
        $cadastros = CadastroGradeCurricular::get([
            'created_at as data',
            'email',
            'curso_titulo',
            'visualizado'
        ])
            ->map(function($cadastro) {
                $arr = $cadastro->toArray();
                $arr['data'] = $cadastro->data;
                $arr['visualizado'] = $cadastro->visualizado ? 'Sim' : 'Não';
                return $arr;
            })->prepend(array_values($campos));

        \Excel::create($file, function($excel) use ($cadastros) {
            $excel->sheet('cadastros-gradecurricular', function($sheet) use ($cadastros) {
                $sheet->fromArray($cadastros, null, 'A1', false, false);
            });
        })->download('xls');
    }
}
