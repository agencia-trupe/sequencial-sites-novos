<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Cursos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Curso;
use Sequencial\Models\Escola\CursoTopicos as Topico;
use Sequencial\Libs\Thumbs;

class TopicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $curso = Curso::find($request->curso_id);
      $registros = $curso->topicos;

      return view('escola.painel.cursos.topicos.index')->with(compact('registros', 'curso'));
    }

    public function getCreateDefault(Request $request)
    {
      $curso = Curso::findOrFail($request->curso_id);
      $topicos = [
        'Mercado de Trabalho',
        'Estágio',
        'Laboratórios',
        'Registros',
        'Duração do Curso',
        'Horários das Aulas',
        'Pré-requisitos',
        'Documentos para Matrícula',
        'Links Relacionados'
      ];

      foreach ($topicos as $key => $value) {
        $curso->topicos()->save( new Topico([
          'titulo' => $value,
          'ordem' => $key
        ]));
      }

      $request->session()->flash('sucesso', 'Tópicos criados com sucesso.');
      return redirect()->back();
    }

    public function create(Request $request)
    {
      $curso = Curso::find($request->curso_id);
      return view('escola.painel.cursos.topicos.create')->with(compact('curso'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required',
        'cursos_id' => 'required|exists:mysql_escola.cursos,id'
    	]);

      $object = new Topico;

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->cursos_id = $request->cursos_id;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.escola.cursos.topicos.index', ['curso_id' => $object->cursos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $registro = Topico::find($id);
      $curso = $registro->curso;
      return view('escola.painel.cursos.topicos.edit')->with('registro', $registro)->with('curso', $curso);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Topico::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.cursos.topicos.index', ['curso_id' => $object->cursos_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Topico::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.escola.cursos.topicos.index', ['curso_id' => $object->cursos_id]);
    }

}
