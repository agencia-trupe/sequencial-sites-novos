<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Cursos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Areas;
use Sequencial\Models\Escola\Curso;
use Sequencial\Libs\Thumbs;

class CursosController extends Controller
{
    protected $pathImagens = 'cursos/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $areas = Areas::ordenado()->get();

        $filtro_area = $request->filtro_area;

        if($filtro_area)
            $registros = Curso::where('areas_id', $filtro_area)->orderBy('ordem', 'asc')->get();
        else
            $registros = Curso::ordenado()->get();

        return view('escola.painel.cursos.index')->with(compact('registros', 'areas', 'filtro_area'));
    }

    public function create(Request $request)
    {
        $areas = Areas::ordenado()->get();
        return view('escola.painel.cursos.create', ['areas' => $areas]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required|unique:mysql_escola.cursos,titulo'
        ]);

        $object = new Curso;

        $object->areas_id = $request->areas_id;
        $object->prefixo = $request->prefixo;
        $object->titulo = $request->titulo;
        $object->slug = str_slug($request->prefixo.'-'.$request->titulo);
        $object->texto = $request->texto;
        $object->cor_curso = $request->cor_curso;
        $object->video = $request->video;

        $imagem = Thumbs::make($request, 'escola','imagem', 400, null, $this->pathImagens);
        if ($imagem) {
            Thumbs::make($request, 'escola','imagem', 200, 200, $this->pathImagens.'thumbs/');
            $object->imagem = $imagem;
        }

        if ($request->hasFile('grade_curricular_arquivo')) {
            $file = $request->file('grade_curricular_arquivo');
            $fileName = str_random(16).$file->getClientOriginalName();
            $file->move(public_path('assets/escola/grade/'), $fileName);
            $object->grade_curricular_arquivo = $fileName;
        }

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Registro criado com sucesso.');

            return redirect()->route('painel.escola.cursos.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $areas = Areas::ordenado()->get();
        return view('escola.painel.cursos.edit')->with('registro', Curso::find($id))->with('areas', $areas);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titulo' => 'required|unique:mysql_escola.cursos,titulo,'.$id
        ]);

        $object = Curso::find($id);

        $object->areas_id = $request->areas_id;
        $object->prefixo = $request->prefixo;
        $object->titulo = $request->titulo;
        $object->slug = str_slug($request->prefixo.'-'.$request->titulo);
        $object->texto = $request->texto;
        $object->cor_curso = $request->cor_curso;
        $object->video = $request->video;

        $imagem = Thumbs::make($request, 'escola','imagem', 400, null, $this->pathImagens);
        if ($imagem) {
            Thumbs::make($request, 'escola','imagem', 200, 200, $this->pathImagens.'thumbs/');
            $object->imagem = $imagem;
        }

        if ($request->hasFile('grade_curricular_arquivo')) {
            $file = $request->file('grade_curricular_arquivo');
            $fileName = str_random(16).'_'.$file->getClientOriginalName();
            $file->move(public_path('assets/escola/grade/'), $fileName);
            $object->grade_curricular_arquivo = $fileName;
        }

        try {

            $object->save();

            $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

            return redirect()->route('painel.escola.cursos.index');

        } catch (\Exception $e) {

            $request->flash();

            return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

        }
    }

    public function destroy(Request $request, $id){
        $object = Curso::find($id);
        $object->delete();

        $request->session()->flash('sucesso', 'Registro removido com sucesso.');

        return redirect()->route('painel.escola.cursos.index');
    }

}
