<?php

namespace Sequencial\Http\Controllers\Escola\Painel\BolsasDeEstudo;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\BolsasDeEstudo;
use Sequencial\Libs\Thumbs;

class BolsasDeEstudoController extends Controller
{
    protected $pathImagens = 'bolsas-de-estudo/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = BolsasDeEstudo::all();

      return view('escola.painel.bolsas-de-estudo.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('escola.painel.bolsas-de-estudo.edit')->with('registro', BolsasDeEstudo::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = BolsasDeEstudo::find($id);

      $object->titulo = $request->titulo;
      $object->olho = $request->olho;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'escola', 'imagem', 400, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'escola', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.escola.bolsas-de-estudo.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
