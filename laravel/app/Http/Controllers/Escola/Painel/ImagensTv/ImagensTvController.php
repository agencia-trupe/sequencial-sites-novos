<?php

namespace Sequencial\Http\Controllers\Escola\Painel\ImagensTv;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\ImagemTv;
use Sequencial\Libs\Thumbs;

class ImagensTvController extends Controller
{
    public $unidades;

    public function __construct()
    {
        $this->unidades = ImagemTv::unidades;
        view()->share('unidades', $this->unidades);
    }

    public function index()
    {
        $unidade = request('unidade');

        if (! $unidade) {
            $unidade = array_keys($this->unidades)[0];
        }

        if (! array_key_exists($unidade, $this->unidades)) {
            abort('404');
        }

        $imagens = ImagemTv::where('unidade', $unidade)->ordenados()->get();

        return view('escola.painel.tv.index', compact('unidade', 'imagens'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'imagem' => 'required_without:video|image',
            'video' => 'required_without:imagem|mimes:mp4'
        ], [
            'imagem.required_without' => 'Adicione um dos arquivos.',
            'video.required_without'  => 'Adicione um dos arquivos.',
            'imagem.image'            => 'O campo imagem deve ser um arquivo de imagem.',
            'video.mimes'            => 'O campo video deve ser um arquivo mp4.'
        ]);

        $object = new ImagemTv;
        $object->unidade = $request->unidade;

        if ($request->hasFile('imagem')) {
            $object->imagem  = Thumbs::make($request, 'escola', 'imagem', 1920, 1080, 'tv/');
        } elseif ($request->hasFile('video')) {
            $file = $request->file('video');
            $fileName = date('YmdHis').$file->getClientOriginalName();
            $file->move(public_path('assets/escola/img/tv/'), $fileName);
            $object->video = $fileName;
        }

        try {

            $object->save();
            $request->session()->flash('sucesso', 'Registro criado com sucesso.');
            return redirect()->route('painel.escola.tv.index', ['unidade' => $object->unidade]);

        } catch (\Exception $e) {

            $request->flash();
            return back()->withErrors(array('Erro ao criar registro. ('.$e->getMessage().')'));

        }
    }

    public function destroy($id) {
        $object = ImagemTv::find($id);
        $object->delete();

        session()->flash('sucesso', 'Registro removido com sucesso.');

        return redirect()->route('painel.escola.tv.index', ['unidade' => $object->unidade]);
    }
}
