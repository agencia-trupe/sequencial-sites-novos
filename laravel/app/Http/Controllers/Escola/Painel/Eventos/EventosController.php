<?php

namespace Sequencial\Http\Controllers\Escola\Painel\Eventos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Escola\Evento;
use Sequencial\Models\Escola\EventoImagem;
use Sequencial\Libs\Thumbs;

class EventosController extends Controller
{
  protected $pathImagens = 'eventos/';

  /**
  * Display a listing of the resource.
  *
  * @return Response
  */
  public function index(Request $request)
  {
    $registros = Evento::ordenado()->get();

    return view('escola.painel.eventos.index')->with(compact('registros'));
  }

  public function create(Request $request)
  {
    return view('escola.painel.eventos.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'titulo' => 'required|unique:mysql_escola.eventos,titulo',
      'data' => 'required|date_format:d/m/Y',
      'imagem' => 'sometimes|image'
    ]);

    $object = new Evento;

    $object->titulo = $request->titulo;
    $object->subtitulo = $request->subtitulo;
    $object->slug = str_slug($request->titulo);
    $object->data = $request->data;
    $object->texto = $request->texto;
    $object->is_juramento = $request->is_juramento ? 1 : 0;

    $imagem = Thumbs::make($request, 'escola', 'imagem', 730, null, $this->pathImagens);
    if($imagem){
      Thumbs::make($request, 'escola', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      $object->imagem = $imagem;
    }

    try {

      $object->save();

      $imagens_albuns = $request->imagem_album;
      if(sizeof($imagens_albuns)){
        foreach ($imagens_albuns as $ordem => $img) {
          $object->imagens()->save( new EventoImagem([
            'imagem' => $img,
            'ordem' => $ordem
          ])
        );
      }
    }

    $request->session()->flash('sucesso', 'Registro criado com sucesso.');

    return redirect()->route('painel.escola.eventos.index');

  } catch (\Exception $e) {

    $request->flash();

    return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

  }
}


/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return Response
*/
public function edit($id)
{
  return view('escola.painel.eventos.edit')->with('registro', Evento::find($id));
}

/**
* Update the specified resource in storage.
*
* @param  Request  $request
* @param  int  $id
* @return Response
*/
public function update(Request $request, $id)
{
  $this->validate($request, [
    'titulo' => 'required|unique:mysql_escola.eventos,titulo,'.$id,
    'data' => 'required|date_format:d/m/Y',
    'imagem' => 'sometimes|image'
  ]);

  $object = Evento::find($id);

  $object->titulo = $request->titulo;
  $object->subtitulo = $request->subtitulo;
  $object->slug = str_slug($request->titulo);
  $object->data = $request->data;
  $object->texto = $request->texto;
  $object->is_juramento = $request->is_juramento;

  $imagem = Thumbs::make($request, 'escola', 'imagem', 730, null, $this->pathImagens);
  if($imagem){
    Thumbs::make($request, 'escola', 'imagem', 200, null, $this->pathImagens.'thumbs/');
    $object->imagem = $imagem;
  }

  try {

    $object->save();

    foreach ($object->imagens as $img_atual)
    $img_atual->delete();

    $imagens_albuns = $request->imagem_album;
    if(sizeof($imagens_albuns)){
      foreach ($imagens_albuns as $ordem => $img) {
        $object->imagens()->save( new EventoImagem([
          'imagem' => $img,
          'ordem' => $ordem
        ])
      );
    }
  }

  $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

  return redirect()->route('painel.escola.eventos.index');

} catch (\Exception $e) {

  $request->flash();

  return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

}
}

public function destroy(Request $request, $id){
  $object = Evento::find($id);
  $object->delete();

  $request->session()->flash('sucesso', 'Registro removido com sucesso.');

  return redirect()->route('painel.escola.eventos.index');
}

}
