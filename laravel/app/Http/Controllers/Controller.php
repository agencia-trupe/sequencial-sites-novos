<?php

namespace Sequencial\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;


class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct(){
      //view()->share('contato', FaleCom::first());

      // Se necessário posso criar até 4 base Controllers

      // Http/Controllers/Escola/Site/SiteBaseController
      // Http/Controllers/Escola/Painel/PainelBaseController

      // Http/Controllers/Faculdade/Site/SiteBaseController
      // Http/Controllers/Faculdade/Painel/PainelBaseController
    }
}
