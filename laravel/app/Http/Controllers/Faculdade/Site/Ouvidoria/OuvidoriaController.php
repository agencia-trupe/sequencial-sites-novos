<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Ouvidoria;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Mail;
use Sequencial\Models\Faculdade\Mensagem;
use Sequencial\Models\Faculdade\Ouvidoria;

class OuvidoriaController extends SiteBaseController
{

    public function getIndex(Request $request)
    {
        $texto = Ouvidoria::first();

        return view('faculdade.site.ouvidoria.index', compact('texto'));
    }

    public function postEnviarOuvidoria(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required',
            'email' => 'required|email',
            'mensagem' => 'required',
            'cpf' => 'required'
        ]);

        $data['nome'] = $request->input('nome');
        $data['email'] = $request->input('email');
        $data['telefone'] = $request->input('telefone');
        $data['cpf'] = $request->input('cpf');
        $data['mensagem'] = $request->input('mensagem');

        Mail::send('faculdade.emails.contato', $data, function($message) use ($data)
        {
            $message->to('contato@sequencialctp.com.br')
                ->subject('Mensagem para Ouvidoria Faculdade Sequencial -'.$data['nome'])
                ->replyTo($data['email'], $data['nome']);
        });

        $registro = Mensagem::create($data);
        $registro->origem = 'ouvidoria';
        $registro->save();

        $request->session()->flash('contato_enviado', true);

        return redirect()->route('faculdade.ouvidoria.index');
    }

}
