<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Novidades;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\Novidade;

class NovidadesController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $novidades = Novidade::ordenado()->paginate(20);

    return view('faculdade.site.novidades.index', [
      'novidades' => $novidades
    ]);
  }


  public function getDetalhes(Request $request, $slug)
  {
    $novidades = Novidade::ordenado()->paginate(20);
    $novidade = Novidade::where('slug', $slug)->first();

    return view('faculdade.site.novidades.detalhes', [
      'novidades' => $novidades,
      'detalhe' => $novidade
    ]);
  }


}
