<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\FaleConosco;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Mail;

use Sequencial\Models\Faculdade\Localizacao;
use Sequencial\Models\Faculdade\Mensagem;

class FaleConoscoController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $texto = Localizacao::first();

    return view('faculdade.site.fale-conosco.index', [
      'texto' => $texto
    ]);
  }

  public function postEnviarContato(Request $request)
  {
    $this->validate($request, [
      'nome' => 'required',
      'email' => 'required|email',
      'mensagem' => 'required'
    ]);

    $data['nome'] = $request->input('nome');
    $data['email'] = $request->input('email');
    $data['telefone'] = $request->input('telefone');
    $data['cpf'] = $request->input('cpf');
    $data['mensagem'] = $request->input('mensagem');

    Mail::send('faculdade.emails.contato', $data, function($message) use ($data)
    {
      $message->to('contato@sequencialctp.com.br')
              ->subject('Contato via site - '.$data['nome'])
              ->replyTo($data['email'], $data['nome']);
    });

    $registro = Mensagem::create($data);
    $registro->origem = 'fale_conosco';
    $registro->save();

    $request->session()->flash('contato_enviado', true);

    return redirect()->route('faculdade.fale-conosco.index');
  }

}
