<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\VestibularSocial;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\VestibularSocial;
use Sequencial\Models\Faculdade\VestibularSocialArquivos;

class VestibularSocialController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $texto = VestibularSocial::first();
    $arquivos = VestibularSocialArquivos::ordenado()->get();

    return view('faculdade.site.vestibular_social.index', [
      'texto' => $texto,
      'arquivos' => $arquivos
    ]);
  }


}
