<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\InscrevaSe;

use Illuminate\Http\Request;

use Sequencial\Http\Requests;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\VestibularSocial;
use Sequencial\Models\Faculdade\VestibularSocialArquivos;
use Sequencial\Models\Faculdade\CursoGraduacao;
use Sequencial\Models\Faculdade\Inscricao;

class InscrevaSeController extends SiteBaseController
{
    public function index()
    {
        $texto    = VestibularSocial::first();
        $arquivos = VestibularSocialArquivos::ordenado()->get();
        $cursos   = CursoGraduacao::ordenado()->get(['id', 'titulo']);

        return view('faculdade.site.inscreva-se.index', compact('texto', 'arquivos', 'cursos'));
    }

    public function post(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required',
            'curso'    => 'required',
            'origem'   => 'required',
            'optIn'    => 'accepted'
        ]);

        Inscricao::create($request->except('optIn'));

        return redirect()->back()->with('enviado', true);
    }
}
