<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Home;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\Banner;
use Sequencial\Models\Faculdade\Chamada;
use Sequencial\Models\Faculdade\CursoGraduacao;
use Sequencial\Models\Faculdade\Depoimento;

class HomeController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $banners = Banner::ordenado()->get();

    $chamadas = Chamada::semCalhaus()->ordenado()->limit(2)->get();

    $calhaus = Chamada::calhaus()->ordenado()->get();

    $cursos = CursoGraduacao::ordenado()->get();
    $depoimento = Depoimento::inRandomOrder()->first();

    return view('faculdade.site.home.index', [
      'banners' => $banners,
      'chamadas' => $chamadas,
      'calhaus'  => $calhaus,
      'cursos' => $cursos,
      'depoimento' => $depoimento
    ]);
  }

  public function getChat(Request $request)
  {
    $banners = Banner::ordenado()->get();

    $chamadas = Chamada::semCalhaus()->ordenado()->limit(2)->get();

    $calhaus = Chamada::calhaus()->ordenado()->get();

    $cursos = CursoGraduacao::ordenado()->get();
    $depoimento = Depoimento::inRandomOrder()->first();

    return view('faculdade.site.home.chat', [
      'banners' => $banners,
      'chamadas' => $chamadas,
      'calhaus'  => $calhaus,
      'cursos' => $cursos,
      'depoimento' => $depoimento
    ]);
  }

}
