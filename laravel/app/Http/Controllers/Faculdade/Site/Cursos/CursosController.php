<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Cursos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\CursoExtensao;
use Sequencial\Models\Faculdade\CursoLivre;
use Sequencial\Models\Faculdade\Apresentacao;

class CursosController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $cursos_extensao = CursoExtensao::proximos()->ordenado()->get();
    $cursos_livres = CursoLivre::proximos()->ordenado()->get();

    return view('faculdade.site.outros_cursos.index', [
      'cursos_extensao' => $cursos_extensao,
      'cursos_livres' => $cursos_livres
    ]);
  }


  public function getCursoLivre(Request $request, $slug)
  {
    $regimento = Apresentacao::first();
    $curso = CursoLivre::where('slug', $slug)->first();
    $cursos = CursoLivre::proximos()->ordenado()->get();

    return view('faculdade.site.outros_cursos.detalhes', [
      'slug_area' => 'cursos_livres',
      'regimento' => $regimento,
      'titulo_pagina' => 'CURSOS LIVRES',
      'curso' => $curso,
      'cursos' => $cursos,
      'rota' => 'curso-livre'
    ]);
  }

  public function getCursoExtensao(Request $request, $slug)
  {
    $regimento = Apresentacao::first();
    $curso = CursoExtensao::where('slug', $slug)->first();
    $cursos = CursoExtensao::proximos()->ordenado()->get();

    return view('faculdade.site.outros_cursos.detalhes', [
      'slug_area' => 'cursos_extensao',
      'regimento' => $regimento,
      'titulo_pagina' => 'CURSOS DE EXTENSÃO',
      'curso' => $curso,
      'cursos' => $cursos,
      'rota' => 'curso-de-extensao'
    ]);
  }

}
