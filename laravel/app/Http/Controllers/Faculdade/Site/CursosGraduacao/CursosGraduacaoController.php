<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\CursosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\CursoGraduacao;
use Sequencial\Models\Faculdade\Lead;

use Mail;

class CursosGraduacaoController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $cursos = CursoGraduacao::ordenado()->get();

    return view('faculdade.site.cursos_graduacao.index', [
      'cursos' => $cursos
    ]);
  }

  public function getDetalhes(Request $request, $slug_curso = '')
  {
    $cursos = CursoGraduacao::ordenado()->get();
    $cursoDetalhes = CursoGraduacao::where('slug', $slug_curso)->first();

    if(!$cursos) abort('404');

    return view('faculdade.site.cursos_graduacao.detalhes', [
      'cursos' => $cursos,
      'cursoDetalhes' => $cursoDetalhes,
    ]);
  }

  public function lead(Request $request)
  {
    $this->validate($request, [
      'email' => 'required|email',
      'curso' => 'required'
    ]);

    $curso = CursoGraduacao::findOrFail($request->get('curso'));

    $data = [
      'email'   => $request->get('email'),
      'curso'   => $curso->titulo,
      'origem'  => 'Mercado de Trabalho'
    ];

    if (! Lead::where('email', $request->get('email'))->where('curso', $curso->titulo)->first()) {
      Lead::create($data);
    }

    $data['curso'] = mb_convert_case($curso->titulo, MB_CASE_TITLE, "UTF-8");
    $data['arquivo'] = asset('assets/faculdade/arquivos/'.$curso->arquivo_sobre_o_mercado);

    Mail::send('faculdade.emails.lead-mercado', $data, function($m) use ($data)
    {
      $m->to($data['email'])
        ->subject('O Mercado de Trabalho para o Profissional de '.$data['curso'])
        ->replyTo('contato@sequencialctp.com.br', 'Faculdade Sequencial');
    });

    return response()->json(['enviado' => true]);
  }

}
