<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Depoimentos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\Depoimento;

class DepoimentosController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $depoimentos = Depoimento::ordenado()->get();

    return view('faculdade.site.depoimentos.index', [
      'depoimentos' => $depoimentos
    ]);
  }


}
