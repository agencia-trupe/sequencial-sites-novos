<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Busca;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\CursoGraduacao;
use Sequencial\Models\Faculdade\CursoPosGraduacao;
use Sequencial\Models\Faculdade\CursoExtensao;
use Sequencial\Models\Faculdade\CursoLivre;
use Sequencial\Models\Faculdade\Evento;
use Sequencial\Models\Faculdade\Novidade;

class BuscaController extends SiteBaseController
{

  public function postBusca(Request $request)
  {
    $termo = $request->termo;
    $resultados = [];

    $qry_graduacao = CursoGraduacao::busca($termo)->get();
    foreach($qry_graduacao as $qry){
      $resultados['graduacao'][$qry->id]['link']  = 'cursos-de-graduacao/'.$qry->slug;
      $resultados['graduacao'][$qry->id]['secao'] = 'CURSOS DE GRADUAÇÃO';
      $resultados['graduacao'][$qry->id]['titulo']= $qry->titulo;
      $resultados['graduacao'][$qry->id]['texto'] = count($qry->topicos) > 0 ? $qry->topicos[0]->texto : '';
    }

    $qry_posgraduacao = CursoPosGraduacao::busca($termo)->get();
    foreach($qry_posgraduacao as $qry){
      $resultados['posgraduacao'][$qry->id]['link']  = 'cursos-de-pos-graduacao/'.$qry->area->slug.'/'.$qry->slug;
      $resultados['posgraduacao'][$qry->id]['secao'] = 'CURSOS DE PÓS-GRADUAÇÃO';
      $resultados['posgraduacao'][$qry->id]['titulo']= $qry->titulo;
      $resultados['posgraduacao'][$qry->id]['texto'] = count($qry->topicos) > 0 ? $qry->topicos[0]->texto : '';
    }

    $qry_cursosextensao = CursoExtensao::busca($termo)->get();
    foreach($qry_cursosextensao as $qry){
      $resultados['cursosextensao'][$qry->id]['link']  = 'cursos/curso-de-extensao/'.$qry->slug;
      $resultados['cursosextensao'][$qry->id]['secao'] = 'CURSOS DE EXTENSÃO UNIVERSITÁRIA';
      $resultados['cursosextensao'][$qry->id]['titulo']= $qry->titulo;
      $resultados['cursosextensao'][$qry->id]['texto'] = $qry->texto;
    }

    $qry_cursoslivres = CursoLivre::busca($termo)->get();
    foreach($qry_cursoslivres as $qry){
      $resultados['cursoslivres'][$qry->id]['link']  = 'cursos/curso-livre/'.$qry->slug;
      $resultados['cursoslivres'][$qry->id]['secao'] = 'CURSOS LIVRES';
      $resultados['cursoslivres'][$qry->id]['titulo']= $qry->titulo;
      $resultados['cursoslivres'][$qry->id]['texto'] = $qry->texto;
    }

    $qry_eventos = Evento::busca($termo)->get();
    foreach($qry_eventos as $qry){
      $resultados['eventos'][$qry->id]['link']  = 'eventos/'.$qry->slug;
      $resultados['eventos'][$qry->id]['secao'] = 'EVENTOS';
      $resultados['eventos'][$qry->id]['titulo']= $qry->titulo;
      $resultados['eventos'][$qry->id]['texto'] = $qry->texto;
    }

    $qry_novidades = Novidade::busca($termo)->get();
    foreach($qry_novidades as $qry){
      $resultados['novidades'][$qry->id]['link']  = 'novidades/'.$qry->slug;
      $resultados['novidades'][$qry->id]['secao'] = 'NOVIDADES';
      $resultados['novidades'][$qry->id]['titulo']= $qry->titulo;
      $resultados['novidades'][$qry->id]['texto'] = $qry->texto;
    }

    return view('faculdade.site.busca.index', [
      'resultados' => $resultados,
      'termo' => $termo
    ]);
  }

}
