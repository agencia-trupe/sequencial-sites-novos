<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\Estagios;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\Estagio;

class EstagiosController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $texto_nube = Estagio::where('slug', 'nube')->first();
    $texto_ciee = Estagio::where('slug', 'ciee')->first();
    $texto_fundap = Estagio::where('slug', 'fundap')->first();

    return view('faculdade.site.estagios.index', [
      'texto_nube'   => $texto_nube,
      'texto_ciee'   => $texto_ciee,
      'texto_fundap' => $texto_fundap
    ]);
  }


}
