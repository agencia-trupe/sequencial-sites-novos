<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\AFaculdade;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\Apresentacao;
use Sequencial\Models\Faculdade\ApresentacaoArquivos;
use Sequencial\Models\Faculdade\Localizacao;
use Sequencial\Models\Faculdade\Infraestrutura;
use Sequencial\Models\Faculdade\InfraestruturaAlbum;
use Sequencial\Models\Faculdade\InfraestruturaAlbumImagem;

class AFaculdadeController extends SiteBaseController
{

  public function getApresentacao(Request $request)
  {
    $texto = Apresentacao::first();
    $arquivos = ApresentacaoArquivos::ordenado()->get();

    return view('faculdade.site.a-faculdade.apresentacao', [
      'texto' => $texto,
      'arquivos' => $arquivos,
      'marcar' => 'apresentacao'
    ]);
  }

  public function getLocalizacao(Request $request)
  {
    $texto = Localizacao::first();

    return view('faculdade.site.a-faculdade.localizacao', [
      'texto' => $texto,
      'marcar' => 'localizacao'
    ]);
  }

  public function getInfraestrutura(Request $request)
  {
    $texto = Infraestrutura::first();
    $imagens_sem_album = ApresentacaoArquivos::ordenado()->get();
    $albuns_infra = InfraestruturaAlbum::ordenado()->where('tipo', 'infraestrutura')->get();
    $albuns_labs = InfraestruturaAlbum::ordenado()->where('tipo', 'laboratorios')->get();
    $imagens_sem_album = InfraestruturaAlbumImagem::whereDoesntHave('album')->ordenado()->get();

    return view('faculdade.site.a-faculdade.infraestrutura', [
      'texto'  => $texto,
      'marcar' => 'infraestrutura',
      'albuns_infra' => $albuns_infra,
      'albuns_labs'  => $albuns_labs,
      'imagens_sem_album' => $imagens_sem_album
    ]);
  }


}
