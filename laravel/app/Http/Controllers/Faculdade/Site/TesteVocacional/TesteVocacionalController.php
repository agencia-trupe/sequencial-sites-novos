<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\TesteVocacional;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\TesteVocacional as Teste;
use Sequencial\Models\Faculdade\Lead;

class TesteVocacionalController extends SiteBaseController
{
    public function index()
    {
        return view('faculdade.site.teste-vocacional.index');
    }

    public function cadastro(Request $request)
    {
        $this->validate($request, [
            'nome'     => 'required',
            'email'    => 'required|email',
            'telefone' => 'required'
        ], [
            'nome.required'     => 'Preencha seu nome.',
            'email.required'    => 'Preencha seu e-mail.',
            'email.email'       => 'Insira um endereço de e-mail válido.',
            'telefone.required' => 'Preencha seu telefone.'
        ]);

        try {
            if ($lead = Lead::where('email', request('email'))->first()) {
                if (! $lead->nome) {
                    $lead->nome = request('nome');
                }
                if (! $lead->telefone) {
                    $lead->telefone = request('telefone');
                }
                $lead->save();
            } else {
                $lead = Lead::create([
                    'nome'     => request('nome'),
                    'email'    => request('email'),
                    'telefone' => request('telefone'),
                    'origem'   => 'Teste Vocacional'
                ]);
            }
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao realizar o cadastro. Tente novamente.'])->withInput();
        }

        session()->forget('testeVocacional');
        session()->put('testeVocacional', [
            'lead'      => $lead,
            'etapa'     => 0,
            'enviado'   => false,
            'pontuacao' => [
                'A' => 0,
                'B' => 0,
                'C' => 0,
                'D' => 0,
                'E' => 0,
                'F' => 0,
                'G' => 0,
            ]
        ]);

        return redirect()->route('faculdade.teste-vocacional.etapas');
    }

    protected function proximaEtapa()
    {
        $session = session('testeVocacional');

        if (! $session) abort(500);

        $session['etapa'] += 1;
        session()->put('testeVocacional', $session);
    }

    public function etapas()
    {
        $session = session('testeVocacional');

        if (! $session || $session['etapa'] < 0) {
            return redirect()->route('faculdade.teste-vocacional.index');
        }

        if ($session['etapa'] == 0) {
            $this->proximaEtapa($session);
            return view('faculdade.site.teste-vocacional.instrucoes');
        } elseif ($session['etapa'] > 0 && $session['etapa'] <= count(Teste::etapas)) {
            $etapa = Teste::etapas[$session['etapa'] - 1];
            $etapa['ordem'] = $session['etapa'];
            return view('faculdade.site.teste-vocacional.etapa', compact('etapa'));
        } elseif ($session['etapa'] > count(Teste::etapas)) {
            return redirect()->route('faculdade.teste-vocacional.resultado');
        }
    }

    protected function somaPontos()
    {
        $session = session('testeVocacional');

        if (! $session) abort(500);

        foreach(array_keys(Teste::inteligencias) as $key) {
            $session['pontuacao'][$key] += (int)request($key);
        }
        session()->put('testeVocacional', $session);
    }

    public function etapasPost(Request $request)
    {
        $session = session('testeVocacional');

        if (! $session || $session['etapa'] <= 0 || $session['etapa'] > count(Teste::etapas)) {
            return redirect()->route('faculdade.teste-vocacional.index');
        }

        $this->validate($request, [
            'A' => 'required|integer|between:1,7|different:B|different:C|different:D|different:E|different:F|different:G',
            'B' => 'required|integer|between:1,7|different:A|different:C|different:D|different:E|different:F|different:G',
            'C' => 'required|integer|between:1,7|different:A|different:B|different:D|different:E|different:F|different:G',
            'D' => 'required|integer|between:1,7|different:A|different:B|different:C|different:E|different:F|different:G',
            'E' => 'required|integer|between:1,7|different:A|different:B|different:C|different:D|different:F|different:G',
            'F' => 'required|integer|between:1,7|different:A|different:B|different:C|different:D|different:E|different:G',
            'G' => 'required|integer|between:1,7|different:A|different:B|different:C|different:D|different:E|different:F',
        ]);

        if (request('etapa') == $session['etapa']) {
            $this->somaPontos();
            $this->proximaEtapa();
        }

        return redirect()->route('faculdade.teste-vocacional.etapas');
    }

    public function resultado()
    {
        $session = session('testeVocacional');

        if (! $session || $session['etapa'] <= count(Teste::etapas)) {
            return redirect()->route('faculdade.teste-vocacional.index');
        }

        $session['lead']->update([
            'teste_vocacional' => \Carbon\Carbon::now(),
            'teste_vocacional_resultado' => json_encode($session['pontuacao'])
        ]);

        $resultado = $this->calculaResultado($session['pontuacao']);

        return view('faculdade.site.teste-vocacional.resultado', compact('resultado'));
    }

    protected function calculaResultado($pontuacao)
    {
        $tabela        = [];
        $inteligencias = [];
        $ocupacoes     = [];

        foreach($pontuacao as $key => $pontos) {
            $tabela[$key] = [
                'titulo' => Teste::inteligencias[$key],
                'pontos' => $pontos
            ];
        }

        arsort($pontuacao);

        $valorMaximo = $pontuacao[array_keys($pontuacao)[0]];

        foreach($pontuacao as $key => $pontos) {
            if ($pontos == $valorMaximo) {
                $inteligencias[] = [
                    'titulo'    => Teste::inteligencias[$key],
                    'descricao' => Teste::resultados[$key]['texto']
                ];
                $ocupacoes = array_merge($ocupacoes, Teste::resultados[$key]['ocupacoes']);
            }
        }

        return [
            'pontuacao'     => $tabela,
            'inteligencias' => $inteligencias,
            'ocupacoes'     => array_unique($ocupacoes),
        ];
    }

    public function enviarEmail()
    {
        $session = session('testeVocacional');

        if ($session['enviado']) {
            return redirect()->route('faculdade.teste-vocacional.resultado');
        }

        $resultado = $this->calculaResultado($session['pontuacao']);
        $lead      = $session['lead'];

        Mail::send('faculdade.emails.teste-vocacional', $resultado, function($m) use ($lead)
        {
            $m->to($lead->email, $lead->nome)
               ->subject('[TESTE VOCACIONAL] Faculdade Sequencial');
        });

        $session['enviado'] = true;
        session()->put('testeVocacional', $session);

        return redirect()->route('faculdade.teste-vocacional.resultado')
            ->with('enviado', true);
    }
}
