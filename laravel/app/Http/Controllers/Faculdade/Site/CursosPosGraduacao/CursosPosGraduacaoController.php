<?php

namespace Sequencial\Http\Controllers\Faculdade\Site\CursosPosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Faculdade\Site\SiteBaseController;

use Sequencial\Models\Faculdade\AreaPosGraduacao;
use Sequencial\Models\Faculdade\CursoPosGraduacao;

class CursosPosGraduacaoController extends SiteBaseController
{

  public function getIndex(Request $request)
  {
    $areas_pos = AreaPosGraduacao::ordenado()->get();

    return view('faculdade.site.cursos_pos_graduacao.index', [
      'areas_pos' => $areas_pos
    ]);
  }

  public function getDetalhes(Request $request, $slug_area = '', $slug_curso = '')
  {
    $area = AreaPosGraduacao::where('slug', $slug_area)->first();

    if(!$area) abort('404');

    $cursoDetalhes = CursoPosGraduacao::where('slug', $slug_curso)->where('areas_pos_graduacao_id', $area->id)->first();

    if(!$cursoDetalhes) abort('404');

    return view('faculdade.site.cursos_pos_graduacao.detalhes', [
      'area' => $area,
      'cursoDetalhes' => $cursoDetalhes,
    ]);
  }

}
