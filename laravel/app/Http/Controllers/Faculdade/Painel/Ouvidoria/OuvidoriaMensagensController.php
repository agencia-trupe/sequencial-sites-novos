<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Ouvidoria;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Mensagem;

class OuvidoriaMensagensController extends Controller
{
    public function index()
    {
      $registros = Mensagem::where('origem', 'ouvidoria')->ordenado()->get();

      return view('faculdade.painel.ouvidoria.mensagens.index')->with(compact('registros'));
    }

    public function show($id)
    {
      return view('faculdade.painel.ouvidoria.mensagens.show')->with('registro', Mensagem::find($id));
    }
}
