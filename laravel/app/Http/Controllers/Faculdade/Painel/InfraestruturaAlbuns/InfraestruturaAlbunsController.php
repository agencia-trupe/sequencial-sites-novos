<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\InfraestruturaAlbuns;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\InfraestruturaAlbum;
use Sequencial\Models\Faculdade\InfraestruturaAlbumImagem;
use Sequencial\Libs\Thumbs;

class InfraestruturaAlbunsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $tipo = $request->tipo;

      if($tipo == 'infraestrutura')
        $registros = InfraestruturaAlbum::where('tipo', 'infraestrutura')->ordenado()->get();
      elseif($tipo == 'laboratorios')
        $registros = InfraestruturaAlbum::where('tipo', 'laboratorios')->ordenado()->get();
      elseif($tipo == 'sem-album')
        $registros = [];
      else{
        $tipo = 'infraestrutura';
        $registros = InfraestruturaAlbum::where('tipo', 'infraestrutura')->ordenado()->get();
      }

      return view('faculdade.painel.infraestrutura_albuns.index')->with(compact('registros', 'tipo'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.infraestrutura_albuns.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = new InfraestruturaAlbum;

      $object->titulo = $request->titulo;
      $object->tipo = $request->tipo;

      try {

        $object->save();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new InfraestruturaAlbumImagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.infraestrutura_albuns.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.infraestrutura_albuns.edit')->with('registro', InfraestruturaAlbum::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = InfraestruturaAlbum::find($id);

      $object->titulo = $request->titulo;
      $object->tipo = $request->tipo;

      try {

        $object->save();

        foreach ($object->imagens as $img_atual)
          $img_atual->delete();

        $imagens_albuns = $request->imagem_album;
        if(sizeof($imagens_albuns)){
          foreach ($imagens_albuns as $ordem => $img) {
            $object->imagens()->save( new InfraestruturaAlbumImagem([
                'imagem' => $img,
                'ordem' => $ordem
              ])
            );
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.infraestrutura_albuns.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = InfraestruturaAlbum::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.infraestrutura_albuns.index');
    }

}
