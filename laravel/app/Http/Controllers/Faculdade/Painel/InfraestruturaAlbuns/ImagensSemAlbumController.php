<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\InfraestruturaAlbuns;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\InfraestruturaAlbumImagem;

class ImagensSemAlbumController extends Controller
{

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit()
  {
    $imgs_sem_album = InfraestruturaAlbumImagem::whereDoesntHave('album')->ordenado()->get();
    return view('faculdade.painel.infraestrutura_albuns.edit_sem_album')->with(compact('imgs_sem_album'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  Request  $request
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request)
  {
    try {

      $imgs_sem_album = InfraestruturaAlbumImagem::whereDoesntHave('album')->get();

      foreach ($imgs_sem_album as $img_atual)
        $img_atual->delete();

      $imagens_albuns = $request->imagem_album;
      if(sizeof($imagens_albuns)){
        foreach ($imagens_albuns as $ordem => $img) {
          $imagem = new InfraestruturaAlbumImagem([
            'imagem' => $img,
            'ordem' => $ordem
          ]);
          $imagem->save();
        }
      }

      $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

      return redirect()->route('painel.faculdade.infraestrutura_albuns.index');

    } catch (\Exception $e) {

      $request->flash();

      return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

    }

}}
