<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\CursosExtensao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\CursoExtensao;
use Sequencial\Libs\Thumbs;

class CursosExtensaoController extends Controller
{
    protected $pathImagens = 'cursos_extensao/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = CursoExtensao::ordenado()->get();

      return view('faculdade.painel.cursos_extensao.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.cursos_extensao.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:mysql_faculdade.cursos_extensao,titulo',
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = new CursoExtensao;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->texto = $request->texto;
      $object->data = $request->data;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 400, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_extensao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.cursos_extensao.edit')->with('registro', CursoExtensao::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:mysql_faculdade.cursos_extensao,titulo,'.$id,
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = CursoExtensao::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->texto = $request->texto;
      $object->data = $request->data;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 400, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_extensao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = CursoExtensao::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.cursos_extensao.index');
    }

}
