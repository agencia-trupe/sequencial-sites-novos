<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Infraestrutura;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Infraestrutura;
use Sequencial\Libs\Thumbs;

class InfraestruturaController extends Controller
{
    protected $pathImagens = 'infraestrutura/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Infraestrutura::all();

      return view('faculdade.painel.infraestrutura.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.infraestrutura.edit')->with('registro', Infraestrutura::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Infraestrutura::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 480, 230, $this->pathImagens);
      if($imagem)
      	$object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.infraestrutura.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
