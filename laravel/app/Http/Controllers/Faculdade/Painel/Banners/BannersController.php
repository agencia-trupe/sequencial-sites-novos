<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Banners;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use View;
use Sequencial\Models\Faculdade\Banner;
use Sequencial\Libs\Thumbs;

class BannersController extends Controller
{

  protected $pathImagens = 'banners/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Banner::ordenado()->get();

      return view('faculdade.painel.banners.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.banners.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = new Banner;

      $object->titulo = $request->titulo;
      $object->link = $request->link;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 950, 400, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 380, 160, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.banners.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.banners.edit')->with('registro', Banner::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Banner::find($id);

      $object->titulo = $request->titulo;
      $object->link = $request->link;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 950, 400, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 380, 160, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.banners.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Banner::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.banners.index');
    }

}
