<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Localizacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Localizacao;
use Sequencial\Libs\Thumbs;

class LocalizacaoController extends Controller
{
    protected $pathImagens = 'localizacao/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Localizacao::all();

      return view('faculdade.painel.localizacao.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.localizacao.edit')->with('registro', Localizacao::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $object = Localizacao::find($id);

      $object->endereco = $request->endereco;
      $object->endereco_obs = $request->endereco_obs;
      $object->google_maps = $request->google_maps;
      $object->horario_atendimento = $request->horario_atendimento;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.localizacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
