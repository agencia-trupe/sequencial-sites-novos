<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Popups;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Popup;
use Sequencial\Libs\Thumbs;

class PopupsController extends Controller
{
    protected $pathImagens = 'popups/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Popup::ordenado()->get();

      return view('faculdade.painel.popups.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.popups.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'imagem' => 'required|image',
        'data_inicio' => 'required|date_format:d/m/Y',
        'data_fim' => 'required|date_format:d/m/Y'
    	]);

      $object = new Popup;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 1600, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      $object->data_inicio = $request->data_inicio;
      $object->data_fim = $request->data_fim;
      $object->mostrar_todas_paginas = $request->mostrar_todas_paginas == 1 ? 1 : 0;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.popups.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.popups.edit')->with('registro', Popup::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'imagem' => 'sometimes|image',
        'data_inicio' => 'required|date_format:d/m/Y',
        'data_fim' => 'required|date_format:d/m/Y'
    	]);

      $object = Popup::find($id);

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 1600, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      $object->data_inicio = $request->data_inicio;
      $object->data_fim = $request->data_fim;
      $object->mostrar_todas_paginas = $request->mostrar_todas_paginas == 1 ? 1 : 0;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.popups.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Popup::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.popups.index');
    }

}
