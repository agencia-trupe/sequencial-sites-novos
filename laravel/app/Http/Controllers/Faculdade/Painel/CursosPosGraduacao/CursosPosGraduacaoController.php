<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\CursosPosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\AreaPosGraduacao;
use Sequencial\Models\Faculdade\CursoPosGraduacao;
use Sequencial\Models\Faculdade\CursoPosGraduacaoTopicos;

class CursosPosGraduacaoController extends Controller
{

    protected $pathImagens = 'cursos_pos_graduacao/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = CursoPosGraduacao::all();

      return view('faculdade.painel.cursos_pos_graduacao.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      $areas = AreaPosGraduacao::ordenado()->get();
      return view('faculdade.painel.cursos_pos_graduacao.create', ['areas' => $areas]);
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:mysql_faculdade.cursos_pos_graduacao,titulo',
        'areas_pos_graduacao_id' => 'required|exists:mysql_faculdade.areas_pos_graduacao,id'
    	]);

      $object = new CursoPosGraduacao;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->areas_pos_graduacao_id = $request->areas_pos_graduacao_id;

      try {

        $object->save();

        // $topicos_titulos = $request->topicos_titulos;
        // if(sizeof($topicos_titulos)){
        //   foreach ($topicos_titulos as $ordem => $topico_titulo) {
        //     $object->topicos()->save( new CursoPosGraduacaoTopicos([
        //         'titulo' => $topico_titulo,
        //         'texto' => $request->topicos_textos[$ordem],
        //         'ordem' => $ordem
        //       ])
        //     );
        //   }
        // }

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_pos_graduacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $areas = AreaPosGraduacao::ordenado()->get();

      return view('faculdade.painel.cursos_pos_graduacao.edit')->with('registro', CursoPosGraduacao::find($id))->with(compact('areas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_faculdade.cursos_pos_graduacao,titulo,'.$id,
        'areas_pos_graduacao_id' => 'required|exists:mysql_faculdade.areas_pos_graduacao,id'
    	]);

      $object = CursoPosGraduacao::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->areas_pos_graduacao_id = $request->areas_pos_graduacao_id;

      try {

        $object->save();

        // foreach ($object->topicos as $topico_atual)
        //   $topico_atual->delete();
        //
        // $topicos_titulos = $request->topicos_titulos;
        // if(sizeof($topicos_titulos)){
        //   foreach ($topicos_titulos as $ordem => $topico_titulo) {
        //     $object->topicos()->save( new CursoPosGraduacaoTopicos([
        //         'titulo' => $topico_titulo,
        //         'texto' => $request->topicos_textos[$ordem],
        //         'ordem' => $ordem
        //       ])
        //     );
        //   }
        // }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_pos_graduacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = CursoPosGraduacao::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.cursos_pos_graduacao.index');
    }

}
