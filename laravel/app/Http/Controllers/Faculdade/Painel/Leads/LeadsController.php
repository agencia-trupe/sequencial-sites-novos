<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Leads;

use Illuminate\Http\Request;

use Sequencial\Http\Requests;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Lead;

class LeadsController extends Controller
{
    public function index(Request $request)
    {
        $fields = [
            'created_at',
            'email',
            'curso',
            'nome',
            'telefone',
            'teste_vocacional',
            'teste_vocacional_resultado',
            'origem'
        ];

        $filtro = $request->get('filtro');
        if ($filtro) {
            $registros = Lead::where('email', 'like', '%'.$filtro.'%')
                ->orWhere('curso', 'like', '%'.$filtro.'%')
                ->orWhere('nome', 'like', '%'.$filtro.'%')
                ->orWhere('telefone', 'like', '%'.$filtro.'%')
                ->orWhere('origem', 'like', '%'.$filtro.'%')
                ->orderBy('id', 'DESC')
                ->paginate(20, $fields);
        } else {
            $registros = Lead::orderBy('id', 'DESC')
                ->paginate(20, $fields);
        }

        return view('faculdade.painel.leads.index', compact('registros'));
    }

    public function exportar()
    {
        $campos = [
            'data',
            'e-mail',
            'nome',
            'telefone',
            'curso',
            'teste vocacional',
            'origem'
        ];

        $leads = Lead::orderBy('id', 'desc')->get()->map(function($lead) {
            return [
                $lead->data,
                $lead->email,
                $lead->nome ?: '-',
                $lead->telefone ?: '-',
                $lead->curso ?: '-',
                strip_tags(str_replace("<br>", "\n", $lead->data_teste.($lead->resultado_teste ? '<br>'.$lead->resultado_teste : ''))),
                $lead->origem
            ];
        })->prepend($campos);

        $file = 'faculdadesequencial_leads_'.date('d-m-Y_His');
        \Excel::create($file, function($excel) use ($leads) {
            $excel->sheet('leads', function($sheet) use ($leads) {
                $sheet->fromArray($leads, null, 'A1', false, false);
                $sheet->getRowDimension(1)->setRowHeight(-1);
                $sheet->getDefaultStyle()->getAlignment()->setWrapText(true);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => [
                        'vertical' => 'top',
                        'horizontal' => 'left'
                    ]
                ]);
            });
        })->download('xls');
    }
}
