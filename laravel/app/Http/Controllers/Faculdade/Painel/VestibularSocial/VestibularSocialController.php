<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\VestibularSocial;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use DB;
use Sequencial\Models\Faculdade\VestibularSocial;
use Sequencial\Models\Faculdade\VestibularSocialArquivos;
use Sequencial\Libs\Thumbs;

class VestibularSocialController extends Controller
{

    protected $pathImagens = 'vestibular_social/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = VestibularSocial::all();

      return view('faculdade.painel.vestibular_social.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.vestibular_social.edit')->with('registro', VestibularSocial::find($id))
                                                            ->with('arquivos', VestibularSocialArquivos::ordenado()->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = VestibularSocial::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->link_banner = $request->link_banner;
      $object->frase_downloads = $request->frase_downloads;
      $object->chamada_titulo = $request->chamada_titulo;
      $object->chamada_texto = $request->chamada_texto;
      $object->chamada_link = $request->chamada_link;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 850, 335, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      $imagem_2 = Thumbs::make($request, 'faculdade', 'imagem_2', 580, 400, $this->pathImagens);
      if($imagem_2){
      	$object->imagem_2 = $imagem_2;
      }

      try {

        $object->save();

        if(is_null($request->arquivos_ids)){

          $arquivos = VestibularSocialArquivos::all();
          foreach($arquivos as $a){
            @unlink('assets/faculdade/arquivos/'.$a->arquivo);
            $a->delete();
          }

        }else{

          $arquivos = VestibularSocialArquivos::whereNotIn('id', $request->arquivos_ids)->get();
          foreach($arquivos as $a){
            @unlink('assets/faculdade/arquivos/'.$a->arquivo);
            $a->delete();
          }

        }

        if(sizeof($request->arquivos_ids)){
          foreach ($request->arquivos_ids as $i => $arquivo_id) {
            DB::connection('mysql_faculdade')->table('vestibular_social_arquivos')->where('id', $arquivo_id)->update(array('ordem' => $i));
          }
        }

        $arquivos = $request->file('arquivos');
        if(sizeof($arquivos)){
          foreach ($arquivos as $ordem => $arquivo) {

            if(strtolower($arquivo->getClientOriginalExtension()) == 'pdf'){

              $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
              $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();

              $arquivo->move('assets/faculdade/arquivos/', $filename);

              $novo_arquivo = new VestibularSocialArquivos([
                'titulo' => $request->arquivos_titulos[$ordem],
                'arquivo' => $filename,
                'ordem' => sizeof($object->arquivos) + $ordem
              ]);

              $novo_arquivo->save();

            }
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.vestibular_social.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
