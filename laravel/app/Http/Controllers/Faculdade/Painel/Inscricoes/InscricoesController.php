<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Inscricoes;

use Illuminate\Http\Request;

use Sequencial\Http\Requests;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Inscricao;

class InscricoesController extends Controller
{
    public function index(Request $request)
    {
        $fields = [
            'created_at as data',
            'nome',
            'email',
            'telefone',
            'curso',
            'origem'
        ];

        $filtro = $request->get('filtro');
        if ($filtro) {
            $registros = Inscricao::where('nome', 'like', '%'.$filtro.'%')
                ->orWhere('email', 'like', '%'.$filtro.'%')
                ->orWhere('telefone', 'like', '%'.$filtro.'%')
                ->orWhere('curso', 'like', '%'.$filtro.'%')
                ->orderBy('id', 'DESC')
                ->paginate(20, $fields);
        } else {
            $registros = Inscricao::orderBy('id', 'DESC')
                ->paginate(20, $fields);
        }

        return view('faculdade.painel.inscricoes.index', compact('registros'));
    }

    public function exportar()
    {
        $file = 'faculdadesequencial_inscricoes_'.date('d-m-Y_His');
        \Excel::create($file, function($excel) {
            $excel->sheet('inscricoes', function($sheet) {
                $sheet->fromModel(\Sequencial\Models\Faculdade\Inscricao::get([
                    'created_at as data',
                    'nome',
                    'email',
                    'telefone',
                    'curso',
                    'origem'
                ]));
            });
        })->download('xls');
    }
}
