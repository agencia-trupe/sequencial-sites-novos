<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Estagios;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Estagio;
use Sequencial\Libs\Thumbs;

class EstagiosController extends Controller
{

    protected $pathImagens = 'estagios/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Estagio::all();

      return view('faculdade.painel.estagios.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.estagios.edit')->with('registro', Estagio::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Estagio::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 160, 80, $this->pathImagens);
      if($imagem)
      	$object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.estagios.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
