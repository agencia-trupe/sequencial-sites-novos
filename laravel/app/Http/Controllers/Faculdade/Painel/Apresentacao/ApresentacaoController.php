<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Apresentacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use DB;
use Sequencial\Models\Faculdade\Apresentacao;
use Sequencial\Models\Faculdade\ApresentacaoArquivos;
use Sequencial\Libs\Thumbs;

class ApresentacaoController extends Controller
{
    protected $pathImagens = 'apresentacao/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Apresentacao::all();

      return view('faculdade.painel.apresentacao.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.apresentacao.edit')->with('registro', Apresentacao::find($id))
                                                       ->with('arquivos', ApresentacaoArquivos::ordenado()->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'regimento' => 'sometimes|mimes:pdf'
    	]);

      $object = Apresentacao::find($id);

      $object->texto = $request->texto;
      $object->chamada = $request->chamada;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 350, 230, $this->pathImagens);
      if($imagem)
      	$object->imagem = $imagem;

      $arquivo = $request->regimento;
      if($arquivo){

        $arquivo_antigo = $object->regimento;

        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();

        $arquivo->move('assets/faculdade/arquivos/', $filename);

        $object->regimento = $filename;
        @unlink('assets/faculdade/arquivos/'.$arquivo_antigo);
      }

      try {

        $object->save();

        if(is_null($request->arquivos_ids)){

          $arquivos = ApresentacaoArquivos::all();
          foreach($arquivos as $a){
            @unlink('assets/faculdade/arquivos/'.$a->arquivo);
            $a->delete();
          }

        }else{

          $arquivos = ApresentacaoArquivos::whereNotIn('id', $request->arquivos_ids)->get();
          foreach($arquivos as $a){
            @unlink('assets/faculdade/arquivos/'.$a->arquivo);
            $a->delete();
          }

        }

        if(sizeof($request->arquivos_ids)){
          foreach ($request->arquivos_ids as $i => $arquivo_id) {
            DB::connection('mysql_faculdade')->table('sequencial_apresentacao_arquivos')->where('id', $arquivo_id)->update(array('ordem' => $i));
          }
        }

        $arquivos = $request->file('arquivos');
        if(sizeof($arquivos)){
          foreach ($arquivos as $ordem => $arquivo) {

            if(strtolower($arquivo->getClientOriginalExtension()) == 'pdf'){

              $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
              $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();

              $arquivo->move('assets/faculdade/arquivos/', $filename);

              $novo_arquivo = new ApresentacaoArquivos([
                'titulo' => $request->arquivos_titulos[$ordem],
                'arquivo' => $filename,
                'ordem' => sizeof($object->arquivos) + $ordem
              ]);

              $novo_arquivo->save();

            }
          }
        }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.apresentacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
