<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\CursosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\CursoGraduacao as Curso;
use Sequencial\Models\Faculdade\CursoGraduacaoTopicos as Topico;
use Sequencial\Libs\Thumbs;

class TopicosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $curso = Curso::find($request->curso_id);
      $registros = $curso->topicos;

      return view('faculdade.painel.cursos_graduacao.topicos.index')->with(compact('registros', 'curso'));
    }

    public function create(Request $request)
    {
      $curso = Curso::find($request->curso_id);
      return view('faculdade.painel.cursos_graduacao.topicos.create')->with(compact('curso'));
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required',
        'cursos_graduacao_id' => 'required|exists:mysql_faculdade.cursos_graduacao,id'
    	]);

      $object = new Topico;

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;
      $object->cursos_graduacao_id = $request->cursos_graduacao_id;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_graduacao.topicos.index', ['curso_id' => $object->cursos_graduacao_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $registro = Topico::find($id);
      $curso = $registro->curso;
      return view('faculdade.painel.cursos_graduacao.topicos.edit')->with('registro', $registro)->with('curso', $curso);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required'
    	]);

      $object = Topico::find($id);

      $object->titulo = $request->titulo;
      $object->texto = $request->texto;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_graduacao.topicos.index', ['curso_id' => $object->cursos_graduacao_id]);

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Topico::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.cursos_graduacao.topicos.index', ['curso_id' => $object->cursos_graduacao_id]);
    }

}
