<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\CursosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\CursoGraduacao;
use Sequencial\Models\Faculdade\CursoGraduacaoTopicos;
use Sequencial\Libs\Thumbs;

class CursosGraduacaoController extends Controller
{

    protected $pathImagens = 'cursos_graduacao/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = CursoGraduacao::ordenado()->get();

      return view('faculdade.painel.cursos_graduacao.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.cursos_graduacao.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_faculdade.cursos_graduacao,titulo',
        'cor_curso' => 'required',
        'imagem' => 'required|image',
        'sobre_o_curso' => 'required',
        'arquivo_sobre_o_curso' => 'sometimes|mimes:pdf',
        'sobre_o_mercado' => 'required',
        'arquivo_sobre_o_mercado' => 'sometimes|mimes:pdf',
        'duracao' => 'required',
        'periodo' => 'required',
        'investimento' => 'required',
    	]);

      $object = new CursoGraduacao;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->prefixo = $request->prefixo;
      $object->legislacao = $request->legislacao;
      $object->cor_curso = $request->cor_curso;
      $object->sobre_o_curso = $request->sobre_o_curso;
      $object->sobre_o_mercado = $request->sobre_o_mercado;
      $object->duracao = $request->duracao;
      $object->periodo = $request->periodo;
      $object->investimento = $request->investimento;
      $object->video = $request->video;

      if($request->arquivo_sobre_o_curso) {
        $arquivo = $request->arquivo_sobre_o_curso;
        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();
        $arquivo->move('assets/faculdade/arquivos/', $filename);
        $object->arquivo_sobre_o_curso = $filename;
      }
      if($request->arquivo_sobre_o_mercado) {
        $arquivo = $request->arquivo_sobre_o_mercado;
        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();
        $arquivo->move('assets/faculdade/arquivos/', $filename);
        $object->arquivo_sobre_o_mercado = $filename;
      }

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 350, 240, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        // $topicos_titulos = $request->topicos_titulos;
        // if(sizeof($topicos_titulos)){
        //   foreach ($topicos_titulos as $ordem => $topico_titulo) {
        //     $object->topicos()->save( new CursoGraduacaoTopicos([
        //         'titulo' => $topico_titulo,
        //         'texto' => $request->topicos_textos[$ordem],
        //         'ordem' => $ordem
        //       ])
        //     );
        //   }
        // }

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_graduacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.cursos_graduacao.edit')->with('registro', CursoGraduacao::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:mysql_faculdade.cursos_graduacao,titulo,'.$id,
        'cor_curso' => 'required',
        'imagem' => 'sometimes|image',
        'sobre_o_curso' => 'required',
        'arquivo_sobre_o_curso' => 'sometimes|mimes:pdf',
        'sobre_o_mercado' => 'required',
        'arquivo_sobre_o_mercado' => 'sometimes|mimes:pdf',
        'duracao' => 'required',
        'periodo' => 'required',
        'investimento' => 'required',
    	]);

      $object = CursoGraduacao::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->prefixo = $request->prefixo;
      $object->legislacao = $request->legislacao;
      $object->cor_curso = $request->cor_curso;
      $object->sobre_o_curso = $request->sobre_o_curso;
      $object->sobre_o_mercado = $request->sobre_o_mercado;
      $object->duracao = $request->duracao;
      $object->periodo = $request->periodo;
      $object->investimento = $request->investimento;
      $object->video = $request->video;

      if($request->arquivo_sobre_o_curso) {
        $arquivo = $request->arquivo_sobre_o_curso;
        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();
        $arquivo->move('assets/faculdade/arquivos/', $filename);
        $object->arquivo_sobre_o_curso = $filename;
      }
      if($request->arquivo_sobre_o_mercado) {
        $arquivo = $request->arquivo_sobre_o_mercado;
        $filename = str_replace('.'.$arquivo->getClientOriginalExtension(), '', $arquivo->getClientOriginalName());
        $filename = str_slug($filename).'_'.date('dmYHis').'.'.$arquivo->getClientOriginalExtension();
        $arquivo->move('assets/faculdade/arquivos/', $filename);
        $object->arquivo_sobre_o_mercado = $filename;
      }

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 350, 240, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        // foreach ($object->topicos as $topico_atual)
        //   $topico_atual->delete();
        //
        // $topicos_titulos = $request->topicos_titulos;
        // if(sizeof($topicos_titulos)){
        //   foreach ($topicos_titulos as $ordem => $topico_titulo) {
        //     $object->topicos()->save( new CursoGraduacaoTopicos([
        //         'titulo' => $topico_titulo,
        //         'texto' => $request->topicos_textos[$ordem],
        //         'ordem' => $ordem
        //       ])
        //     );
        //   }
        // }

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_graduacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = CursoGraduacao::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.cursos_graduacao.index');
    }

}
