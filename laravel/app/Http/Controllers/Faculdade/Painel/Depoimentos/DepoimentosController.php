<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Depoimentos;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\Depoimento;
use Sequencial\Libs\Thumbs;

class DepoimentosController extends Controller
{

    protected $pathImagens = 'depoimentos/';
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = Depoimento::all();

      return view('faculdade.painel.depoimentos.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.depoimentos.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'texto' => 'required',
        'autor_nome' => 'required'
    	]);

      $object = new Depoimento;

      $object->texto = $request->texto;
      $object->autor_nome = $request->autor_nome;
      $object->autor_descricao = $request->autor_descricao;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens);
      if($imagem)
      	$object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.depoimentos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.depoimentos.edit')->with('registro', Depoimento::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'texto' => 'required',
        'autor_nome' => 'required'
    	]);

      $object = Depoimento::find($id);

      $object->texto = $request->texto;
      $object->autor_nome = $request->autor_nome;
      $object->autor_descricao = $request->autor_descricao;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 200, 200, $this->pathImagens);
      if($imagem)
      	$object->imagem = $imagem;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.depoimentos.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = Depoimento::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.depoimentos.index');
    }

}
