<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\Usuarios;

use Sequencial\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Hash;

use Sequencial\Models\Faculdade\Admin;

class UsuariosController extends Controller{

	public function index()
	{
		$usuarios = Admin::all();

		return view('faculdade.painel.usuarios.index')->with(compact('usuarios'));
	}

	public function create()
	{
		return view('faculdade.painel.usuarios.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
    	'login'            => 'required|unique:mysql_faculdade.admin,login',
    	'password'         => 'required|min:6',
    	'password_confirm' => 'required|min:6|same:password',
  	]);

		$object = new Admin;

		$object->login     = $request->login;
		$object->email    = $request->email;
		$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário criado com sucesso.');

			return redirect()->route('painel.faculdade.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function edit($id)
	{
		$usuario = Admin::find($id);

		return view('faculdade.painel.usuarios.edit')->with(compact('usuario'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
    	'login'             => 'required|unique:mysql_faculdade.admin,login,'.$id,
    	'password'         => 'min:6',
    	'password_confirm' => 'required_with:password|min:6|same:password',
  	]);

		$object = Admin::find($id);

		$object->email = $request->email;
		$object->login = $request->login;

		if($request->has('password'))
			$object->password = Hash::make($request->password);

		try {

			$object->save();

			$request->session()->flash('sucesso', 'Usuário alterado com sucesso.');

			return redirect()->route('painel.faculdade.usuarios.index');

		} catch (\Exception $e) {

			$request->flash();

			return back()->withErrors(array('Erro ao criar usuário! ('.$e->getMessage().')'));

		}
	}

	public function destroy(Request $request, $id)
	{
		$object = Admin::find($id);
		$object->delete();

		$request->session()->flash('sucesso', 'Usuário removido com sucesso.');

		return redirect()->route('painel.faculdade.usuarios.index');
	}
}
