<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\CursosLivres;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use Sequencial\Models\Faculdade\CursoLivre;
use Sequencial\Libs\Thumbs;

class CursosLivresController extends Controller
{
    protected $pathImagens = 'cursos_livres/';

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = CursoLivre::ordenado()->get();

      return view('faculdade.painel.cursos_livres.index')->with(compact('registros'));
    }

    public function create(Request $request)
    {
      return view('faculdade.painel.cursos_livres.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'titulo' => 'required|unique:mysql_faculdade.cursos_livres,titulo',
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = new CursoLivre;

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->texto = $request->texto;
      $object->data = $request->data;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 730, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro criado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_livres.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao criar registro! ('.$e->getMessage().')'));

      }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.cursos_livres.edit')->with('registro', CursoLivre::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_faculdade.cursos_livres,titulo,'.$id,
        'data' => 'required|date_format:d/m/Y',
        'imagem' => 'sometimes|image'
    	]);

      $object = CursoLivre::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->texto = $request->texto;
      $object->data = $request->data;

      $imagem = Thumbs::make($request, 'faculdade', 'imagem', 730, null, $this->pathImagens);
      if($imagem){
      	Thumbs::make($request, 'faculdade', 'imagem', 200, null, $this->pathImagens.'thumbs/');
      	$object->imagem = $imagem;
      }

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.cursos_livres.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

    public function destroy(Request $request, $id){
      $object = CursoLivre::find($id);
      $object->delete();

      $request->session()->flash('sucesso', 'Registro removido com sucesso.');

      return redirect()->route('painel.faculdade.cursos_livres.index');
    }

}
