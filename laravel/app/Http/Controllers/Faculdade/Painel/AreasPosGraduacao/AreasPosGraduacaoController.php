<?php

namespace Sequencial\Http\Controllers\Faculdade\Painel\AreasPosGraduacao;

use Illuminate\Http\Request;
use Sequencial\Http\Controllers\Controller;

use View;
use Sequencial\Models\Faculdade\AreaPosGraduacao;

class AreasPosGraduacaoController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
      $registros = AreaPosGraduacao::all();

      return view('faculdade.painel.areas_pos_graduacao.index')->with(compact('registros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      return view('faculdade.painel.areas_pos_graduacao.edit')->with('registro', AreasPosGraduacao::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
      	'titulo' => 'required|unique:mysql_faculdade.areas_pos_graduacao,titulo,'.$id
    	]);

      $object = AreaPosGraduacao::find($id);

      $object->titulo = $request->titulo;
      $object->slug = str_slug($request->titulo);
      $object->cor = $request->cor;

      try {

        $object->save();

        $request->session()->flash('sucesso', 'Registro alterado com sucesso.');

        return redirect()->route('painel.faculdade.areas_pos_graduacao.index');

      } catch (\Exception $e) {

        $request->flash();

        return back()->withErrors(array('Erro ao alterar registro! ('.$e->getMessage().')'));

      }
    }

}
