<?php

Route::group([
    'domain' => ENV('DOMINIO_FACULDADE'),
    'namespace' => 'Faculdade'
  ], function() {

    Route::get('', ['as' => 'faculdade.index', 'uses' => 'Site\Home\HomeController@getIndex']);

    Route::get('cursos-de-graduacao', ['as' => 'faculdade.cursos-de-graduacao.index', 'uses' => 'Site\CursosGraduacao\CursosGraduacaoController@getIndex']);
    Route::get('cursos-de-graduacao/{slug_curso}', ['as' => 'faculdade.cursos-de-graduacao.detalhes', 'uses' => 'Site\CursosGraduacao\CursosGraduacaoController@getDetalhes']);
    Route::get('cursos-de-pos-graduacao', ['as' => 'faculdade.cursos-de-pos-graduacao.index', 'uses' => 'Site\CursosPosGraduacao\CursosPosGraduacaoController@getIndex']);
    Route::get('cursos-de-pos-graduacao/{slug_curso}', ['as' => 'faculdade.cursos-de-pos-graduacao.detalhes', 'uses' => 'Site\CursosPosGraduacao\CursosPosGraduacaoController@getDetalhes']);
    Route::get('cursos-de-pos-graduacao/{slug_area}/{slug_curso}', ['as' => 'faculdade.cursos-de-pos-graduacao.detalhes', 'uses' => 'Site\CursosPosGraduacao\CursosPosGraduacaoController@getDetalhes']);
    Route::get('vestibular-social', ['as' => 'faculdade.vestibular-social.index', 'uses' => 'Site\VestibularSocial\VestibularSocialController@getIndex']);
    Route::get('a-faculdade/apresentacao', ['as' => 'faculdade.apresentacao', 'uses' => 'Site\AFaculdade\AFaculdadeController@getApresentacao']);
    Route::get('a-faculdade/localizacao', ['as' => 'faculdade.localizacao', 'uses' => 'Site\AFaculdade\AFaculdadeController@getLocalizacao']);
    Route::get('a-faculdade/infraestrutura', ['as' => 'faculdade.infraestrutura', 'uses' => 'Site\AFaculdade\AFaculdadeController@getInfraestrutura']);
    Route::get('cursos', ['as' => 'faculdade.cursos.index', 'uses' => 'Site\Cursos\CursosController@getIndex']);
    Route::get('cursos/curso-livre/{slug_curso}', ['as' => 'faculdade.cursos. curso-livre.detalhes', 'uses' => 'Site\Cursos\CursosController@getCursoLivre']);
    Route::get('cursos/curso-de-extensao/{slug_curso}', ['as' => 'faculdade.cursos. curso-extensao.detalhes', 'uses' => 'Site\Cursos\CursosController@getCursoExtensao']);
    Route::get('novidades', ['as' => 'faculdade.novidades.index', 'uses' => 'Site\Novidades\NovidadesController@getIndex']);
    Route::get('novidades/{slug_novidade}', ['as' => 'faculdade.novidades.detalhes', 'uses' => 'Site\Novidades\NovidadesController@getDetalhes']);
    Route::get('eventos', ['as' => 'faculdade.eventos.index', 'uses' => 'Site\Eventos\EventosController@getIndex']);
    Route::get('eventos/{slug_evento}', ['as' => 'faculdade.eventos.detalhes', 'uses' => 'Site\Eventos\EventosController@getDetalhes']);
    Route::get('estagios', ['as' => 'faculdade.estagios.index', 'uses' => 'Site\Estagios\EstagiosController@getIndex']);
    Route::get('depoimentos', ['as' => 'faculdade.depoimentos.index', 'uses' => 'Site\Depoimentos\DepoimentosController@getIndex']);
    Route::get('fale-conosco', ['as' => 'faculdade.fale-conosco.index', 'uses' => 'Site\FaleConosco\FaleConoscoController@getIndex']);
    Route::post('fale-conosco', ['as' => 'faculdade.fale-conosco.enviar', 'uses' => 'Site\FaleConosco\FaleConoscoController@postEnviarContato']);
    Route::get('ouvidoria', ['as' => 'faculdade.ouvidoria.index', 'uses' => 'Site\Ouvidoria\OuvidoriaController@getIndex']);
    Route::post('ouvidoria', ['as' => 'faculdade.ouvidoria.enviar', 'uses' => 'Site\Ouvidoria\OuvidoriaController@postEnviarOuvidoria']);
    Route::post('busca', ['as' => 'faculdade.busca', 'uses' => 'Site\Busca\BuscaController@postBusca']);
    Route::get('previa-chat-ESoERUGuebJxiRXYmxNYVw3WLObmT', ['as' => 'faculdade.previa-chat', 'uses' => 'Site\Home\HomeController@getChat']);
    Route::get('termos-de-uso', function() {
      return view('faculdade.site.termos-de-uso.index')->with('popup', []);
    })->name('faculdade.termos-de-uso.index');
    Route::get('inscreva-se', 'Site\InscrevaSe\InscrevaSeController@index')->name('faculdade.inscreva-se.index');
    Route::post('inscreva-se', 'Site\InscrevaSe\InscrevaSeController@post')->name('faculdade.inscreva-se.post');
    Route::post('lead', 'Site\CursosGraduacao\CursosGraduacaoController@lead');

    Route::group([
      'namespace' => 'Site\TesteVocacional',
      'prefix'    => 'teste-vocacional'
    ], function() {
      Route::get('/', [
        'as'   => 'faculdade.teste-vocacional.index',
        'uses' => 'TesteVocacionalController@index'
      ]);
      Route::post('/', [
        'as'   => 'faculdade.teste-vocacional.cadastro',
        'uses' => 'TesteVocacionalController@cadastro'
      ]);
      Route::get('etapas', [
        'as'   => 'faculdade.teste-vocacional.etapas',
        'uses' => 'TesteVocacionalController@etapas'
      ]);
      Route::post('etapas', [
        'as'   => 'faculdade.teste-vocacional.etapasPost',
        'uses' => 'TesteVocacionalController@etapasPost'
      ]);
      Route::get('resultado', [
        'as'   => 'faculdade.teste-vocacional.resultado',
        'uses' => 'TesteVocacionalController@resultado'
      ]);
      Route::get('enviar', [
        'as'   => 'faculdade.teste-vocacional.enviar',
        'uses' => 'TesteVocacionalController@enviarEmail'
      ]);
    });

    Route::get('painel/auth/login', ['as' => 'faculdade.painel.login','uses' => 'Painel\Auth\AuthController@getLogin']);
    Route::post('painel/auth/login', ['as' => 'faculdade.painel.auth','uses' => 'Painel\Auth\AuthController@postLogin']);
    Route::get('painel/auth/logout', ['as' => 'faculdade.painel.logout','uses' => 'Painel\Auth\AuthController@getLogout']);

    Route::group([
      'middleware' => 'auth:faculdade',
      'namespace' => 'Painel',
      'prefix' => 'painel'
    ], function() {

      Route::get('/', ['as' => 'faculdade.painel.dashboard', 'uses' => function() {
        return view('faculdade.painel.dashboard.index');
      }]);

      Route::post('gravar-ordem-registros', function(Illuminate\Http\Request $request){
        $itens = $request->input('data');
        $tabela = $request->input('tabela');
        for ($i = 0; $i < count($itens); $i++)
	       DB::connection('mysql_faculdade')->table($tabela)->where('id', $itens[$i])->update(array('ordem' => $i));
      });

      Route::group(['middleware' => 'authAdminFaculdade'], function() {
          Route::resource('faculdade/usuarios', 'Usuarios\UsuariosController');

          Route::post('imagens/upload', 'Imagens\ImagensController@postUpload');
          Route::get('infraestrutura_imgs_sem_album', ['as' => 'painel.faculdade.infraestrutura_imgs_sem_album.edit', 'uses' => 'InfraestruturaAlbuns\ImagensSemAlbumController@edit']);
          Route::post('infraestrutura_imgs_sem_album', ['as' => 'painel.faculdade.infraestrutura_imgs_sem_album.update', 'uses' => 'InfraestruturaAlbuns\ImagensSemAlbumController@update']);

          // NOVAS ROTAS DO PAINEL:
          Route::resource('faculdade/popups', 'Popups\PopupsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/ouvidoria', 'Ouvidoria\OuvidoriaController', ['only' => ['index', 'edit', 'update']]);
          Route::get('faculdade/ouvidoria/mensagens/exportar', function() {
            $file = 'faculdadesequencial-ouvidoria'.date('d-m-Y_His');
            \Excel::create($file, function($excel) {
                $excel->sheet('mensagens', function($sheet) {
                    $sheet->fromModel(\Sequencial\Models\Faculdade\Mensagem::where('origem', 'ouvidoria')->ordenado()->get(['nome', 'email', 'telefone', 'cpf', 'mensagem', 'created_at as data']));
                });
            })->download('xls');
          })->name('painel.faculdade.ouvidoria.mensagens.exportar');
          Route::resource('faculdade/ouvidoria/mensagens', 'Ouvidoria\OuvidoriaMensagensController', ['only' => ['index', 'show']]);
          Route::resource('faculdade/novidades', 'Novidades\NovidadesController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/cursos_livres', 'CursosLivres\CursosLivresController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/cursos_extensao', 'CursosExtensao\CursosExtensaoController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/eventos', 'Eventos\EventosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/infraestrutura_albuns', 'InfraestruturaAlbuns\InfraestruturaAlbunsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/infraestrutura', 'Infraestrutura\InfraestruturaController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/localizacao', 'Localizacao\LocalizacaoController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/apresentacao', 'Apresentacao\ApresentacaoController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/vestibular_social', 'VestibularSocial\VestibularSocialController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/estagios', 'Estagios\EstagiosController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/depoimentos', 'Depoimentos\DepoimentosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/cursos_pos_graduacao', 'CursosPosGraduacao\CursosPosGraduacaoController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/cursos_pos_graduacao/topicos', 'CursosPosGraduacao\TopicosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/areas_pos_graduacao', 'AreasPosGraduacao\AreasPosGraduacaoController', ['only' => ['index', 'edit', 'update']]);
          Route::resource('faculdade/cursos_graduacao', 'CursosGraduacao\CursosGraduacaoController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/chamadas', 'Chamadas\ChamadasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
          Route::resource('faculdade/banners', 'Banners\BannersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
        });

        Route::group(['middleware' => 'authInscricoesFaculdade'], function() {
          Route::get('faculdade/inscricoes', 'Inscricoes\InscricoesController@index')->name('painel.faculdade.inscricoes');
          Route::get('faculdade/inscricoes/exportar', 'Inscricoes\InscricoesController@exportar')->name('painel.faculdade.inscricoes.exportar');
          Route::get('faculdade/leads', 'Leads\LeadsController@index')->name('painel.faculdade.leads');
          Route::get('faculdade/leads/exportar', 'Leads\LeadsController@exportar')->name('painel.faculdade.leads.exportar');
        });
    });

    Route::any('portais/dominios', function(\Illuminate\Http\Request $request) {
      $token = 'IfPAQKfk5b7DOEcfzh5dcvlMPk7gduQR.5WhRbuL7E8Fy6TATaCiMBGROqSSKc0Nl';
      if ($request->token !== $token) abort('404');
      if ($request->getMethod() === 'POST') {
        foreach($request->only(['portal-aluno', 'portal-professor']) as $k => $v) {
          \Illuminate\Support\Facades\Cache::forever('dominios.'.$k, $v);
        }
        return back();
      }
      return view('dominios');
    });

    Route::post('cookie-consent', function() {
      \Sequencial\Models\Faculdade\CookieConsent::create([
        'ip' => Request::ip()
      ]);
    });
});
