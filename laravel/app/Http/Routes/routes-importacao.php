<?php

Route::group([
    'domain' => ENV('DOMINIO_ESCOLA'),
    'namespace' => 'Escola'
  ], function() {

    Route::get('import/novidades', 'Site\Import\ImportNovidadesController@execute');
    Route::get('import/juramentos', 'Site\Import\ImportJuramentosController@execute');
    Route::get('import/eventos', 'Site\Import\ImportEventosController@execute');
    Route::get('import/eventos-fix-thumbs', 'Site\Import\ImportEventosController@corrigeImagens');
    Route::get('import/depoimentos', 'Site\Import\ImportDepoimentosController@execute');

    Route::get('import/novidades-com-link', 'Site\Import\ImportNovidadesController@listComLink');
    Route::get('import/eventos-com-link', 'Site\Import\ImportEventosController@listComLink');
    Route::get('import/juramentos-com-link', 'Site\Import\ImportJuramentosController@listComLink');

    

});
