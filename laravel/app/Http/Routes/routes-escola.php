<?php

Route::group([
    'domain' => ENV('DOMINIO_ESCOLA'),
    'namespace' => 'Escola'
], function() {

    Route::get('cursos-tecnicos', ['as' => 'escola.cursos-tecnicos', 'uses' => 'Site\CursosTecnicos\CursosTecnicosController@getIndex']);
    Route::get('cursos-tecnicos/{slug}', ['as' => 'escola.cursos-tecnicos.detalhes', 'uses' => 'Site\CursosTecnicos\CursosTecnicosController@getDetalhes']);
    Route::get('depoimentos', ['as' => 'escola.depoimentos', 'uses' => 'Site\Depoimentos\DepoimentosController@getIndex']);
    Route::get('unidades', ['as' => 'escola.unidades', 'uses' => 'Site\Unidades\UnidadesController@getIndex']);
    Route::get('unidades/{slug}', ['as' => 'escola.unidades.detalhes', 'uses' => 'Site\Unidades\UnidadesController@getDetalhes']);
    Route::get('bolsas-de-estudo', ['as' => 'escola.bolsas-de-estudo', 'uses' => 'Site\BolsasDeEstudo\BolsasDeEstudoController@getIndex']);
    Route::get('a-sequencial', ['as' => 'escola.a-sequencial', 'uses' => 'Site\ASequencial\ASequencialController@getIndex']);
    Route::get('novidades', ['as' => 'escola.novidades', 'uses' => 'Site\Novidades\NovidadesController@getNoticias']);
    Route::get('novidades/noticias', ['as' => 'escola.novidades.noticias', 'uses' => 'Site\Novidades\NovidadesController@getNoticias']);
    Route::get('novidades/{slug}', ['as' => 'escola.novidades.detalhes', 'uses' => 'Site\Novidades\NovidadesController@getDetalhes']);
    Route::get('eventos', ['as' => 'escola.eventos', 'uses' => 'Site\Eventos\EventosController@getIndex']);
    Route::get('eventos/juramentos', ['as' => 'escola.eventos.juramentos', 'uses' => 'Site\Eventos\EventosController@getJuramentos']);
    Route::get('eventos/{slug}', ['as' => 'escola.eventos.detalhes', 'uses' => 'Site\Eventos\EventosController@getDetalhes']);
    Route::get('perguntas-frequentes', ['as' => 'escola.perguntas-frequentes', 'uses' => 'Site\PerguntasFrequentes\PerguntasFrequentesController@getIndex']);
    Route::get('trabalhe-conosco', ['as' => 'escola.trabalhe-conosco', 'uses' => 'Site\TrabalheConosco\TrabalheConoscoController@getIndex']);
    Route::get('ouvidoria', ['as' => 'escola.ouvidoria', 'uses' => 'Site\Ouvidoria\OuvidoriaController@getIndex']);
    Route::post('ouvidoria', ['as' => 'escola.ouvidoria.enviar', 'uses' => 'Site\Ouvidoria\OuvidoriaController@postEnviar']);
    Route::get('fale-conosco', ['as' => 'escola.fale-conosco', 'uses' => 'Site\FaleConosco\FaleConoscoController@getIndex']);
    Route::post('fale-conosco', ['as' => 'escola.fale-conosco.enviar', 'uses' => 'Site\FaleConosco\FaleConoscoController@postEnviar']);
    Route::post('busca', ['as' => 'escola.busca', 'uses' => 'Site\Busca\BuscaController@postBusca']);
    Route::get('termos-de-uso', function() {
        return view('escola.site.termos-de-uso.index')->with('popup', [])->with('listaCursos', \Sequencial\Models\Escola\Curso::ordenado()->get());
    })->name('escola.termos-de-uso.index');

    Route::get('tv/{unidade}', 'Site\ImagensTv\ImagensTvController@index');

    Route::get('', ['as' => 'escola.index', 'uses' => 'Site\Home\HomeController@getIndex']);
    Route::post('pre-inscricao', [
        'as' => 'escola.pre-inscricao',
        'uses'=> 'Site\Home\HomeController@postPreInscricao'
    ]);

    Route::post('cursos-tecnicos/{slug}/grade-curricular', [
        'as' => 'escola.cursos-tecnicos.post-grade',
        'uses' => 'Site\CursosTecnicos\CursosTecnicosController@postGrade'
    ]);
    Route::get('cursos-tecnicos/{slug}/grade-curricular', [
        'as' => 'escola.cursos-tecnicos.grade',
        'uses' => 'Site\CursosTecnicos\CursosTecnicosController@getGrade'
    ]);

    Route::get('painel/auth/login', ['as' => 'escola.painel.login','uses' => 'Painel\Auth\AuthController@getLogin']);
    Route::post('painel/auth/login', ['as' => 'escola.painel.auth','uses' => 'Painel\Auth\AuthController@postLogin']);
    Route::get('painel/auth/logout', ['as' => 'escola.painel.logout','uses' => 'Painel\Auth\AuthController@getLogout']);

    Route::group([
        'middleware' => 'auth:escola',
        'namespace' => 'Painel',
        'prefix' => 'painel'
    ], function() {

        Route::get('/', ['as' => 'escola.painel.dashboard', 'uses' => function() {
            return view('escola.painel.dashboard.index');
        }]);

        Route::post('gravar-ordem-registros', function(Illuminate\Http\Request $request){
            $itens = $request->input('data');
            $tabela = $request->input('tabela');
            $orderField = $request->has('campo') ? $request->campo : 'ordem';
            for ($i = 0; $i < count($itens); $i++)
                DB::connection('mysql_escola')->table($tabela)->where('id', $itens[$i])->update(array($orderField => $i));
        });

        Route::group(['middleware' => 'authAdminEscola'], function() {
            Route::resource('escola/tv', 'ImagensTv\ImagensTvController', ['only' => ['index', 'store', 'destroy']]);

            Route::resource('escola/popups', 'Popups\PopupsController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

            Route::resource('escola/usuarios', 'Usuarios\UsuariosController');

            Route::post('imagens/upload', 'Imagens\ImagensController@postUpload');

            Route::resource('escola/chamadas', 'Chamadas\ChamadasController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/banners', 'Banners\BannersController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);

            Route::any('ckeditor/upload', 'Imagens\ImagensController@postCkeUpload');

            // NOVAS ROTAS DO PAINEL:
            Route::resource('escola/depoimentos', 'Depoimentos\DepoimentosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::get('escola/ouvidoria/mensagens/exportar', function() {
                $file = 'escolasequencial-ouvidoria'.date('d-m-Y_His');
                \Excel::create($file, function($excel) {
                    $excel->sheet('mensagens', function($sheet) {
                        $sheet->fromModel(\Sequencial\Models\Escola\MensagemOuvidoria::ordenado()->get());
                    });
                })->download('xls');
            })->name('painel.escola.ouvidoria.mensagens.exportar');
            Route::resource('escola/ouvidoria/mensagens', 'Ouvidoria\OuvidoriaMensagensController', ['only' => ['index', 'show']]);
            Route::resource('escola/ouvidoria', 'Ouvidoria\OuvidoriaController', ['only' => ['index', 'edit', 'update']]);
            Route::resource('escola/trabalhe_conosco', 'TrabalheConosco\TrabalheConoscoController', ['only' => ['index', 'edit', 'update']]);
            Route::resource('escola/perguntas_frequentes', 'PerguntasFrequentes\PerguntasFrequentesController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/novidades', 'Novidades\NovidadesController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/eventos', 'Eventos\EventosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/a-sequencial', 'ASequencial\ASequencialController', ['only' => ['index', 'edit', 'update']]);
            Route::resource('escola/bolsas-de-estudo', 'BolsasDeEstudo\BolsasDeEstudoController', ['only' => ['index', 'edit', 'update']]);
            Route::resource('escola/unidades', 'Unidades\UnidadesController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/unidades/topicos', 'Unidades\TopicosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/cursos', 'Cursos\CursosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::resource('escola/cursos/topicos', 'Cursos\TopicosController', ['only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']]);
            Route::get('escola/cursos/topicos/create-default', ['as' => 'painel.escola.cursos.topicos.create-default', 'uses' =>  'Cursos\TopicosController@getCreateDefault']);
            Route::resource('escola/areas', 'Areas\AreasController', ['only' => ['index', 'edit', 'update']]);
            Route::get('escola/unidades/cursos', ['as' => 'painel.escola.unidades.cursos.index', 'uses' => 'Unidades\UnidadesCursosController@getIndex']);
            Route::post('escola/unidades/cursos', ['as' => 'painel.escola.unidades.cursos.store', 'uses' => 'Unidades\UnidadesCursosController@postStore']);
        });

        Route::group(['middleware' => 'authPreInscricoesEscola'], function() {
            Route::get('escola/pre-inscricoes', 'PreInscricoes\PreInscricoesController@index')->name('painel.escola.pre-inscricoes');
            Route::get('escola/pre-inscricoes/exportar', function() {
                $file = 'escolasequencial_pre-inscricoes_'.date('d-m-Y_His');
                \Excel::create($file, function($excel) {
                    $excel->sheet('pre-inscricoes', function($sheet) {
                        $sheet->fromModel(\Sequencial\Models\Escola\PreInscricao::get([
                            'created_at as data',
                            'nome',
                            'email',
                            'telefone',
                            'origem'
                        ]));
                    });
                })->download('xls');
            })->name('painel.escola.pre-inscricoes.exportar');

            Route::get('escola/cadastros-gradecurricular', 'CadastrosGradeCurricular\CadastrosGradeCurricularController@index')->name('painel.escola.cadastros-gradecurricular');
            Route::get('escola/cadastros-gradecurricular/exportar', 'CadastrosGradeCurricular\CadastrosGradeCurricularController@exportar')->name('painel.escola.cadastros-gradecurricular.exportar');
        });
    });

    Route::any('portais/dominios', function(\Illuminate\Http\Request $request) {
      $token = 'IfPAQKfk5b7DOEcfzh5dcvlMPk7gduQR.5WhRbuL7E8Fy6TATaCiMBGROqSSKc0Nl';
      if ($request->token !== $token) abort('404');
      if ($request->getMethod() === 'POST') {
        foreach($request->only(['portal-aluno', 'portal-professor']) as $k => $v) {
          \Illuminate\Support\Facades\Cache::forever('dominios.'.$k, $v);
        }
        return back();
      }
      return view('dominios');
    });

    Route::post('cookie-consent', function() {
      \Sequencial\Models\Escola\CookieConsent::create([
        'ip' => Request::ip()
      ]);
    });
});
