<?php

Route::group([
    'domain' => ENV('DOMINIO_GRUPO'),
    'namespace' => 'Grupo'
  ], function() {

    Route::get('', ['as' => 'grupo.index', 'uses' => 'Site\SiteController@getIndex']);
    Route::get('nosso-historico', ['as' => 'grupo.nosso-historico', 'uses' => 'Site\SiteController@getNossoHistorico']);
    Route::get('faculdade', ['as' => 'grupo.faculdade', 'uses' => 'Site\SiteController@getFaculdade']);
    Route::get('escola-tecnica', ['as' => 'grupo.escola-tecnica', 'uses' => 'Site\SiteController@getEscolaTecnica']);
    Route::get('ensino-a-distancia', ['as' => 'grupo.ensino-a-distancia', 'uses' => 'Site\SiteController@getEnsinoADistancia']);
    Route::get('fale-conosco', ['as' => 'grupo.fale-conosco', 'uses' => 'Site\SiteController@getFaleConosco']);
    Route::post('fale-conosco', ['as' => 'grupo.enviar-contato', 'uses' => 'Site\SiteController@postEnviarContato']);

});
