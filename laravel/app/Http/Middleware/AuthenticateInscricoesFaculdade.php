<?php

namespace Sequencial\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthenticateInscricoesFaculdade
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'faculdade')
    {
        if (!Auth::guard($guard)->user()->acessoInscricoes()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect('painel');
            }
        }

        return $next($request);
    }
}
